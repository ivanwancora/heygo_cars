<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	var $data;
	 
	public function __construct()
	{
		parent::__construct();

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		if ($this->ion_auth->logged_in())
		{
			if(!$this->ion_auth->is_admin()){
				$this->data['user'] = $this->ion_auth->user()->row();
			}
										
		}

		// Carga del SEO
		$this->data['title'] = lang('seo_title_default');
		$this->data['description'] = lang('seo_desc_default');
		$this->data['keys'] = lang('seo_keys_default');

		if($this->router->fetch_method() != '' && $this->router->fetch_method() != 'index')
			$seo = $this->metaseomodel->getMetaseo($this->router->fetch_method());
		else
			$seo = $this->metaseomodel->getMetaseo($this->router->fetch_class());
	
		if(isset($seo)){
			$title = 'title_'.$this->lang->lang();
			$description = 'description_'.$this->lang->lang();
			$keys = 'keys_'.$this->lang->lang();

			$this->data['title'] = $seo->$title;
			$this->data['description'] = $seo->$description;
			$this->data['keys'] = $seo->$keys;

		}

		//Carga de las páginas del footer
		$this->data['pages'] = $this->paginasmodel->getPaginas($this->lang->lang());
		$this->data['contact'] = $this->contactmodel->getContact();

		if($this->router->fetch_class() == 'principal' || $this->router->fetch_class() == 'vehiculos' || $this->router->fetch_class() == 'centros' 
		|| $this->router->fetch_class() == 'empresa' || $this->router->fetch_class() == 'mi_cuenta'){
			$make = $model = $modelyear = $color = $kilometers = $price = $offerprice = $noofdoors = $seats = $bodytype = $poweroutput = $horsespower = $geartype = $fueltype = '';
			$amake = $amodel = $amodelyear = $acolor = $akilometers = $aprice = $aofferprice = $anoofdoors = $aseats = $abodytype = $apoweroutput = $ahorsespower = $ageartype = $afueltype = array();

			$this->data['news'] = $this->commonsmodel->getNews();
			$this->data['centros'] = $this->commonsmodel->getCentros($this->lang->lang());
			$this->data['destacados'] = $this->commonsmodel->getDestacados($this->lang->lang());
			$this->data['centros_imagenes'] = $this->commonsmodel->getAll('centros_imagenes');
			$this->data['count_centros'] = $this->commonsmodel->getCentrosImagenes();
			$this->data['marcas'] = $this->commonsmodel->getMarcas($this->lang->lang());
			$this->data['marcas_model'] = $this->commonsmodel->getMarcasModel();
			$this->data['tipo_flotas'] = $this->commonsmodel->getTipoFlotas($this->lang->lang());
			$this->data['provincias'] = $this->commonsmodel->getAll('provincias', 'provincia');
			$this->data['flotas'] = $this->commonsmodel->getAllFlotas('flotas', 'vehicleid', $this->lang->lang());

			if(isset($this->data['flotas'])){
				foreach($this->data['flotas'] as $flota) {
					
					array_push($amake, $flota->make);
					array_push($amodel, $flota->model);
					array_push($amodelyear, $flota->modelyear);
					array_push($acolor, $flota->color);
					array_push($akilometers, $flota->kilometers);
					array_push($apoweroutput, $flota->poweroutput);
					array_push($ahorsespower, $flota->horsespower);
					array_push($aprice, $flota->price);
					array_push($aofferprice, $flota->offerprice);
					array_push($anoofdoors, $flota->noofdoors);
					array_push($aseats, $flota->seats);
					array_push($abodytype, $flota->bodytype);
					array_push($ageartype, $flota->geartype);
					array_push($afueltype, $flota->fueltype);
				}

				//ordenamos + limpiamos arrays de los elementos duplicados
				sort($amake, SORT_STRING);
				sort($amodel, SORT_STRING);
				sort($amodelyear, SORT_STRING);
				sort($acolor, SORT_STRING);
				sort($akilometers, SORT_STRING);
				sort($apoweroutput, SORT_STRING);
				sort($ahorsespower, SORT_STRING);
				sort($aprice, SORT_STRING);
				sort($aofferprice, SORT_STRING);
				sort($anoofdoors, SORT_STRING);
				sort($aseats, SORT_STRING);
				sort($abodytype, SORT_STRING);
				sort($ageartype, SORT_STRING);
				sort($afueltype, SORT_STRING);
				
				$this->data['amake'] = array_unique($amake);
				$this->data['amodel'] = array_unique($amodel);
				$this->data['amodelyear'] = array_unique($amodelyear);
				$this->data['minamodelyear'] = min($this->data['amodelyear']);
				$this->data['maxamodelyear'] = max($this->data['amodelyear']);
				$this->data['acolor'] = array_unique($acolor);
				$this->data['akilometers'] = array_unique($akilometers);
				$this->data['minakilometers'] = min($this->data['akilometers']);
				$this->data['maxakilometers'] = max($this->data['akilometers']);
				$this->data['apoweroutput'] = array_unique($apoweroutput);
				$this->data['minapoweroutput'] = min($this->data['apoweroutput']);
				$this->data['maxapoweroutput'] = max($this->data['apoweroutput']);
				$this->data['aprice'] = array_unique($aprice);
				$this->data['minaprice'] = min($this->data['aprice']);
				$this->data['maxaprice'] = max($this->data['aprice']);
				$this->data['aofferprice'] = array_unique($aofferprice);
				$this->data['minaofferprice'] = min($this->data['aofferprice']);
				$this->data['maxaofferprice'] = max($this->data['aofferprice']);
				$this->data['anoofdoors'] = array_unique($anoofdoors);
				$this->data['minanoofdoors'] = min($this->data['anoofdoors']);
				$this->data['maxanoofdoors'] = max($this->data['anoofdoors']);
				$this->data['ahorsespower'] = array_unique($ahorsespower);
				$this->data['minahorsespower'] = min($this->data['ahorsespower']);
				$this->data['maxahorsespower'] = max($this->data['ahorsespower']);
				$this->data['aseats'] = array_unique($aseats);
				$this->data['minaseats'] = min($this->data['aseats']);
				$this->data['maxaseats'] = max($this->data['aseats']);
				$this->data['abodytype'] = array_unique($abodytype);
				$this->data['ageartype'] = array_unique($ageartype);
				$this->data['afueltype'] = array_unique($afueltype);
			}
		}
			
	}
	
}