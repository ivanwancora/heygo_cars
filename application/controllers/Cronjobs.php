<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjobs extends MY_Controller {
	
	//Controlador Cronjobs
	public function __construct()
	{
		error_reporting (0);
		parent::__construct();
	}
	
	public function generate_url_slug_flotas($url_slug){	  
	    $unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
        
        $url_slug = filter_var($url_slug, FILTER_SANITIZE_STRING);
        $url_slug = strip_tags($url_slug);  
        $url_slug = stripslashes($url_slug);
        $url_slug = strtr($url_slug, $unwanted_array);  
		$url_slug = strtolower(url_title($url_slug));
		
		return $url_slug;
	}
	
	public function index($sin=null)
	{		
		error_reporting (0);
		//$this->load->library('bitly.php');
		$xml_file = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/uploads/xml/StockUgarteKeygo.xml");		
		
		$flotas = new SimpleXMLElement($xml_file);

		$auxdealerid = '';
		$dealerid = 0;
		$x = 1;

		//$flotas_old = $this->commonsmodel->getAllFlotasHistorico('flotas', 'vehicleid', $this->lang->lang());
		$ruta = $_SERVER['DOCUMENT_ROOT'].'/uploads/flotas/';
		$flotas_new = $flotas_images = $array_imatges_server = $array_imatges_a_pujar = array();
		
		foreach($flotas->children() as $fchild){
			foreach($fchild->children() as $fsubc){
				foreach($fsubc->children() as $fsubsubc){
					//$dealerid = $fsubsubc[0]; //extreiem el dealer, per a quan hi hagi més d'un (delegacions)
					if($dealerid==0){
						$auxdealerid = $fsubsubc[0];
						$dealerid = 1;		
					}

					foreach($fsubsubc->children() as $final){
						
						foreach($final->children() as $images){
							$i = 0;
							$aux = '';

							foreach($images->children() as $vimage){
								$ii = "$vimage->imagename";
								$foto = pathinfo($ii, PATHINFO_FILENAME) .'.'. pathinfo($ii, PATHINFO_EXTENSION);
								
								//echo $ii;
								//echo '<br />';
								//echo $ruta.$foto;
								
								//$imagen = file_get_contents($ii);
								
								//works

								if(!is_null($sin)){
									copy($ii, $ruta.$foto);
								}
																
								if($i==0){
									$aux = $aux . $foto;		
								}else{
									$aux = $aux . ','. $foto;		
								}
								$i = $i + 1;
								$images = array(
									'dealerid' => $auxdealerid,
									'vehicleid' => $final->vehicleid,
									'image' => $foto, 
									'lang' => 'es'
								);
								array_push($flotas_images, $images);	
							}
						} 
						
						$url_slug_es = $final->make.'-'.$final->model.'-'.$final->version.'-'.$final->modelyear . '-'.$final->vehicleid;
						
						$url_slug_es = $this->generate_url_slug_flotas($url_slug_es);

						//$result_bitly = $this->bitly->shorten(base_url($this->lang->lang().'/vehiculos/detalle/'.$url_slug_es));
						$result_bitly = base_url($this->lang->lang().'/vehiculos/detalle/'.$url_slug_es);
						if($final->offerprice!=0){
							$finalprice = $final->offerprice;
						}else{
							$finalprice = $final->price;
						}
						$newdata =  array (
							'dealerid' 			=> $auxdealerid,
							'vehicleid' 		=> $final->vehicleid, 
							'tipoficha' 		=> $final->tipoficha,
							'url_slug_es' 		=> $url_slug_es,
							'make' 				=> $final->make,
							'model' 			=> $final->model,
							'version' 			=> $final->version,
							'modelyear' 		=> $final->modelyear,
							'color'				=> $final->color,
							'metallized' 		=> $final->metallized,
							'price' 			=> $final->price,
							'finalprice' 		=> $finalprice,
							'pricedisabled' 	=> $final->pricedisabled,
							'offerprice' 		=> $final->offerprice,
							'financingfee' 		=> $final->financingfee,
							'financingtext' 	=> $final->financingtext,
							'kilometers' 		=> $final->kilometers,
							'bodytype' 			=> $final->bodytype,
							'fueltype' 			=> $final->fueltype,
							'geartype' 			=> $final->geartype,
							'noofdoors' 		=> $final->noofdoors,
							'poweroutput' 		=> $final->poweroutput,
							'horsespower' 		=> $final->horsespower,
							'horseskw' 			=> $final->horseskw,
							'optionserietext' 	=> $final->optionserietext,
							'optiontext' 		=> $final->optiontext,
							'description' 		=> $final->description,
							'warranty' 			=> $final->warranty,
							'officialwarranty' 	=> $final->officialwarranty,
							'regNumber' 		=> $final->regNumber,
							'emailinteresado' 	=> $final->emailinteresado,
							'VIN' 				=> $final->VIN,
							'seats' 			=> $final->seats,
							'cartype'		 	=> $final->cartype,
							'images' 			=> $aux,
							'bitly' 			=> $result_bitly,
							'available' 		=> 1,
							'lang' 				=> 'es'
						);

						//inserim flotas a l'array i imatges a flotas_images
						array_push($flotas_new, $newdata);						
					}
				}
			}
		}

		$resultat = $this->commonsmodel->cronjobs_flotas_auto($flotas_new);
		
		$make_model = $this->commonsmodel->getMarcasModelFlota();
		if(!is_null($make_model)){
			$truncate = $this->commonsmodel->truncate('marcas_model');
			if($truncate==true){
				$res_make_model = $this->commonsmodel->insert_batch('marcas_model',$make_model);
			}			
		}
	}

	public function unlink(){
		error_reporting (0);
		$files = glob('./uploads/flotas/*');
			foreach($files as $file){
			if(is_file($file))
				unlink($file);
			}	
	}

	/*public function prova_truncate(){
		$make_model = $this->commonsmodel->getMarcasModelFlota();
		if(!is_null($make_model)){
			$truncate = $this->commonsmodel->truncate('marcas_model');
			if($truncate==true){
				$res_make_model = $this->commonsmodel->insert_batch('marcas_model',$make_model);
			}			
		}		
	}*/

}
