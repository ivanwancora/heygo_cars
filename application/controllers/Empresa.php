<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa extends MY_Controller {
	
	//Controlador Equipo
	public function __construct()
	{
		parent::__construct();		
		$this->data['seo'] = $this->metaseomodel->getMetaseo();			
	}

	public function index()
	{
		$this->data['body'] = 'empresa';
		$this->data['equipos'] = $this->commonsmodel->getAllEquipo($this->lang->lang());
				
		$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'content/empresa',$this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'footer');
	}
}
