<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticias extends MY_Controller {

	
	//Controlador Blog 
	public function __construct()
	{
		parent::__construct();
			
	}

	public function index($categoria='all')
	{		
		$this->data['body'] = 'noticia-bg';
		if($categoria == 'all') $categoria = null;
		$this->data['selcategory'] = $categoria;
		$this->data['categories'] = $this->newsmodel->getCategories();
		$limit_per_page = 10;
        $page = ($this->uri->segment(4)) ? ($this->uri->segment(4) - 1) : 0;        
        
        $total_records = $this->newsmodel->getTotalNews($categoria);        
        
        if ($total_records > 0)
        {
            // get current page records
            $this->data['resultados'] = $this->newsmodel->getNews(null, 'publication_date', $limit_per_page, $page*$limit_per_page, $categoria);
            if(isset($this->data['resultados'])){
	            foreach ($this->data['resultados'] as $resultado) {
	            	$this->data['categorias'][$resultado->id] = $this->newsmodel->getCategoriesByIDNew($resultado->id);
	            }
            }
            if(!isset($categoria)) $url = base_url($this->lang->lang().'/'.lang('news').'/all');
            else $url = base_url($this->lang->lang().'/'.lang('news').'/'.$categoria);	                 
            $config['base_url'] = $url;
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 4;
             
            // custom paging configuration
            $config['num_links'] = 3;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;
             
            $config['full_tag_open'] = '<div class="pagination">';
            $config['full_tag_close'] = '</div>';
             
            $config['next_link'] = lang("Siguiente");
            $config['next_tag_open'] = '<div class="next">';
            $config['next_tag_close'] = '</div>';            
             
            $this->pagination->initialize($config);
                 
            // build paging links
            $this->data['links'] = $this->pagination->create_links();
        }


		$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'content/noticias', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'footer');
	}

	public function detalle($url_slug=null)
	{	
		if(isset($url_slug)){
			$this->data['body'] = 'noti-single-bg';
			$this->data['new'] = $this->newsmodel->getNewsByUrlSlug($url_slug);	
			//SI L'EQUIP DEL TEMA I EL DEL USUARI CONNECTAT SON DIFERENTS I EL TEMA ES PRIVAT O L'USUARI CONECTAT PERTANY A UN REPTE I EL TEMA PERTANY A UN ALTRE REPTE NO ES DONA ACCÉS
			if(!isset($this->data['new'])){
				$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);				
				$this->load->view($this->config->item('theme_path_frontend'). 'content/error404', $this->data); 
				$this->load->view($this->config->item('theme_path_frontend'). 'footer');
			}
			else{
				$this->data['categories'] = $this->newsmodel->getCategoriesByIDNew($this->data['new'][0]->id);	
				$this->data['nextnew'] = $this->newsmodel->getForwardNew($this->data['new'][0]->id);	
				$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
				$this->load->view($this->config->item('theme_path_frontend'). 'content/noticias_single', $this->data);
				$this->load->view($this->config->item('theme_path_frontend'). 'footer', $this->data);
			}			
			
		}	
		else{
			$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
			$this->load->view($this->config->item('theme_path_frontend'). 'content/error404', $this->data); 
			$this->load->view($this->config->item('theme_path_frontend'). 'footer');
		}
		
	}

}
