<?php
class Contact extends CI_Controller {
		

	public function __construct(){
		parent::__construct();		
		
	}
	
	public function index(){	
		
	}

	public function wsdl(){
		$data = 'p976M8o7'.date('jnY');
		$methodHeygocar = "AES-256-CBC";
		$keyHeygocar = 'keyugarteheygokeyugarteheygougar';
		$iv = '1234567890123456';
		$options =  0;
		$enc = openssl_encrypt($data, $methodHeygocar, $keyHeygocar, $options, $iv);
		return $enc;
	}

	//formulario newsletter
	public function send_newsletter_crm(){
		//Se manda un email de contacto la web
		//Se obtienen los datos del formulario
		$data = $news = array();

		$email = trim($this->input->post("email"));

		$client = new SoapClient("https://www.grupougarte.es/heygows/heygo.asmx?WSDL");
		$passwd_encrypted = $this->wsdl();
		
		$wwpass = $passwd_encrypted;
		$wwemail = $email;
		$wwid = 'newsletter';
		
		$params = array(
			'wwpass'	=> $wwpass,
			'wwemail'	=> $wwemail,
			'wwid'		=> $wwid,
			'wwprecio'	=> 0
		);

		$response = $client->__soapCall("GuardarContacto", array($params));

		function getUserIpAddr(){
			if(!empty($_SERVER['HTTP_CLIENT_IP'])){
				//ip from share internet
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
				//ip pass from proxy
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			}else{
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			return $ip;
		} 

		$ipaddress = getUserIpAddr();
		$navigator = $_SERVER['HTTP_USER_AGENT']; 
		
		$news = array(
			'email' => $email,
			'ipaddress' => $ipaddress,
			'navigator' => $navigator,
			'confirmed' => 'active',
			'status' => 'active'
		);

		$retorn = $this->commonsmodel->insert('newsletter', $news);

		if($retorn){
			$data['result']="true";
			$data['sms']= lang('newsletter_registered');
		}	
		else{
			$data['result']="error";
		}
		
		$this->load->view('json_view', array('data'=>$data)); 
	}

	//formulario bajada de precio
	public function send_bajadaprecio_crm(){
		//Se obtienen los datos del formulario
		$data = array();

		$email = trim($this->input->post("email"));
		$vehicleid = $this->input->post("vehicleid");
		$wwprecio = $this->input->post("precio");

		$client = new SoapClient("https://www.grupougarte.es/heygows/heygo.asmx?WSDL");
		$passwd_encrypted = $this->wsdl();
		
		$wwpass = $passwd_encrypted;
		$wwemail = $email;
		$wwnombre = 'bajadaprecio';
		$wwid = $vehicleid;
		$wwprecio = $wwprecio;
		
		$params = array(
			'wwpass'	=> $wwpass,
			'wwnombre'	=> $wwnombre,
			'wwemail'	=> $wwemail,
			'wwid'		=> $wwid,
			'wwprecio'	=> $wwprecio
		);

		$response = $client->__soapCall("GuardarContacto", array($params));

		function getUserIpAddr(){
			if(!empty($_SERVER['HTTP_CLIENT_IP'])){
				//ip from share internet
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
				//ip pass from proxy
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			}else{
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			return $ip;
		} 

		$ipaddress = getUserIpAddr();
		$navigator = $_SERVER['HTTP_USER_AGENT']; 
		
		$news = array(
			'email' => $email,
			'ipaddress' => $ipaddress,
			'navigator' => $navigator,
			'confirmed' => 'active',
			'status' => 'active'
		);

		$retorn = $this->commonsmodel->insert('bajadaprecio', $news);

		if($retorn){
			$data['result']="true";
			$data['sms']= lang('bajada_precio');
		}	
		else{
			$data['result']="error";
		}
		
		$this->load->view('json_view', array('data'=>$data)); 
	}

	//formulario contacto
	public function send_contacto_crm(){
		//Se manda un email de contacto la web
		//Se obtienen los datos del formulario
		$data = $contacto = array();

		$name = $this->input->post("name");
		$email = $this->input->post("email");
		$phone = $this->input->post("phone");
		$msg = $this->input->post("msg");
		$fnews = $this->input->post("fnews");

		$client = new SoapClient("https://www.grupougarte.es/heygows/heygo.asmx?WSDL");
		$passwd_encrypted = $this->wsdl();

		$wwpass = $passwd_encrypted;
		$wwnombre = $name;
		$wwprovincia = '';
		$wwtelefono = $phone;
		$wwemail = $email;
		$wwcomentario = $msg;
		$wwid = '';
		$wwmarca = '';
		$wwemailinteresado = '';

		$params = array(
			'wwpass'			=> $wwpass,
			'wwnombre'			=> $wwnombre,
			'wwprovincia'		=> $wwprovincia,
			'wwtelefono'		=> $wwtelefono,
			'wwemail'			=> $wwemail,
			'wwcomentario'		=> $wwcomentario,
			'wwid'				=> $wwid,
			'wwprecio'			=> 0, 
			'wwmarca'			=> $wwmarca,
			'wwemailinteresado'	=> $wwemailinteresado
		);

		$response = $client->__soapCall("GuardarContacto", array($params));

		if($fnews==1){
			$wwid = 'newsletter';
			$params2 = array(
				'wwpass'	=> $wwpass,
				'wwemail'	=> $wwemail,
				'wwid'		=> $wwid,
				'wwprecio'	=> 0
			);
			$response2 = $client->__soapCall("GuardarContacto", array($params2));
		}
				
		function getUserIpAddr(){
			if(!empty($_SERVER['HTTP_CLIENT_IP'])){
				//ip from share internet
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
				//ip pass from proxy
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			}else{
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			return $ip;
		} 

		$ipaddress = getUserIpAddr();
		$navigator = $_SERVER['HTTP_USER_AGENT']; 
		
		$contacto = array(
			'name' => $name,
			'email' => $email,
			'phone' => $phone,
			'message' => $msg,
			'tipo_contacto' => 'contacto',
			'lang' => $this->lang->lang(),
			'ip' => $ipaddress,
			'navigator' => $navigator
		);
		$retorn = $this->commonsmodel->insert('fcontacts', $contacto);
		
		if($retorn){
			$data['result']="true";
			$data['sms']= lang('contact_sended');
		}	
		else{
			$data['result']="error";
		}
		
		$this->load->view('json_view', array('data'=>$data)); 
	}

	//formulario keygonline
	public function send_keygonline_crm(){
		//Se manda un email de contacto la web
		//Se obtienen los datos del formulario
		$data = $contacto = array();

		$name = $this->input->post("name");
		$email = $this->input->post("email");
		$phone = $this->input->post("phone");
		$vehiculos = $this->input->post("vehiculos");
		$dia = $this->input->post("dia");
		$hora = $this->input->post("hora");
		$msg = $this->input->post("msg");
		$fnews = $this->input->post("fnews");

		$client = new SoapClient("https://www.grupougarte.es/heygows/heygo.asmx?WSDL");
		$passwd_encrypted = $this->wsdl();

		$wwpass = $passwd_encrypted;
		$wwnombre = $name;
		$wwprovincia = '';
		$wwtelefono = $phone;
		$wwvehiculos = $vehiculos;
		$wwdia = $dia;
		$wwhora = $hora;
		$wwemail = $email;
		$wwcomentario = $msg;
		$wwid = '';
		$wwmarca = '';
		$wwemailinteresado = '';

		$params = array(
			'wwpass'			=> $wwpass,
			'wwnombre'			=> $wwnombre,
			'wwprovincia'		=> $wwprovincia,
			'wwtelefono'		=> $wwtelefono,
			'wwvehiculos'		=> $wwvehiculos,
			'wwdia'				=> $wwdia,
			'wwhora'			=> $wwhora,
			'wwemail'			=> $wwemail,
			'wwcomentario'		=> $wwcomentario,
			'wwid'				=> $wwid,
			'wwprecio'			=> 0, 
			'wwmarca'			=> $wwmarca,
			'wwemailinteresado'	=> $wwemailinteresado
		);

		$response = $client->__soapCall("GuardarKeygonline", array($params));

		if($fnews==1){
			$wwid = 'newsletter';
			$params2 = array(
				'wwpass'	=> $wwpass,
				'wwemail'	=> $wwemail,
				'wwid'		=> $wwid,
				'wwprecio'	=> 0
			);
			$response2 = $client->__soapCall("GuardarContacto", array($params2));
		}
				
		function getUserIpAddr(){
			if(!empty($_SERVER['HTTP_CLIENT_IP'])){
				//ip from share internet
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
				//ip pass from proxy
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			}else{
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			return $ip;
		} 

		$ipaddress = getUserIpAddr();
		$navigator = $_SERVER['HTTP_USER_AGENT']; 
		
		$contacto = array(
			'name' => $name,
			'email' => $email,
			'phone' => $phone,
			'message' => $msg,
			'tipo_contacto' => 'keygonline',
			'lang' => $this->lang->lang(),
			'ip' => $ipaddress,
			'navigator' => $navigator
		);
		$retorn = $this->commonsmodel->insert('fcontacts', $contacto);
		
		if($retorn){
			$data['result']="true";
			$data['sms']= lang('contact_sended');
		}	
		else{
			$data['result']="error";
		}
		
		$this->load->view('json_view', array('data'=>$data)); 
	}
	
	//formulario vehiculo single
	public function send_vehiculo_crm(){
		//Se manda un email de contacto la web
		//Se obtienen los datos del formulario
		$data = $vehiculo = array();

		$name = $this->input->post("name");
		$email = $this->input->post("email");
		$phone = $this->input->post("phone");
		$cp = $this->input->post("cp");
		$msg = $this->input->post("msg");
		$vehicleid = $this->input->post("vehicleid");
		$wwprecio = $this->input->post("precio");
		$make = $this->input->post("make");
		$emailinteresado = $this->input->post("emailinteresado");
		$fnews = $this->input->post("fnews");


		$client = new SoapClient("https://www.grupougarte.es/heygows/heygo.asmx?WSDL");
		$passwd_encrypted = $this->wsdl();
		
		$wwpass = $passwd_encrypted;
		$wwnombre = $name;
		$wwprovincia = $cp;
		$wwtelefono = $phone;
		$wwemail = $email;
		$wwcomentario = $msg;
		$wwid = $vehicleid;
		$wwprecio = $wwprecio;
		$wwmarca = $make;
		$wwemailinteresado = $emailinteresado;

		$params = array(
			'wwpass'			=> $wwpass,
			'wwnombre'			=> $wwnombre,
			'wwprovincia'		=> $wwprovincia,
			'wwtelefono'		=> $wwtelefono,
			'wwemail'			=> $wwemail,
			'wwcomentario'		=> $wwcomentario,
			'wwid'				=> $wwid,
			'wwprecio'			=> $wwprecio,
			'wwmarca'			=> $wwmarca,
			'wwemailinteresado'	=> $wwemailinteresado
		);

		$response = $client->__soapCall("GuardarContacto", array($params));
		
		if($fnews==1){
			$wwid = 'newsletter';
			$params2 = array(
				'wwpass'	=> $wwpass,
				'wwemail'	=> $wwemail,
				'wwid'		=> $wwid,
				'wwprecio'	=> 0
			);
			$response2 = $client->__soapCall("GuardarContacto", array($params2));
		}
	
		function getUserIpAddr(){
			if(!empty($_SERVER['HTTP_CLIENT_IP'])){
				//ip from share internet
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
				//ip pass from proxy
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			}else{
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			return $ip;
		} 

		$ipaddress = getUserIpAddr();
		$navigator = $_SERVER['HTTP_USER_AGENT']; 
		
		$vehiculo = array(
			'name' => $name,
			'email' => $email,
			'phone' => $phone,
			'message' => $msg,
			'codigo_postal' => $cp,
			'tipo_contacto' => 'vehiculo',
			'lang' => $this->lang->lang(),
			'ip' => $ipaddress,
			'navigator' => $navigator
		);
		$retorn = $this->commonsmodel->insert('fcontacts', $vehiculo);
		
		if($retorn){
			$data['result']="true";
			$data['sms']= lang('vehiculo_sended');
		}	
		else{
			$data['result']="error";
		}
		
		$this->load->view('json_view', array('data'=>$data)); 
	}

	//Formulario reserva + TPV
	public function send_reserva_crm(){
		$data = $contacto = array();

		$reserva_id = $this->input->post("reserva_id");
		$reserva_vehicleid = $this->input->post("reserva_vehicleid");
		$reserva_make = $this->input->post("reserva_make");
		$reserva_price = $this->input->post("reserva_price");
		$reserva_emailinteresado = $this->input->post("reserva_emailinteresado");
		$name = $this->input->post("reserva_name");
		$email = $this->input->post("reserva_email");
		$phone = $this->input->post("reserva_phone");
		$msg = $this->input->post("reserva_sms");
		$codigo_postal = $this->input->post("reserva_cp");
		$ipaddress = $this->input->post("ipaddress");
		$navigator = $this->input->post("navigator");
		$fnews = $this->input->post("fnews");
		$reserva_precio = $this->input->post("reserva_precio");

		$client = new SoapClient("https://www.grupougarte.es/heygows/heygo.asmx?WSDL");
		$passwd_encrypted = $this->wsdl();

		$wwpass = $passwd_encrypted;
		$wwreserva_id = $reserva_id;
		$wwreserva_price = $reserva_price;
		$wwnombre = $name;
		$wwprovincia = '';
		$wwtelefono = $phone;
		$wwcp = $codigo_postal;
		$wwemail = $email;
		$wwcomentario = $msg;
		$wwid = $reserva_vehicleid;
		$wwmarca = $reserva_make;
		$wwemailinteresado = $reserva_emailinteresado;

		$params = array(
			'wwpass'			=> $wwpass,
			'wwreservaid'		=> $wwreserva_id,
			'wwreservaprice'	=> $wwreserva_price,
			'wwreservapagado'	=> '0',
			'wwnombre'			=> $wwnombre,
			'wwprovincia'		=> $wwprovincia,
			'wwtelefono'		=> $wwtelefono,
			'wwcp'				=> $wwcp,
			'wwemail'			=> $wwemail,
			'wwcomentario'		=> $wwcomentario,
			'wwid'				=> $wwid,
			'wwprecio'			=> $reserva_precio, 
			'wwmarca'			=> $wwmarca,
			'wwemailinteresado'	=> $wwemailinteresado
		);

		$response = $client->__soapCall("GuardarReserva", array($params));
		
		if($fnews==1){
			$wwidnews = 'newsletter';
			$params2 = array(
				'wwpass'	=> $wwpass,
				'wwemail'	=> $wwemail,
				'wwid'		=> $wwidnews,
				'wwprecio'	=> 0
			);
			$response2 = $client->__soapCall("GuardarContacto", array($params2));
		}
				
		function getUserIpAddr(){
			if(!empty($_SERVER['HTTP_CLIENT_IP'])){
				//ip from share internet
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
				//ip pass from proxy
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			}else{
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			return $ip;
		} 

		$ipaddress = getUserIpAddr();
		$navigator = $_SERVER['HTTP_USER_AGENT']; 
		
		$contacto = array(
			'reserva_id' 		=> $wwreserva_id,
			'reserva_price' 	=> $wwreserva_price,
			'name' 				=> $wwnombre,
			'email' 			=> $email,
			'phone' 			=> $phone,
			'codigo_postal'		=> $wwcp,
			'message' 			=> $msg,
			'wwid' 				=> $wwid,
			'wwemailinteresado' => $wwemailinteresado,
			'wwmarca' 			=> $wwmarca,
			'tipo_contacto' 	=> 'reserva',
			'lang' => $this->lang->lang(),
			'ip' => $ipaddress,
			'navigator' => $navigator
		);
		$retorn = $this->commonsmodel->insert('fcontacts', $contacto);
		
		if($retorn){
			$data['result']="true";
			$data['sms']= lang('contact_sended');
		}	
		else{
			$data['result']="error";
		}
		
		$this->load->view('json_view', array('data'=>$data)); 
	}
	
}
?>