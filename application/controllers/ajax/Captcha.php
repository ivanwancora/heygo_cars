<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Captcha extends CI_Controller
{
    function __construct() {
        parent::__construct();
        // Load the captcha helper
        $this->load->helper('captcha');
    }
    
    public function index(){
        
    }
    
    public function refresh(){
        // Captcha configuration
        $config = array(
            'img_path'      => 'assets/img/captcha_images/',
            'img_url'       => base_url().'assets/img/captcha_images/',
            'img_width'     => '200',
            'img_height'    => 50,
            'word_length'   => 8,
            'font_size'     => 20
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
    }

    public function validate(){
        // If captcha form is submitted        
        $inputCaptcha = $this->input->post('captcha');
        $sessCaptcha = $this->session->userdata('captchaCode');
        if($inputCaptcha === $sessCaptcha){
        	$data['result']="true";			
			$data['detail']=lang('Captcha_ok');	            
        }else{
            $data['result']="error";			
			$data['detail']=lang('Captcha_ko');	         
        }
        $data['csrf_hash'] = $this->security->get_csrf_hash();               
        $this->load->view('json_view',  array('data'=>$data));
    }

    public function validateNewsletter(){
        // If captcha form is submitted        
        $inputCaptcha = $this->input->post('captcha');
        $sessCaptcha = $this->session->userdata('captchaCode');
        if($inputCaptcha === $sessCaptcha){
        	$data['result']="true";			
			$data['detail']=lang('Captcha_ok');	            
        }else{
            $data['result']="error";			
			$data['detail']=lang('Captcha_ko');	         
        }
        $data['csrf_hash'] = $this->security->get_csrf_hash();               
        $this->load->view('json_view',  array('data'=>$data));
    }
    
}