<?php
class Commons extends CI_Controller {
		

	public function __construct(){
		parent::__construct();		
		
	}
	
	public function index(){	
		
	}

	public function check_favoritos(){
		$data = $my_cars = $subcars = array();
		
		$data['my_cars'] = $this->input->post('my_cars');;
		$this->session->set_userdata('my_cars', $data['my_cars']);
		
		$data['result'] = 'true';
		$this->load->view('json_view', array('data'=>$data)); 
	}

	public function get_vehicles(){
		$data = array();
		$where = " where available=1 and lang='".$this->lang->lang()."'";
		$orderby = ''; 
		$dataFilter = '';
		$cercador = '';
		$ss = '';
		$x = 0;
		$y = 0;

		$search = $this->input->post('search');
		$cartype = $this->input->post('cartype');
		$cMarca = $this->input->post('cMarca');
		$cModel = $this->input->post('cModel');
		$cKmFrom = $this->input->post('cKmFrom');
		$cKmTo = $this->input->post('cKmTo');
		$cPriceFrom = $this->input->post('cPriceFrom');
		$cPriceTo = $this->input->post('cPriceTo');
		$cYearFrom = $this->input->post('cYearFrom');
		$cYearTo = $this->input->post('cYearTo');
		$cPowerFrom = $this->input->post('cPowerFrom');
		$cPowerTo = $this->input->post('cPowerTo');
		$cGas = $this->input->post('cGas');
		$cChange = $this->input->post('cChange');
		$cColor = $this->input->post('cColor');
		$cLocation = $this->input->post('cLocation');
		$cSeats = $this->input->post('cSeats');
		$cDoors = $this->input->post('cDoors');
		$orderBy = $this->input->post('orderBy');
		$page = $this->input->post('page');
		$limit = $this->input->post('limit');
		
		$tipus_flotalist = $this->input->post('tipus_flotalist');
		$tipus_flotalisticon = $this->input->post('tipus_flotalisticon');
		$tipus_flotalistmobil = $this->input->post('tipus_flotalistmobil');
		$tipus_flotalistmobilicon = $this->input->post('tipus_flotalistmobilicon');
		$sales = $this->input->post('sales');

		$language = $this->lang->lang();
		if($search!=''){	
			$cercador = explode(" ", $search);
			
			$x = count($cercador);
			
			if($x==1){
				$ss = " and (make LIKE '%$search%' or model LIKE '%$search%'";
			}else{
				foreach ($cercador as $cercador) {
					if($y==0){
						$ss = " and (make like '%$cercador%'";
					}else{
						$ss = $ss ." and model like '%$cercador%'";
					}	
					$y = $y +1;
				}
			}
			
			$ss = $ss . ')';

			$where = $where . $ss;
			$dataFilter = $dataFilter."<span class='m-tag-item' data-input='search'>".$search."<span class='close icon-x'></span></span>";
		}
		if($cartype!='' && isset($cartype) && $cartype!='undefined'){			
			$where = $where . " and cartype LIKE '%$cartype%'";
			//$dataFilter = $dataFilter."<span class='m-tag-item' data-input='cartype'>".$cartype."<span class='close icon-x'></span></span>";
		}
		if($cMarca!=''){			
			$where = $where . " and make LIKE '%$cMarca%'";
			$dataFilter = $dataFilter."<span class='m-tag-item' data-select='cMarca'>".$cMarca."<span class='close icon-x'></span></span>";
		}
		if($cModel!='' && $cModel != "null"){
			$where = $where . " and model LIKE '%$cModel%'";
			$dataFilter = $dataFilter."<span class='m-tag-item' data-select='cModel'>".$cModel."<span class='close icon-x'></span></span>";
		}
		if($cKmFrom!=''){			
			$where = $where . " and kilometers>=".$cKmFrom."";
			$dataFilter = $dataFilter."<span class='m-tag-item' data-select='cKmFrom'>". lang('vehiculos_from') . ' ' . $cKmFrom."<span class='close icon-x'></span></span>";
		}
		if($cKmTo!=''){			
			$where = $where . " and kilometers<=".$cKmTo."";
			$dataFilter = $dataFilter."<span class='m-tag-item' data-select='cKmTo'>".lang('vehiculos_to') . ' ' . $cKmTo."<span class='close icon-x'></span></span>";
		}
		if($cPriceFrom!=''){			
			$where = $where . " and finalprice>=".$cPriceFrom."";
			$dataFilter = $dataFilter."<span class='m-tag-item' data-select='cPriceFrom'>".lang('vehiculos_from') . ' ' . $cPriceFrom."<span class='close icon-x'></span></span>";
		}
		if($cPriceTo!=''){			
			$where = $where . " and finalprice<=".$cPriceTo."";
			$dataFilter = $dataFilter."<span class='m-tag-item' data-select='cPriceTo'>".lang('vehiculos_to') . ' ' . $cPriceTo."<span class='close icon-x'></span></span>";
		}
		if($cYearFrom!=''){			
			$where = $where . " and modelyear>=".$cYearFrom."";
			$dataFilter = $dataFilter."<span class='m-tag-item' data-select='cYearFrom'>".lang('vehiculos_from') . ' ' . $cYearFrom."<span class='close icon-x'></span></span>";
		}
		if($cYearTo!=''){			
			$where = $where . " and modelyear<=".$cYearTo."";
			$dataFilter = $dataFilter."<span class='m-tag-item' data-select='cYearTo'>".lang('vehiculos_to') . ' ' . $cYearTo."<span class='close icon-x'></span></span>";
		}
		if($cGas!=''){			
			$where = $where . " and fueltype LIKE '%$cGas%'";
			$dataFilter = $dataFilter."<span class='m-tag-item' data-select='cGas'>".$cGas."<span class='close icon-x'></span></span>";
		}
		if($cChange!=''){			
			$where = $where . " and geartype LIKE '%$cChange%'";
			$dataFilter = $dataFilter."<span class='m-tag-item' data-select='cChange'>".$cChange."<span class='close icon-x'></span></span>";
		}
		/*if($cPowerFrom!=''){			
			$where = $where . " and poweroutput>=".$cPowerFrom."";
			$dataFilter = $dataFilter."<span class='m-tag-item' data-input='cPowerFrom'>".lang('vehiculos_from') . ' ' . $cPowerFrom."<span class='close icon-x'></span></span>";
		}
		if($cPowerTo!=''){			
			$where = $where . " and poweroutput<=".$cPowerTo."";
			$dataFilter = $dataFilter."<span class='m-tag-item' data-input='cPowerTo'>".lang('vehiculos_to') . ' ' . $cPowerTo."<span class='close icon-x'></span></span>";
		}
		if($cColor!=''){			
			$where = $where . " and color LIKE '%$cColor%'";
			$dataFilter = $dataFilter."<span class='m-tag-item' data-input='cColor'>".$cColor."<span class='close icon-x'></span></span>";
		}
		if($cLocation!=''){		
			//falta sync amb BBDD	
			//$where = $where . " and provincia LIKE '%$cLocation%'";
			//$dataFilter = $dataFilter."<span class='m-tag-item' data-input='cLocation'>".$cLocation."<span class='close icon-x'></span></span>";
		}*/
		if($cSeats!=''){			
			$where = $where . " and seats LIKE '%$cSeats%'";
			$dataFilter = $dataFilter."<span class='m-tag-item' data-multiple='cSeats' data-input='cSeats'>".$cSeats. ' ' . lang('vehiculo_asientos') . "<span class='close icon-x'></span></span>";
		}
		if($cDoors!=''){			
			$where = $where . " and noofdoors LIKE '%$cDoors%'";
			$dataFilter = $dataFilter."<span class='m-tag-item' data-multiple='cDoors' data-input='cDoors'>" .$cDoors. ' ' . lang('vehiculo_puertas') . "<span class='close icon-x'></span></span>";
		}
		if($tipus_flotalist!=''){		
			$porciones = explode(",", $tipus_flotalisticon);
			foreach ($porciones as $values) {
				$dataFilter = $dataFilter."<span class='m-tag-item' data-input='flota' data-flota='".$values."'>".$values."<span class='close icon-x'></span></span>";
			}
			$tipus_flotalist = str_replace(",","','",$tipus_flotalist);
			$where = $where . " and bodytype IN ('$tipus_flotalist')";
		}
		if($tipus_flotalistmobil!=''){			
			$porciones = explode(",", $tipus_flotalistmobilicon);
			foreach ($porciones as $values) {
				$dataFilter = $dataFilter."<span class='m-tag-item' data-input='flota' data-flota='".$values."'>".$values."<span class='close icon-x'></span></span>";
			}
			$tipus_flotalistmobil = str_replace(",","','",$tipus_flotipus_flotalistmobiltalist);
			$where = $where . " and bodytype IN ($tipus_flotalistmobil)";
		}
		if($sales!=''){			
			$where = $where . " and offerprice!=0";
			$dataFilter = $dataFilter."<span class='m-tag-item' data-input='sales' data-sales='sales[]'>Oferta<span class='close icon-x'></span></span>";
		}

		//ordenació
		if(isset($orderBy) and $orderBy!=''){
			$orderBy = ' order by '.$orderBy;
		}
		
		//ordenació
		if(isset($page) and $page!=''){
			$pagelimit = ' limit '.$page .',' . $limit;
		}

		$sqlwithout = $where;
		$sql = $where . $orderBy . $pagelimit;
		
		$data['vehicles'] = $this->commonsmodel->getSearchFlotas($sql); //retorna tots els cotxes
		$data['vehicles_total'] = $this->commonsmodel->getSearchFlotas($sqlwithout, 0); //retorna tots els cotxes
		//$data['vehicles'] = $sql; //veure la sentència sql de retorn. 
		$data['dataFilter'] = $dataFilter;
		

		$data['result'] = true;
		$this->load->view('json_view',  array('data'=>$data));
	}
}

?>