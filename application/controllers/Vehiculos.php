<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehiculos extends MY_Controller {
	
	//Controlador Contact
	public function __construct()
	{
		parent::__construct();		
		$this->data['seo'] = $this->metaseomodel->getMetaseo();	
		$this->load->library('pagination');		
	}

	public function index()
	{
		$this->data['body'] = 'vehiculos';
		$this->data['home'] = $this->input->post('home');
		$this->data['homemarcas'] = $this->input->post('search');
		$this->data['otrovehiculo'] = $this->input->post('otrovehiculo');
		$this->data['vehicles'] = $this->input->post('vehicles');
		$this->data['mobile'] = $this->input->post('mobile');
		$this->data['cartype'] = $this->input->post('cartype');
		$this->data['tots'] = $this->input->post();
		$this->data['header_mobile'] = $this->input->post('header_mobile');

		$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'content/vehiculos_filter',$this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'footer');
	}

	//Esta funcion carga la pagina escogida del footer
	public function detalle($url_slug=null){
		$this->data['body'] = 'vehiculos-single';
		if(!is_null($url_slug)){
			$this->data['flota'] = $this->commonsmodel->getFlotaBySlug($url_slug,'es');
			$this->data['fotos_flota'] = $this->commonsmodel->getAllFotosFlotaByVehicleId($this->data['flota']->vehicleid,'es');

			$this->data['mflota'] = $this->commonsmodel->getFlotaBySlugAndBrand($url_slug,'es');
			$this->data['relatedflota'] = $this->commonsmodel->getFlotaByBodyType($this->data['flota']->bodytype,'es');
			$this->data['dealer'] = $this->commonsmodel->getCentrosDealer($this->data['flota']->dealerid,'es');
			
			if($this->data['flota']->vehicleid!=''){
				$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
				$this->load->view($this->config->item('theme_path_frontend'). 'content/vehiculos_single',$this->data);
				$this->load->view($this->config->item('theme_path_frontend'). 'footer');						
			}else{
				redirect('/'.$this->lang->lang().'/vehiculos');
			}
		}else{
			redirect('/'.$this->lang->lang().'/vehiculos');
		}
	}
}
