<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mi_cuenta extends MY_Controller {
	
	//Controlador Principal 
	public function __construct()
	{
		parent::__construct();
		$this->data['seo'] = $this->metaseomodel->getMetaseo();
	}

	public function index(){
		
	}

	//Esta funcion carga la pagina de mis favoritos
	public function favoritos(){
		
		$vehicles = $this->session->userdata('my_cars'); 
		$this->data['flotas'] = $this->commonsmodel->getAllFlotasByVehicleIds($vehicles, $this->lang->lang());
		
		$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'content/favoritos',$this->data); 
		$this->load->view($this->config->item('theme_path_frontend'). 'footer');			
	}

	//Esta funcion carga la pagina de mis favoritos
	public function check_favoritos(){
		$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'content/check_favoritos',$this->data); 
		$this->load->view($this->config->item('theme_path_frontend'). 'footer');			
	}

}
