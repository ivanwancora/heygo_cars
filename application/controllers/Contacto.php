<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends MY_Controller {
	
	//Controlador Contact
	public function __construct()
	{
		parent::__construct();		
		$this->data['seo'] = $this->metaseomodel->getMetaseo();			
	}
	
	public function wsdl(){
		$data = 'p976M8o7'.date('jnY');
		$methodHeygocar = "AES-256-CBC";
		$keyHeygocar = 'keyugarteheygokeyugarteheygougar';
		$iv = '1234567890123456';
		$options =  0;
		$enc = openssl_encrypt($data, $methodHeygocar, $keyHeygocar, $options, $iv);
		return $enc;
	}

	public function index()
	{		
		$this->data['body'] = 'contact';
		$this->data['contact'] = $this->contactmodel->getContact();
		$this->data['centros'] = $this->commonsmodel->getCentros($this->lang->lang());
		
		$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'content/contacto',$this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'footer',$this->data);
	}

	public function thankyou()
	{		
		$this->data['body'] = 'contact';
		
		$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'content/thank_you_contact',$this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'footer',$this->data);
	}

	public function thankyoukeygonline()
	{		
		$this->data['body'] = 'contact';
		
		$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'content/thank_you_keygonline',$this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'footer',$this->data);
	}

	public function thankyouvsingle()
	{		
		$this->data['body'] = 'contact';
		
		$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'content/thank_you_vsingle',$this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'footer',$this->data);
	}

	public function confirmacion_reserva()
	{		
		$this->data['body'] = 'confirmacio_reserva';
		
		$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'content/reserva_confirmacion_ok',$this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'footer',$this->data);
	}

	//mètodes REDSYS
	public function pagoTPV() {
		$this->load->library('Redsys');
		$redsys = new Redsys;
		$codigo_comercio = '124354481'; //real
		$codigo_comercio = '999008881'; //pruebas

		//Pruebas TPV - OK
		/*Tarjeta aceptada:
		o Numeración: 4548 8120 4940 0004
		o Caducidad: 12/20
		o Código CVV2: 123
		o Para compras seguras, en la que se requiere la autenticación del titular, el código de
		autenticación personal (CIP) es 123456.*/

		//Pruebas TPV - KO
		/*Tarjeta denegada (Código de Respuesta = 190):
		o Numeración: 5576 4400 2278 8500
		o Caducidad: 12/20
		o Código CVV2: 123
		o Para compras seguras, en la que se requiere la autenticación del titular, el código de
		autenticación personal (CIP) es 123456.*/

		$reserva_id = $this->input->post("reserva_id");
		$codigo_order = $reserva_id;
		$reserva_vehicleid = $this->input->post("reserva_vehicleid");
		$reserva_make = $this->input->post("reserva_make");
		$reserva_price = $this->input->post("reserva_price");
		$reserva_emailinteresado = $this->input->post("reserva_emailinteresado");
		$name = $this->input->post("reserva_name");
		$email = $this->input->post("reserva_email");
		$phone = $this->input->post("reserva_phone");
		$msg = $this->input->post("reserva_msg");
		$codigo_postal = $this->input->post("reserva_cp");
		$ipaddress = $this->input->post("ipaddress");
		$navigator = $this->input->post("navigator");
		$fnews = $this->input->post("fnews");
		$total = $this->input->post("reserva_price");

		$this->session->set_userdata('codigo_order', $codigo_order);

		$url_merchant = $this->lang->lang().'/'.lang('slug_contacto').'/redsys_callback';
		$url_redsysok = $this->lang->lang().'/'.lang('slug_contacto').'/redsys_ok';
		$url_redsysko = $this->lang->lang().'/'.lang('slug_contacto').'/redsys_ko';

		$redsys->setParameter("DS_MERCHANT_AMOUNT", $total*100);
		$redsys->setParameter("DS_MERCHANT_ORDER", $codigo_order);
		$redsys->setParameter("DS_MERCHANT_MERCHANTCODE", $codigo_comercio);
		$redsys->setParameter("DS_MERCHANT_CURRENCY", 978);
		$redsys->setParameter("DS_MERCHANT_TRANSACTIONTYPE", 0);
		$redsys->setParameter("DS_MERCHANT_TERMINAL", 001);
		$redsys->setParameter("DS_MERCHANT_MERCHANTURL", base_url($url_merchant));
		$redsys->setParameter("DS_MERCHANT_URLOK", base_url($url_redsysok));
		$redsys->setParameter("DS_MERCHANT_MERCHANTNAME", 'AUTO HUGAR');
		$redsys->setParameter("DS_MERCHANT_URLKO",base_url($url_redsysko));
		

		$version = "HMAC_SHA256_V1";
		$kc = 'BwM4j+9gxf03bj6RM2gTcrU0x7/0QTW8'; //real
		$kc = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7'; //pruebas
		$params = $redsys->createMerchantParameters();
		$signature = $redsys->createMerchantSignature($kc);
		$this->load->view($this->config->item('theme_path_frontend'). '/redsys/tpv_form_view', compact('params' , 'signature' , 'version'));
	}
	
	public function redsys_callback() {  
		$this->load->library('Redsys');
		$redsys           = new Redsys;
		$version          = $_POST['Ds_SignatureVersion'];
		$parameters       = $_POST['Ds_MerchantParameters'];
		$signatureReceive = $_POST['Ds_Signature'];

		$decodec    = $redsys->decodeMerchantParameters($parameters);
		$decodec    = json_decode($decodec , true);
		$codigo_comercio   = $decodec['Ds_Order'];
		$kc         = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7';
		$signature  = $redsys->createMerchantSignatureNotif($kc,$parameters); 
		
		if($signature === $signatureReceive){         
			if($decodec['Ds_Response'] < 100 || $decodec['Ds_Response']== '0000'){        
				
				$Ds_Order = $decodec['Ds_Order'];
				
				$array_data = array(
					'confirmed'	 => 1,
				);

				$array_data_flotas = array(
					'reserved'	 => 1,
				);

				$ret = $this->commonsmodel->updateComandes($Ds_Order, $array_data);

				$ret_comandes = $this->commonsmodel->getReservaByID($Ds_Order);
				if(!is_null($ret_comandes)){
					$ret = $this->commonsmodel->updateFlotas($ret_comandes->wwid, $array_data_flotas);
				}				

				/* Actualitzem webservice flota */

				$client = new SoapClient("https://www.grupougarte.es/heygows/heygo.asmx?WSDL");
				$passwd_encrypted = $this->wsdl();

				$wwpass = $passwd_encrypted;
				$wwreserva_id = $ret_comandes->reserva_id;
				$wwreserva_price = $ret_comandes->reserva_price;
				$wwnombre = $ret_comandes->name;
				$wwprovincia = '';
				$wwtelefono = $ret_comandes->phone;
				$wwcp = $ret_comandes->codigo_postal;
				$wwemail = $ret_comandes->email;
				$wwcomentario = $ret_comandes->message;
				$wwid = $ret_comandes->vehicleid;
				$wwmarca = $ret_comandes->make;
				$wwprecio = $ret_comandes->preciocoche;
				$wwemailinteresado = $ret_comandes->wwemailinteresado;

				$params = array(
					'wwpass'			=> $wwpass,
					'wwreservaid'		=> $wwreserva_id,
					'wwreservaprice'	=> $wwreserva_price,
					'wwreservapagado'	=> '1',
					'wwnombre'			=> $wwnombre,
					'wwprovincia'		=> $wwprovincia,
					'wwtelefono'		=> $wwtelefono,
					'wwcp'				=> $wwcp,
					'wwemail'			=> $wwemail,
					'wwcomentario'		=> $wwcomentario,
					'wwid'				=> $wwid,
					'wwprecio'			=> $wwprecio, 
					'wwmarca'			=> $wwmarca,
					'wwemailinteresado'	=> $wwemailinteresado
				);

				$response = $client->__soapCall("GuardarReserva", array($params));

				if($fnews==1){
					$wwidnews = 'newsletter';
					$params2 = array(
						'wwpass'	=> $wwpass,
						'wwemail'	=> $wwemail,
						'wwid'		=> $wwidnews,
						'wwprecio'	=> 0
					);
					$response2 = $client->__soapCall("GuardarContacto", array($params2));
				}
						
				function getUserIpAddr(){
					if(!empty($_SERVER['HTTP_CLIENT_IP'])){
						//ip from share internet
						$ip = $_SERVER['HTTP_CLIENT_IP'];
					}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
						//ip pass from proxy
						$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
					}else{
						$ip = $_SERVER['REMOTE_ADDR'];
					}
					return $ip;
				} 

				$ipaddress = getUserIpAddr();
				$navigator = $_SERVER['HTTP_USER_AGENT']; 
				
				$contacto = array(
					'reserva_id' 		=> $wwreserva_id,
					'reserva_price' 	=> $wwreserva_price,
					'name' 				=> $wwnombre,
					'email' 			=> $email,
					'phone' 			=> $phone,
					'codigo_postal'		=> $wwcp,
					'message' 			=> $msg,
					'wwid' 				=> $wwid,
					'wwemailinteresado' => $wwemailinteresado,
					'wwmarca' 			=> $wwmarca,
					'tipo_contacto' 	=> 'reserva',
					'lang' => $this->lang->lang(),
					'ip' => $ipaddress,
					'navigator' => $navigator
				);
				$retorn = $this->commonsmodel->insert('fcontacts', $contacto);

				/* fi actualitzar flota */
				
				$this->data['dades'] = $dades; 
				$this->data['result'] = true;
			}else{ //si hi ha qualsevol error, el captem
				$Ds_Order = $decodec['Ds_Order'];
				
				$array_data = array(
					'confirmed'	 => -1,
				);

				$ret = $this->commonsmodel->updateComandes($Ds_Order, $array_data);

				$ret_comandes = $this->commonsmodel->getReservaByID($Ds_Order);
							
				/* Actualitzem webservice flota */

				$client = new SoapClient("https://www.grupougarte.es/heygows/heygo.asmx?WSDL");
				$passwd_encrypted = $this->wsdl();

				$wwpass = $passwd_encrypted;
				$wwreserva_id = $ret_comandes->reserva_id;
				$wwreserva_price = $ret_comandes->reserva_price;
				$wwnombre = $ret_comandes->name;
				$wwprovincia = '';
				$wwtelefono = $ret_comandes->phone;
				$wwcp = $ret_comandes->codigo_postal;
				$wwemail = $ret_comandes->email;
				$wwcomentario = $ret_comandes->message;
				$wwid = $ret_comandes->vehicleid;
				$wwmarca = $ret_comandes->make;
				$wwprecio = $ret_comandes->preciocoche;
				$wwemailinteresado = $ret_comandes->wwemailinteresado;

				$params = array(
					'wwpass'			=> $wwpass,
					'wwreservaid'		=> $wwreserva_id,
					'wwreservaprice'	=> $wwreserva_price,
					'wwreservapagado'	=> '0',
					'wwnombre'			=> $wwnombre,
					'wwprovincia'		=> $wwprovincia,
					'wwtelefono'		=> $wwtelefono,
					'wwcp'				=> $wwcp,
					'wwemail'			=> $wwemail,
					'wwcomentario'		=> $wwcomentario,
					'wwid'				=> $wwid,
					'wwprecio'			=> $wwprecio, 
					'wwmarca'			=> $wwmarca,
					'wwemailinteresado'	=> $wwemailinteresado
				);

				$response = $client->__soapCall("GuardarReserva", array($params));

				if($fnews==1){
					$wwidnews = 'newsletter';
					$params2 = array(
						'wwpass'	=> $wwpass,
						'wwemail'	=> $wwemail,
						'wwid'		=> $wwidnews,
						'wwprecio'	=> 0
					);
					$response2 = $client->__soapCall("GuardarContacto", array($params2));
				}
						
				function getUserIpAddr(){
					if(!empty($_SERVER['HTTP_CLIENT_IP'])){
						//ip from share internet
						$ip = $_SERVER['HTTP_CLIENT_IP'];
					}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
						//ip pass from proxy
						$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
					}else{
						$ip = $_SERVER['REMOTE_ADDR'];
					}
					return $ip;
				} 

				$ipaddress = getUserIpAddr();
				$navigator = $_SERVER['HTTP_USER_AGENT']; 
				
				$contacto = array(
					'reserva_id' 		=> $wwreserva_id,
					'reserva_price' 	=> $wwreserva_price,
					'name' 				=> $wwnombre,
					'email' 			=> $email,
					'phone' 			=> $phone,
					'codigo_postal'		=> $wwcp,
					'message' 			=> $msg,
					'wwid' 				=> $wwid,
					'wwemailinteresado' => $wwemailinteresado,
					'wwmarca' 			=> $wwmarca,
					'tipo_contacto' 	=> 'reserva',
					'lang' => $this->lang->lang(),
					'ip' => $ipaddress,
					'navigator' => $navigator
				);
				$retorn = $this->commonsmodel->insert('fcontacts', $contacto);

				/* fi actualitzar flota */
				
				$this->data['dades'] = $dades; 
				$this->data['result'] = true;		
			}
		}
	}
	
	public function redsys_ok(){
		$this->data['body'] = 'shadowbg';
		$this->data['metode'] = 'confirmacion_ok';
		$this->data['step'] = 0;

		$reserva_id = $this->session->userdata('codigo_order');
						
		$array_data = array(
			'confirmed'	 => 1,
		);

		$ret = $this->commonsmodel->updateComandes($reserva_id, $array_data);
		if(isset($reserva_id)){
			$this->data['reserva'] = $this->commonsmodel->getReservaByID($reserva_id);
			$ret_reserva = $this->data['reserva'];
			if(!is_null($ret_reserva)){
				$array_data_flotas = array(
					'reserved'	 => 1,
				);
				$ret = $this->commonsmodel->updateFlotas($ret_reserva->vehicleid, $array_data_flotas);
			}	
			$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
			$this->load->view($this->config->item('theme_path_frontend'). 'content/reserva_confirmacion_ok',$this->data);
			$this->load->view($this->config->item('theme_path_frontend'). 'footer');
		}else{
			redirect('/', 'refresh');
		}	
	}
	
	public function redsys_ko(){
		$this->data['body'] = 'shadowbg';
		$this->data['metode'] = 'confirmacion_ko';
		$this->data['step'] = 0;

		$reserva_id = $this->session->userdata('codigo_order');
						
		$array_data = array(
			'confirmed'	 => -1,
		);

		$ret = $this->commonsmodel->updateComandes($reserva_id, $array_data);
		if(isset($reserva_id)){
			$this->data['reserva'] = $this->commonsmodel->getReservaByID($reserva_id);
			$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
			$this->load->view($this->config->item('theme_path_frontend'). 'content/reserva_confirmacion_ko',$this->data);
			$this->load->view($this->config->item('theme_path_frontend'). 'footer');
		}else{
			redirect('/', 'refresh');
		}			
	}
}
