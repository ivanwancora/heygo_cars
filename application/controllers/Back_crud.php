<?php
ini_set('display_errors', 0);
error_reporting(0);

defined('BASEPATH') OR exit('No direct script access allowed');

class Back_crud extends CI_Controller {

	var $data;
	var $password;

	public function __construct(){
		parent::__construct();		
        $this->load->library('grocery_CRUD');
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group('admin'))
		{
			redirect(base_url('principal/'), 'refresh');
		}
    }

	public function index(){
		if ($this->ion_auth->logged_in()){
			redirect(base_url('Back_dashboard/'), 'refresh');
		}else{
			redirect(base_url('Back_dashboard/'), 'refresh');
		}
	}

	//BACKEND GESTOR PARA HEYGO_CARS

	public function encrypt_password($post_array, $primary_key = null){	  
	    $this->load->helper('security');
	    $this->password = $post_array['password'];	    
	    $post_array['password'] = $this->ion_auth->hash_password($post_array['password']);	    
	    return $post_array;
    }

    public function set_created_at($post_array, $primary_key = null){ 
	    $post_array['created_at'] = date('Y-m-d H:i:s');	    
	    return $post_array;
    }	

    public function generate_url_slug($post_array, $primary_key = null){	  
	    $unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
            
        $url_slug_es = filter_var($post_array['title_es'], FILTER_SANITIZE_STRING);        
        $url_slug_es = strip_tags($url_slug_es);    
        $url_slug_es = stripslashes($url_slug_es);        
        $url_slug_es = strtr($url_slug_es, $unwanted_array);   
		$titleURL_es = strtolower(url_title($url_slug_es));
		
		$url_slug_en = filter_var($post_array['title_en'], FILTER_SANITIZE_STRING);        
        $url_slug_en = strip_tags($url_slug_en);    
        $url_slug_en = stripslashes($url_slug_en);        
        $url_slug_en = strtr($url_slug_en, $unwanted_array);   
		$titleURL_en = strtolower(url_title($url_slug_en));
		
        /*if(isUrlExists('news',$titleURL_es)){
            $titleURL_es = $titleURL_es.'-'.time(); 
		} 
		if(isUrlExists('news',$url_slug_en)){
            $titleURL_en = $titleURL_en.'-'.time(); 
		}*/
		 
		$array_data = array();        
	    $array_data['url_slug_es'] = $titleURL_es;	     
	    $array_data['url_slug_en'] = $titleURL_en;	     
	    $this->commonsmodel->update($primary_key, 'news', $array_data);
	    return $post_array;
	}
	
    public function generate_url_slug_paginas($post_array, $primary_key = null){	  
	    $unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
            
		$array_data = array();        
		
		$url_slug_es = filter_var($post_array['nombre_es'], FILTER_SANITIZE_STRING);        
        $url_slug_es = strip_tags($url_slug_es);    
        $url_slug_es = stripslashes($url_slug_es);        
        $url_slug_es = strtr($url_slug_es, $unwanted_array);   
        $titleURL_es = strtolower(url_title($url_slug_es));            
				
		$url_slug_en = filter_var($post_array['nombre_en'], FILTER_SANITIZE_STRING);        
        $url_slug_en = strip_tags($url_slug_en);    
        $url_slug_en = stripslashes($url_slug_en);        
        $url_slug_en = strtr($url_slug_en, $unwanted_array);   
		$titleURL_en = strtolower(url_title($url_slug_en));            

		$array_data['url_slug_es'] = $titleURL_es;	     
		$array_data['url_slug_en'] = $titleURL_en;	     
		
		$this->commonsmodel->update($primary_key, 'paginas', $array_data);  
	    return $post_array;
    }

    public function usuarios(){
		$crud = new grocery_CRUD();
		$crud->set_table('users');
		$crud->set_subject('Usuarios');
		$crud->columns('email', 'first_name', 'last_name', 'groups', 'active');				
		$crud->unset_fields(array('salt', 'ip_address', 'activation_code', 'forgotten_password_code', 'forgotten_password_time', 'remember_code', 'created_on', 'last_login', 'company'));
		$crud->unset_edit_fields('salt', 'ip_address', 'activation_code','password', 'forgotten_password_code', 'forgotten_password_time', 'remember_code', 'created_on', 'last_login', 'company');
		$crud->change_field_type('password','password');
		$crud->required_fields('email','username','first_name','last_name','active','password');
		
		$crud->unique_fields(array('email'));		
		$crud->set_relation_n_n('groups', 'users_groups', 'groups', 'user_id','group_id', 'name');			
				
		$state = $crud->getState();

		if($state == 'list' || $state == 'edit' || $state == 'add')
		{		    
		    $crud->display_as('groups','Tipo de rol');
		}
		$crud->display_as('username','Usuario');
		$crud->display_as('first_name','Nombre');		 
		$crud->display_as('last_name','Apellidos');		
		$crud->display_as('phone','Telefono');
		$crud->display_as('active','Estado');
		 
		$crud->set_lang_string('delete_error_message', 'No puedes eliminarte a ti mismo como usuario conectado.');		
		$crud->callback_before_insert(array($this,'encrypt_password'));
		$crud->callback_before_update(array($this,'encrypt_password'));
		
		$crud->unset_export();
		$crud->unset_print();
		$crud->unset_delete();
		$crud->order_by('id','desc');	
		
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function clear_all_user_dependencies($primary_key = null){	  	    
	    $user = $this->ion_auth->user()->row();
	    if($primary_key == $user->id){	  	
	    	return FALSE;
	    } 	    
	    return TRUE;
    }

	public function export_to_xls($state_info = null)
	{
		$data = $this->get_common_data();
		 
		$data->order_by  = $this->order_by;
		$data->types  = $this->get_field_types();
		 
		$data->list = $this->get_list();
		$data->list = $this->change_list($data->list , $data->types);
		$data->list = $this->change_list_add_actions($data->list);
		 
		$data->total_results = $this->get_total_results();
		 
		$data->columns  = $this->get_columns();
		$data->primary_key  = $this->get_primary_key();
		 
		@ob_end_clean();
		$this->_export_to_xls($data);
	}

	protected function _export_to_xls($data)
	{
		$string_to_export = "";
		foreach($data->columns as $column){
			$string_to_export .= $column->display_as."\t";
		}
		$string_to_export .= "\n";
	 
		foreach($data->list as $num_row => $row){
			foreach($data->columns as $column){
				$string_to_export .= $this->_trim_export_string($row->{$column->field_name})."\t";
			}
			$string_to_export .= "\n";
		}
	 
		// Convert to UTF-16LE and Prepend BOM
		$string_to_export = "\xFF\xFE" .mb_convert_encoding($string_to_export, 'UTF-16LE', 'UTF-8');
	 
		$filename = "export-".date("Y-m-d_H:i:s").".xls";
	 
		header('Content-type: application/vnd.ms-excel;charset=UTF-16LE');
		header('Content-Disposition: attachment; filename='.$filename);
		header("Cache-Control: no-cache");
		echo $string_to_export;
		die();
	}

	function _date($value, $row)
	{
		return "<span style='visibility:hidden;display:none;'>".date('Y-m-d H:i:s', strtotime($value))."</span>".date('d/m/Y H:i:s', strtotime($value));
	}	

	public function contact(){
		$crud = new grocery_CRUD();	
			
		$crud->set_table('contact');
		$crud->set_subject('Contacto');
		$crud->unset_add();
		$crud->unset_delete();
		$crud->columns('comercial_name', 'cif', 'address', 'location', 'city', 'email');		
		$crud->unique_fields(array('email'));		
		$crud->required_fields(array('commercial_name', 'address', 'postal_code', 'location', 'city', 'email', 'telephone1', 'lat', 'long'));
				
		$crud->display_as('comercial_name','Nombre Comercial');
		$crud->display_as('contenido_es','Contenido Español');
		$crud->display_as('contenido_en','Contenido Inglés');
		$crud->display_as('cif','CIF');
		$crud->display_as('address','Dirección');
		$crud->display_as('postal_code','Código Postal');		
		$crud->display_as('location','Población');
		$crud->display_as('city','Ciudad');		
		$crud->display_as('email','Email');
		$crud->display_as('cif','CIF/NIF');
		$crud->display_as('telephone1','Teléfono Href');
		$crud->display_as('telephone2','Teléfono');			
		$crud->display_as('lat','Latitud');
		$crud->display_as('lon','Longitud');
		
		$crud->unset_export();
		$crud->unset_print();
		$crud->order_by('id','desc');
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function paginas(){
		$crud = new grocery_CRUD();	
			
		$crud->set_table('paginas');
		$crud->set_subject('Paginas');		
		
		$crud->columns('nombre_es', 'nombre_en', 'status');	
		$crud->unset_fields(array('url_slug_es','url_slug_en','url_slug_ca', 'created_at', 'updated_at'));		
		$crud->required_fields(array('nombre_es', 'status', 'contenido_es'));

		$crud->display_as('nombre_es','Nombre Página Español');			
		$crud->display_as('nombre_en','Nombre Página Inglés');			
		$crud->display_as('contenido_es','Contenido Español');			
		$crud->display_as('contenido_en','Contenido Inglés');			
		$crud->display_as('status','Estado');		

		$crud->callback_after_insert(array($this,'generate_url_slug_paginas'));
		$crud->callback_after_update(array($this,'generate_url_slug_paginas'));
		
		$crud->unset_export();
		$crud->unset_print();		
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function metaseo(){
		$crud = new grocery_CRUD();	
			
		$crud->set_table('metaseo');
		$crud->set_subject('SEO');
		$crud->unset_add();
		$crud->unset_delete();
		$crud->columns('sitename', 'title_es', 'title_en', 'title_ca');		
		$crud->unique_fields(array('sitename'));		
		$crud->required_fields(array('sitename','title_es','description_es','keys_es'));

		$crud->set_lang_string('update_error', 'El nombre de la página no puede estar vacío y debe ser único');
		$crud->display_as('sitename','Nombre WebSite');
		$crud->display_as('title_es','Título Español');		
		$crud->display_as('title_en','Título Inglés');
		$crud->display_as('description_es','Descripcion Español');		
		$crud->display_as('description_en','Descripcion Inglés');
		$crud->display_as('keys_es','Keys Español');		
		$crud->display_as('keys_en','Keys Inglés');

		$crud->unset_export();
		$crud->unset_print();
		$crud->order_by('id','desc');
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function settings(){
		$crud = new grocery_CRUD();	
			
		$crud->set_table('settings');
		$crud->set_subject('Herramientas');

		$state = $crud->getState();
		if($state == "edit")
		{
			$crud->field_type('nombre','readonly');
		}

		$crud->unset_delete();
		$crud->columns('nombre', 'descripcion', 'valor');				
		$crud->required_fields(array('nombre','descripcion','valor'));
		$crud->change_field_type('valor','integer');
		$crud->unset_texteditor('nombre','varchar');
		$crud->unset_texteditor('valor','text');
		$crud->unset_export();
		$crud->unset_print();
		$crud->order_by('nombre','desc');
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function media(){
		$crud = new grocery_CRUD();
		$crud->set_table('media');
		$crud->set_subject('Media');
		$crud->field_type('url','readonly');
		$crud->columns('img','url', 'fecha');
		$crud->required_fields('img');
		$crud->unset_fields('fecha');	
		$crud->unset_export();
		$crud->unset_print();
		$ruta = 'uploads/media/';
		if(!file_exists($ruta)){
			mkdir($ruta, 0777, true);
		}
		$crud->set_field_upload('img', $ruta);
		$crud->order_by('id','desc');
		
		$crud->callback_after_insert(array($this, 'generar_urls_media'));
		$crud->callback_after_update(array($this, 'generar_urls_media'));

		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
	}

	public function generar_urls_media($post_array,$primary_key){ 
		$img = filter_var($post_array['img'], FILTER_SANITIZE_STRING); 
		$url = base_url('uploads/media/' . $img);
		
		$dades = array(
			"id" => $primary_key,
			"url" => $url
		);
		$this->db->update('media',$dades,array('id' => $primary_key));
		return true;
	}

	public function slider(){
		$crud = new grocery_CRUD();	
			
		$crud->set_table('slider');
		$crud->set_subject('Slider');		
		
		$crud->columns('content_es', 'content_en', 'status');	
		$crud->unset_fields(array('created_at', 'updated_at'));	
		$crud->required_fields(array('image','image_tablet','image_mobile','status'));	

		$crud->display_as('content_es','Descripción Español');			
		$crud->display_as('content_en','Descripción Inglés');			
		$crud->display_as('image','Imagen Desktop');		
		$crud->display_as('image_tablet','Imagen Tablet');	
		$crud->display_as('image_mobile','Imagen Mobile');		
		$crud->display_as('button','Insertar botón');		
		$crud->display_as('text_button_es','Texto botón Español');		
		$crud->display_as('link_button_es','Enlace botón Español');	
		$crud->display_as('text_button_en','Texto botón Inglés');		
		$crud->display_as('link_button_en','Enlace botón Inglés');			
		$crud->display_as('status','Estado');		
		
		$ruta = 'uploads/sliders/';
		
		if(!file_exists($ruta)){
			mkdir($ruta, 0777, true);
		}		
		
		$crud->set_field_upload('image', $ruta);
		$crud->set_field_upload('image_tablet', $ruta);
		$crud->set_field_upload('image_mobile', $ruta);

		$crud->unset_export();
		$crud->unset_print();		
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function flotas_tipo(){
		$crud = new grocery_CRUD();	
			
		$crud->set_table('flotas_tipo');
		$crud->set_subject('Tipos de flota');		
		
		$crud->columns('title_es', 'title_en', 'orden', 'status');	
		$crud->unset_fields(array('created_at', 'updated_at'));	
		$crud->required_fields('title_es','title_en','icono','status');	

		$crud->display_as('title_es','Tipo de flota Español');			
		$crud->display_as('title_en','Tipo de flota Inglés');			
		$crud->display_as('icono','Clase para el icono');		
		$crud->display_as('status','Estado');		
		
		$crud->unset_export();
		$crud->unset_print();		
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function destacados(){
		$crud = new grocery_CRUD();	
			
		$crud->set_table('featureds');
		$crud->set_subject('Destacados');		
		
		$crud->columns('title_es','title_en','content_es', 'content_en', 'status');	
		$crud->unset_fields(array('created_at', 'updated_at'));		
		$crud->required_fields(array('title_es','content_es','image','status'));	

		$crud->display_as('title_es','Título Español');			
		$crud->display_as('content_es','Contenido Español');
		$crud->display_as('title_en','Título Inglés');			
		$crud->display_as('content_en','Contenido Inglés');		
		$crud->display_as('image','Imagen');		
		$crud->display_as('status','Estado');		
		
		$ruta = 'uploads/destacados/';
		if(!file_exists($ruta)){
			mkdir($ruta, 0777, true);
		}		
		
		$crud->set_field_upload('image', $ruta);

		$crud->unset_export();
		$crud->unset_print();		
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function banners(){
		$crud = new grocery_CRUD();	
			
		$crud->set_table('banners');
		$crud->set_subject('Banners');		
		
		$crud->columns('content_es', 'content_en', 'status');	
		$crud->unset_fields(array('created_at', 'updated_at'));	
		$crud->required_fields(array('content_es','image','type','status'));		

		$crud->display_as('content_es','Banner Español');			
		$crud->display_as('content_en','Banner Inglés');			
		$crud->display_as('image','Imagen');		
		$crud->display_as('type','Tipo Banner');		
		$crud->display_as('status','Estado');		

		$ruta = 'uploads/banners/';
		if(!file_exists($ruta)){
			mkdir($ruta, 0777, true);
		}		

		$crud->set_field_upload('image', $ruta);

		$crud->unset_export();
		$crud->unset_print();		
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function centros(){
		$crud = new grocery_CRUD();	
			
		$crud->set_table('centros');
		$crud->set_subject('Centros');		
		
		$crud->columns('title_es', 'place_es', 'address_es', 'schedule_es', 'phone', 'email', 'status');
		$crud->unset_fields(array('created_at', 'updated_at'));	
		$crud->required_fields(array('title_es', 'place_es', 'address_es', 'schedule_es', 'phone', 'email','latitude','longitude','schedule_es','image','status'));		

		$crud->display_as('title_es','Nombre Español');			
		$crud->display_as('title_en','Nombre Inglés');			
		$crud->display_as('place_es','Lugar Español');			
		$crud->display_as('place_en','Lugar Inglés');			
		$crud->display_as('address_es','Dirección Español');			
		$crud->display_as('address_en','Dirección Inglés');			
		$crud->display_as('latitude','Latitude (google)');			
		$crud->display_as('longitude','Longitude (google)');			
		$crud->display_as('schedule_es','Horario Español');			
		$crud->display_as('schedule_en','Horario Inglés');			
		$crud->display_as('phone','Teléfono');			
		$crud->display_as('email','Email');			
		$crud->display_as('image','Imagen destacada');		
		$crud->display_as('status','Estado');		

		$crud->add_action('Añadir Imágenes', '', 'Back_crud/anadir_imagenes','fa fa-building');

		$ruta = 'uploads/centros/';
		if(!file_exists($ruta)){
			mkdir($ruta, 0777, true);
		}		

		$crud->set_field_upload('image', $ruta);

		$crud->unset_export();
		$crud->unset_print();		
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function anadir_imagenes($idcentro){
		
		$crud = new grocery_CRUD();	

		$crud->set_table('centros_imagenes');
		$crud->set_subject('Centros Imágenes');		
		
		$crud->set_relation('idCentro','centros','title_es');

		$crud->columns('idCentro', 'image', 'status');
		$crud->required_fields(array('idCentro', 'image', 'status'));		
		
		$crud->display_as('idCentro','Nombre Centro');			
		$crud->display_as('image','Imagen');		
		$crud->display_as('status','Estado');		

		$ruta = 'uploads/centros/';
		if(!file_exists($ruta)){
			mkdir($ruta, 0777, true);
		}		

		$crud->set_field_upload('image', $ruta);

		$crud->where('idCentro', $idcentro); //extraer by centro

		$crud->unset_export();
		$crud->unset_print();		
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function equipo(){
		$crud = new grocery_CRUD();	
			
		$crud->set_table('equipo');
		$crud->set_subject('Equipo');		
		$crud->columns('name_es', 'name_en', 'job_es', 'job_en', 'orden','status');
		$crud->unset_fields(array('created_at', 'updated_at'));		
		$crud->required_fields(array('name_es', 'job_es', 'image', 'status'));	

		$crud->display_as('name_es','Nombre Español');			
		$crud->display_as('name_en','Nombre Inglés');			
		$crud->display_as('job_es', 'Posición trabajo Español');			
		$crud->display_as('job_en', 'Posición trabajo Inglés');			
		$crud->display_as('image','Imagen destacada');		
		$crud->display_as('status','Estado');		

		$ruta = 'uploads/equipo/';
		if(!file_exists($ruta)){
			mkdir($ruta, 0777, true);
		}		

		$crud->set_field_upload('image', $ruta);

		$crud->unset_export();
		$crud->unset_print();		
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function fcontacts(){
		$crud = new grocery_CRUD();	
			
		$crud->set_table('fcontacts');
		$crud->set_subject('Formulario Contacto');		
		$crud->columns('name', 'email', 'phone', 'message', 'lang');
		$crud->unset_fields(array('wwid','wwemailinteresado','wwmarca','created_at', 'updated_at'));		

		$crud->display_as('name','Nombre');			
		$crud->display_as('email','Email');			
		$crud->display_as('phone', 'Teléfono');			
		$crud->display_as('message', 'Mensaje');			
		$crud->display_as('lang','Idioma');		

		$crud->unset_add();		
		$crud->unset_edit();		
		$crud->unset_print();		
		$crud->unset_delete();		
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function marcas(){
		$crud = new grocery_CRUD();	
			
		$crud->set_table('marcas');
		$crud->set_subject('Marcas');		
		$crud->columns('title_es', 'title_en', 'image', 'status');
		$crud->unset_fields(array('created_at', 'updated_at'));		
		$crud->required_fields('title_es','image','status');

		$ruta = 'uploads/marcas/';
		if(!file_exists($ruta)){
			mkdir($ruta, 0777, true);
		}

		$crud->set_field_upload('image', $ruta);
		
		$crud->display_as('title_es','Nombre Español');			
		$crud->display_as('title_en','Nombre Inglés');			
		$crud->display_as('image', 'Imagen');			
		$crud->display_as('status','Estado');		

		$crud->unset_export();		
		$crud->unset_print();		
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function noticias(){
		$crud = new grocery_CRUD();		
		$crud->set_table('news');
		$crud->set_subject('Noticias');		
		$crud->columns('title_es','excerpt_es', 'image_featured', 'publication_date', 'status', 'created_at', 'updated_at');
		$crud->required_fields('title_es', 'excerpt_es', 'content_es', 'image_featured', 'title_seo_es', 'description_seo_es', 'keywords_seo_es', 'publication_date','status','categories');
		$crud->unset_fields(array('url_slug_es', 'url_slug_en', 'created_at', 'updated_at'));
		$ruta_imagen = 'uploads/news/images/';		
		if(!file_exists($ruta_imagen)){
			mkdir($ruta_imagen, 0777, true);
		}
		
		$crud->set_field_upload('image_featured', $ruta_imagen);
		$crud->set_field_upload('image_secondary', $ruta_imagen);			

		$crud->display_as('title_es','Título Español')->display_as('excerpt_es','Resumen Español');
		$crud->display_as('content_es','Contenido Español')->display_as('image_featured','Imagen Destacada');
		$crud->display_as('title_en','Título Inglés')->display_as('excerpt_en','Resumen Inglés');
		$crud->display_as('content_en','Contenido Inglés');
		$crud->display_as('status','Estado')->display_as('image_secondary','Imagen Secundaria');
		
		$crud->set_relation_n_n('categories', 'news_categories', 'categories', 'idnew','idcategory', 'name_es');
		$crud->display_as('categories','Categories')->display_as('publication_date','Fecha de publicación');
		$crud->display_as('created_at','Creado')->display_as('updated_at','Modificado');

		$crud->display_as('title_seo_es','Título SEO Español')->display_as('description_seo_es','Descripción SEO Español')->display_as('keywords_seo_es','Keywords SEO Español');
		$crud->display_as('title_seo_en','Título SEO Inglés')->display_as('description_seo_en','Descripción SEO Inglés')->display_as('keywords_seo_en','Keywords SEO Inglés');
		
		$crud->callback_after_insert(array($this,'generate_url_slug'));
		$crud->callback_after_update(array($this,'generate_url_slug'));

		$crud->unset_export();
		$crud->unset_print();
		$crud->order_by('id','desc');
		
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'nav');
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function categorias(){
		$crud = new grocery_CRUD();	
		$crud->set_table('categories');
		$crud->set_subject('Categorias');		
		$crud->columns('name_es', 'name_en',  'status', 'created_at');
		$crud->unset_fields(array('name_slug_es', 'name_slug_en', 'created_at', 'updated_at'));
		$crud->unset_delete();
		$crud->required_fields('name_es', 'status');

		$crud->display_as('name_es','Nombre Español');
		$crud->display_as('name_en','Nombre Inglés');
		$crud->display_as('created_at','Creado')->display_as('updated_at','Modificado');

		$crud->callback_after_insert(array($this,'generate_name_slug_categories'));
		$crud->callback_after_update(array($this,'generate_name_slug_categories'));
		
				
		$crud->unset_export();
		$crud->unset_print();
		
		$crud->order_by('id','asc');

		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'nav');
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function newsletter(){
		$crud = new grocery_CRUD();	
			
		$crud->set_table('newsletter');
		$crud->set_subject('Formulario Newsletter');		
		$crud->columns('email', 'confirmed', 'status');
		$crud->unset_fields(array('created_at', 'updated_at'));		

		$crud->display_as('email','Email');			
		$crud->display_as('confirmed', 'Confirmado');			
		$crud->display_as('status','Estado');		

		$crud->unset_add();		
		$crud->unset_edit();		
		$crud->unset_print();		
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function traducciones(){
		$crud = new grocery_CRUD();	
			
		$crud->set_table('traducciones');
		$crud->set_subject('Traducciones');		
		
		$crud->columns('clave', 'es', 'en', 'Traducir');	
		$crud->unset_fields(array('creado', 'modificado'));	
		//$crud->required_fields('clave', 'es','en');	

		$crud->unset_texteditor('clave','varchar');
		$crud->unset_texteditor('es','varchar');
		$crud->unset_texteditor('en','varchar');

		$crud->display_as('es','Español');			
		$crud->display_as('en','Inglés');			

		$state = $crud->getState();
		if($state == "edit")
		{
			$crud->field_type('clave','readonly');
		}
		
		$crud->callback_after_insert(array($this,'generate_traducciones'));
		$crud->callback_after_update(array($this,'generate_traducciones'));
		
		$crud->order_by('id','asc');	

		$crud->unset_delete();
		$crud->unset_export();
		$crud->unset_print();		
		$output = $crud->render();
		
		$this->load->view($this->config->item('theme_path_backend'). 'header');
		$this->load->view($this->config->item('theme_path_backend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_backend'). 'topnav');
		$this->load->view($this->config->item('theme_path_backend'). 'crud', $output);
		$this->load->view($this->config->item('theme_path_backend'). 'footer');
	}

	public function generate_traducciones($post_array, $primary_key = null){	  

	    if(isset($post_array['es']) and $post_array['es'] != '') $this->commonsmodel->updatelangfile('es','cars');
		if(isset($post_array['en']) and $post_array['en'] != '') $this->commonsmodel->updatelangfile('en','cars');
		
	    return $post_array;
	}
	
	//public function routes() {
		//$this->load->model('traduccionesmodel');	    
		//$marques = $this->marcasmodel->getMarcas();
		//$sectores = $this->sectoresmodel->getSectores();
	/*	$currentdate = date('Y-m-d');
		$default_controller = 'default_controller';
		$default_controller = '$route["'.$default_controller.'"];';
		$routesstr = '';
	*/	
	//	$routesstr = $routesstr . "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
		/**
		*
		* Created:  ".$currentdate." by Wancora
		*
		* Description:  Routes file for general project
		*/
		//"."\n\n";      
	/*
		$routesstr.= "\$route['default_controller'] = \"principal\";"."\n";
		$routesstr.= "\$route['404_override'] = \"custom404\";"."\n";
		$routesstr.= "\$route['translate_uri_dashes'] = FALSE;"."\n";

		$routesstr.= "\$route['^(es|en|fr|it|de|ru|tr|cs|pl|ko)/products'] = \"products\";"."\n";

		foreach($marques as $marca){
			$routesstr.= "\$route['^(es|en|fr|it|de|ru|tr|cs|pl|ko)/$marca->url_slug'] = \"products/brands/$marca->url_slug\";"."\n";
			$routesstr.= "\$route['^(es|en|fr|it|de|ru|tr|cs|pl|ko)/$marca->url_slug/(:any)'] = \"products/detail/$1\";"."\n";
			$routesstr.= "\$route['^(es|en|fr|it|de|ru|tr|cs|pl|ko)/$marca->url_slug/(:any)'] = \"products/detail/$1/$2\";"."\n";
		}

		foreach($sectores as $sector){
			$routesstr.= "\$route['^es/$sector->url_slug_es/(:any)'] = \"products/sectors/$sector->url_slug_es/$1\";"."\n";
			$routesstr.= "\$route['^en/$sector->url_slug_en/(:any)'] = \"products/sectors/$sector->url_slug_en/$1\";"."\n";
			$routesstr.= "\$route['^fr/$sector->url_slug_fr/(:any)'] = \"products/sectors/$sector->url_slug_fr/$1\";"."\n";
			$routesstr.= "\$route['^it/$sector->url_slug_it/(:any)'] = \"products/sectors/$sector->url_slug_it/$1\";"."\n";
			$routesstr.= "\$route['^de/$sector->url_slug_de/(:any)'] = \"products/sectors/$sector->url_slug_de/$1\";"."\n";
			$routesstr.= "\$route['^ru/$sector->url_slug_ru/(:any)'] = \"products/sectors/$sector->url_slug_ru/$1\";"."\n";
			$routesstr.= "\$route['^tr/$sector->url_slug_tr/(:any)'] = \"products/sectors/$sector->url_slug_tr/$1\";"."\n";
			$routesstr.= "\$route['^cs/$sector->url_slug_cs/(:any)'] = \"products/sectors/$sector->url_slug_cs/$1\";"."\n";
			$routesstr.= "\$route['^pl/$sector->url_slug_pl/(:any)'] = \"products/sectors/$sector->url_slug_pl/$1\";"."\n";
			$routesstr.= "\$route['^ko/$sector->url_slug_ko/(:any)'] = \"products/sectors/$sector->url_slug_ko/$1\";"."\n";

			$routesstr.= "\$route['^es/$sector->url_slug_es'] = \"products/sectors/$sector->url_slug_es\";"."\n";
			$routesstr.= "\$route['^en/$sector->url_slug_en'] = \"products/sectors/$sector->url_slug_en\";"."\n";
			$routesstr.= "\$route['^fr/$sector->url_slug_fr'] = \"products/sectors/$sector->url_slug_fr\";"."\n";
			$routesstr.= "\$route['^it/$sector->url_slug_it'] = \"products/sectors/$sector->url_slug_it\";"."\n";
			$routesstr.= "\$route['^de/$sector->url_slug_de'] = \"products/sectors/$sector->url_slug_de\";"."\n";
			$routesstr.= "\$route['^ru/$sector->url_slug_ru'] = \"products/sectors/$sector->url_slug_ru\";"."\n";
			$routesstr.= "\$route['^tr/$sector->url_slug_tr'] = \"products/sectors/$sector->url_slug_tr\";"."\n";
			$routesstr.= "\$route['^cs/$sector->url_slug_cs'] = \"products/sectors/$sector->url_slug_cs\";"."\n";
			$routesstr.= "\$route['^pl/$sector->url_slug_pl'] = \"products/sectors/$sector->url_slug_pl\";"."\n";
			$routesstr.= "\$route['^ko/$sector->url_slug_ko'] = \"products/sectors/$sector->url_slug_ko\";"."\n";
		}

		//estàtiques
		$routesstr.= "\$route['^(es|en|fr|it|de|ru|tr|cs|pl|ko)/vanto/contact'] = \"contact\";"."\n";
		$routesstr.= "\$route['^(es|en|fr|it|de|ru|tr|cs|pl|ko)/vanto/services'] = \"services\";"."\n";
		$routesstr.= "\$route['^(es|en|fr|it|de|ru|tr|cs|pl|ko)/vanto/about'] = \"about\";"."\n";
		$routesstr.= "\$route['^(es|en|fr|it|de|ru|tr|cs|pl|ko)/vanto/login'] = \"login\";"."\n";
		$routesstr.= "\$route['^(es|en|fr|it|de|ru|tr|cs|pl|ko)/vanto/signup'] = \"signup\";"."\n";
		$routesstr.= "\$route['^(es|en|fr|it|de|ru|tr|cs|pl|ko)/vanto/register'] = \"register\";"."\n";
		$routesstr.= "\$route['^(es|en|fr|it|de|ru|tr|cs|pl|ko)/vanto/forgot_password'] = \"auth/forgot_password\";"."\n";
		$routesstr.= "\$route['^(es|en|fr|it|de|ru|tr|cs|pl|ko)/vanto/Back_dashboard'] = \"Back_dashboard\";"."\n";
		$routesstr.= "\$route['^(es|en|fr|it|de|ru|tr|cs|pl|ko)/vanto/Back_dashboard/routes'] = \"Back_dashboard/routes/\";"."\n";

		$routesstr.= "\$route['^(es|en|fr|it|de|ru|tr|cs|pl|ko)/(.+)$'] = \"$2\";"."\n";
		$routesstr.= "\$route['^(es|en|fr|it|de|ru|tr|cs|pl|ko)$'] = $default_controller";
		
		write_file('./application/config/routes.php', $routesstr);

	}*/

}