<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Centros extends MY_Controller {
	
	//Controlador Centros
	public function __construct()
	{
		parent::__construct();		
		$this->data['seo'] = $this->metaseomodel->getMetaseo();			
	}

	public function index()
	{
		$this->data['body'] = 'centros';
		$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'content/centros',$this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'footer');
	}
}
