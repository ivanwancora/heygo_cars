<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keygonline extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->data['seo'] = $this->metaseomodel->getMetaseo();					
    }
    
	public function index(){
		$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'content/keygonline',$this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'footer');
	}

}