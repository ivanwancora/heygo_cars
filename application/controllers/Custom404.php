<?php
class Custom404 extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->data['seo'] = $this->metaseomodel->getMetaseo();		
	}
 
	public function index()
	{
		$this->output->set_status_header('404');
		$this->data['content'] = 'error404';  // View name
		$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'nav', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'content/error404', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'footer',$this->data);
	}
}
?>