<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acceso extends MY_Controller {
	
	//Controlador Acceso Usuarios
	public function __construct()
	{
		parent::__construct();	
		$this->data['seo'] = $this->metaseomodel->getMetaseo();				
	}

	public function index()
	{		
		$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'content/acceso',$this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'footer',$this->data);
	}
}
