<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->data['seo'] = $this->metaseomodel->getMetaseo();					
	}

	public function generate_url_slug_flotas($url_slug){	  
	    $unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
        
        $url_slug = filter_var($url_slug, FILTER_SANITIZE_STRING);
        $url_slug = strip_tags($url_slug);  
        $url_slug = stripslashes($url_slug);
        $url_slug = strtr($url_slug, $unwanted_array);  
		$url_slug = strtolower(url_title($url_slug));
		
		return $url_slug;
    }

	public function index(){
		$this->data['sliders'] = $this->commonsmodel->getSliders($this->lang->lang());
		$this->data['ocasion'] = $this->commonsmodel->getFlotaByCarType('Ocasion','es');
		$this->data['km'] = $this->commonsmodel->getFlotaByCarType('KM0','es');
		$this->data['seminuevos'] = $this->commonsmodel->getFlotaByCarType('Seminuevo','es');

		$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'content/index',$this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'footer');
	}

	/*public function bcrypt(){
		
		$data = 'p976M8o7'.date('jnY');
		$methodHeygocar = "AES-256-CBC";
		$keyHeygocar = 'keyugarteheygokeyugarteheygougar';
		$iv = '1234567890123456';
		$options =  0;
		$enc = openssl_encrypt($data, $methodHeygocar, $keyHeygocar, $options, $iv);
		$encManual = 'kvevphqeAuoWMknSantaKw==';
		echo "string: ".$data. "<br />key: ".$keyHeygocar." <br />encrypted: ".$enc;
	}*/

}
