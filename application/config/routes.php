<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
		/**
		*
		* Created:  2019-08-21 by Wancora
		*
		* Description:  Routes file for general project
		*/

$route['default_controller'] = 'principal';
$route['404_override'] = 'Custom404';
$route['translate_uri_dashes'] = FALSE;

//$route['^(es|en)/valencia'] = "vechiculos/dealer/valencia";
//$route['^(es|en)/valencia/(:any)'] = "vehiculos/detalle/$1";

$route['^(es|en)/keygocars/(:any)'] = "keygocars/index/$2";

//To use multilanguaje
$route['^(en|es|ca)/(.+)$'] = "$2";
$route['^(en|es|ca)$'] = $route['default_controller'];