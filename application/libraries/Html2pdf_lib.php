<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Html2pdf_lib {
  
  function __construct($params = null) {
    require_once( __DIR__ .'/html2pdf/vendor/autoload.php');  
    require_once(APPPATH.'libraries/html2pdf/src/Html2Pdf.php');    
    $this->html2pdf = new Spipu\Html2Pdf\HTML2PDF('P', 'A4', 'es');
    //$this->html2pdf->setTestTdInOnePage(false);
    $this->CI =& get_instance();
  }
  
  function converHtml2pdf($content, $filename = null, $save_to = null) {
    $this->html2pdf->writeHTML($content);
    if ($save_to == '' || $save_to == null) {
      $pdffile = $this->html2pdf->Output($filename);
    }
    else {
      $this->CI->load->helper('file');
      $pdffile = $this->html2pdf->Output($filename);
      if (! write_file($save_to.'/'.$filename,$pdffile)) {
        return false;
      } 
      else {
        return true;
      }
    }
  }
}

?>
