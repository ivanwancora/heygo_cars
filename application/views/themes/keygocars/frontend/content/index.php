    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_slider_home', $this->data); ?>

    <div class="p-filtros-header"></div>
    <section class="p-home--filter">
        <div class="w-content">
            <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/section_filter_home', $this->data); ?>
        </div>
        <div class="scroll-down">
                <?php echo lang('home_scroll'); ?>
            <span class="icon-point-line"></span>
                <?php echo lang('home_down'); ?>
        </div>  
    </section>  
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_keyonline_home', $this->data); ?>

    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_featured', $this->data); ?>

    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_our_vehicles', $this->data); ?>
    
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_sales', $this->data); ?>
    
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_centros', $this->data); ?>
    
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_marcas', $this->data); ?>
    
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_aboutus', $this->data); ?>
    
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_blog', $this->data); ?>
    
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_newsletter', $this->data); ?>
    
    
   