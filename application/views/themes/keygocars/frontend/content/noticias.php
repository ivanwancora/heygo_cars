<main class="p-noticias-page a-padding-section">
    <div class="pgp-head">    
        <h4 class="pgp-tag"><?php echo lang('Noticias Eventos');?></h4>
        <h1 class="pgp-header"><?php echo lang('Noticias-header');?></h1>
    </div>
    <nav class="m-menu--noticias">
        <div class="dropdown">
            <p>Filtro Categorías</p>
        </div>
        <ul class="categoria-container">
            <?php
              $name = 'name_'.$this->lang->lang();
              $tit = 'title_'.$this->lang->lang();
              $cont = 'content_'.$this->lang->lang();  
              $res = 'excerpt_'.$this->lang->lang(); 
              $url_slug = 'url_slug_'.$this->lang->lang();
              $name_slug = 'name_slug_'.$this->lang->lang();
              if(isset($categories)){
                $classactive = "";
                foreach ($categories as $categoria) {
                  if($categoria->$name_slug == $selcategory) $classactive = 'class="active"';
                  echo '<li>
                            <a '.$classactive.' href="'.base_url($this->lang->lang().'/'.lang('news').'/'.$categoria->$name_slug).'">'.$categoria->$name.'</a>   
                        </li>';
                  $classactive = "";
                }
              }
            ?>   
        </ul>
    </nav>
    <section class="o-noticias-container">
       <?php
                if(isset($resultados) and count($resultados) > 0){                    
                    $total = count($resultados);
                    
                    for ($i=0; $i < count($resultados); $i = $i+2) {   
                        echo '<div class="row">'; 
                        $categories = $categorias[$resultados[$i]->id];
                        $link_cat = "";
                        if(isset($categories)){
                            foreach ($categories as $categ) {
                              $link_cat .= '<a href="'.base_url($this->lang->lang().'/'.lang('news').'/'.$categ->$name_slug).'">'.$categ->$name.'</a> ';
                            }                            
                        }

                        echo '<article class="m-noticias-item" style="background-image:url('.base_url('uploads/news/images/'.$resultados[$i]->image_featured).')">';
                        echo '  <div class="content">';
                        echo '    <h4 class="categorias">'.$link_cat.'</h4>';
                        echo '    <h2 class="title" href="">'.$resultados[$i]->$tit.'</h2>';
                        echo '    <a href="'.base_url($this->lang->lang().'/'.lang('news').'/'.lang('detail').'/'.$resultados[$i]->$url_slug).'" class="more">'.lang('Read More').'</a>';
                        echo '  </div>';
                        echo '</article>';
                        if(($i+1) < $total){
                          $categories = $categorias[$resultados[$i+1]->id];
                          $link_cat = "";
                          if(isset($categories)){
                              foreach ($categories as $categ) {
                                $link_cat .= '<a href="'.base_url($this->lang->lang().'/'.lang('news').'/'.$categ->$name_slug).'">'.$categ->$name.'</a> ';
                              }
                          }
                          echo '<article class="m-noticias-item" style="background-image:url('.base_url('uploads/news/images/'.$resultados[$i+1]->image_featured).')">';
                          echo '  <div class="content">';
                          echo '    <h4 class="categorias">'.$link_cat.'</h4>';
                          echo '    <h2 class="title" href="">'.$resultados[$i+1]->$tit.'</h2>';
                          echo '    <a href="'.base_url($this->lang->lang().'/'.lang('news').'/'.lang('detail').'/'.$resultados[$i+1]->$url_slug).'" class="more">'.lang('Read More').'</a>';
                          echo '  </div>';
                          echo '</article>';
                        }
                        echo '</div>';
                     
                        

                    }
                }
                else{
                    echo '<h2>'.lang('No_news').'</h2>';
                }
        ?> 
        
    </section>  
    <?php 
          if (isset($links)) { 
              echo $links;
          } 
      ?>
</main>