	<section class="home-contacto contacto-form">
		<div class="w-content">
			<div class="image404">
				<img src="<?php echo base_url('assets/img/404/404.png'); ?>" align="left" alt="" width="250">
			</div>
			<div class="text404">
				<h1><?php echo lang('vaya'); ?></h1>
				<h3><?php echo lang('parece'); ?></h3>
				<p>ERROR 404</p>
				<a href="<?php echo base_url($this->lang->lang() . '/');?>" class="btn"><?php echo lang('btn_inici'); ?></a>
			</div>			
		</div>
	</section>
	