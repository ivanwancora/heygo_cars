<main class="p-vehiculos-search-page">
    <div class="figure-header stock">
        <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/stock.png');?>" alt="Stock" class="w100">
    </div>
    <h2 class="blue">
        <?php echo lang('vehiculos_title'); ?>
    </h2>
    <h4 class="blue">
        <?php echo lang('vehiculos_subtitle'); ?>
    </h4>
    <div class="pvsp-options-header">
        <nav id="m-menu--sales--flota">
            <ul>
                <li class="mmo-action"><a class="mmo-prev"><div class="icon-prev"></div></a></li>
                <li class="mmo-option todos init" data-type="Todos"><span ><?php echo lang('vehiculos_todos'); ?></span></li>
                <li class="mmo-option ocasion" data-type="Ocasion"><span ><?php echo lang('vehiculos_ocasion'); ?></span></li>
                <li class="mmo-option km0" data-type="Km0"><span><?php echo lang('vehiculos_km0'); ?></span></li>
                <li class="mmo-option seminuevo seminuevos last" data-type="Seminuevo"><span><?php echo lang('vehiculos_seminuevos'); ?></span></li>
                <li class="mmo-action"><a class="mmo-next"><div class="icon-next"></div></a></li>
            </ul>
        </nav>
    
        <button class="a-button--vehiculos-form">
            <?php echo lang('vehiculos_search_filter'); ?>
        </button>
        <a href="<?php echo base_url($this->lang->lang().'/mi_cuenta/favoritos'); ?>" class="a-button--favs--filter">
            <span></span>
        </a>
    </div>
    <div class="pvsp-main" id="sticky-parent">
        <section class="pvsp-m-form">
            <div class="sticky-element">
                <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/section_filter_vehicles', $this->data); ?>
            </div>
        </section>
        <section class="pvsp-m-grid">
            
            <!-- inici tags del filtre -->            
            <div class="o-tags-container" id="tags-items">
            </div>
            <!-- /fi tags del filtre -->

            <div class="pvsp-counter">
                <div class="counter"><span id="total_vehicles" class="total"></span></div>
                <div class="pvsp-c-select">
                    <div class="tag"><?php echo lang('vehiculos_orderby'); ?></div>
                    <div class="select">
                        <select class="select--order-grid" name="orderBy" id="orderBy">
                            <option value=""><?php echo lang('vehiculos_orderby_default'); ?></option>
                            <option value="price desc"><?php echo lang('vehiculos_orderby_maxprice'); ?></option>
                            <option value="price asc"><?php echo lang('vehiculos_orderby_minprice'); ?></option>
                            <option value="updated_at asc"><?php echo lang('vehiculos_orderby_new'); ?></option>
                            <option value="kilometers desc"><?php echo lang('vehiculos_orderby_maxkm'); ?></option>
                            <option value="kilometers asc"><?php echo lang('vehiculos_orderby_minkm'); ?></option>
                        </select>
                    </div>
                </div>
            </div>

            <div id="itemresults" class="o-vehicles-container">   
                <!-- contenidor del cotxes -->
            </div>
        </section>
    </div>
    <div class="offset-footer"></div>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/keygonline_general'); ?>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_newsletter', $this->data); ?>
</main>