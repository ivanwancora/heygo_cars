<main class="p-vehiculos-search-page favoritos">
    <div class="figure-header stock">
        <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/stock.png');?>" alt="Stock" class="w100">
    </div>
    <h2 class="blue">
        <?php echo lang('favoritos_title'); ?>
    </h2>
    <h4 class="blue">
        <?php echo lang('favoritos_subtitle'); ?>
    </h4>
    <div class="pvsp-main sticky-parent">
        <section class="">
            <div class="o-vehicles-container" id="favoritos" ></div>
        </section>
    </div>
    
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_newsletter', $this->data); ?>
</main>

<script type="text/javascript">                    
    var cotxes = <?php echo json_encode($flotas) ?>;
    vehicleSlides(cotxes, "#favoritos");
    swiperVehicleItem();
    vehiclePopUps();
    favorits();
</script>