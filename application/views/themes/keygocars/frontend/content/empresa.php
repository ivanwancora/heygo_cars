<main class="p-empresa-page">
    <div class="figure-header empresa">
        <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/empresa.png');?>" alt="Stock" class="w100">
    </div>
    <h2 class="blue">
        <?php echo lang('empresa_title'); ?>
    </h2>
    <h4 class="blue">
        <?php echo lang('empresa_subtitle'); ?>
    </h4>
    <div class="pcp-hero">
        <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/oficines/equipo.jpg');?>" class="w100" alt="">
    </div>
    <div class="pep-main">
        <div class="wrap">
            <div class="pep-m-header">
                <h1><?php echo lang('empresa_header'); ?></h1>
            </div>
            <div class="pep-m-section1">
                <p class="primary"><?php echo lang('empresa_sectionprimary'); ?></p>                
                <p><?php echo lang('empresa_section_one'); ?></p>
                <p><?php echo lang('empresa_section_two'); ?></p>
            </div>
            <div class="pep-m-section2">
                <div class="col1">
                    <div class="logo"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/logo/hgc_menu.svg');?>" alt=""></div>
                    <div class="pep-item">
                        <h4><?php echo lang('empresa_item1_head'); ?></h4>
                        <p><?php echo lang('empresa_item1_desc'); ?></p>
                    </div>
                    <div class="pep-item">
                        <h4><?php echo lang('empresa_item2_head'); ?></h4>
                        <p><?php echo lang('empresa_item2_desc'); ?></p>
                    </div>
                </div>
                <div class="col2">
                    <div class="pep-item">
                        <h4><?php echo lang('empresa_item3_head'); ?></h4>
                        <p><?php echo lang('empresa_item3_desc'); ?></p>
                    </div>
                    <div class="pep-item">
                        <h4><?php echo lang('empresa_item4_head'); ?></h4>
                        <p><?php echo lang('empresa_item4_desc'); ?></p>
                    </div>
                    <div class="pep-item">
                        <h4><?php echo lang('empresa_item5_head'); ?></h4>
                        <p><?php echo lang('empresa_item5_desc'); ?></p>
                    </div>
                </div>
            </div>
            <section class="pep-team-section">
                <h2 class="blue"><?php echo lang('empresa_ourteam_head'); ?></h2>
                <h4 class="blue"><?php echo lang('empresa_ourteam_desc'); ?></h4>

                <div class="o-team-container">
                    <?php if(isset($equipos)){ ?>
                        <?php foreach($equipos as $equipo){ ?>
                            <article class="m-team-item">
                                <div class="img">
                                    <img class="a-object-cover" src="<?php echo base_url('uploads/equipo/'.$equipo->image);?>" alt="">
                                </div>
                                <h3><?php echo $equipo->name; ?></h3>
                                <p><?php echo $equipo->job; ?></p>
                            </article>    
                        <?php } ?>
                    <?php } ?>
                </div>
            </section>
        </div>
    </div>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_centros', $this->data); ?>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_our_vehicles_compressed', $this->data); ?>
    <?php  $this->load->view($this->config->item('theme_path_frontend'). 'partials/keygonline_general'); ?>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_newsletter', $this->data); ?>
</main>