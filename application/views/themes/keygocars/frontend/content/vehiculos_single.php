<main class="p-vehiculos-single-page">
    <div class="pvsp-header">
        <button class="a-button--back" id="btn-back">
            <span class="icon-prev"></span>
            Volver a listado
        </button>
    </div>
    <section class="pvsp-main">
        <div class="w-content">
            <div class="pvsp-content">
                <div class="o-swiper--veh-single">
                    <div class="swiper-wrapper">
                        <?php
                            $arrayImatges = array();
                            $arrayImatges = explode(",",$flota->images);
                            $this->data['preu'] = '';
                            $i = 0;
                        ?>
                        <?php 
                        if(isset($arrayImatges)){
                            foreach ($arrayImatges as $imatge) { 
                                if(file_exists("./uploads/flotas/".$imatge)) { ?>
                                    <div class="swiper-slide" data-id="lightimg<?php echo $i;?>">
                                        <div class="wrap">
                                            <div class="image">
                                                <img class="a-object-contain swiper-lazy" data-src="<?php echo base_url('uploads/flotas/'.$imatge); ?>">
                                                <div class="swiper-lazy-preloader"></div>
                                            </div>
                                        </div>  
                                    </div>
                            <?php 
                                $i++;
                                } 
                            } 
                        }
                        ?>
                    </div>
                    <div class="swiper-veh-single-button-prev a-button--mutant--prev"></div>
                    <div class="swiper-veh-single-button-next a-button--mutant--next swiper-button-next"></div>
                    <?php 
                        $j = 0;
                        if(isset($arrayImatges)){
                            foreach ($arrayImatges as $imatge) { 
                                if(file_exists("./uploads/flotas/".$imatge)) { ?>
                                    <a href="<?php echo base_url('uploads/flotas/'.$imatge); ?>" data-lightbox="<?php echo $flota->make . ' ' . $flota->model; ?>" class="lightimg" id="lightimg<?php echo $j;?>"></a>
                            <?php 
                                $j++;
                                } 
                            } 
                        }
                    ?>
                    
                    <button class="a-button--lightbox icon-search"></button>
                </div>
                <section class="pvsp-c-header">
                    <div class="pvsp-ch-logo">
                        <?php if(isset($mflota)){ ?>
                            <img class="a-object-cover" src="<?php echo base_url('/uploads/marcas/'.$mflota->image.''); ?>" alt="<?php echo $mflota->title; ?>"> 
                        <?php } ?>                        
                    </div>
                    <div class="pvsp-ch-icon">
                        <span class="i-img icon-<?php echo $flota->icono; ?>"></span>
                        <h3 class="i-name"><?php echo $flota->bodytype; ?></h3>
                    </div>
                    <div class="pvsp-ch-model">
                        <h1 class="model"><?php echo $flota->make . ' ' . $flota->model; ?></h1>
                        <h2 class="version"><?php echo $flota->version; ?></h2>
                        <p class="ref">Ref. <?php echo $flota->vehicleid; ?></p>
                    </div>
                </section>
                <section class="pvsp-header-price">
                      <div class="pvsp-hp-sale-info m-vehicles-sale-info--bg-prim">
                            <div class="sale-wrap">
                                
                                <!-- CASO 1 -->
                                <?php if($flota->tipoficha==1){ $this->data['preu'] =  number_format($flota->price, 0, ',', '.'); ?>
                                    <div class="r1">
                                        <div class="svi-des-init">
                                            <div class="svi-price--pvp">PVP</div>    
                                        </div>
                                    </div>
                                    <div class="r2">
                                        <h3 class="svi-price--final"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                                    </div>
                                <?php } ?>

                                <!-- CASO 5 -->
                                <?php if($flota->tipoficha==5){ $this->data['preu'] =  number_format($flota->offerprice, 0, ',', '.'); ?>
                                    <div class="r1">
                                        <div class="svi-des-init">
                                            <div class="svi-price--pvp">PVP</div>    
                                            <div class="svi-discount"><?php echo number_format(($flota->offerprice*100/$flota->price)-100, 0, ',', '.');?>%</div>
                                                <h3 class="svi-price--erased"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                                        </div>
                                    </div>
                                    <div class="r2">
                                        <h3 class="svi-price--final"><?php echo number_format($flota->offerprice, 0, ',', '.'); ?>€</h3>
                                    </div>
                                    <div class="r3">
                                        <div class="svi-fee">
                                            <span class="svi-from"><?php echo lang('vehiculos_from'); ?></span>
                                            <span class="svi-price-fee quota"><?php echo number_format($flota->financingfee, 0, ',', '.'); ?>€</span>
                                            <span class="svi-price-fee divider">/</span>
                                            <span class="svi-price-fee month"><?php echo lang('vehiculos_month'); ?></span>
                                            <span class="svi-price-fee comment">*</span>
                                            <span class="icon-info">
                                                <div class="m-popup--vehicle-item popup">
                                                    <div class="wrap"><p>
                                                        <?php if($flota->description!=''){ ?>
                                                            <?php echo '<p>' . lang('vehiculo_offer_heygo') . ': ' . $flota->description . '</p>'; ?>
                                                        <?php } ?>
                                                        <?php if($flota->financingtext!=''){ ?>
                                                            <?php echo '<p>' . lang('vehiculo_customfinance') . ': ' . $flota->financingtext . '</p>'; ?>
                                                        <?php } ?></p>
                                                    </div>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                <?php } ?>

                                <!-- CASO 2 -->
                                <?php if($flota->tipoficha==2){ $this->data['preu'] =  number_format($flota->offerprice, 0, ',', '.'); ?>
                                    <div class="r1">
                                        <div class="svi-des-init">
                                            <div class="svi-price--pvp">PVP</div>    
                                            <div class="svi-discount"><?php echo number_format(($flota->offerprice*100/$flota->price)-100, 0, ',', '.');?>%</div>
                                            <h3 class="svi-price--erased"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                                        </div>
                                    </div>
                                    <div class="r2">
                                        <h3 class="svi-price--final"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                                    </div>
                                <?php } ?>

                                <!-- CASO 3 -->
                                <?php if($flota->tipoficha==3){ $this->data['preu'] =  number_format($flota->offerprice, 0, ',', '.'); ?>
                                    <div class="r1">
                                        <div class="svi-des-init">
                                            <div class="svi-price--pvp">PVP</div>   
                                        </div>
                                    </div>
                                    <div class="r2">
                                        <h3 class="svi-price--initial"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                                    </div>
                                    <div class="r3">
                                        <h3 class="svi-price--final"><?php echo number_format($flota->offerprice, 0, ',', '.'); ?>€*</h3>
                                    </div>
                                <?php } ?>

                                <!-- CASO 4 -->
                                <?php if($flota->tipoficha==4){ $this->data['preu'] =  number_format($flota->price, 0, ',', '.'); ?>
                                    <div class="r1">
                                        <div class="svi-des-init">
                                            <div class="svi-price--pvp">PVP</div>   
                                        </div>
                                    </div>
                                    <div class="r2">
                                        <h3 class="svi-price--final"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                                    </div>
                                    <div class="r3">
                                        <div class="svi-fee">
                                            <span class="svi-from"><?php echo lang('vehiculos_from'); ?></span>
                                            <span class="svi-price-fee quota"><?php echo number_format($flota->financingfee, 0, ',', '.'); ?>€</span>
                                            <span class="svi-price-fee divider">/</span>
                                            <span class="svi-price-fee month"><?php echo lang('vehiculos_month'); ?></span>
                                            <span class="svi-price-fee comment">*</span>
                                            <span class="icon-info">
                                                <div class="m-popup--vehicle-item popup">
                                                    <div class="wrap"><p>
                                                        <?php if($flota->description!=''){ ?>
                                                            <?php echo '<p>' . lang('vehiculo_offer_heygo') . ': ' . $flota->description . '</p>'; ?>
                                                        <?php } ?>
                                                        <?php if($flota->financingtext!=''){ ?>
                                                            <?php echo '<p>' . lang('vehiculo_customfinance') . ': ' . $flota->financingtext . '</p>'; ?>
                                                        <?php } ?></p>
                                                    </div>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                <?php } ?>                                
                                
                            </div>
                             <!-- CASO 2 ó 5 (icono oferta)-->
                             <?php if($flota->tipoficha==2 || $flota->tipoficha==5){ ?>
                                <div class="svi-sale-badget--big">
                                    <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/logo/oferta.svg');?>" alt="">
                                </div>
                            <?php } ?>
                      </div> 
                      <div class="pvsp-hp-buttons">
                          <div class="button-wrap">
                              <button type="button" data-id="#form-vsingle .form" class="a-button--contactar btn-to-id">
                                <span class="icon-email"></span>
                                <?php echo lang('vehiculo_contactar'); ?>
                              </button>
                              <!-- <a href="" class="a-button--llamamos">    
                                <span class="icon-phone"></span>
                                te llamamos
                              </a> 
                              <a href="" class="a-button--baja-precio">
                                <span class="icon-people"></span>
                                <?php //echo lang('vehiculo_avisame'); ?>
                              </a>-->
                          </div>
                      </div>
                </section>
                <section class="pvsp-description">
                    <div class="desc-wrap">
                        <div class="o-caracteristics-container">
                            <div class="m-caracteristics-item">
                                <div class="icon-calendar"></div>
                                <div class="body">
                                    <h4><?php echo lang('vehiculo_year'); ?></h4>
                                    <p><?php echo $flota->modelyear;?></p>
                                </div>
                            </div>
                            <div class="m-caracteristics-item">
                                <div class="icon-km"></div>
                                <div class="body">
                                    <h4><?php echo lang('vehiculo_kms'); ?></h4>
                                    <p><?php echo number_format($flota->kilometers, 0, ',', '.'); ?> km</p>
                                </div>
                            </div>
                            <div class="m-caracteristics-item">
                                <div class="icon-people"></div>
                                <div class="body">
                                    <h4><?php echo lang('vehiculo_capacity'); ?></h4>
                                    <p><?php echo $flota->seats;?></p>
                                </div>
                            </div>
                            <div class="m-caracteristics-item">
                                <div class="icon-gasoline"></div>
                                <div class="body">
                                    <h4><?php echo lang('vehiculo_fuel'); ?></h4>
                                    <p><?php echo $flota->fueltype;?></p>
                                </div>
                            </div>
                            <div class="m-caracteristics-item">
                                <div class="icon-cambio"></div>
                                <div class="body">
                                    <h4><?php echo lang('vehiculo_cambio'); ?></h4>
                                    <p><?php echo $flota->geartype;?></p>
                                </div>
                            </div>
                            <div class="m-caracteristics-item">
                                <div class="icon-color"></div>
                                <div class="body">
                                    <h4><?php echo lang('vehiculo_color'); ?>Color</h4>
                                    <p><?php echo $flota->color;?></p>
                                </div>
                            </div>
                            <div class="m-caracteristics-item">
                                <div class="icon-volante"></div>
                                <div class="body">
                                    <h4><?php echo lang('vehiculo_cilindrada'); ?></h4>
                                    <?php $potencia = ''; 
                                    if(isset($flota->poweroutput) && $flota->poweroutput!=''){ $potencia .= $flota->poweroutput . 'CC <br />'; }
                                    if(isset($flota->horsespower) && $flota->horsespower!=''){ $potencia .= $flota->horsespower . 'CV <br />'; }
                                    if(isset($flota->horseskw) && $flota->horseskw!=''){ $potencia .= $flota->horseskw . 'kW '; }
                                    ?>
                                    <p><?php echo $potencia; ?></p>
                                </div>
                            </div>
                            <div class="m-caracteristics-item">
                                <div class="icon-doors"></div>
                                <div class="body">
                                    <h4><?php echo lang('vehiculo_npuertas'); ?></h4>
                                    <p><?php echo $flota->noofdoors . ' ' .lang('vehiculo_puertas'); ?></p>
                                </div>
                            </div>
                            <div class="m-caracteristics-item">
                                <div class="icon-garantia"></div>
                                <div class="body">
                                    <h4><?php echo lang('vehiculo_garantia'); ?></h4>
                                    <p><?php echo $flota->warranty;?></p>
                                </div>
                            </div>
                        </div>
                        <span class="a-divider--primary"></span>
                            <?php if($flota->description!=''){ ?>
                                <p class="pcsp-d-item">
                                    <span class="blue"><?php echo lang('vehiculo_offer_heygo'); ?></span> / <?php echo $flota->description; ?>
                                </p>
                            <?php } ?>
                            <?php if($flota->financingtext!=''){ ?>
                                <p class="pcsp-d-item">
                                    <span class="blue"><?php echo lang('vehiculo_customfinance'); ?></span> / <?php echo $flota->financingtext; ?>
                                </p>
                            <?php } ?>
                            <div class="mgb-30"></div>
                            <?php if($flota->description!=''){ ?>
                                <span class="a-divider--primary"></span>
                                <div class="pscp-certificate">
                                    <div class="ico">
                                        <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/logo-certificate.svg');?>" class="w100" />
                                    </div>
                                    <a class="box" href="">
                                        <?php echo lang('vehicle_informe_pericial'); ?>
                                    </a>
                                </div>
                            <?php } ?>
                        </p>
                    </div>
                </section>
                <div class="o-equipment-tabs o-tabs-module">
                    <nav class="m-menu--tabs m-menu--tabs--two">
                        <div class="wrap">
                            <ul>
                                <li><a class="active" data-section="#section1"><?php echo lang('vehiculo_equipamiento_serie'); ?></a></li>
                                <li><a data-section="#section2"><?php echo lang('vehiculo_equipamiento_opcional'); ?></a></li>
                            </ul>    
                        </div>    
                    </nav>
                    <section class="tabs-section active" id="section1">
                        <div class="w-tabs">   
                            <div class="content">
                                <?php if(isset($flota->optionserietext) && $flota->optionserietext!=''){ 
                                    $texte = str_replace(";", "</li><li>", $flota->optionserietext);
                                    echo '<ul class="a-list--dot"><li>';
                                        echo $texte; 
                                    echo '</li></ul>';
                                } ?>                                    
                            </div> 
                        </div>             
                    </section>
                    <section class="tabs-section" id="section2">
                        <div class="w-tabs">   
                            <div class="content">
                                <?php if(isset($flota->optiontext) && $flota->optiontext!=''){ 
                                    $texte = str_replace(";", "</li><li>", $flota->optiontext);
                                    echo '<ul class="a-list--dot"><li>';
                                        echo $texte; 
                                    echo '</li></ul>';
                                } ?>                          
                            </div> 
                        </div>             
                    </section>
                </div>
                <div class="pvsp-batges">
                    <div class="wrap">
                        <div class="pvsp-batge certificate">
                            <div class="img">
                                <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/thumbsup.png');?>" alt="">
                            </div>
                            <div class="body">
                                <p><?php echo lang('vehiculo_coche_rev'); ?></p>
                            </div>
                        </div>
                        <div class="pvsp-batge warranty">
                            <div class="img">
                                <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/check.png');?>" alt="">
                            </div>
                            <div class="body">
                                <p><?php echo lang('vehiculo_coche_garantia100'); ?></p>
                                <div class="logo">
                                    <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/logo/hgc_primary.svg');?>" alt="">                         
                                </div>
                            </div>
                        </div>   
                    </div>

                </div>
                <div class="pvsp-center">
                    <h4><?php echo lang('vehiculo_available'); ?> <?php echo $dealer->place . ' (' . $dealer->title . ')'; ?></h4>
                    <div class="wrap">
                        <div class="col1">
                            <div class="mci-info adress">
                                <span class="icon-point-line"></span>
                                <p class="body">
                                    <?php echo $dealer->address; ?>
                                    <br />
                                    <a target="_blank" href="<?php echo "http://maps.google.com/?q=".$dealer->latitude.','.$dealer->longitude;  ?>" class="it-primary">> <?php echo lang('general_view_map'); ?></a>
                                </p>
                            </div>
                        </div>
                        <div class="col2">
                            <div class="mci-info schedule">
                                <span class="icon-clock"></span>
                                <div class="body">
                                    <u><?php echo lang('centros_horario'); ?></u>
                                    <?php echo $dealer->schedule; ?>
                                </div>
                            </div>
                            <div class="mci-info phone">
                                <span class="icon-phone"></span>
                                <p class="body">
                                <u><?php echo lang('centro_att'); ?></u><br><a href="tel:<?php echo $dealer->phone; ?>"><?php echo $dealer->phone; ?></a>
                                </p>
                            </div>
                            <div class="mci-info email">
                                <span class="icon-email"></span>
                                <p class="body">
                                    <a href="mailto:<?php echo $dealer->email; ?>"><?php echo $dealer->email; ?></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form class="pvsp-form sticky-parent" id="form-vsingle">
                <div class="sticky-element">
                    <div class="wrap-sticky">
                        <div class="form-sale-info m-vehicles-sale-info">
                            <div class="sale-wrap">
                                <?php // control dels 5 casos del cotxe ?>
                                <!-- CASO 1 -->
                                <?php if($flota->tipoficha==1){ ?>
                                    <div class="r1">
                                        <div class="svi-des-init">
                                            <div class="svi-price--pvp">PVP</div>   
                                        </div>
                                    </div>
                                    <div class="r2">
                                        <h3 class="svi-price--final"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                                    </div>
                                <?php } ?>

                                <!-- CASO 2 -->
                                <?php if($flota->tipoficha==2){ ?>
                                    <div class="r1">
                                        <div class="svi-des-init">
                                            <div class="svi-price--pvp">PVP</div>    
                                            <div class="svi-discount"><?php echo number_format(($flota->offerprice*100/$flota->price)-100, 0, ',', '.');?>%</div>
                                                <h3 class="svi-price--erased"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                                        </div>
                                    </div>
                                    <div class="r2">
                                        <h3 class="svi-price--final"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                                    </div>
                                <?php } ?>

                                <!-- CASO 3 -->
                                <?php if($flota->tipoficha==3){ ?>
                                    <div class="r1">
                                        <div class="svi-des-init">
                                            <div class="svi-price--pvp">PVP</div>   
                                        </div>
                                    </div>
                                    <div class="r2">
                                            <h3 class="svi-price--initial"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                                        </div>
                                        <div class="r3">
                                            <h3 class="svi-price--final"><?php echo number_format($flota->offerprice, 0, ',', '.'); ?>€*</h3>
                                        </div>
                                <?php } ?>

                                <!-- CASO 4 -->
                                <?php if($flota->tipoficha==4){ ?>
                                    <div class="r1">
                                        <div class="svi-des-init">
                                            <div class="svi-price--pvp">PVP</div>   
                                        </div>
                                    </div>
                                    <div class="r2">
                                        <h3 class="svi-price--final"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                                    </div>
                                    <div class="r3">
                                        <div class="svi-fee">
                                            <span class="svi-from"><?php echo lang('vehiculos_from'); ?></span>
                                            <span class="svi-price-fee quota"><?php echo number_format($flota->financingfee, 0, ',', '.'); ?>€</span>
                                            <span class="svi-price-fee divider">/</span>
                                            <span class="svi-price-fee month"><?php echo lang('vehiculos_month'); ?></span>
                                            <span class="svi-price-fee comment">*</span>
                                            <span class="icon-info">
                                                <div class="m-popup--vehicle-item popup">
                                                    <div class="wrap"><p>
                                                        <?php if($flota->description!=''){ ?>
                                                            <?php echo '<p>' . lang('vehiculo_offer_heygo') . ': ' . $flota->description . '</p>'; ?>
                                                        <?php } ?>
                                                        <?php if($flota->financingtext!=''){ ?>
                                                            <?php echo '<p>' . lang('vehiculo_customfinance') . ': ' . $flota->financingtext . '</p>'; ?>
                                                        <?php } ?></p>
                                                    </div>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                <?php } ?>

                                <!-- CASO 5 -->
                                <?php if($flota->tipoficha==5){ ?>
                                    <div class="r1">
                                        <div class="svi-des-init">
                                            <div class="svi-price--pvp">PVP</div>    
                                            <div class="svi-discount"><?php echo number_format(($flota->offerprice*100/$flota->price)-100, 0, ',', '.');?>%</div>
                                                <h3 class="svi-price--erased"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                                        </div>
                                    </div>
                                    <div class="r2">
                                        <h3 class="svi-price--final"><?php echo number_format($flota->offerprice, 0, ',', '.'); ?>€</h3>
                                    </div>
                                    <div class="r3">
                                        <div class="svi-fee">
                                            <span class="svi-from"><?php echo lang('vehiculos_from'); ?></span>
                                            <span class="svi-price-fee quota"><?php echo number_format($flota->financingfee, 0, ',', '.'); ?>€</span>
                                            <span class="svi-price-fee divider">/</span>
                                            <span class="svi-price-fee month"><?php echo lang('vehiculos_month'); ?></span>
                                            <span class="svi-price-fee comment">*</span>
                                            <span class="icon-info">
                                                <div class="m-popup--vehicle-item popup">
                                                    <div class="wrap"><p>
                                                        <?php if($flota->description!=''){ ?>
                                                            <?php echo '<p>' . lang('vehiculo_offer_heygo') . ': ' . $flota->description . '</p>'; ?>
                                                        <?php } ?>
                                                        <?php if($flota->financingtext!=''){ ?>
                                                            <?php echo '<p>' . lang('vehiculo_customfinance') . ': ' . $flota->financingtext . '</p>'; ?>
                                                        <?php } ?></p>
                                                    </div>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <!-- CASO 2 ó 5 (icono oferta)-->
                            <?php if($flota->tipoficha==2 || $flota->tipoficha==5){ ?>
                            <div class="svi-sale-badget--big">
                                <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/logo/oferta.svg');?>" alt="">
                            </div>
                            <?php } ?>
                            <div class="w100 pdt-20 pdb-20">
                            <?php if($flota->reserved==0){ //en stock ?>
                                <button type="button" id="button-reservar" class="a-button--reservar">
                                    <span class="icon-arrow"></span>
                                    <?php echo lang('button_reservar'); ?>
                                </button>
                            <?php }else{ //sin stock, ya reservado ?>
                                <button type="button" <?php echo $flota->reserved; ?>" class="">
                                    <span class="icon-arrow"></span>
                                    <?php echo lang('general_vehiculo_reservado_single'); ?>
                                </button>
                            <?php } ?>
                            </div>
                            <span class="a-divider--primary"></span>
                        </div> 

                        <!-- Inici formulari vehicle single -->
                        <div class="form">
                                <input type="text" name="vehiculo_name" id="vehiculo_name" class="input-mirror" data-mirror="reserva_name" placeholder="<?php echo lang('vehiculo_cname'); ?>">
                                <input type="email" name="vehiculo_email" id="vehiculo_email" class="input-mirror" data-mirror="reserva_email" placeholder="<?php echo lang('vehiculo_email'); ?>" pattern="[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})">
                                <input type="text" name="vehiculo_phone" id="vehiculo_phone" class="input-mirror" data-mirror="reserva_phone" placeholder="<?php echo lang('vehiculo_phone'); ?>">
                                <input type="text" name="vehiculo_cp" id="vehiculo_cp" class="input-mirror" data-mirror="reserva_cp" placeholder="<?php echo lang('vehiculo_cp'); ?>">
                                <textarea name="vehiculo_sms" id="vehiculo_sms" class="input-mirror" data-mirror="reserva_sms" placeholder="<?php echo lang('contact_form_msg'); ?>"></textarea>
                                <label class="a-one-line-checkbox" for="politica">
                                    <input type="checkbox" name="politica" id="politica">
                                    <span></span>
                                    <?php echo lang('vehiculo_accept_politica'); ?>
                                </label>
                                <label class="validation_error_message" for="politica"></label>
                                <label class="a-one-line-checkbox" for="aviso">
                                    <input type="checkbox" name="aviso" id="aviso">
                                    <span></span>
                                    <?php echo lang('vehiculo_accept_aviso'); ?>
                                </label>
                                <label class="validation_error_message" for="aviso"></label>
                                <label class="a-one-line-checkbox" for="message-newsletter">
                                    <input type="checkbox" name="message-newsletter" id="message-newsletter" value="1">
                                    <span></span>
                                    <?php echo lang('vehiculo_accept_newsletter'); ?>
                                </label>
                                <label class="validation_error_message" for="message-newsletter"></label>
                                <input type="hidden" name="ipaddress" id="ipaddress" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
                                <input type="hidden" name="navigator" id="navigator" value="<?php echo $_SERVER['HTTP_USER_AGENT']; ?>">
                                <input type="hidden" name="vehiculo_single" id="vehiculo_single" value="vehiculo_single">
                                <input type="hidden" name="vehicleid" id="vehicleid" value="<?php echo $flota->vehicleid; ?>">
                                <input type="hidden" name="precio" id="precio" value="<?php echo $flota->finalprice; ?>">
                                <input type="hidden" name="make" id="make" value="<?php echo $flota->make; ?>">
                                <input type="hidden" name="emailinteresado" id="emailinteresado" value="<?php echo $flota->emailinteresado; ?>">
                                <div data-callback="imNotARobot" class="g-recaptcha" data-sitekey="6LfQJZAUAAAAAMUkEtt9lozeOMTy13yOab-rSqYl"></div>
                        </div>
                        <div class="buttons">
                            <button type="submit" id="btn-submit" class="a-button--contactar">
                                <span class="icon-email"></span>
                                <?php echo lang('vehiculo_contactar'); ?>
                            </button>
                            <div id="sms"></div>
                            <div class="w100 pdt-10">
                                <a href="<?php echo base_url($this->lang->lang().'/keygonline/'); ?>" class="a-button--videocall">    
                                    <span class="icon-video"></span>
                                    <?php echo lang('solicitar_video_llamada'); ?>
                                </a>
                            </div>
                            <button type="button" class="a-button--baja-precio">
                                <span class="icon-people"></span>
                                <?php echo lang('vehiculo_bajar_precio'); ?>
                            </button>
                        </div>
                        <!-- Fi formulari vehicle single -->
                    </div>
                </div>
            </form>
        </div>
    </section>
    <section class="pvsp-share">
        <div class="wrapper">
            <div class="share-module">
                <h5><?php echo lang('vehiculo_share'); ?></h5>
                <?php $versio = str_replace('&','',$flota->version); ?>
                <nav class="m-menu--social--share">
                    <ul>
                        <li><div data-href="<?php echo $flota->url_slug; ?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="icon-facebook-share" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $flota->bitly; ?>"></a></div></li>
                        <li><a class="icon-twitter-share" href="https://twitter.com/intent/tweet?text=<?php echo $flota->make.'+'.$flota->model.'+'.$versio.'+'.$flota->modelyear.'+'.$flota->color; ?>+<?php echo $flota->bitly; ?>" target="_blank"></a></li>
                        <!--<li><a class="icon-whatssap-share" href="https://wa.me/?text=<?php echo $flota->bitly; ?>" data-action="share/whatsapp/share" target="_blank"></a></li>-->
                        <li><a class="icon-linkedin-share" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $flota->bitly; ?>&title=titol&summary=desc&source=" target="_blank"></a></li>
                        <li><a class="icon-email-share" href="mailto:?subject=<?php echo $flota->make.' '.$flota->model.' '.$versio.' '.$flota->modelyear.' '.$flota->color; ?>&body=<?php echo $flota->bitly; ?>" target="_blank"></a></li>
                    </ul>
                </nav>
            </div>
        </div>                
    </section>

    <section class="p-home--sales">
        <div class="w-content">
            <h2 class="blue">
                <?php echo lang('vehiculo_otros'); ?>
            </h2>
            <h4>
                <?php echo lang('vehiculo_otros_usuarios'); ?>
            </h4>
            <div class="phs-container">
                <div class="o-swiper--home-sales active">
                    <div class="home-sales-navigation secondary-active">
                        <div class="home-sales-pagination"></div>
                    </div>
                    <div id="relacionats" class="swiper-wrapper">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/keygonline_general', $this->data); ?>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_why_heygo', $this->data); ?>
</main>

<form class="m-form--watch-price" id="form-watch-price" autocomplete="off" method="post" action="#">
    <div class="watch-cross"></div>
    <div class="wrap">
        <h3><?php echo lang('watch-header'); ?></h3>
        <p><?php echo lang('watch-body'); ?></p>
        <input type="text" name="watch-price" id="watch-price" placeholder="<?php echo lang('watch-placeholder');?>" />
        <input type="hidden" name="vehicleid" id="vehicleid" value="<?php echo $flota->vehicleid; ?>">
        <input type="hidden" name="precio" id="precio" value="<?php echo $flota->finalprice; ?>">
        
        <label class="a-one-line-checkbox">
        <input type="checkbox" required name="watch-accept" id="watch-accept">
            <span></span>   
            <div class="body"><?php echo lang('newsletter_check'); ?></div>
        </label> 
        <button type="button" id="btn-submit-wp" class="a-button--offset--small">
            <div class="btn">
                <?php echo lang('newsletter_send'); ?>
            </div>
            <div class="shadow"></div>
        </button>
        <div id="smswatchprice"></div>
    </div>
</form>
<span class="m-form--watch-price-bg"></span>

<?php if($flota->reserved==0){ //si no està en stock, deshabilitem formulari ?> 
<form class="m-form--reserve" id="form-reserve" autocomplete="off" method="post" action="#">
    <div class="mfr-header">
        <div class="wrap">
            <h2><?php echo lang('form_reserva_vehículo_header'); ?></h2>
            <div class="reserva-cross icon-close"></div>
        </div>
    </div>
    <div class="mfr-info">
        <div class="wrap">
            <div class="mfr-model">
                <h1 class="model"><?php echo $flota->make . ' ' . $flota->model; ?></h1>
                <h2 class="version"><?php echo $flota->version; ?></h2>
                <p class="ref">Ref. <?php echo $flota->vehicleid; ?></p>
            </div>
            <div class="m-vehicles-sale-info">
                <div class="sale-wrap">
                    <?php // control dels 5 casos del cotxe ?>
                    <!-- CASO 1 -->
                    <?php if($flota->tipoficha==1){ ?>
                        <div class="r1">
                            <div class="svi-des-init">
                                <div class="svi-price--pvp">PVP</div>   
                            </div>
                        </div>
                        <div class="r2">
                            <h3 class="svi-price--final"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                        </div>
                    <?php } ?>

                    <!-- CASO 2 -->
                    <?php if($flota->tipoficha==2){ ?>
                        <div class="r1">
                            <div class="svi-des-init">
                                <div class="svi-price--pvp">PVP</div>    
                                <div class="svi-discount"><?php echo number_format(($flota->offerprice*100/$flota->price)-100, 0, ',', '.');?>%</div>
                                    <h3 class="svi-price--erased"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                            </div>
                        </div>
                        <div class="r2">
                            <h3 class="svi-price--final"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                        </div>
                    <?php } ?>

                    <!-- CASO 3 -->
                    <?php if($flota->tipoficha==3){ ?>
                        <div class="r1">
                            <div class="svi-des-init">
                                <div class="svi-price--pvp">PVP</div>   
                            </div>
                        </div>
                        <div class="r2">
                                <h3 class="svi-price--initial"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                            </div>
                            <div class="r3">
                                <h3 class="svi-price--final"><?php echo number_format($flota->offerprice, 0, ',', '.'); ?>€*</h3>
                            </div>
                    <?php } ?>

                    <!-- CASO 4 -->
                    <?php if($flota->tipoficha==4){ ?>
                        <div class="r1">
                            <div class="svi-des-init">
                                <div class="svi-price--pvp">PVP</div>   
                            </div>
                        </div>
                        <div class="r2">
                            <h3 class="svi-price--final"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                        </div>
                        <div class="r3">
                            <div class="svi-fee">
                                <span class="svi-from"><?php echo lang('vehiculos_from'); ?></span>
                                <span class="svi-price-fee quota"><?php echo number_format($flota->financingfee, 0, ',', '.'); ?>€</span>
                                <span class="svi-price-fee divider">/</span>
                                <span class="svi-price-fee month"><?php echo lang('vehiculos_month'); ?></span>
                                <span class="svi-price-fee comment">*</span>
                                <span class="icon-info">
                                    <div class="m-popup--vehicle-item popup">
                                        <div class="wrap"><p>
                                            <?php if($flota->description!=''){ ?>
                                                <?php echo '<p>' . lang('vehiculo_offer_heygo') . ': ' . $flota->description . '</p>'; ?>
                                            <?php } ?>
                                            <?php if($flota->financingtext!=''){ ?>
                                                <?php echo '<p>' . lang('vehiculo_customfinance') . ': ' . $flota->financingtext . '</p>'; ?>
                                            <?php } ?></p>
                                        </div>
                                    </div>
                                </span>
                            </div>
                        </div>
                    <?php } ?>

                    <!-- CASO 5 -->
                    <?php if($flota->tipoficha==5){ ?>
                        <div class="r1">
                            <div class="svi-des-init">
                                <div class="svi-price--pvp">PVP</div>    
                                <div class="svi-discount"><?php echo number_format(($flota->offerprice*100/$flota->price)-100, 0, ',', '.');?>%</div>
                                    <h3 class="svi-price--erased"><?php echo number_format($flota->price, 0, ',', '.'); ?>€</h3>
                            </div>
                        </div>
                        <div class="r2">
                            <h3 class="svi-price--final"><?php echo number_format($flota->offerprice, 0, ',', '.'); ?>€</h3>
                        </div>
                        <div class="r3">
                            <div class="svi-fee">
                                <span class="svi-from"><?php echo lang('vehiculos_from'); ?></span>
                                <span class="svi-price-fee quota"><?php echo number_format($flota->financingfee, 0, ',', '.'); ?>€</span>
                                <span class="svi-price-fee divider">/</span>
                                <span class="svi-price-fee month"><?php echo lang('vehiculos_month'); ?></span>
                                <span class="svi-price-fee comment">*</span>
                                <span class="icon-info">
                                    <div class="m-popup--vehicle-item popup">
                                        <div class="wrap"><p>
                                            <?php if($flota->description!=''){ ?>
                                                <?php echo '<p>' . lang('vehiculo_offer_heygo') . ': ' . $flota->description . '</p>'; ?>
                                            <?php } ?>
                                            <?php if($flota->financingtext!=''){ ?>
                                                <?php echo '<p>' . lang('vehiculo_customfinance') . ': ' . $flota->financingtext . '</p>'; ?>
                                            <?php } ?></p>
                                        </div>
                                    </div>
                                </span>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <!-- CASO 2 ó 5 (icono oferta)-->
                <?php if($flota->tipoficha==2 || $flota->tipoficha==5){ ?>
                <div class="svi-sale-badget--big">
                    <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/logo/oferta.svg');?>" alt="Oferta">
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- Inici formulari vehicle single -->
    <div class="mfr-form">
        <div class="wrap">
            <h3><?php echo lang('form_reserva_datos_personales_header'); ?></h3>
            <p class="grey"><?php echo lang('form_reserva_datos_personales_subtitle'); ?></p>
            <input type="text" name="reserva_name" id="reserva_name" class="input-mirror" data-mirror="vehiculo_name" placeholder="<?php echo lang('vehiculo_cname'); ?>" required>
            <input type="email" name="reserva_email" id="reserva_email" class="input-mirror" data-mirror="vehiculo_email" placeholder="<?php echo lang('vehiculo_email'); ?>" required pattern="[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})">
            <input type="text" name="reserva_phone" id="reserva_phone" class="input-mirror" data-mirror="vehiculo_phone" placeholder="<?php echo lang('vehiculo_phone'); ?>" required minlength=9>
            <input type="text" name="reserva_cp" id="reserva_cp" class="input-mirror" data-mirror="vehiculo_cp" placeholder="<?php echo lang('vehiculo_cp'); ?>" required>
            <textarea name="reserva_sms" id="reserva_sms" class="input-mirror" data-mirror="vehiculo_sms" placeholder="<?php echo lang('contact_form_msg'); ?>"></textarea>
            <div class="a-divider--primary"></div>
            <h3><?php echo lang('form_reserva_importe_header'); ?></h3>
            <p class="grey"><?php echo lang('form_reserva_importe_subtitle'); ?></p>
            <div class="a-input-total-import">
                <div class="input-wrap">
                    <h5><?php echo lang('general_total_reserva'); ?></h5>
                    <div class="price">
                        <input type="number" name="reserva_price" id="reserva_price" min="300" required />
                        <span class="symbol">€</span>
                    </div>
                </div>
                <label class="validation_error_message" for="reserva_price"></label>
            </div>
            <div class="a-center">
                <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/tarjetas.png');?>" alt="">
            </div>
            <div class="a-divider--primary"></div> 

            <div class="a-ic-info">                
                <div class="ico icon-info"></div>
                <div class="text small secondary"><?php echo lang('form_reserva_importe_info'); ?></div>
            </div>            
            <div class="a-divider--primary"></div>    
            <label class="a-one-line-checkbox" for="reserva_politica">
                <input type="checkbox" required name="reserva_politica" id="reserva_politica">
                <span></span>
                <?php echo lang('vehiculo_accept_politica'); ?>
            </label>
            <label class="validation_error_message" for="reserva_politica"></label>

            <label class="a-one-line-checkbox" for="reserva_aviso">
                <input type="checkbox" required name="reserva_aviso" id="reserva_aviso"> 
                <span></span>
                <?php echo lang('vehiculo_accept_aviso'); ?>
            </label>
            <label class="validation_error_message" for="reserva_aviso"></label>

            <label class="a-one-line-checkbox" for="reserva_message-newsletter">
                <input type="checkbox" name="reserva_message-newsletter" id="reserva_message-newsletter" value="1">
                <span></span>
                <?php echo lang('vehiculo_accept_newsletter'); ?>
            </label>
            <label class="validation_error_message" for="reserva_message-newsletter"></label>

            <input type="hidden" name="reserva_ipaddress" id="reserva_ipaddress" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
            <input type="hidden" name="reserva_navigator" id="reserva_navigator" value="<?php echo $_SERVER['HTTP_USER_AGENT']; ?>">
            <input type="hidden" name="reserva_vehiculo_single" id="reserva_vehiculo_single" value="vehiculo_single">
            <input type="hidden" name="reserva_vehicleid" id="reserva_vehicleid" value="<?php echo $flota->vehicleid; ?>">
            <input type="hidden" name="reserva_precio" id="reserva_precio" value="<?php echo $flota->finalprice; ?>">
            <input type="hidden" name="reserva_make" id="reserva_make" value="<?php echo $flota->make; ?>">
            <input type="hidden" name="reserva_emailinteresado" id="reserva_emailinteresado" value="<?php echo $flota->emailinteresado; ?>">
            <?php
            $idreserva = round(microtime(true) * 1000);
		    $idreserva = substr($idreserva,0,12);
            $codigo_order = strval($idreserva);
            ?>
            <input type="hidden" name="reserva_id" id="reserva_id" value="<?php echo $codigo_order; ?>">
            <div data-callback="imNotARobot" class="g-recaptcha" data-sitekey="6LfQJZAUAAAAAMUkEtt9lozeOMTy13yOab-rSqYl"></div>
            <div class="buttons mgt-30">
                <button type="submit" id="btn-reserva_submit" class="a-button--contactar">
                    <?php echo lang('confirmacion_y_pago'); ?>
                </button>
            </div>
        </div>
    </div>
</form>
<?php } ?>

<span class="m-form--reserve-bg"></span>

<?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/menu_single_vehicle', $this->data); ?>

<script type="text/javascript">

    jQuery.validator.setDefaults({
        debug: true,
        success: "valid"
    });
    //formulario contacto vehiculo single
    $("#form-vsingle").validate({
        debug: false,
        rules: {
            vehiculo_name: { required: true },
            vehiculo_email: { required: true, email: true },
            vehiculo_phone: { required: true, minlength: 9, digits: true },
            vehiculo_cp: { required: true },
            vehiculo_sms: { required: true },
            politica: { required: true },
            aviso: { required: true }
        },
        ignore: [],
        messages:{
            vehiculo_name: { required: "<?php echo lang('required_name')?>", email: '<?php echo lang('valid_name')?>' },
            vehiculo_email: { required: "<?php echo lang('required_email')?>", email: '<?php echo lang('valid_email_format')?>'},
            vehiculo_phone: { required: "<?php echo lang('required_phone')?>", email: '<?php echo lang('valid_phone')?>' },
            vehiculo_cp: { required: "<?php echo lang('required_cp')?>", email: '<?php echo lang('valid_cp')?>' },
            vehiculo_sms: { required: "<?php echo lang('required_msg')?>", email: '<?php echo lang('valid_msg')?>' },
            politica: { required: "<?php echo lang('required_politica')?>", email: '<?php echo lang('valid_politica')?>' },
            aviso: { required: "<?php echo lang('required_aviso')?>", email: '<?php echo lang('valid_aviso')?>' },
        },
        errorPlacement: function(error, element) {
            var $id = element.prop('id');
            var errorSelector = '.validation_error_message[for="' + $id + '"]';
            var $element = $(errorSelector);
            if ($element.length) { 
                $(errorSelector).html(error.html());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
           enviarformvehiculo();
        }
    });
    
    //formulario reserva vehiculo single    
    $("#form-reserve").validate({
        debug: false,
        rules: {
            reserva_name: { required: true },
            reserva_email: { required: true, email: true },
            reserva_phone: { required: true, minlength: 9, digits: true },
            reserva_cp: { required: true },
            reserva_sms: { required: true },
            reserva_price: { required: true },
            reserva_politica: { required: true },
            reserva_aviso: { required: true }
        },
        ignore: [],
        messages:{
            reserva_name: { required: "<?php echo lang('required_name')?>", email: '<?php echo lang('valid_name')?>' },
            reserva_email: { required: "<?php echo lang('required_email')?>", email: '<?php echo lang('valid_email_format')?>' },
            reserva_phone: { required: "<?php echo lang('required_phone')?>", email: '<?php echo lang('valid_phone')?>', minlength: '<?php echo lang('valid_min_phone')?>' },
            reserva_cp: { required: "<?php echo lang('required_cp')?>", email: '<?php echo lang('valid_cp')?>' },
            reserva_sms: { required: "<?php echo lang('required_msg')?>", email: '<?php echo lang('valid_msg')?>' },
            reserva_price: { required: "<?php echo lang('required_price')?>", email: '<?php echo lang('valid_price')?>', number: '<?php echo lang('valid_number')?>', digits: '<?php echo lang('valid_digits')?>', min: '<?php echo lang('valid_min')?>',  },
            reserva_politica: { required: "<?php echo lang('required_politica')?>", email: '<?php echo lang('valid_politica')?>' },
            reserva_aviso: { required: "<?php echo lang('required_aviso')?>", email: '<?php echo lang('valid_aviso')?>' },
        },
        errorPlacement: function(error, element) {
            var $id = element.prop('id');
            var errorSelector = '.validation_error_message[for="' + $id + '"]';
            var $element = $(errorSelector);
            if ($element.length) { 
                $(errorSelector).html(error.html());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
           enviarreservavehiculo(); //petició ajax TPV
        }
    });
    
     //formulario bajada precio vehiculo single
    $("#btn-submit-wp").click(function() {
        
        var email = $("input[name=watch-price]").val();
        var vehicleid = $("input[name=vehicleid]").val();
        var precio = $("input[name=precio]").val();
        var watch_accept = ($("#watch-accept").prop("checked") == true ) ? 1 : 0;

        if(watch_accept==1){
            $.ajax({       
                url: base_url+'ajax/contact/send_bajadaprecio_crm', 
                type:'POST', 
                data:{ email:email, vehicleid:vehicleid, precio:precio },
            }).done(function(msg){ 
                $('.loader').hide();  
                if(msg.result=="true"){                 
                    showNotification(msg.detail, 'success');  
                    $("#form-watch-price")[0].reset(); 
                    $('#smswatchprice').html(msg.sms);
                    return true;    
                }
                else{
                    showNotification(msg.detail, 'error');                  
                    return false;           
                }     
            }).fail(function(jqXHR, textStatus, errorThrown){
                $('.loader').hide(); 
            });  
        }
    });
    
    $(document).ready(function(){
        initControls();
    });

    function initControls(){
        window.location.hash="red";
        window.location.hash="Red" //chrome
        window.onhashchange=function(){window.location.hash="red";}
    }
    
</script>