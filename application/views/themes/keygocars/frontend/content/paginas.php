
<!-- Maincontent starts -->
<main class="p-basic-page">
    <h2 class="blue"><?php echo $pagina->nombre; ?></h2>
    <section class="pbp-main">
        <div class="wrapper">
            <?php echo $pagina->contenido; ?>
        </div>
    </section>
</main>
<!-- Maincontent ends -->