<main class="p-thank-you-page">
    <section class="thank-you-content">
        <div class="figure-header">
            <picture>
                <source media="(max-width: 767px)" srcset="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/thanks_mb_'.$this->lang->lang().'.png');?>" class="w100">
                <source media="(max-width: 1240px)" srcset="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/thanks_tb_'.$this->lang->lang().'.png');?>" class="w100">
                <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/thanks_dt_'.$this->lang->lang().'.png');?>" class="w100">
            </picture>
        </div>
        <div class="thank-you-main">
            <h3 class="thank-you-header"><?php echo lang('thank_you_title'); ?></h3>
            <div class="body-message">
                <h4 class="blue"><?php echo lang('thank_you_desc1'); ?></h4>
                <h4 class="blue"><?php echo lang('thank_you_desc2'); ?></h4>
                <h4 class="blue"><?php echo lang('thank_you_desc3'); ?> <span class="primary">:)</span> <?php echo lang('thank_you_desc4'); ?></h4>
            </div>
            <a href="<?php echo base_url($this->lang->lang()); ?>" class="a-button--offset">
                <div class="btn"><?php echo lang('thank_you_home'); ?></div>
                <div class="shadow"></div>
            </a>
        </div>
    </section>
</main>