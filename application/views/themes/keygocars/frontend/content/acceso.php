<main class="p-holy-page p-auth-page">
    <div class="w-content">
        <section class="acceso">                        
                <form id="formlogin" class="m-contact-item--login" autocomplete="off" method="post" >                          
                    <div class="pdt-20 pdb-10">     
                        <label for="identity"><?php echo lang('Email');?></label>
                        <input type="email" id="email" name="email" />
                    </div>
                    <div class="pdt-10 pdb-10">         
                        <label for="password"><?php echo lang('Password');?></label>                        
                        <input type="password" id="password" name="password" />
                    </div>
                    <div class="pdt-10 pdb-20">     
                        <a href="<?php echo base_url($this->lang->lang().'/auth/forgot_password'); ?>"><?php echo lang('¿Olvidaste la contraseña?');?></a>
                    </div>
                    <div id="infoMessage"><?php if(isset($message)) echo $message;?></div>
                    <div class="pdt-20 pdb-20">                          
                        <button type="submit" id="btnsubmit"  class="a-button--offset--dark">
                            <div class="btn">
                                <?php echo lang('Access'); ?><br>
                            </div>
                            <div class="shadow"></div>
                        </button>
                    </div>                   
                </form>
                <script>
                    jQuery.validator.setDefaults({
                    debug: true,
                    success: "valid"
                    });
                    $("#formlogin").validate({
                        rules: {
                            email: { required: true},
                            password: { required: true},                            
                        },
                        messages:{                           
                            email: { required: "<?php echo lang('mand_email')?>", email: '<?php echo lang('valid_email')?>' },
                            password: { required: "<?php echo lang('mand_password')?>" },                            
                        },
                        submitHandler: function (form) {
                            login();
                        }                           
                    });
                </script>    
        </section>
    </div>
</main>
