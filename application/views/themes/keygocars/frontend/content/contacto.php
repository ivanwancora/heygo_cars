<main class="p-about-page">
    <div class="figure-header empresa">
        <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/contacto.png');?>" alt="Contacto" class="w100">
    </div>
    <h2 class="blue">
        <?php echo lang('contact_title'); ?>   
    </h2>
    <h4 class="blue">
        <?php echo lang('contact_subtitle'); ?> 
    </h4>

    <div class="pap-map gmap">
        <div id="map"></div>    
    </div>

    <section class="pap-container">
        <article class="m-contact-item pap-general">
            <div class="figure">
                <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/informaciongeneral.png');?>" alt="">
            </div>
            <h3> <?php echo lang('contact_info_general'); ?></h3>
            <h4> <?php echo lang('contact_att_cliente'); ?></h4>
            <a class="info-blue" href="tel:0034<?php echo $contact->telephone1; ?>">(+34) <span class="big"><?php echo $contact->telephone2; ?></span></a><br>
            <a class="info-blue" href="mailto:<?php echo $contact->email; ?>" target="_blank"><?php echo $contact->email; ?></a>
        </article>
        <article class="m-contact-item pap-schedule">
            <div class="figure">
                <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/horariocomercial.png');?>" alt="">
            </div>
            <h3><?php echo lang('contact_horario_title'); ?></h3>
            <h4><?php echo lang('contact_exp_ventas'); ?></h4>
            <p class="info-blue">
                <?php echo lang('contact_horario_desc'); ?>
            </p>
        </article>
        <form class="m-contact-item--full pap-form" id="form-contacto" autocomplete="off" method="post" action="#">
            <h3><?php echo lang('contact_form_title'); ?></h3>
            <p class="grey"><?php echo lang('contact_form_subtitle'); ?></p>
            <div class="grid-row">
                <div class="grid-xs-12">
                    <input type="text" name="name" id="name" required placeholder="<?php echo lang('contact_form_name'); ?>">
                </div>    
            </div>
            <div class="grid-row">
                <div class="grid-xs-12 grid-lg-6 pdr-lg-10">
                    <input type="email" name="email" id="email" required placeholder="<?php echo lang('contact_form_email'); ?>">
                </div>    
                <div class="grid-xs-12 grid-lg-6 pdl-lg-10">
                    <input type="tel" name="phone" id="phone" required placeholder="<?php echo lang('contact_form_phone'); ?>">
                </div>    
            </div>
            <div class="grid-row">
                <div class="grid-xs-12">
                    <textarea name="msg" id="msg" cols="30" rows="10" required placeholder="<?php echo lang('contact_form_msg'); ?>"></textarea>
                </div>    
            </div>
            <div class="action-grid">
                <div class="checkboxes">
                    <label class="a-one-line-checkbox">
                        <input type="checkbox" required name="faccept" id="faccept">
                        <span></span>   
                        <div class="body"><?php echo lang('contact_form_check'); ?></div>
                    </label> 
                    <label class="validation_error_message" for="faccept"></label>
                    <label class="a-one-line-checkbox">
                        <input type="checkbox" required name="faccept2" id="faccept2">
                        <span></span>   
                        <div class="body"><?php echo lang('contact_form_check_aviso'); ?></div>
                    </label> 
                    <label class="validation_error_message" for="faccept2"></label>
                    <label class="a-one-line-checkbox">
                        <input type="checkbox" name="fnews" id="fnews" value="1">
                        <span></span>   
                        <div class="body"><?php echo lang('contact_form_newsletter'); ?></div>
                    </label>
                    <input type="hidden" name="ipaddress" id="ipaddress" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
                    <input type="hidden" name="navigator" id="navigator" value="<?php echo $_SERVER['HTTP_USER_AGENT']; ?>">
                    <input type="hidden" name="news" id="contacto" value="contacto">
                    <div id="sms"></div>
                    <div class="g-recaptcha"
                        data-sitekey="6LcbDcIUAAAAAA7p7_HjhC8tPXQz-9IeMKuuhlVN"
                        data-callback="onSubmit"
                        data-size="invisible">
                    </div>
                </div>
                <div class="a-center">
                    <button type="submit" id="btn-submit" class="a-button--offset--small">
                        <div class="btn"><?php echo lang('contact_form_send'); ?></div>
                        <div class="shadow"></div>
                    </button>
                    
                </div>
            </div>
        </form>
    </section>
</main>

<?php 
if(isset($centros)){
    $gmaps = '[';
    $i = 1;
    foreach($centros as $centro){
        $gmaps = $gmaps . "['<div class=\'gmaps-popup\'><h3>".$centro->title."</h3><h4>".$centro->place."</h4><p>".$centro->address."</p><p><a href=\'mailto:".$centro->email."\'>".$centro->email."</a></p><p><a href=\'tel:".$centro->phone."\'>".$centro->phone."</a></p></div>'";
        $gmaps = $gmaps . ",".$centro->latitude.",".$centro->longitude.",".$i."],";
        $i = $i + 1;
    }
    $gmaps = $gmaps . ']';
}
?>
<div class="p-about-footer-header">
    <h2 class="blue">Let’s go!</h2>        

    <div class="figure">
        <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/cars/Citroen-C4-Grand-Picasso.png');?>" class="w100" />
    </div>
</div>

<script type="text/javascript">

    $("#faccept").click(function() {
        $("#btn-submit").prop('disabled', false); 
    }); 

    jQuery.validator.setDefaults({
        debug: true,
        success: "valid"
    });
    $("#form-contacto").validate({
        rules: {
            email: { required: true },
            name: { required: true },
            msg: { required: true },
            phone: { number: true },
            faccept: { required: true },
            faccept2: { required: true }
        },
        ignore: [],
        messages:{
            email: { required: "<?php echo lang('required_email')?>", email: '<?php echo lang('valid_email')?>' },
            name: { required: "<?php echo lang('required_name')?>", email: '<?php echo lang('valid_name')?>' },
            phone: { required: "<?php echo lang('required_phone')?>", email: '<?php echo lang('valid_phone')?>' },
            msg: { required: "<?php echo lang('required_msg')?>", email: '<?php echo lang('valid_msg')?>' },
            faccept: { required: "<?php echo lang('required_politica')?>", email: '<?php echo lang('valid_politica')?>' }, 
            faccept2: { required: "<?php echo lang('required_aviso')?>", email: '<?php echo lang('valid_aviso')?>' }
        },
        errorPlacement: function(error, element) {
            var $id = element.prop('id');
            var errorSelector = '.validation_error_message[for="' + $id + '"]';
            var $element = $(errorSelector);
            if ($element.length) { 
                $(errorSelector).html(error.html());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            enviarformcontacto();
        }
    });
    
</script>

<script type="text/javascript">
   
    var locations = <?php echo $gmaps; ?>;

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(39.4807141, -0.4592938),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>