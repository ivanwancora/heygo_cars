<main class="p-noticias-single-page a-padding-section">
    <div class="header">
        <div class="prev">
            <a href="javascript:history.back()"><?php echo lang("Volver"); ?></a>
        </div>
        <?php
          $name = 'name_'.$this->lang->lang();
          $tit = 'title_'.$this->lang->lang();
          $cont = 'content_'.$this->lang->lang();  
          $res = 'excerpt_'.$this->lang->lang(); 
          $url_slug = 'url_slug_'.$this->lang->lang();
          $name_slug = 'name_slug_'.$this->lang->lang();          
        ?>   
        <h1 class="pgp-header"><?php if(isset($new)) echo $new[0]->$tit ?></h1>
        <nav class="m-menu--share">
            <ul>
                <li class="share"><?php echo lang('Comparte'); ?></li>
                <li class="divider"><img src="<?php echo base_url('assets/img/ui/divider.svg');?>" alt="" class="w100"></li>
                <li class="icon linkedin"><a href=""></a></li>
                <li class="icon facebook"><a href=""></a></li>
                <li class="icon twitter"><a href=""></a></li>
            </ul>
        </nav>
    </div>
    <div class="pns-main">
        <img src="<?php if(isset($new)) echo base_url('uploads/news/images/'.$new[0]->image_featured); ?>" alt="<?php if(isset($new)) echo $new[0]->$tit ?>" class="w100">
        <div class="content">
            <div class="head">
                <p class="date"><?php if(isset($new)) echo $new[0]->publication_date_format; ?></p>
                <p class="categories">
                    <?php                     
                      if(isset($categories)){
                        foreach ($categories as $categoria) {
                          echo '<a href="">'.$categoria->$name.'</a>';
                        }
                      }
                    ?>                    
                </p>
            </div>
            <p>
                <?php if(isset($new)) echo $new[0]->$cont; ?>
            </p>
            <nav class="m-menu--share">
                <ul>
                    <li class="share"><?php echo lang('Comparte'); ?></li>
                    <li class="divider"><img src="<?php echo base_url('assets/img/ui/divider.svg');?>" alt="" class="w100"></li>
                    <li class="icon linkedin"><a href=""></a></li>
                    <li class="icon facebook"><a href=""></a></li>
                    <li class="icon twitter"><a href=""></a></li>
                </ul>
            </nav>
        </div>
    </div>
    <?php
        if(isset($nextnew)){
    ?>
    <div class="next-noticia">
        <div class="next"><a href="<?php echo base_url($this->lang->lang().'/'.lang('news').'/'.lang('detail').'/'.$nextnew->$url_slug); ?>"><?php echo lang('Siguiente articulo');?></a></div>
        <h3><?php if(isset($nextnew)) echo $nextnew->$tit ?></h3>
        <a href="<?php echo base_url($this->lang->lang().'/'.lang('news').'/'.lang('detail').'/'.$nextnew->$url_slug); ?>" class="button"><?php echo lang('Ver articulo'); ?></a>
    </div>
    <h2 class="nye"><?php echo lang('Noticias Eventos');?></h2>
    <?php
        }
    ?>
</main>