<main class="p-reserva-page p-reserva-page--confirmacion">
    <div class="figure-header">
        <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/stock.png');?>" alt="Stock" class="w100">
    </div>
    <div class="flota-content">
        <div class="prp-header">
            <h2 class="blue">
                <?php echo lang('reserva_no_cars_title'); ?>
            </h2>
            <h4 class="blue">
                <?php echo lang('reserva_no_cars_subtitle'); ?>
            </h4>
        </div>
        <div id="reserva-top"></div>
        <section class="reserva-confir-main">
            <h3 class="flota-header"><?php echo lang('reserva_no_header'); ?></h3>

            <article class="m-confirmacion-item">
                <div class="mci-header">
                    <div class="mci-h-content">
                        <p><?php echo lang('reserva_num_reserva'); ?> <span class="serial-number"><?php echo $reserva->reserva_id; ?></span></p>
                        <p><?php echo lang('reserva_estado_reserva'); ?> <span class="confirm"><?php if($reserva->confirmed==1){ echo lang('reserva_confirmada'); }else{ echo lang('reserva_no_confirmada'); } ?></span></p>
                    </div>
                    <div class="logo">
                        <?php
                            $arrayImatges = array();
                            $arrayImatges = explode(",",$reserva->images);
                            $this->data['preu'] = '';
                            $i = 0;
                        ?>
                        <?php 
                        if(isset($arrayImatges)){
                            foreach ($arrayImatges as $imatge) { 
                                if(file_exists("./uploads/flotas/".$imatge) && $i==0) { ?>
                                    <img src="<?php echo base_url('uploads/flotas/'.$imatge); ?>" class="w100">
                            <?php 
                                } 
                                $i++;
                            } 
                        }
                        ?>                        
                    </div>
                </div>
                <div class="mci-body">
                    <span class="a-divider--grey"></span>    
                    <h3><?php echo lang('reserva_datos'); ?></h3>
                    <p><?php echo lang('reserva_nombre_apellidos'); ?> <span class="blue"><?php echo $reserva->name; ?></span></p>
                    <p><?php echo lang('reserva_email'); ?> <span class="blue"><?php echo $reserva->email; ?></span></p>
                    <p><?php echo lang('reserva_phone'); ?> <span class="blue"><?php echo $reserva->phone; ?></span></p>
                    <p><?php echo lang('reserva_cp'); ?> <span class="blue"><?php echo $reserva->codigo_postal; ?></span></p>
                    <span class="a-divider--grey"></span>
                    <p><?php echo lang('reserva_id_vehiculo'); ?> <span class="blue"><?php echo $reserva->wwid; ?></span></p>
                    <p><?php echo lang('reserva_marca'); ?> <span class="blue"><?php echo $reserva->wwmarca; ?></span></p>
                    <p><?php echo lang('reserva_precio'); ?> <span class="blue"><?php echo number_format($reserva->finalprice, 0, ',', '.'); ?> €</span></p>
                    <span class="a-divider--grey"></span>
                    <p><?php echo lang('reserva_no_importe_reserva'); ?> <span class="price"><?php echo $reserva->reserva_price; ?> €</span></p>
                    <span class="a-divider--grey"></span>
                </div>
                <div class="a-center mgt-50">
                    <a href="<?php echo base_url($this->lang->lang()); ?>" class="a-button--offset">
                        <div class="btn"><?php echo lang('thank_you_home'); ?></div>
                        <div class="shadow"></div>
                    </a>
                </div>
                <div class="logo">
                    <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/logo/logo-header.svg');?>">
                </div>
            </article>
        </section>
</main>