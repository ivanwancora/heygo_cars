<main class="p-keygonline-page">
    <div class="figure-header">
            <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/online.svg');?>" class="w100">
    </div>
    <h2 class="blue">
        <?php echo lang('keygonline_title'); ?>
    </h2>
    <h4 class="blue">
        <?php echo lang('keygonline_subtitle'); ?>
    </h4>
    <div class="pkop-hero">
        <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/temp/keygonline-hero.jpg');?>" class="w100" alt="">
    </div>
    <section class="pkop-main">
        <div class="wrap">
            <article class="m-keygonline-item">
                <div class="koi-img">
                    <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/num1.svg');?>" class="num" />
                </div>
                <div class="koi-content">
                    <h4><?php echo lang('keygonline-item1-h'); ?></h4>
                    <p><?php echo lang('keygonline-item1-body'); ?></p>
                </div>
            </article>
            <article class="m-keygonline-item">
                <div class="koi-img">
                    <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/num2.svg');?>" class="num" />
                </div>
                <div class="koi-content">
                    <h4><?php echo lang('keygonline-item2-h'); ?></h4>
                    <p><?php echo lang('keygonline-item2-body'); ?></p>
                </div>
            </article>
            <article class="m-keygonline-item">
                <div class="koi-img">
                    <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/num3.svg');?>" class="num" />
                </div>
                <div class="koi-content">
                    <h4><?php echo lang('keygonline-item3-h'); ?></h4>
                    <p><?php echo lang('keygonline-item3-body'); ?></p>
                </div>
            </article>
        </div>
    </section>
    <section class="pkop-form">
        <div class="wrap">
            <div class="pkop-form-header">
                <h2 class="blue">
                    <?php echo lang('keygonline_form_section_title'); ?>
                </h2>
                <h4 class="blue">
                    <?php echo lang('keygonline_form_section_subtitle'); ?>
                </h4>
            </div>
            <form class="m-contact-item--full pap-form" id="form-video-cita" autocomplete="off" method="post" action="#">
                <h3><?php echo lang('keygonline_form_title'); ?></h3>
                <p class="grey"><?php echo lang('keygonline_form_subtitle'); ?></p>
                <div class="grid-row">
                    <div class="grid-xs-12">
                        <input type="text" name="vehiculos" id="vehiculos" required placeholder="<?php echo lang('keygonline_que_vehiculos_interesan'); ?>">
                    </div>
                </div>
                <div class="grid-row">
                    <div class="grid-xs-12 grid-lg-6 pdr-lg-10">
                        <input type="text" name="dia" id="dia" required placeholder="<?php echo lang('keygonline_dia_llamada'); ?>">
                    </div>
                    <div class="grid-xs-12 grid-lg-6 pdl-lg-10">
                        <input type="tel" name="hora" id="hora" required placeholder="<?php echo lang('keygonline_hora_llamada'); ?>">
                    </div>
                </div>
                <div class="grid-row">
                    <div class="grid-xs-12">
                        <input type="text" name="name" id="name" required placeholder="<?php echo lang('contact_form_name'); ?>">
                    </div>
                </div>
                <div class="grid-row">
                    <div class="grid-xs-12 grid-lg-6 pdr-lg-10">
                        <input type="email" name="email" id="email" required placeholder="<?php echo lang('contact_form_email'); ?>">
                    </div>    
                    <div class="grid-xs-12 grid-lg-6 pdl-lg-10">
                        <input type="tel" name="phone" id="phone" required placeholder="<?php echo lang('contact_form_phone'); ?>">
                    </div>    
                </div>
                <div class="grid-row">
                    <div class="grid-xs-12">
                        <textarea name="msg" id="msg" cols="30" rows="10" required placeholder="<?php echo lang('contact_form_msg'); ?>"></textarea>
                    </div>    
                </div>
                <div class="action-grid">
                    <div class="checkboxes">
                        <label class="a-one-line-checkbox">
                            <input type="checkbox" required name="faccept" id="faccept">
                            <span></span>   
                            <div class="body"><?php echo lang('contact_form_check'); ?></div>
                        </label> 
                        <label class="validation_error_message" for="faccept"></label>
                        <label class="a-one-line-checkbox">
                            <input type="checkbox" required name="faccept2" id="faccept2">
                            <span></span>   
                            <div class="body"><?php echo lang('contact_form_check_aviso'); ?></div>
                        </label> 
                        <label class="validation_error_message" for="faccept2"></label>
                        <label class="a-one-line-checkbox">
                            <input type="checkbox" name="fnews" id="fnews" value="1">
                            <span></span>   
                            <div class="body"><?php echo lang('contact_form_newsletter'); ?></div>
                        </label>
                        <input type="hidden" name="ipaddress" id="ipaddress" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
                        <input type="hidden" name="navigator" id="navigator" value="<?php echo $_SERVER['HTTP_USER_AGENT']; ?>">
                        <input type="hidden" name="news" id="contacto" value="contacto">
                        <div id="sms"></div>
                        <div class="g-recaptcha"
                            data-sitekey="6LcbDcIUAAAAAA7p7_HjhC8tPXQz-9IeMKuuhlVN"
                            data-callback="onSubmit"
                            data-size="invisible">
                        </div>
                    </div>
                    <div class="a-center">
                        <button type="submit" id="btn-submit-keygonline" class="a-button--video-cita-primary">
                            <div class="btn">
                                <?php echo lang('general_solicitar_video_cita'); ?>
                                <span class="icon-arrow"></span>
                            </div>
                            <div class="shadow"></div>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</main>

<script type="text/javascript">

    $("#faccept").click(function() {
        $("#btn-submit").prop('disabled', false); 
    }); 

    jQuery.validator.setDefaults({
        debug: true,
        success: "valid"
    });
    $("#form-video-cita").validate({
        rules: {
            email: { required: true },
            name: { required: true },
            msg: { required: true },
            phone: { number: true },
            vehiculos: { required: true },
            dia: { required: true },
            hora: { required: true },
            faccept: { required: true },
            faccept2: { required: true }
        },
        ignore: [],
        messages:{
            email: { required: "<?php echo lang('required_email')?>", email: '<?php echo lang('valid_email')?>' },
            name: { required: "<?php echo lang('required_name')?>", email: '<?php echo lang('valid_name')?>' },
            phone: { required: "<?php echo lang('required_phone')?>", email: '<?php echo lang('valid_phone')?>' },
            vehiculos: { required: "<?php echo lang('required_vehiculos')?>", email: '<?php echo lang('valid_vehiculos')?>' },
            dia: { required: "<?php echo lang('required_dia')?>", email: '<?php echo lang('valid_dia')?>' },
            hora: { required: "<?php echo lang('required_hora')?>", email: '<?php echo lang('valid_hora')?>' },
            msg: { required: "<?php echo lang('required_msg')?>", email: '<?php echo lang('valid_msg')?>' },
            faccept: { required: "<?php echo lang('required_politica')?>", email: '<?php echo lang('valid_politica')?>' }, 
            faccept2: { required: "<?php echo lang('required_aviso')?>", email: '<?php echo lang('valid_aviso')?>' }
        },
        errorPlacement: function(error, element) {
            var $id = element.prop('id');
            var errorSelector = '.validation_error_message[for="' + $id + '"]';
            var $element = $(errorSelector);
            if ($element.length) { 
                $(errorSelector).html(error.html());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            enviarformkeygonline();
            
        }
    });
    
</script>