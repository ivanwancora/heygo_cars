<main class="p-centros-page">
    <div class="figure-header centros">
        <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/centros.png');?>" alt="Stock" class="w100">
    </div>
    <h2 class="blue">
        <?php echo lang('centros_title'); ?>
    </h2>
    <h4 class="blue">
        <?php echo lang('centros_subtitle'); ?>
    </h4>
    <div class="pcp-hero">
        <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/oficines/centros.jpg');?>" alt="" class="w100">
    </div>
    <div class="pcp-catch">
        <h4><?php echo lang('centros_slidetitle'); ?></h4>
        <div class="wrap">
            <h4 class="primary"><?php echo lang('centros_item1'); ?></h4>
            <h4 class="primary"><?php echo lang('centros_item2'); ?></h4>
            <h4 class="primary"><?php echo lang('centros_item3'); ?></h4>
        </div>
    </div>
    <section class="o-centros-container">
            <?php if(isset($centros)){ ?>
                <?php 
                $i = 0; 
                $x = 0; 
                foreach ($centros as $centro) { $i = $i + 1; ?>

                    <article class="m-centros-item--right <?php echo strtolower($centro->title); ?> <?php if($i == 1){ echo 'active'; } ?>">
                        <div class="mci-slider">
                            <div class="o-swiper--centros">
                                <div class="swiper-wrapper">
                                    
                                    <?php foreach ($centros_imagenes as $centro_imagen) {
                                        if($centro_imagen->idCentro==$centro->id){ $x = $x + 1; ?>
                                            <div class="swiper-slide">
                                                <img class="a-object-cover" src="<?php echo base_url('uploads/centros/'.$centro_imagen->image.''); ?>" alt="<?php echo $centro->title; ?>">
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <?php if($x>1){ ?>
                                    <div class="swiper--centros-pagination"></div>
                                <?php } 
                                $x = 0;
                                ?>
                            </div>
                        </div>
                        <div class="mci-content">
                            <h4 class="mci-town primary"><?php echo $centro->place; ?></h4>
                            <h3 class="mci-province"><?php echo $centro->title; ?></h3>
                            <div class="mci-info adress">
                                <span class="icon-point-line"></span>
                                <p class="body">
                                    <?php echo $centro->address; ?>
                                    <a target="_blank" href="<?php echo 'http://maps.google.com/maps?q='.$centro->latitude.','.$centro->longitude; ?>" class="it-primary">> <?php echo lang('general_view_map'); ?></a>
                                </p>
                            </div>
                            <div class="mci-info schedule">
                                <span class="icon-clock"></span>
                                <div class="body">
                                    <u><?php echo lang('centros_horario'); ?></u>
                                    <?php echo $centro->schedule; ?>
                                </div>
                            </div>
                            <div class="mci-info phone">
                                <span class="icon-phone"></span>
                                <p class="body">
                                    <u><?php echo lang('centro_att'); ?></u><br>
                                    <a href="tel:<?php echo lang('header_telf_num'); ?>"><?php echo lang('header_telf'); ?></a>
                                </p>
                            </div>
                            <div class="mci-info email">
                                <span class="icon-email"></span>
                                <p class="body">
                                    <a href="mailto:<?php echo lang('header_mail'); ?>"><?php echo lang('header_mail'); ?></a>
                                </p>
                            </div>
                            <div class="a-center">
                                <a class="a-button--offset--dark" href="<?php echo base_url($this->lang->lang().'/vehiculos/'); ?>">
                                    <div class="btn">
                                        <?php echo lang('general_ver_stock'); ?>
                                        <span class="icon-arrow"></span>
                                    </div>
                                    <div class="shadow"></div>
                                </a>
                            </div>
                        </div>
                    </article>
                    <?php if($i != count($centros)){?><div class="a-divider--icon icon-point"></div><?php } 
                }
            }?>
                
    </section>
        <?php  $this->load->view($this->config->item('theme_path_frontend'). 'partials/keygonline_general'); ?>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/section_why_heygo', $this->data); ?>
</main>