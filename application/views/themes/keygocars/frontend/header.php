<?php
    $title = 'title_'.$this->lang->lang();
    $description = 'description_'.$this->lang->lang();  
    $keys = 'keys_'.$this->lang->lang(); 
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="title" content="<?php if(isset($seo)) { echo $seo->$title; }?>">
	<meta name="description" content="<?php if(isset($seo)) { echo $seo->$description; }?>">
	<meta name="keywords" content="<?php if(isset($seo)) { echo $seo->$keys; }?>" />
	<meta property="og:title" content="<?php if(isset($seo)) { echo $seo->$title; }?>"/>
	<meta property="og:description" content="<?php if(isset($seo)) { echo $seo->$description; }?>" /> 
	<meta property="og:url" content="<?php echo base_url(); ?>"/>
	<meta property="og:site_name" content="<?php if(isset($seo)) { echo $seo->sitename; }?>" /> 
	<meta property="og:type" content="website"/>  	    
	<meta property="og:image" content="" />  
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/favicon/apple-touch-icon-120x120.png'); ?>" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/favicon/apple-touch-icon-144x144.png'); ?>" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/favicon/apple-touch-icon-152x152.png'); ?>" />
	<link rel="icon" type="image/png" href="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/favicon/favicon-196x196.png'); ?>" sizes="196x196" />
	<link rel="icon" type="image/png" href="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/favicon/favicon-96x96.png'); ?>" sizes="96x96" />
	<link rel="icon" type="image/png" href="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/favicon/favicon-32x32.png'); ?>" sizes="32x32" />
	<link rel="icon" type="image/png" href="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/favicon/favicon-16x16.png'); ?>" sizes="16x16" />
	<link rel="icon" type="image/png" href="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/favicon/favicon-128.png'); ?>" sizes="128x128" />	    
	<link rel="shortcut icon" href="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/logo.png'); ?>">
	<meta name="application-name" content="<?php echo $this->config->item('project_name'); ?>"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/favicon/mstile-144x144.png'); ?>" />
	<meta name="msapplication-square70x70logo" content="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/favicon/mstile-70x70.png'); ?>" />
	<meta name="msapplication-square150x150logo" content="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/favicon/mstile-150x150.png'); ?>" />
	<meta name="msapplication-wide310x150logo" content="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/favicon/mstile-310x150.png'); ?>" />
	<meta name="msapplication-square310x310logo" content="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/favicon/mstile-310x310.png'); ?>" />		
	
	<meta name="google-site-verification" content="K8Emg78aNYncJXe0szOBBXnH1X5CNiooTpkfOPFA9I8" />
	
	<link rel="stylesheet" href="<?php echo base_url('assets/'.$this->config->item('versionat').'/css/app.css');?>">

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NXCSL8V');</script>
	<!-- End Google Tag Manager -->

	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '279391613194177');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=279391613194177&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->


	<script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/jquery-3.3.1.min.js');?>"></script>
	<script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/jquery.validate.min.js');?>"></script>
	<script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/additional-methods.min.js');?>"></script>
	<script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/modernizr.custom.js');?>"></script>
	<script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/classie.js');?>"></script>
	<script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/notificationFx.js');?>"></script>

	<?php if($this->router->fetch_class() == 'contacto') { ?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAgl2lYollmN-ZjFolSmrd8uQ_lTIILS0M&callback=initMap"></script>
	<?php } ?>
	
	<title><?php echo $this->config->item('project_name'); ?></title>
	<script type="text/javascript">
		var base_url = '<?php echo base_url($this->lang->lang().'/'); ?>';
		var base_url_img = '<?php echo base_url(); ?>';
		var versionat  = '<?php echo $this->config->item('versionat'); ?>'
	</script>
</head>

<body class="<?php if(isset($body)){ echo $body; } ?>">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NXCSL8V"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

    <div id="container"><!-- container -->
        <header>
            <div class="wrapper">
                <a href="<?php echo base_url($this->lang->lang().'/'); ?>" class="logo">
                    <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/logo/logo-header.svg');?>" alt="<?php echo $this->config->item('project_name'); ?>">
                </a>
                <div class="header-content">
                    <div class="contact">
                        <a href="tel:<?php echo lang('header_telf_num'); ?>" class="a-phone-link--white">
                            <span><?php echo lang('header_telf'); ?></span>
                        </a>
                        <a href="mailto:<?php echo lang('header_mail'); ?>" class="a-email-link--white">
                            <span><?php echo lang('header_mail'); ?></span>
                        </a>
                    </div>
					<div class="m-menu--switcher--header">
						<button class="a-button--search-form-growing">
							<?php echo lang('header_encuentra'); ?>
						</button>
					</div>
					
					<a class="m-menu--burger primary">
						<span></span>
						<span></span>
						<span></span>
					</a>
					<?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/nav', $this->data); ?>
                    
                </div>
            </div>
		</header>
		<section id="o-header--sticky">
			<a href="<?php echo base_url($this->lang->lang().'/'); ?>"  class="logo">
				<img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/logo/hgc_menu_sticky.svg');?>" alt="<?php echo $this->config->item('project_name'); ?>">
			</a>
			<a class="m-menu--burger primary">
				<span></span>
				<span></span>
				<span></span>
			</a>
		</section>
		<?php 
			$this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/section_filter_mobile', $this->data); 
			$this->load->view($this->config->item('theme_path_frontend'). 'partials/menu_mobile', $this->data); 
		?>