    <section class="p-centros-section">
    
        <h2 class="pcs-header blue"><?php echo lang('centros_title'); ?></h2>
        <h4 class="pcs-subheader blue">
            <?php echo lang('centros_subheader'); ?>
        </h4>
        <div class="pcs-nav">
            <div class="w-content">
                <nav class="m-menu--centros">
                    <li>
                        <div class="mm-centro-dropdown has-children">
                            <span class="center"><?php echo $centros[0]->place; ?></span>
                            <?php if(count($centros)> 1) { ?>
                                <span class="arrow icon-down">
                                    <ul class="m-menu--dropdown">
                                        <?php   $j = 0;
                                                foreach ($centros as $centro) { $j = $j + 1 ?>
                                            <li>
                                                <button type="button" class="mm-centro-item-drop <?php echo 'sede'.$j; ?> <?php if($j == 1){ echo 'active';} ?>" data-center="<?php echo $centro->place;  ?>" data-class="<?php echo 'sede'.$j; ?>"><?php echo $centro->place; ?></button>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </span>
                            <?php } ?>
                        </div>
                    </li>
                    <?php $i = 0; 
                    foreach ($centros as $centro) { $i = $i + 1; ?>
                        <li class="mm-centro-tab">
                            <button type="button" class="mm-centro-item <?php echo 'sede'.$i; ?> <?php if($i == 1){ echo 'active'; } ?>" data-center="<?php echo 'sede'.$i; ?>" data-class="<?php echo 'sede'.$i; ?>">
                            <?php echo $centro->place; ?>
                            </button>
                        </li>
                    <?php } ?>
                </nav>
            </div>
        </div>
        <div class="pcs-content">
            <div class="pcs-container w-content">
                <?php 
                $i = 0; 
                $x = 0; 
                foreach ($centros as $centro) { $i = $i + 1; ?>

                    <article class="m-centros-item--left <?php  echo 'sede'.$i; ?> <?php if($i == 1){ echo 'active'; } ?>">
                        <div class="mci-slider">
                            <div class="o-swiper--centros">
                                <div class="swiper-wrapper">
                                    <?php foreach ($centros_imagenes as $centro_imagen) {   
                                        if($centro_imagen->idCentro==$centro->id){ $x = $x + 1; ?>
                                            <div class="swiper-slide">
                                                <img class="a-object-cover" src="<?php echo base_url('uploads/centros/'.$centro_imagen->image.''); ?>" alt="<?php echo $centro->title; ?>">
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <?php if($x>1){ ?>
                                    <div class="swiper--centros-pagination"></div>
                                <?php } 
                                $x = 0;
                                ?>
                            </div>                    
                        </div>
                        <div class="mci-content">
                            <h4 class="mci-town primary"><?php echo $centro->place; ?></h4>
                            <h3 class="mci-province"><?php echo $centro->title; ?></h3>
                            <div class="mci-info adress">
                                <span class="icon-point-line"></span>
                                <p class="body">
                                    <?php echo $centro->address; ?>
                                    <a target="_blank" href="<?php echo 'http://maps.google.com/maps?q='.$centro->latitude.','.$centro->longitude; ?>" class="it-primary">> <?php echo lang('general_view_map'); ?></a>
                                </p>
                            </div>
                            <div class="mci-info schedule">
                                <span class="icon-clock"></span>
                                <div class="body">
                                <u><?php echo lang('centros_horario'); ?></u>
                                <?php echo $centro->schedule; ?>
                                        </div>
                            </div>
                            <div class="mci-info phone">
                                <span class="icon-phone"></span>
                                <p class="body">
                                    <u><?php echo lang('centro_att'); ?></u><br>
                                    <a href="tel:<?php echo lang('header_telf_num'); ?>"><?php echo lang('header_telf'); ?></a>
                                </p>
                            </div>
                            <div class="mci-info email">
                                <span class="icon-email"></span>
                                <p class="body">
                                    <a href="mailto:<?php echo lang('header_mail'); ?>"><?php echo lang('header_mail'); ?></a>
                                </p>
                            </div>
                            <div class="a-center">
                                <a class="a-button--offset--dark" href="<?php echo base_url($this->lang->lang().'/vehiculos/'); ?>">
                                    <div class="btn">
                                        <?php echo lang('general_ver_stock'); ?>
                                        <span class="icon-arrow"></span>
                                    </div>
                                    <div class="shadow"></div>
                                </a>
                            </div>
                        </div>
                    </article>
                
                <?php } ?>
            </div>
        </div>
    </section>