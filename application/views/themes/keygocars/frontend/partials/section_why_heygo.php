<section class="p-why-heygo-section">
        <div class="wrap">
            <h2 class="blue"><?php echo lang('centros_why'); ?></h2>
            <div class="o-why-container">
                <div class="m-why-item">
                    <div class="img volante"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/volante.png');?>" alt=""></div>
                    <h4 class="blue"><?php echo lang('centros_answer1'); ?></h4>
                </div>
                <div class="m-why-item">
                    <div class="img anyos"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/2anyosgarantia.png');?>" alt=""></div>
                    <h4 class="blue"><?php echo lang('centros_answer2'); ?></h4>
                </div>
                <div class="m-why-item">
                    <div class="img compra"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/valoracioncompra.png');?>" alt=""></div>
                    <h4 class="blue"><?php echo lang('centros_answer3'); ?></h4>
                </div>
                <div class="m-why-item">
                    <div class="img financiacion"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/financiacionflexible.png');?>" alt=""></div>
                    <h4 class="blue"><?php echo lang('centros_answer4'); ?></h4>
                </div>
                <div class="m-why-item">
                    <div class="img peritacion">
                        <div class="new-icon-wrapper">
                            <img class="new-icon" src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/new-icon.svg');?>" alt="<?php echo lang('new'); ?>" />    
                        </div>
                        <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/certificadoperitacion.png');?>" alt="" />
                    </div>
                    <h4 class="blue"><?php echo lang('centros_answer5'); ?></h4>
                </div>
                <div class="m-why-item">
                    <div class="img online">
                        <div class="new-icon-wrapper">
                            <img class="new-icon" src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/new-icon.svg');?>" alt="<?php echo lang('new'); ?>" />    
                        </div>
                        <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/online.png');?>" alt="" />
                    </div>
                    <h4 class="blue"><?php echo lang('centros_answer6'); ?></h4>
                </div>
            </div>
            <a href="<?php echo base_url($this->lang->lang().'/vehiculos/'); ?>" class="a-button--offset--stock">
                <div class="btn"><?php echo lang('centros_ver_stock'); ?><span class="icon-arrow"></span></div>
                <div class="shadow"></div>
            </a>
            <?php /*if($this->router->fetch_class() == 'centros'){?>
                <a href="<?php echo base_url($this->lang->lang().'/vehiculos/'); ?>" class="a-button--offset--stock">
                    <div class="btn"><?php echo lang('centros_ver_stock'); ?><span class="icon-arrow"></span></div>
                    <div class="shadow"></div>
                </a>
            <?php }else if($this->router->fetch_class() == 'vehiculos'){ ?>
                <form class="m-form--flota m-form--flota--home" id="formsearchitemshome" autocomplete="off" method="post" action="<?php echo base_url($this->lang->lang().'/vehiculos/'); ?>">
                    <div class="m-form--quick-search">
                        <input type="hidden" name="otrovehiculo" value="otrovehiculo">
                        <input type="text" name="search" placeholder="<?php echo lang('search_otro_vehiculo'); ?>" value="<?php if(isset($search) and $search != false) echo $search;?>"/>
                        <button type="submit" class="icon-search"></button>
                    </div>
                </form>
            <?php } */?>
        </div>      
</section>