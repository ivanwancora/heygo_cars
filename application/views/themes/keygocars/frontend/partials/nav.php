    <nav class="m-menu--primary">
        <ul>
            <li><a class="<?php if($this->router->fetch_class() == 'principal') { echo "active"; } ?>" href="<?php echo base_url('/'); ?>"><?php echo lang('menu_inicio'); ?></a></li>
            <li><a class="<?php if($this->router->fetch_class() == 'vehiculos') { echo "active"; } ?> btn-menu-vehiculos"><?php echo lang('menu_vehiculos'); ?></a></li>
            <li><a class="<?php if($this->router->fetch_class() == 'centros') { echo "active"; } ?>" href="<?php echo base_url($this->lang->lang().'/centros/'); ?>"><?php echo lang('menu_centros'); ?></a></li>
            <li><a class="<?php if($this->router->fetch_class() == 'empresa') { echo "active"; } ?>" href="<?php echo base_url($this->lang->lang().'/empresa/'); ?>"><?php echo lang('menu_empresa'); ?></a></li>
            <li><a class="bgw <?php if($this->router->fetch_class() == 'keygonline' || $this->router->fetch_method() == 'thankyoukeygonline' || $this->router->fetch_method() == 'redsys_ok') { echo "active"; } ?>"  href="<?php echo base_url($this->lang->lang().'/keygonline/'); ?>"><?php echo lang('menu_keygonline'); ?></a></li>
            <!--<li><a class="inactive"><?php echo lang('menu_noticias'); ?></a></li>-->
            <!--<li><a class="inactive"><?php echo lang('menu_alquiler'); ?></a></li>-->
            <li><a class="<?php if($this->router->fetch_class() == 'contacto' && ($this->router->fetch_method() != 'thankyoukeygonline' && $this->router->fetch_method() != 'redsys_ok')) { echo "active"; } ?>" href="<?php echo base_url($this->lang->lang().'/contacto/'); ?>"><?php echo lang('menu_contatco'); ?></a></li>
        </ul>
    </nav>
  