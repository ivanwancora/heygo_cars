<section class="p-home--hero">
    <div class="wrapper">
        <div class="o-swiper--hero">
            <div class="swiper-wrapper">
                <?php if(isset($sliders)){
                    foreach ($sliders as $slider) { 
                            $alt = strip_tags ($slider->content);
                        ?>
                        <div role="img" aria-label="<?php echo $alt; ?>" class="swiper-slide" data-mobile="<?php echo base_url('uploads/sliders/'.$slider->image_mobile); ?>" data-tablet="<?php echo base_url('uploads/sliders/'.$slider->image_tablet); ?>" data-desktop="<?php echo base_url('uploads/sliders/'.$slider->image); ?>">
                            <?php if($slider->button=='si'){ ?> 
                                <a href="<?php echo $slider->link_button; ?>" class="swiper-btn a-button--offset--growing--stock--primary">
                                    <div class="btn">
                                        <span class="text"><?php echo $slider->text_button; ?></span>
                                        <span class="icon-arrow"></span>
                                    </div>
                                    <div class="shadow"></div>

                                </a>
                            <?php } ?>
                        </div>            
                    <?php }
                } ?>           
            </div>
            <div class="swiper-home-hero-button-prev swiper-opacity swiper-button-prev">
                <div class="btn"></div>
                <div class="shadow"></div>          
            </div>
            <div class="swiper-home-hero-button-next swiper-opacity swiper-button-next">
                <div class="btn"></div>
                <div class="shadow"></div>          
            </div>
            <div class="swiper-home-hero-pagination swiper-pagination"></div>
        </div>
    </div>
</section>