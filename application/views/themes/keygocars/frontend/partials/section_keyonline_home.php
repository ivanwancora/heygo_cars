<section class="p-home--keygonline bg-pale-blue">
    <div class="wrapper">
        <div class="phk-header">
            <h2 class="secondary">
                <?php echo lang('keygonline_header'); ?>
            </h2>
            <h4><?php echo lang('section_keygonline_subheader'); ?></h4>
        </div>
        <?php  $this->load->view($this->config->item('theme_path_frontend'). 'partials/keygonline_home_swiper'); ?>
        <div class="a-center mgt-30">
            <a href="<?php echo base_url($this->lang->lang().'/keygonline/'); ?>" class="a-button--primary">
                <div class="btn">
                    <span class="text"><?php echo lang('general_solicitar_video_cita'); ?></span>
                    <span class="icon-arrow"></span>
                </div>
                <div class="shadow"></div>
            </a>
        </div>
        <div class="a-center mgt-40">
            <p class="kgo-tag">
                <?php echo lang('section_keygonline_tag'); ?>
            </p>
        </div>
    </div>
</section>