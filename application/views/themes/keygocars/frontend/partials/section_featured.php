    <section class="p-home--featured bg-blue">
        <div class="wrapper">
            <div class="o-swiper--featured">
                <div class="featured-bg">
                    <div class="wrapper feat-content">
                        <div class="position">
                            <div class="swiper-featured-button-prev a-button--blue--prev">
                                <div class="btn"></div>
                                <div class="shadow"></div>          
                            </div>
                            <div class="swiper-featured-button-next a-button--blue--next">
                                <div class="btn"></div>
                                <div class="shadow"></div>          
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-wrapper">
                <?php foreach ($destacados as $destacado) { ?>
                    
                    <div class="swiper-slide">
                        <div class="wrapper">
                            <div class="image">
                                <img class="a-object-cover" src="<?php echo base_url('uploads/destacados/'.$destacado->image.'');?>" alt="<?php echo $destacado->title; ?>">  
                            </div>
                            <div class="content">
                                <h2><?php echo $destacado->title; ?></h2>
                                <div class="body"><?php echo $destacado->content; ?></div>
                            </div>
                        </div>  
                    </div>

                <?php } ?>
                </div>
            </div>
        </div>
    </section>