    <section class="p-marcas-section p-marcas-section--home">
        <h2 class="pms-header blue"><?php echo lang('marcas_title'); ?></h2>
        <h4 class="pms-subheader blue">
            <?php echo lang('marcas_subheader'); ?>
        </h4>
        <div class="pms-container">
            <div class="w-content">
                <input type="hidden" name="search" id="homemarcas" value="homemarcas">
                <div class="o-swiper--marcas">
                        <div id="home_marcas" class="swiper-wrapper"> 
                            <?php if(isset($marcas)) { ?>
                                <?php foreach ($marcas as $marca) { ?>
                                    <span class="m-marcas-item swiper-slide"  onclick="return enviarmarca();" data-type="<?php echo $marca->title; ?>" >
                                        <img class="o-object-contain" src="<?php echo base_url('/uploads/marcas/'.$marca->image.''); ?>" alt="<?php echo $marca->title; ?>">
                                    </span>
                                <?php }  ?> 
                            <?php }  ?> 
                        </div>
                </div>
            </div>
            <div class="swiper-marcas-button-prev swiper-opacity swiper-button-prev"></div>
            <div class="swiper-marcas-button-next swiper-opacity swiper-button-next"></div>
        </div>
    </section>
