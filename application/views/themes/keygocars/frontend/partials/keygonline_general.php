<section class="o-keygonline-module">
    <div class="w-content">
        <div class="kgo-header">
            <h2 class="blue">
                <?php echo lang('keygonline_header'); ?>
            </h2>
            <h4><?php echo lang('section_keygonline_subheader'); ?></h4>
        </div>
        <div class="o-swiper--keygonline">
            <div class="swiper-wrapper">
                <article class="m-nos-veh-item--compressed swiper-slide">
                    <div class="image general_svg"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/informaciongeneral_gris_secondary.svg');?>" alt="<?php echo lang('section_keygonline_item1_h'); ?>" /></div>
                    <div class="content general">
                        <h4><?php echo lang('section_keygonline_item1_h'); ?></h4>
                    </div>
                </article>
                <article class="m-nos-veh-item--compressed swiper-slide">
                    <div class="image online"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/online.png');?>" alt="<?php echo lang('section_keygonline_item2_h'); ?>" /></div>
                    <div class="content online">
                        <h4><?php echo lang('section_keygonline_item2_h'); ?></h4>
                    </div>
                </article>
                <article class="m-nos-veh-item--compressed swiper-slide">
                    <div class="image casa"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/entrega_casa_secondary.svg');?>" alt="<?php echo lang('section_keygonline_item3_h'); ?>" /></div>
                    <div class="content casa">
                        <h4><?php echo lang('section_keygonline_item3_h'); ?></h4>
                    </div>
                </article>
            </div>
            <div class="keygonline-pagination"></div>
        </div>
        <div class="a-center mgt-30">
            <a href="<?php echo base_url($this->lang->lang().'/keygonline/'); ?>" class="a-button--video-cita">
                <div class="btn">
                    <span class="text"><?php echo lang('general_solicitar_video_cita'); ?></span>
                    <span class="icon-arrow"></span>
                </div>
                <div class="shadow"></div>
            </a>
        </div>
        <div class="a-center mgt-40">
            <p class="kgo-tag">
                <?php echo lang('section_keygonline_tag'); ?>
            </p>
        </div>
    </div>
</section>