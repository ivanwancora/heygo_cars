<nav class="m-menu--single-vehicle">
    <div class="wrap">
        <div class="mmsv-price">
            <?php echo $preu; ?>€
        </div>
        <button type="button" data-id="#form-vsingle .form" class="a-button--contactar btn-to-id">
            <span class="icon-email"></span>
            <?php echo lang('vehiculo_contactar'); ?>
        </button>
    </div>
</nav>