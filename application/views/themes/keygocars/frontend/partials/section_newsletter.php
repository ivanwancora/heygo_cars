<section class="p-newsletter-section">
    <div class="wrap">
        <form class="form" id="form-newsletter" autocomplete="off" method="post" action="#">
            <h3><?php echo lang('newsletter_title'); ?> </h3>
            <h4 class="blue"><?php echo lang('newsletter_subtitle'); ?></h4>
            <div class="pns-container">
                <div class="pns-inputs">
                    <input type="email" name="email" id="email" required class="input-newsletter" placeholder="email" />
                    <div class="checks">
                        <label class="a-one-line-checkbox">
                            <input type="checkbox" name="newsletter_aviso" id="newsletter_aviso">
                            <span></span>   
                            <div class="body"><?php echo lang('newsletter_check'); ?></div>
                        </label> 
                        <label class="validation_error_message" for="newsletter_aviso"></label>
                        <label class="a-one-line-checkbox">
                            <input type="checkbox" name="newsletter_politica" id="newsletter_politica">
                            <span></span>   
                            <div class="body"><?php echo lang('newsletter_check_privacidad'); ?></div>
                            <div id="sms"></div>
                        </label>    
                        <label class="validation_error_message" for="newsletter_politica"></label>
                        <input type="hidden" name="ipaddress" id="ipaddress" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
                        <input type="hidden" name="navigator" id="navigator" value="<?php echo $_SERVER['HTTP_USER_AGENT']; ?>">
                        <input type="hidden" name="news" id="news" value="news">
                        <div class="g-recaptcha"
                            data-sitekey="6LcbDcIUAAAAAA7p7_HjhC8tPXQz-9IeMKuuhlVN"
                            data-callback="onSubmit"
                            data-size="invisible">
                        </div>
                    </div>
                </div>
                <div class="pns-btn">
                    <button type="submit" id="btn-submit" class="a-button--round--primary">
                        <div class="btn">
                        <?php echo lang('newsletter_send'); ?><br>
                            <span class="icon-arrow"></span>
                        </div>
                        <div class="shadow"></div>
                    </button>                
                </div>
            </div>
        </form>
        <div class="pns-img">
            <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/temp/newsletter-KCARS.jpg');?>" alt="" class="a-object-cover">        
        </div>
    </div>
</section>

<script type="text/javascript">
    // $("#newsletter_check_1, #newsletter_check_2").on('change', function() {
    //     disableSubmit()
    // });    

    // function disableSubmit(){
    //     var aux = 0,
    //         inp1 = document.querySelector('#newsletter_check_1'),
    //         inp2 = document.querySelector('#newsletter_check_2')            ;
        
    //     if(inp1.checked && inp2.checked){
    //         $("#btn-submit").prop('disabled', false);
    //     }else{
    //         $("#btn-submit").prop('disabled', true);   
    //     }   
    // }


    jQuery.validator.setDefaults({
        debug: true,
        success: "valid"
    });
    var $fr = $("#form-newsletter").validate({
        rules: {
            email: { required: true },
            newsletter_aviso: { required: true },
            newsletter_politica: { required: true }
        },
        ignore: [],
        messages:{
            email: { required: "<?php echo lang('required_email')?>", email: '<?php echo lang('valid_email')?>' },
            newsletter_politica: { required: "<?php echo lang('required_politica')?>", email: '<?php echo lang('valid_politica')?>' },
            newsletter_aviso: { required: "<?php echo lang('required_aviso')?>", email: '<?php echo lang('valid_aviso')?>' },
        },
        errorPlacement: function(error, element) {
            var id = element.prop('id');
            var errorSelector = '.validation_error_message[for="' + id + '"]';
            var $element = $(errorSelector);
            if ($element.length) { 
                $(errorSelector).html(error.html());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            enviarformnewsletter($fr);
        }
    });
 
</script>