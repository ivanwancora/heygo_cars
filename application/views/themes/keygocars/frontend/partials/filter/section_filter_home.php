<form class="m-form--flota m-form--flota--home" id="formsearchitemshome" autocomplete="off" method="post" onsubmit="return searchAndGo(event, 'home');">
    <input type="hidden" name="home" value="home">
    <h4 class="form-header"><?php echo lang('filtro_title'); ?></h4>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part3_flota.php', $this->data); ?>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part2_select_flota.php', $this->data); ?>
    <div class="a-divider--primary"></div>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part4_form.php', $this->data); ?>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part5_buttons.php', $this->data); ?>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part6_clear_btn.php', $this->data); ?>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part7_submit_home.php', $this->data); ?>
</form>



