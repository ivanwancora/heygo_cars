<div class="o-flota-pick-container">
    <?php if(isset($tipo_flotas)) {  ?>
        <?php foreach ($tipo_flotas as $tf) {  ?>
            <span class="m-flota-pick-item <?php echo $tf->icono; ?>" data-flota="<?php echo $tf->icono; ?>">
                <input type="checkbox" name="flota-pick" value="<?php echo $tf->title; ?>" />    
                <div class="mfpi-img icon-<?php echo $tf->icono; ?>"></div>
                <h3 class="mfpi-name"><?php echo ucfirst($tf->title); ?></h3>
            </span>
        <?php } ?>
    <?php } ?>
</div>