    function buscaritems(){   
        var tipus_flotalist = [];
        var tipus_flotalisticon = [];
        var tipus_flotalistmobil = [];
        var tipus_flotalistmobilicon = [];
        var sales = [];
        var page = 0;
        if(window.matchMedia('(max-width:767px)').matches){
            var limit = 6;
        }else if(window.matchMedia('(min-width:768px) and (max-width: 1239px)').matches){
            var limit = 12;
        }else if(window.matchMedia('(min-width:1240px)').matches){
            var limit = 24;    
        }

        var distToBottom, data, dataObj;
        var pollingForData = false;
        var search = '',
            cartype = '',
            cMarca = '',
            cModel = '',
            cKmFrom = '',
            cKmTo = '',
            cPriceFrom = '',
            cPriceTo = '',
            cYearFrom = '',
            cYearTo = '',
            cPowerFrom = '',
            cPowerTo = '',
            cGas = '',
            cChange = '',
            cColor = '',
            cLocation = '',
            cSeats = '',
            cDoors = '',
            orderBy = '';
       
            search = $("input[name=search]").val();
            cartype = ($("#cartype").val() == 'Todos') ? "" : $("#cartype").val();
            cMarca = $("select[name=cMarca]").val();
            cModel = $("select[name=cModel]").val();
            cKmFrom = $('select[name=cKmFrom]').val();
            cKmTo = $("select[name=cKmTo]").val();
            cPriceFrom = $("select[name=cPriceFrom]").val();
            cPriceTo = $("select[name=cPriceTo]").val();
            cYearFrom = $("select[name=cYearFrom]").val();
            cYearTo = $("select[name=cYearTo]").val();
            cPowerFrom = $("select[name=cPowerFrom]").val();
            cPowerTo = $("select[name=cPowerTo]").val();
            cGas = $("select[name=cGas]").val();
            cChange = $("select[name=cChange]").val();
            cColor = $("select[name=cColor]").val();
            cLocation = $("select[name=cLocation]").val();
            cSeats = $("#idcSeats").val();
            cDoors = $("#idcDoors").val();
            orderBy = $("select[name=orderBy]").val();

            
            //partial 3
            $(".o-flota-pick-container input:checkbox:checked").each(function() {
                tipus_flotalisticon.push($(this).closest('.m-flota-pick-item').data('flota'));
                tipus_flotalist.push($(this).val());
            });

            $("#oferta:checked").each(function () {
                console.log($(this).val());
                sales.push($(this).val());
            });        
            
        storeFrom('home');

        var query = "search="+search+"&cartype="+cartype+"&cMarca="+cMarca+"&cModel="+cModel+"&cKmFrom="+cKmFrom+"&cKmTo="+cKmTo+"&cPriceFrom="+cPriceFrom+"&cPriceTo="+cPriceTo+"&cYearFrom="+cYearFrom+"&cYearTo="+cYearTo+"&cPowerFrom="+cPowerFrom+"&cPowerTo="+cPowerTo+"&cGas="+cGas+"&cChange="+cChange+"&cColor="+cColor+"&cLocation="+cLocation+"&cSeats="+cSeats+"&cDoors="+cDoors+"&tipus_flotalist="+tipus_flotalist+"&tipus_flotalist="+tipus_flotalist+"&tipus_flotalisticon="+tipus_flotalisticon+"&tipus_flotalistmobil="+tipus_flotalistmobil+"&tipus_flotalistmobilicon="+tipus_flotalistmobilicon+"&sales="+sales+"&orderBy="+orderBy;
        $('body').removeClass("menu-open");
        $(window).off('scroll');

        function getDistFromBottom () {
            var scrollPosition = window.pageYOffset;
            var windowSize     = window.innerHeight;
            var bodyHeight     = document.body.offsetHeight;
            var footerHeight   = document.querySelector('.offset-footer').offsetTop - 200; 
            return Math.max(footerHeight - (scrollPosition + windowSize), 0);
        }

        $.ajax({
            type: 'POST',
            data: query+"&page=0&limit="+limit,
            url: base_url+'ajax/commons/get_vehicles',
            beforeSend: pollingTrue,
            success: function(data) {
                if(data.result===true){ 
                    $("#total_vehicles").html(data.vehicles_total + ' <?php echo lang('vehiculos_found'); ?>');
                    if(data.vehicles != null && data.vehicles.length > 0){ 
                        pollingForData = false;
                        $("#itemresults").html('');
                        $('ul.pagination').css('display','none');
                        $("#tags-items").html(data.dataFilter);
                        chargeVehicles(data);
                        tagVehicle();
                    } 
                    else{                   
                        $("#itemresults").html('');
                        $("#total_vehicles").html('<?php echo lang('no_items_found'); ?>');
                        $("#tags-items").html(data.dataFilter);
                        tagVehicle();
                    }
                    return true;    
                }                               
            }
        });

        $(window).on('scroll', function(){
            distToBottom = getDistFromBottom();

            if (!pollingForData && distToBottom > 0 && distToBottom <= 988) {
            page++;
            var init = page * limit;
                $.ajax({
                    type: 'POST',
                    data: query+"&page="+init+"&limit="+limit,
                    url: base_url+'ajax/commons/get_vehicles',
                    beforeSend: pollingTrue,
                    success: function(data) {
                        if(data.result===true){ 
                            if(data.vehicles != null && data.vehicles.length > 0){ 
                                pollingForData = false;
                                chargeVehicles(data);
                            }
                            return true;    
                        }                               
                    }
                });

            }
        });

        function pollingTrue(){
            pollingForData = true;
        }

        function chargeVehicles(data){
            var content = '',
                arrayImatges = '',
                urlDef = '';

            for(var i = 0; i < data.vehicles.length; i++){
                switch(data.vehicles[i].tipoficha){
                    case '1':
                        content += vehicleT1(data.vehicles[i]);
                    break;
                    case '2':
                        content += vehicleT2(data.vehicles[i]);
                    break;
                    case '3':
                        content += vehicleT3(data.vehicles[i]);
                    break;
                    case '4':
                        content += vehicleT4(data.vehicles[i]);
                    break;
                    case '5':
                        content += vehicleT5(data.vehicles[i]);
                    break; 
                }
            }

            $("#itemresults").append(content);
            var oldI = page * limit;
            swiperVehicleItemLoop(oldI);
            vehiclePopUps();
            favorits();
        }

        return false;
    } 