    
<div class="mff-content-container">
    <div class="mff-container mff-container-tipo mff-container-tb2cols">
        <label for=""><?php echo lang('filtro_tipo_vehiculo_title'); ?></label>
        <div class="mff-selects">
            <div class="select">
                <select class="select--vehicle-form selects-dependent" id="cMarca" data-pair="cModel">
                    <option value=""><?php echo lang('filtro_marcavehiculo'); ?></option>
                    <?php if(isset($marcas_model)) { $marca = '';  ?>
                        <?php foreach($marcas_model as $make) { 
                            if($marca!=$make->make){ $marca = $make->make; ?>
                                <option value="<?php echo $make->make; ?>"><?php echo $make->make; ?></option>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>
            <div class="select">
                <select class="select--vehicle-form" id="cModel">
                    <option value=""><?php echo lang('filtro_modelovehiculo'); ?></option>
                    <?php if(isset($marcas_model)) { ?>
                        <?php foreach($marcas_model as $model) { ?>
                            <option data-make="<?php echo $model->make; ?>" value="<?php echo $model->model; ?>"><?php echo $model->model; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="mff-container mff-container-km">
        <label class="mobile-inactive" for=""><?php echo lang('filtro_kmprecio'); ?></label>
        <label class="mobile-active" for=""><?php echo lang('filtro_km'); ?></label>
        <div class="mff-selects">
            <div class="select mobile-active">
                <select class="select--vehicle-form selects-pair from" id="cKmFrom" data-pair="cKmTo">
                    <option value=""><?php echo lang('filtro_fromkm'); ?></option>
                    <option value="1000">1.000km</option>
                    <option value="5000">5.000km</option>
                    <option value="10000">10.000km</option>
                    <option value="20000">20.000km</option>
                    <option value="30000">30.000km</option>
                    <option value="40000">40.000km</option>
                    <option value="50000">50.000km</option>
                    <option value="60000">60.000km</option>
                    <option value="70000">70.000km</option>
                    <option value="80000">80.000km</option>
                </select>
            </div>
            <div class="select">
            <select class="select--vehicle-form selects-pair to" id="cKmTo" data-pair="cKmFrom">
                    <option value=""><?php echo lang('filtro_tokm'); ?></option>
                    <option value="1000">1.000km</option>
                    <option value="5000">5.000km</option>
                    <option value="10000">10.000km</option>
                    <option value="20000">20.000km</option>
                    <option value="30000">30.000km</option>
                    <option value="40000">40.000km</option>
                    <option value="50000">50.000km</option>
                    <option value="60000">60.000km</option>
                    <option value="70000">70.000km</option>
                    <option value="80000">80.000km</option>
                    <option value="90000">90.000km</option>
                    <option value="100000">100.000km</option>
                    <option value="120000">120.000km</option>
                    <option value="130000">130.000km</option>
                    <option value="140000">140.000km</option>
                    <option value="150000">150.000km</option>
                </select>
            </div>
        </div>
    </div>
    <div class="mff-container mff-container-price">
        <label class="mobile-active" for=""><?php echo lang('filtro_precio'); ?></label>
        <div class="mff-selects">
            <div class="select mobile-active">
            <select class="select--vehicle-form selects-pair from" id="cPriceFrom" data-pair="cPriceTo">
                    <option value=""><?php echo lang('filtro_fromprecio'); ?></option>
                    <option value="1000">1.000€</option>
                    <option value="5000">5.000€</option>
                    <option value="10000">10.000€</option>
                    <option value="20000">20.000€</option>
                    <option value="30000">30.000€</option>
                    <option value="40000">40.000€</option>
                    <option value="50000">50.000€</option>
                </select>
            </div>
            <div class="select">
            <select class="select--vehicle-form selects-pair to" id="cPriceTo" data-pair="cPriceFrom">
                    <option value=""><?php echo lang('filtro_toprecio'); ?></option>
                    <option value="1000">1.000€</option>
                    <option value="5000">5.000€</option>
                    <option value="10000">10.000€</option>
                    <option value="20000">20.000€</option>
                    <option value="30000">30.000€</option>
                    <option value="40000">40.000€</option>
                    <option value="50000">50.000€</option>
                    <option value="60000">60.000€</option>
                    <option value="400000">+60.000€</option>
                </select>
            </div>
        </div>
    </div>
    <div class="mff-container mff-container-inactive mff-container-year">
        <label for=""><?php echo lang('filtro_yearmat'); ?></label>
        <div class="mff-selects">
            <div class="select">
            <select class="select--vehicle-form selects-pair from" id="cYearFrom" data-pair="cYearTo">
                    <option value=""><?php echo lang('filtro_fromyearmat'); ?></option>
                    <?php if(isset($maxamodelyear) && isset($minamodelyear)) { 
                        for($i=$maxamodelyear; $i>=$minamodelyear; $i--){ ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>
            <div class="select">
            <select class="select--vehicle-form selects-pair to" id="cYearTo" data-pair="cYearFrom">
                    <option value=""><?php echo lang('filtro_toyearmat'); ?></option>
                    <?php if(isset($maxamodelyear) && isset($minamodelyear)) { 
                        for($i=$maxamodelyear; $i>=$minamodelyear; $i--){ ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <!-- <div class="mff-container mff-container-inactive mff-container-power">
        <label for=""><?php echo lang('filtro_potencia'); ?></label>
        <div class="mff-selects">
            <div class="select">
                <select class="select--vehicle-form" id="cPowerFrom">
                    <option value=""><?php echo lang('filtro_frompotencia'); ?></option>
                    <?php if(isset($maxahorsespower) && isset($minahorsespower)) { 
                        for($i=$minahorsespower; $i<=$maxahorsespower; $i++){ ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>
            <div class="select">
                    <select class="select--vehicle-form" id="cPowerTo">
                    <option value=""><?php echo lang('filtro_topotencia'); ?></option>
                    <?php if(isset($maxahorsespower) && isset($minahorsespower)) { 
                        for($i=$minahorsespower; $i<=$maxahorsespower; $i++){ ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php }  ?>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div> -->
    <div class="mff-container mff-container-inactive mff-container-others">
        <label for=""><?php echo lang('filtro_otros'); ?></label>
        <div class="mff-selects">
            <div class="select">
                <select class="select--vehicle-form" id="cGas">
                    <option value=""><?php echo lang('filtro_combustible'); ?></option>
                    <?php if(isset($afueltype)) { ?>
                        <?php foreach($afueltype as $fueltype) { ?>
                            <option value="<?php echo $fueltype; ?>"><?php echo $fueltype; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>
            <div class="select">
                <select class="select--vehicle-form" id="cChange">
                    <option value=""><?php echo lang('filtro_cambio'); ?></option>
                    <?php if(isset($ageartype)) { ?>
                        <?php foreach($ageartype as $geartype) { ?>
                            <option value="<?php echo $geartype; ?>"><?php echo $geartype; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>
            <!-- <div class="select desktop-hide">
                <select class="select--vehicle-form" id="cColor">
                    <option value=""><?php echo lang('filtro_color'); ?></option>
                    <?php if(isset($acolor)) { ?>
                        <?php foreach($acolor as $color) { ?>
                            <option value="<?php echo $color; ?>"><?php echo $color; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div> -->
        </div>
    </div>
    <div class="mff-container mff-container-triple">
        <div class="mff-container mff-container-inactive mff-container-seats">
            <label for=""><?php echo lang('filtro_nplazas'); ?></label>
            <div class="mff-selects">
                <div class="m-group-values cSeats">
                    <input type="text" class="input-val" id="cSeats" name="cSeats" value="">
                        <?php if(isset($minaseats) && isset($maxaseats)) { $tSeats = ($maxaseats - $minaseats) + 1;  ?>
                    <div class="mgv-wrap items<?php echo $tSeats; ?>">
                        <?php
                        for($y=$minaseats; $y<=$maxaseats; $y++){ ?>
                            <span class="mgv-option" data-option="<?php echo $y; ?>"><?php echo $y; ?></span>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="mff-container mff-container-inactive mff-container-doors">
            <label for=""><?php echo lang('filtro_npuertas'); ?></label>
            <div class="mff-selects">
                <div class="m-group-values cDoors">
                    <input type="text" class="input-val" id="cDoors" name="cDoors" value="">
                    <?php if(isset($maxanoofdoors) && isset($minanoofdoors)) { $tnoofdoors =  ($maxanoofdoors - $minanoofdoors) + 1; ?>
                    <div class="mgv-wrap items<?php echo $tnoofdoors; ?>">
                        <?php
                            for($i=$minanoofdoors; $i<=$maxanoofdoors; $i++){ ?>
                                <span class="mgv-option" data-option="<?php echo $i; ?>"><?php echo $i; ?></span>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>   
    