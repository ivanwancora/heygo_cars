<form class="m-form--flota m-form--flota--reduced" id="formsearchitems" autocomplete="off" method="post" onsubmit="return buscaritems();">

    <input type="hidden" name="vehicles" value="vehicles">
    <h4 class="form-header"><?php echo lang('filtro_title'); ?></h4>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part1_input.php', $this->data); ?>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part3_flota.php', $this->data); ?>
    <div class="a-divider--primary"></div>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part4_form.php', $this->data); ?>
    <input type="hidden" id="cartype" name="cartype" value="">
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part5_buttons.php', $this->data); ?>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part7_submit.php', $this->data); ?>
</form>