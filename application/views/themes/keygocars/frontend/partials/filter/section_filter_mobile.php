<div class="o-form--mobile">
    <div class="omm-header">
        <div class="logo">
            <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/logo/hgc_secondary.svg');?>" alt="<?php echo $this->config->item('project_name'); ?>">
        </div>
        <a class="m-menu--burger cross"></a>
    </div>
    <?php if($this->router->fetch_class() == 'vehiculos' && $this->router->fetch_method() !='detalle') { //pàgina diferents a vehicles ?>
        <form class="m-form--flota m-form--flota--mobile" id="formsearchitemsmobile" autocomplete="off" method="post" onsubmit="return searchMobile(event,'mobile'); ">
    <?php }else{ //pàgina de vehicles ?>
        <form class="m-form--flota m-form--flota--mobile" id="formsearchitemsmobile" autocomplete="off" method="post" onsubmit="return searchAndGo(event,'mobile'); "> 
    <?php } ?>
        <input type="hidden" name="mobile" id="mobile" value="mobile">
        <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part1_input_mobile.php', $this->data); ?>
        <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part2_select_flota.php', $this->data); ?>
        <div class="a-divider--primary"></div>
        <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part4_mobile_form.php', $this->data); ?>
        <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part5_buttons_mobile.php', $this->data); ?>
        <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part6_clear_btn.php', $this->data); ?>
        <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_part7_submit_mobile.php', $this->data); ?>
    </form>  
    
</div>
</div>