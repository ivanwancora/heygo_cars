<div class="mmf-all-buttons">
    <div class="mmf-buttons">
        <button class="a-button--remove-filters" type="button"> <span class="round"><span class="icon-cross"></span></span> <?php echo lang('filtro_reset');?></button>
        
        <label class="a-checkbox--sales">
            <input type="checkbox" id="sales">
            <span class="round"><span class="icon-check"></span></span> <?php echo lang('filtro_ofertas'); ?>
        </label>