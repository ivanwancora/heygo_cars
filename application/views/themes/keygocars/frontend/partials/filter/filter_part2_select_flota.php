<div class="mff-flota-selector o-flota-selec-module active">
    <div class="msi-select">
        <span class="selected">
            <span class="icon-cxcsuv"></span> <?php echo lang('filter_selecciona'); ?>
        </span>
        <div class="btn">
            <div class="icon-down"></div>
        </div>
    </div>
    <div class="wrap">
        <div class="m-menu--dropdown">
            <ul>
            <?php if(isset($tipo_flotas)) {  ?>
                <?php foreach ($tipo_flotas as $tf) {  ?>
                    <li>
                        <div class="m-flota-selec-item <?php echo $tf->icono; ?>" data-flota="<?php echo $tf->icono; ?>">
                            <input type="checkbox" name="flota-selec[]" value="<?php echo $tf->icono; ?>" />
                            <span class="icon-<?php echo $tf->icono; ?>"></span> 
                            <span class="body"><?php echo ucfirst($tf->title); ?></span>
                        </div>
                    </li>
                <?php } ?>
            <?php } ?>
            </ul>
            <button type="button" class="a-button--ok">Ok</button>
        </div>
    </div>
</div>