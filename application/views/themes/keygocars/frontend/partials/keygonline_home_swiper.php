<div class="o-swiper--keygonline">
    <div class="swiper-wrapper">  
        <article class="m-nos-veh-item--compressed swiper-slide">
                <div class="image general_svg"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/informaciongeneral_gris_secondary.svg');?>" alt="<?php echo lang('section_keygonline_item1_h'); ?>" /></div>
                <div class="content general">
                    <h4><?php echo lang('section_keygonline_item1_h'); ?></h4>
                    <p><?php echo lang('section_keygonline_item1_body'); ?></p>
                </div>
        </article>
        <article class="m-nos-veh-item--compressed swiper-slide">
                <div class="image online"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/online.svg');?>" alt="<?php echo lang('section_keygonline_item2_h'); ?>" /></div>
                <div class="content online">
                    <h4><?php echo lang('section_keygonline_item2_h'); ?></h4>
                    <p><?php echo lang('section_keygonline_item2_body'); ?></p>
                </div>  
        </article>
        <article class="m-nos-veh-item--compressed swiper-slide">
                <div class="image casa"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/entrega_casa_secondary.svg');?>" alt="<?php echo lang('section_keygonline_item3_h'); ?>" /></div>
                <div class="content casa">
                    <h4><?php echo lang('section_keygonline_item3_h'); ?></h4>
                    <p><?php echo lang('section_keygonline_item3_body'); ?></p>
                </div>
        </article>
    </div>
    <div class="keygonline-pagination"></div>
</div>