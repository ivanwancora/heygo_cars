    <section class="socket">
        <div class="w-content">
            <nav class="m-menu--socket">
                <ul>
                    <?php foreach ($pages as $page) { ?>
                        <li><a href="<?php echo base_url($this->lang->lang().'/keygocars/'.$page->url_slug); ?>"><?php echo ucfirst($page->nombre); ?></a></li>
                    <?php } ?>
                </ul>
            </nav>
            <nav class="m-menu--social">
                <ul>
                    <li><a class="mms-icon facebook" href="<?php echo lang('facebook_valencia'); ?>" target="_blank"></a></li>
                    <li><a class="mms-icon instagram" href="<?php echo lang('instagram_valencia'); ?>" target="_blank"></a></li>
                </ul>
            </nav>
        </div>
    </section>