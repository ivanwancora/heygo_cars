<section class="p-nos-veh--compressed">
    <h2 class="blue">
        <?php echo lang('nuestros_vehiculos_titulo_comp'); ?>
    </h2>
    <h4 class="blue">
        <?php echo lang('nuestros_vehiculos_titulo_subcomp'); ?>
    </h4>
    <div class="o-nos-veh-container--compressed">
        <article class="m-nos-veh-item--compressed">
                <div class="image volante"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/volante.png');?>" alt="<?php echo lang('nuestros_vehiculos_dest1_titulo'); ?>" /></div>
                <div class="content">
                    <h4><?php echo lang('nuestros_vehiculos_dest1_titulo'); ?></h4>
                    <p><?php echo lang('nuestros_vehiculos_dest1_desc'); ?></p>
                </div>
        </article>
        <article class="m-nos-veh-item--compressed">
                <div class="image anyos"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/2anyosgarantia.png');?>" alt="<?php echo lang('nuestros_vehiculos_dest2_titulo'); ?>" /></div>
                <div class="content anyos">
                    <h4><?php echo lang('nuestros_vehiculos_dest2_titulo'); ?></h4>
                    <p><?php echo lang('nuestros_vehiculos_dest2_desc'); ?></p>
                </div>  
        </article>
        <article class="m-nos-veh-item--compressed">
                <div class="image compra"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/valoracioncompra.png');?>" alt="<?php echo lang('nuestros_vehiculos_dest3_titulo'); ?>" /></div>
                <div class="content">
                    <h4><?php echo lang('nuestros_vehiculos_dest3_titulo'); ?></h4>
                    <p><?php echo lang('nuestros_vehiculos_dest3_desc'); ?></p>
                </div>
        </article>
        <article class="m-nos-veh-item--compressed">
                <div class="image financiacion"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/financiacionflexible.png');?>" alt="<?php echo lang('nuestros_vehiculos_dest4_titulo'); ?>" /></div>
                <div class="content">
                    <h4><?php echo lang('nuestros_vehiculos_dest4_titulo'); ?></h4>
                    <p><?php echo lang('nuestros_vehiculos_dest4_desc'); ?></p>
                </div>
        </article>
        <article class="m-nos-veh-item--compressed swiper-slide">
            <div class="image peritacion">
                <div class="new-icon-wrapper">
                    <img class="new-icon" src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/new-icon.svg');?>" alt="<?php echo lang('new'); ?>" />    
                </div>    
                <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/certificadoperitacion.png');?>" alt="<?php echo lang('nuestros_vehiculos_dest5_titulo'); ?>" />
            </div>
            <div class="content peritacion">

                <h4>
                    <?php echo lang('nuestros_vehiculos_dest5_titulo'); ?>
                </h4>
                <p><?php echo lang('nuestros_vehiculos_dest5_desc'); ?></p>
            </div>
        </article>
        <article class="m-nos-veh-item--compressed swiper-slide">
            <div class="image online">
                <div class="new-icon-wrapper">
                    <img class="new-icon" src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/new-icon.svg');?>" alt="<?php echo lang('new'); ?>" />    
                </div>    
                <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/online.png');?>" alt="<?php echo lang('nuestros_vehiculos_dest5_titulo'); ?>" />
            </div>
            <div class="content online">
                <h4>
                    <?php echo lang('nuestros_vehiculos_dest6_titulo'); ?>
                </h4>
                <p><?php echo lang('nuestros_vehiculos_dest6_desc'); ?></p>
            </div>
        </article>
    </div>
</section>