    <section class="p-about-us-section">
        <h2 class="pms-header blue"><?php echo lang('quienes_titulo'); ?></h2>
        <h4 class="pms-subheader blue">
            <?php echo lang('quienes_subheader'); ?> 
        </h4>
        <div class="o-about-us-module">
            <div class="m-video-item">
                <div class="wrapper">
                <img class="w100" src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/temp/video1.jpg'); ?>" />
                </div>
            </div>
            <div class="oau-content">
                <h4 class="blue"><?php echo lang('quienes_subcontent'); ?></h4>
                <p class="body">
                    <?php echo lang('quienes_content'); ?>
                </p>
                <a class="a-button--offset--dark" href="<?php echo base_url($this->lang->lang().'/empresa/'); ?>">
                    <div class="btn"><?php echo lang('general_masinfo'); ?></div>
                    <div class="shadow"></div>
                </a>
            </div>
        </div>
    </section>