<section class="p-home--sales">
            <div class="w-content">
                <h2 class="blue">
                    <?php echo lang('ofertas_titulo'); ?>
                </h2>
                <h4>
                    <?php echo lang('ofertas_subheader'); ?>
                </h4>
                <div class="o-home-sales-module">
                    <nav id="m-menu--sales--home">
                        <ul>
                            <li class="mmo-action"><a class="mmo-prev"><div class="icon-prev"></div></a></li>
                            <li class="mmo-option ocasion active" data-type="ocasion"><a href="<?php echo base_url($this->lang->lang().'/vehiculos/ocasion'); ?>"><span><?php echo lang('vehiculos_ocasion'); ?></span></a></li>
                            <li class="mmo-option km0" data-type="km0"><a href="<?php echo base_url($this->lang->lang().'/vehiculos/km0'); ?>"><span><?php echo lang('vehiculos_km0'); ?></span></a></li>
                            <li class="mmo-option seminuevo seminuevos last" data-type="seminuevos"><a href="<?php echo base_url($this->lang->lang().'/vehiculos/seminuevos'); ?>"><span><?php echo lang('vehiculos_seminuevos'); ?></span></a></li>
                            <li class="mmo-action"><a class="mmo-next"><div class="icon-next"></div></a></li>
                        </ul>
                    </nav>
                    <div class="phs-container">

                        <!-- Inici Ocasion -->
                        <div class="o-swiper--home-sales ocasion active">
                            <div class="home-sales-navigation">
                                <div class="home-sales-pagination"></div>
                            </div>
                            <div class="swiper-wrapper" id="ocasion">
                                <!-- contingut cotxes ocasion -->
                            </div>
                        </div>
                         <!-- /Fi ocasion -->
                        
                        <!-- Inici Km0 -->
                        <div class="o-swiper--home-sales km0">
                            <div class="home-sales-navigation">
                                <div class="home-sales-pagination"></div>
                            </div>
                            <div class="swiper-wrapper" id="km0">
                                <!-- contingut cotxes km0 -->
                            </div>
                        </div>
                         <!-- /Fi km0 -->

                        <!-- Inici Seminuevos -->
                        <div class="o-swiper--home-sales seminuevos">
                            <div class="home-sales-navigation">
                                <div class="home-sales-pagination"></div>
                            </div>
                            <div class="swiper-wrapper" id="seminuevos">
                                <!-- contingut cotxes seminuevos -->
                            </div>
                        </div>
                         <!-- /Fi Seminuevos -->
                    </div>
                </div>
            </div>
    </section>