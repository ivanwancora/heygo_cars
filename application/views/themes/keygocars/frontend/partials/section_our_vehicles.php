<div class="p-nos-veh-header">
        <div class="figure">
            <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/cars/Citroen-C4-Grand-Picasso.png');?>" class="w100" />
        </div>
    </div>
    <section class="p-nos-veh">
        <h2 class="blue">
            <?php echo lang('nuestros_vehiculos_titulo'); ?>
        </h2>
        <h4 class="blue">
            <?php echo lang('nuestros_vehiculos_subheader'); ?>
        </h4>
        <div class="wrapper">
            <div class="o-swiper--nos-veh">
                <div class="swiper-wrapper">    
                    <article class="m-nos-veh-item swiper-slide">
                            <div class="image volante"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/volante.png');?>" alt="<?php echo lang('nuestros_vehiculos_dest1_titulo'); ?>" /></div>
                            <div class="content">
                                <h4><?php echo lang('nuestros_vehiculos_dest1_titulo'); ?></h4>
                                <p><?php echo lang('nuestros_vehiculos_dest1_desc'); ?></p>
                            </div>
                    </article>
                    <article class="m-nos-veh-item swiper-slide">
                            <div class="image anyos"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/2anyosgarantia.png');?>" alt="<?php echo lang('nuestros_vehiculos_dest2_titulo'); ?>" /></div>
                            <div class="content">
                                <h4><?php echo lang('nuestros_vehiculos_dest2_titulo'); ?></h4>
                                <p><?php echo lang('nuestros_vehiculos_dest2_desc'); ?></p>
                            </div>  
                    </article>
                    <article class="m-nos-veh-item swiper-slide">
                            <div class="image compra"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/valoracioncompra.png');?>" alt="<?php echo lang('nuestros_vehiculos_dest3_titulo'); ?>" /></div>
                            <div class="content">
                                <h4><?php echo lang('nuestros_vehiculos_dest3_titulo'); ?></h4>
                                <p><?php echo lang('nuestros_vehiculos_dest3_desc'); ?></p>
                            </div>
                    </article>
                    <article class="m-nos-veh-item swiper-slide">
                            <div class="image financiacion"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/financiacionflexible.png');?>" alt="<?php echo lang('nuestros_vehiculos_dest4_titulo'); ?>" /></div>
                            <div class="content">
                                <h4><?php echo lang('nuestros_vehiculos_dest4_titulo'); ?></h4>
                                <p><?php echo lang('nuestros_vehiculos_dest4_desc'); ?></p>
                            </div>
                    </article>
                    <article class="m-nos-veh-item swiper-slide">
                            <div class="image peritacion"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/certificadoperitacion.png');?>" alt="<?php echo lang('nuestros_vehiculos_dest5_titulo'); ?>" /></div>
                            <div class="content">
                                <h4 class="new-icon-wrapper">
                                    <img class="new-icon" src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/ui/new-icon.svg');?>" alt="<?php echo lang('new'); ?>" />    
                                    <?php echo lang('nuestros_vehiculos_dest5_titulo'); ?>
                                </h4>
                                <p><?php echo lang('nuestros_vehiculos_dest5_desc'); ?></p>
                            </div>
                    </article>
                </div>
                <div class="nos-veh-pagination"></div>
            </div>
            <div class="a-center mgt-40">
                <a href="<?php echo base_url($this->lang->lang().'/vehiculos/'); ?>" class="a-button--offset--growing--stock">
                    <div class="btn">
                        <span class="text"><?php echo lang('general_ver_stock_vehiculos'); ?></span>
                        <span class="icon-arrow"></span>
                    </div>
                    <div class="shadow"></div>
                </a>
            </div>
        </div>
    </section>