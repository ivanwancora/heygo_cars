<div class="o-menu--mobile">
    <div class="omm-header">
        <div class="logo">
            <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/logo/hgc_menu.svg');?>" alt="Cars4You">
        </div>
        <a class="m-menu--burger cross primary">

        </a>
    </div>
    <nav class="m-menu--primary--mobile">
        <ul>
            <li>
                <span class="icon-point"></span>
            </li>
            <li>
                <a class="<?php if($this->router->fetch_class() == 'principal') { echo "active"; } ?>" href="<?php echo base_url('/'); ?>"><?php echo lang('menu_mobile_inicio'); ?></a>
            </li>
            <li>
                <a class="<?php if($this->router->fetch_class() == 'vehiculos') { echo "active"; } ?> btn-menu-vehiculos"><?php echo lang('menu_mobile_vehiculos'); ?></a>
            </li>
            <li>
                <a class="<?php if($this->router->fetch_class() == 'centros') { echo "active"; } ?>" href="<?php echo base_url($this->lang->lang().'/centros/'); ?>"><?php echo lang('menu_mobile_centros'); ?></a>
            </li>
            <li>
                <a class="<?php if($this->router->fetch_class() == 'empresa') { echo "active"; } ?>" href="<?php echo base_url($this->lang->lang().'/empresa/'); ?>"><?php echo lang('menu_mobile_empresa'); ?></a>
            </li>
            <li>
                <a class="bgw <?php if($this->router->fetch_class() == 'keygonline') { echo "active"; } ?>" href="<?php echo base_url($this->lang->lang().'/keygonline/'); ?>"><?php echo lang('menu_mobile_keygonline'); ?></a>
            </li>
            <!--<li>
                <a class="inactive"><?php echo lang('menu_mobile_noticias'); ?></a>
            </li>-->
            <!--<li>
                <a class="inactive"><?php echo lang('menu_mobile_alquiler'); ?></a>
            </li>-->
            <li>
                <a class="<?php if($this->router->fetch_class() == 'contacto') { echo "active"; } ?>" href="<?php echo base_url($this->lang->lang().'/contacto/'); ?>"><?php echo lang('menu_mobile_contatco'); ?></a>
            </li>
        </ul>
    </nav>
    <div class="a-center">
    <button class="a-button--search-form-growing">
        encuentra tu coche ideal
    </button>
    </div>
    <div class="contact">
        <a href="tel:<?php echo lang('header_telf_num'); ?>" class="a-phone-link--white">
            <span><?php echo lang('header_telf'); ?></span>
        </a>
        <a href="mailto:<?php echo lang('header_mail'); ?>" class="a-email-link--white">
            <span><?php echo lang('header_mail'); ?></span>
        </a>
    </div>
</div>