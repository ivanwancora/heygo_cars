
        <footer>
        <div class="w-content">
            <a href="" class="logo">
                <img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/logo/logo-footer.svg');?>" alt="<?php echo $this->config->item('project_name'); ?>" class="w100" >
            </a>
            <div class="of-content">
                <nav class="m-menu--footer">
                    <ul class="col-2">
                        <li>
                            <a class="<?php if($this->router->fetch_class() == 'principal') { echo "active"; } ?>" href="<?php echo base_url('/'); ?>"><?php echo lang('menu_inicio'); ?></a>
                        </li>
                        <li>
                            <a class="<?php if($this->router->fetch_class() == 'vehiculos') { echo "active"; } ?> btn-menu-vehiculos"><?php echo lang('menu_vehiculos'); ?></a>
                        </li>
                        <li>
                            <a class="<?php if($this->router->fetch_class() == 'centros') { echo "active"; } ?>" href="<?php echo base_url($this->lang->lang().'/centros/'); ?>"><?php echo lang('menu_centros'); ?></a>
                        </li>
                        <li>
                            <a class="<?php if($this->router->fetch_class() == 'empresa') { echo "active"; } ?>" href="<?php echo base_url($this->lang->lang().'/empresa/'); ?>"><?php echo lang('menu_empresa'); ?></a>
                        </li>
                        <li>
                            <a class="bgw <?php if($this->router->fetch_class() == 'keygonline') { echo "active"; } ?>"  href="<?php echo base_url($this->lang->lang().'/keygonline/'); ?>"><?php echo lang('menu_keygonline'); ?></a>
                        </li>
                        <!--<li><a class="inactive"><?php echo lang('menu_noticias'); ?></a></li>-->
                        <!--<li><a class="inactive"><?php echo lang('menu_alquiler'); ?></a></li>-->
                        <li>
                            <a class="<?php if($this->router->fetch_class() == 'contacto') { echo "active"; } ?>" href="<?php echo base_url($this->lang->lang().'/contacto/'); ?>"><?php echo lang('menu_contatco'); ?></a>
                        </li>
                    </ul>
                </nav>
                <div class="of-contact">
                    <div class="ofc-line address">
                        <span><?php echo lang('footer_direccion'); ?></span>
                    </div>
                    <a class="ofc-line a-phone-link--footer" href="tel:<?php echo lang('header_telf_num'); ?>">
                        <span><?php echo lang('header_telf'); ?></span>
                    </a>
                    <a class="ofc-line a-email-link--footer" href="mailto:<?php echo lang('header_mail'); ?>">
                        <span><?php echo lang('header_mail'); ?></span>
                    </a>
                </div>
            </div>
        </div>
       
    </footer>
    <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/socket', $this->data); ?>
</div><!-- // container -->

<?php if($this->router->fetch_class() == 'principal' && $this->router->fetch_method() =='index') { 
    $this->load->view($this->config->item('theme_path_frontend'). 'partials/keygonline_popup', $this->data);
} ?>

    <script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/usuarios.js');?>"></script>
    <script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/utils.js');?>"></script>
    <script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/login.js');?>"></script>
    <script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/stickybits.min.js');?>"></script>
    <script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/swiper.min.js');?>"></script>
    <script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/jquery.slickmodal.min.js');?>"></script>
    <script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/lightbox.min.js');?>"></script>
    <script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/stoCookie.min.js'); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/ofi.min.js');?>"></script>
    <script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/datepicker.min.js');?>"></script>
    <script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/datepicker.en.js');?>"></script>
    <script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/datepicker.es.js');?>"></script>
    <!-- <script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/jquery.visible.min.js');?>"></script>-->
    <script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/select2.full.min.js');?>"></script>
    <script src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/app.js');?>"></script>
    <?php if($this->router->fetch_class() == 'vehiculos' || ($this->router->fetch_class() == 'principal' && $this->router->fetch_method() =='index') || ($this->router->fetch_class() == 'mi_cuenta' && $this->router->fetch_method() =='favoritos')) { ?>
    <script>
        <?php  $this->load->view($this->config->item('theme_path_frontend'). 'partials/filter/filter_script'); ?>
        <?php $this->load->view($this->config->item('theme_path_frontend'). 'partials/casos/tpl_vehiculos.php', $this->data); ?>
    </script>
    <?php } ?>
    <?php if($this->router->fetch_class() == 'mi_cuenta' || $this->router->fetch_class() == 'principal' || $this->router->fetch_class() == 'empresa' || $this->router->fetch_class() == 'contacto' || ($this->router->fetch_class() == 'vehiculos' && $this->router->fetch_method() =='detalle')) { ?>
        <script>
            var onSubmit = function(token) {
            console.log('success!');
            };

            var onloadCallback = function() {
                grecaptcha.render('submit', {
                    'sitekey' : '6LcbDcIUAAAAAA7p7_HjhC8tPXQz-9IeMKuuhlVN',
                    'callback' : onSubmit
                });
            };
        </script>
        <script>enviarmarca();</script>
    <?php } ?>
    <?php if($this->router->fetch_class() == 'vehiculos' && $this->router->fetch_method() !='detalle') { ?>
        <script>
            // Params Vehicles
            paramsVehicle();
            $('#formsearchitems').submit(); 
        </script>
    <?php } ?>
    <?php if($this->router->fetch_class() == 'vehiculos' && $this->router->fetch_method() =='detalle') { ?>
    <script type="text/javascript">                    
        var cotxes = <?php echo json_encode($relatedflota) ?>;
        vehicleSlides(cotxes, "#relacionats");
        swiperVehicleItem();
        vehiclePopUps();
        favorits();
    </script>
    <?php } ?>
    <?php if($this->router->fetch_class() == 'principal' && $this->router->fetch_method() =='index') { ?>
    <script type="text/javascript">                    
        var ocasion = <?php echo json_encode($ocasion) ?>;
        var km = <?php echo json_encode($km) ?>;
        var seminuevos = <?php echo json_encode($seminuevos) ?>;
        console.log(ocasion);
        vehicleSlides(ocasion, "#ocasion");
        vehicleSlides(km, "#km0");
        vehicleSlides(seminuevos, "#seminuevos");
        swiperVehicleItem();
        vehiclePopUps();
        favorits();
    </script>
    <?php } ?>

    <?php if($this->router->fetch_class() == 'mi_cuenta' && $this->router->fetch_method() =='favoritos') { ?>
    <script type="text/javascript">                    
        var cotxes = <?php echo json_encode($flotas) ?>;
        console.log(cotxes);
        vehicleSlides(cotxes, "#favoritos");
        swiperVehicleItem();
        vehiclePopUps();
        favorits();
    </script>
    <?php } ?>
    <?php if($this->router->fetch_class() == 'principal' && $this->router->fetch_method() =='index') { ?>
        <script>
            jQuery('.keygonline_modal').SlickModals({
                popup_type: 'instant',
                popup_wrapperClass: 'bluePopup',
				popup_closeButtonAlign: 'right',
				popup_closeButtonPlace: 'inside',
                popup_position: 'center',
                popup_css: {
                    'width'             : '90%',
                    'height'            : 'auto',
                    'background'        : 'rgba(20,54,73,0.90)',
                    'margin'            : '24px',
                    'padding'           : '40px 24px 24px',
                    'animation-duration': '0.4s',
                    'border-radius'     : '20px',
                },
                mobile_css: {
                    'background'        : '#143649',
                    'margin'            : '10px',
                    'padding'           : '12px',
                    'border-radius'     : '20px',
                },
				overlay_css: {
					'background': 'background: rgba(0, 0, 0, 0)'
                },
                callback_afterVisible: function(){
                    $('.keygonline_modal').find('.dashed_animation').addClass('active');
                }
            });
        </script>
    <?php } ?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-150320245-1"></script>
    <script type="text/javascript">
      jQuery(document).ready(function($){

        $('body').stoCookie({
            message: '<?php echo lang('cookies_message'); ?>',
            cookieName: 'stoCookie',           
            cookieLife: 365,                    
            mode: 'cookie',                    
            scrollClose: false,                 
            position: 'bottom',               
            background: '#143649',             
            color: '#ffffff',                   
            linkColor: '#0FB7CD',               
            slide: true,                        
            fade: true,                         
            btnText: '<?php echo lang('newsletter_ok'); ?>',                      
            //denyText: 'No',
            buttonBg: '#0FB7CD',               
            buttonColor: '#ffffff',            
            buttonAlign: 'center',             
            buttonOutlineColor: 'transparent',
            buttonSecondaryBg:"rgba(0,0,0,0)",
            buttonSecondaryColor:"#ffffff",
            buttonSecondaryOutlineColor:"#ffffff",
            buttonCorner: 'center',            
            innerClass: '',                    
            accepted: function() {
                //Google Analytics
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', 'UA-150320245-1');
            },
            denied: function() {

            },
            acceptOnClose : false
        });
      });
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-150320245-1');
    </script>
  </body>
</html>