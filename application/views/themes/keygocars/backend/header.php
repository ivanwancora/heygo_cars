<!DOCTYPE html>
<html>
<head>
    <title><?php echo $this->config->item('project_name'); ?></title>  
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets_admin/css/vendor.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets_admin/css/flat-admin.css'); ?>">   
    <meta name="application-name" content="<?php echo $this->config->item('project_name'); ?>"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />       
    <script type="text/javascript" src="<?php echo base_url('assets/'.$this->config->item('versionat').'/js/jquery-3.3.1.min.js'); ?>"></script> 
    <script type="text/javascript" src="<?php echo base_url('assets_admin/js/vendor.js');?>"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
	<script type="text/javascript">
        var base_url = '<?php echo base_url($this->lang->lang().'/'); ?>';
        var base_url_img = '<?php echo base_url(); ?>';
    </script>
</head>
<body>
  <div class="app app-default">
