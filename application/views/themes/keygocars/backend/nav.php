<aside class="app-sidebar" id="sidebar">
  <div class="sidebar-header">
    <a class="sidebar-brand" href="<?php echo base_url($this->lang->lang().'/Back_dashboard/'); ?>"><img src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/logo/logo-header.svg'); ?>" height="75" alt="<?php echo $this->config->item('project_name'); ?>" title="<?php echo $this->config->item('project_name'); ?>"></a>
    <button type="button" class="sidebar-toggle">
      <i class="icon fa fa-times"></i>
    </button>
  </div>
  <div class="sidebar-menu">
    <ul class="sidebar-nav">
      <li class="<?php if($this->router->fetch_class() === 'Back_dashboard') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_dashboard/'); ?>">
          <div class="icon">
            <i class="icon fa fa-tasks" aria-hidden="true"></i>
          </div>
          <div class="title">Dashboard</div>
        </a>
      </li>

      <li class="<?php if($this->router->fetch_method() === 'marcas') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/marcas'); ?>">
          <div class="icon">
            <i class="icon fa fa-trademark" aria-hidden="true"></i>
          </div>
          <div class="title">Marcas</div>
        </a>
      </li>

      <li class="<?php if($this->router->fetch_method() === 'newsletter') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/newsletter'); ?>">
          <div class="icon">
            <i class="icon fa fa-newspaper-o" aria-hidden="true"></i>
          </div>
          <div class="title">Newsletter</div>
        </a>
      </li>

      <li class="<?php if($this->router->fetch_method() === 'noticias') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/noticias'); ?>">
          <div class="icon">
            <i class="icon fa fa-envelope" aria-hidden="true"></i>
          </div>
          <div class="title">Noticias</div>
        </a>
      </li>

      <li class="<?php if($this->router->fetch_method() === 'categorias') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/categorias'); ?>">
          <div class="icon">
            <i class="icon fa fa-tags" aria-hidden="true"></i>
          </div>
          <div class="title">Categorías noticias</div>
        </a>
      </li>

      <li class="<?php if($this->router->fetch_method() === 'fcontacts') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/fcontacts'); ?>">
          <div class="icon">
            <i class="icon fa fa-users" aria-hidden="true"></i>
          </div>
          <div class="title">Formularios de contacto</div>
        </a>
      </li>

      <li class="<?php if($this->router->fetch_method() === 'equipo') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/equipo'); ?>">
          <div class="icon">
            <i class="icon fa fa-odnoklassniki-square" aria-hidden="true"></i>
          </div>
          <div class="title">Equipo</div>
        </a>
      </li>

      <li class="<?php if($this->router->fetch_method() === 'centros') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/centros'); ?>">
          <div class="icon">
            <i class="icon fa fa-bullseye" aria-hidden="true"></i>
          </div>
          <div class="title">Centros</div>
        </a>
      </li>

      <li class="<?php if($this->router->fetch_method() === 'slider') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/slider'); ?>">
          <div class="icon">
            <i class="icon fa fa-sliders" aria-hidden="true"></i>
          </div>
          <div class="title">Visor dinámico Imágenes</div>
        </a>
      </li>

      <li class="<?php if($this->router->fetch_method() === 'flotas_tipo') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/flotas_tipo'); ?>">
          <div class="icon">
            <i class="icon fa fa-car" aria-hidden="true"></i>
          </div>
          <div class="title">Tipo de flotas</div>
        </a>
      </li>

      <li class="<?php if($this->router->fetch_method() === 'destacados') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/destacados'); ?>">
          <div class="icon">
            <i class="icon fa fa-star" aria-hidden="true"></i>
          </div>
          <div class="title">Destacados</div>
        </a>
      </li>

      <li class="<?php if($this->router->fetch_method() === 'usuarios') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/usuarios'); ?>">
          <div class="icon">
            <i class="icon fa fa-user-plus" aria-hidden="true"></i>
          </div>
          <div class="title">Usuarios</div>
        </a>
      </li> 

      <li class="<?php if($this->router->fetch_method() === 'contact') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/contact'); ?>">
          <div class="icon">
            <i class="icon fa fa-sticky-note" aria-hidden="true"></i>
          </div>
          <div class="title">Contacto</div>
        </a>
      </li>

      <li class="<?php if($this->router->fetch_method() === 'paginas') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/paginas'); ?>">
          <div class="icon">
            <i class="icon fa fa-file-text" aria-hidden="true"></i>
          </div>
          <div class="title">Páginas</div>
        </a>
      </li>

      <li class="<?php if($this->router->fetch_method() === 'traducciones') { echo 'active'; } ?>">
        <a href="<?php echo base_url(); echo $this->lang->lang() . '/Back_crud/traducciones/';?>">
          <div class="icon">
            <i class="icon fa fa-asl-interpreting"></i>
          </div>
          <div class="title">Traducciones</div>
        </a>
      </li>

      <!--

         <li class="<?php if($this->router->fetch_method() === 'media') { echo 'active'; } ?>">
        <a href="<?php echo base_url(); echo $this->lang->lang() . '/Back_crud/media/';?>">
          <div class="icon">
            <i class="icon fa fa-picture-o fa-4x"></i>
          </div>
          <div class="title">Media</div>
        </a>
      </li>

        <li class="<?php if($this->router->fetch_method() === 'metaseo') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/metaseo'); ?>">
          <div class="icon">
            <i class="icon fa fa-search" aria-hidden="true"></i>
          </div>
          <div class="title">SEO</div>
        </a>
      </li>

      <li class="<?php if($this->router->fetch_method() === 'clients') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/clients'); ?>">
          <div class="icon">
            <i class="icon fa fa-male" aria-hidden="true"></i>
          </div>
          <div class="title">Clients</div>
        </a>
      </li> 

      <li class="<?php if($this->router->fetch_method() === 'routes') { echo 'active'; } ?>">
        <a href="<?php echo base_url(); echo $this->lang->lang() . '/Back_crud/routes/';?>">
          <div class="icon">
            <i class="icon fa fa-road"></i>
          </div>
          <div class="title">Actualizar rutas</div>
        </a>
      </li>
      
      <li class="<?php if($this->router->fetch_method() === 'banners') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/banners'); ?>">
          <div class="icon">
            <i class="icon fa fa-bullhorn" aria-hidden="true"></i>
          </div>
          <div class="title">Banners</div>
        </a>
      </li>
      <li class="<?php if($this->router->fetch_method() === 'categorias') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/categorias'); ?>">
          <div class="icon">
            <i class="icon fa fa-folder-open-o" aria-hidden="true"></i>
          </div>
          <div class="title">Categorías</div>
        </a>
      </li>
      
      <li class="<?php if($this->router->fetch_method() === 'settings') { echo 'active'; } ?>">
        <a href="<?php echo base_url($this->lang->lang().'/Back_crud/settings'); ?>">
          <div class="icon">
            <i class="icon fa fa-cog" aria-hidden="true"></i>
          </div>
          <div class="title">Herramientas</div>
        </a>
      </li>
      -->
      
    </ul>
  </div>

</aside>