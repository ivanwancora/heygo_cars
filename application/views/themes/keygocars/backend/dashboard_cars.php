<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets_admin/css/wancora.css'); ?>"  />

<div class="row">

	<!--
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-green-light" href="<?php echo base_url(); echo $this->lang->lang() . '/Back_crud/noticias/';?>">
		  <div class="card-body">
		    <i class="icon fa fa-briefcase fa-4x"></i>
		    <div class="content">
		      <div class="title">Noticias</div>
		      <div class="value">Noticias</div>
		    </div>
		  </div>
	   </a>
	</div>
	
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-green-light" href="<?php echo base_url(); echo $this->lang->lang() . '/Back_crud/categorias/';?>">
		  <div class="card-body">
		    <i class="icon fa fa-tags"></i>
		    <div class="content">
		      <div class="title">Categorías</div>
		      <div class="value">Categorías</div>
		    </div>
		  </div>
	   </a>
	</div>
	
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-green-light" href="<?php echo base_url(); echo $this->lang->lang() . '/Back_crud/metaseo/';?>">
		  <div class="card-body">
		    <i class="icon fa fa-search"></i>
		    <div class="content">
		      <div class="title">Meta SEO</div>
		      <div class="value">SEO</div>
		    </div>
		  </div>
	   </a>
	</div>

	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      	<a class="card card-banner card-green-light" href="<?php echo base_url(); echo $this->lang->lang() . '/Back_crud/media/';?>">
		  	<div class="card-body">
		    	<i class="icon fa fa-picture-o fa-4x"></i>
			    <div class="content">
			      	<div class="title">Media</div>
			      	<div class="value">Media</div>
			    </div>
		  	</div>
	   	</a>
	</div>

	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-green-light" href="<?php echo base_url(); echo $this->lang->lang() . '/Back_crud/settings/';?>">
		  <div class="card-body">
		    <i class="icon fa fa-cog"></i>
		    <div class="content">
		      <div class="title">Configuración</div>
		      <div class="value">Herramientas</div>
		    </div>
		  </div>
	   </a>
	</div>
	-->

	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-green-light" href="<?php echo base_url(); echo $this->lang->lang() . '/Back_crud/noticias/';?>">
		  <div class="card-body">
		    <i class="icon fa fa-briefcase fa-4x"></i>
		    <div class="content">
		      <div class="title">Noticias</div>
		      <div class="value">Noticias</div>
		    </div>
		  </div>
	   </a>
	</div>
	
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-green-light" href="<?php echo base_url(); echo $this->lang->lang() . '/Back_crud/categorias/';?>">
		  <div class="card-body">
		    <i class="icon fa fa-tags"></i>
		    <div class="content">
		      <div class="title">Categorías</div>
		      <div class="value">Categorías</div>
		    </div>
		  </div>
	   </a>
	</div>
	
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-green-light" href="<?php echo base_url(); echo $this->lang->lang() . '/Back_crud/marcas/';?>">
		  <div class="card-body">
		    <i class="icon fa fa-search"></i>
		    <div class="content">
		      <div class="title">Marcas</div>
		      <div class="value">Marcas</div>
		    </div>
		  </div>
	   </a>
	</div>

	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
		<a class="card card-banner card-green-light" href="<?php echo base_url(); echo $this->lang->lang() . '/Back_crud/usuarios/';?>">
		  <div class="card-body">
		    <i class="icon fa fa-user-plus fa-4x"></i>
		    <div class="content">
		      <div class="title">Gestión Usuarios</div>
		      <div class="value">Usuarios</div>
		    </div>
		  </div>
	   	</a>
	</div>	

	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-green-light" href="<?php echo base_url(); echo $this->lang->lang() . '/Back_crud/flotas_tipo/';?>">
		  <div class="card-body">
		    <i class="icon fa fa-car"></i>
		    <div class="content">
		      <div class="title">Tipo de flotas</div>
		      <div class="value">Tipo de flotas</div>
		    </div>
		  </div>
	   </a>
	</div>

	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-green-light" href="<?php echo base_url(); echo $this->lang->lang() . '/Back_crud/contact/';?>">
		  <div class="card-body">
		    <i class="icon fa fa-sticky-note"></i>
		    <div class="content">
		      <div class="title">Contacto</div>
		      <div class="value">Contacto</div>
		    </div>
		  </div>
	   </a>
	</div>	

	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-green-light" href="<?php echo base_url(); echo $this->lang->lang() . '/Back_crud/paginas/';?>">
		  <div class="card-body">
		    <i class="icon fa fa-file-text"></i>
		    <div class="content">
		      <div class="title">Gestión Páginas</div>
		      <div class="value">Páginas</div>
		    </div>
		  </div>
	   </a>
	</div>	

	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      	<a class="card card-banner card-green-light" href="<?php echo base_url(); echo $this->lang->lang() . '/Back_crud/traducciones/';?>">
		  	<div class="card-body">
		    	<i class="icon fa fa-asl-interpreting"></i>
			    <div class="content">
			      	<div class="title">Traducciones</div>
			      	<div class="value">Traducciones</div>
			    </div>
		  	</div>
	   	</a>
	</div>
		
</div>

