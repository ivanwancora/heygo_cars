<div class="app-container">
  <nav class="navbar navbar-default" id="navbar">
  <div class="container-fluid">
    <div class="navbar-collapse collapse in">
      <ul class="nav navbar-nav navbar-mobile">
        <li>
          <button type="button" class="sidebar-toggle">
            <i class="fa fa-bars"></i>
          </button>
        </li>
        <li class="logo">
          <a class="navbar-brand" href="<?php echo base_url($this->lang->lang().'/Back_dashboard/'); ?>"><span class="highlight"></span></a>
        </li>
        <li>
          <button type="button" class="navbar-toggle">
             <img class="profile-img" height="75" src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/logo/logo-header.svg'); ?>">
          </button>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-left">
        <?php
            $cursect = "";
            if($this->router->fetch_class() === 'Back_dashboard') { 
              $cursect = "Dashboard &nbsp;";
              if($this->router->fetch_method() === 'cars') { 
                $cursect = $cursect . "<b>HeyGo CARS (VENTA)</b>";
              }
              if($this->router->fetch_method() === 'rent') { 
                $cursect = $cursect . "<b>HeyGo RENT (ALQUILER)</b>";
              }
            } 
            else if($this->router->fetch_method() === 'categorias') { 
              $cursect = "Categorías";
            }
            else if($this->router->fetch_method() === 'usuarios') { 
              $cursect = "Usuarios";
            }
            else if($this->router->fetch_method() === 'contact') { 
              $cursect = "Contacto";
            }
            else if($this->router->fetch_method() === 'paginas') { 
              $cursect = "Páginas";
            }
            else if($this->router->fetch_method() === 'metaseo') { 
              $cursect = "SEO";
            }
            else if($this->router->fetch_method() === 'media') { 
              $cursect = "Media";
            }
            else if($this->router->fetch_method() === 'marcas') { 
              $cursect = "Marcas";
            }
            else if($this->router->fetch_method() === 'settings') { 
              $cursect = "Herramientas";
            }
            else if($this->router->fetch_method() === 'slider') { 
              $cursect = "Visor dinámico Imágenes";
            }
            else if($this->router->fetch_method() === 'banners') { 
              $cursect = "Banners";
            }
            else if($this->router->fetch_method() === 'centros') { 
              $cursect = "Centros";
            }
            else if($this->router->fetch_method() === 'equipo') { 
              $cursect = "Equipo";
            }
            else if($this->router->fetch_method() === 'fcontacts') { 
              $cursect = "Formulario Contacto";
            }
            else if($this->router->fetch_method() === 'noticias') { 
              $cursect = "Noticias";
            }
            else if($this->router->fetch_method() === 'routes') { 
              $cursect = "Actualizar rutas proyecto";
            }
            else if($this->router->fetch_method() === 'newsletter') { 
              $cursect = "Newsletter";
            }
            else if($this->router->fetch_method() === 'destacados') { 
              $cursect = "Destacados";
            }
        ?>
        <li class="navbar-title">Administración <?php if($cursect != "") echo '/ '.$cursect; ?></li>        
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown profile">
          <a href="#" class="dropdown-toggle"  data-toggle="dropdown">
            <img height="75" src="<?php echo base_url('assets/'.$this->config->item('versionat').'/img/logo/logo-header.svg'); ?>">
            <div class="title">Perfil</div>
          </a>
          <div class="dropdown-menu">
            <div class="profile-info">
              <h4 class="username">Administrador</h4>
            </div>
            <ul class="action">
              <li>
                <a style="cursor:pointer;" onclick="logout();">Salir</a>
              </li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>



