<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>404 Page Not Found</title>

</style>
</head>
<body>
	<link rel="stylesheet" type="text/css" href="<?php //echo base_url('assets/DataTables/datatables.min.css'); ?>"  />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/grocery_crud/css/jquery_plugins/chosen/chosen.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets_admin/css/wancora.css'); ?>"  />
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>" ></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/additional-methods.min.js'); ?>" ></script>
	<script type="text/javascript" src="<?php //echo base_url('assets/DataTables/datatables.min.js'); ?>" ></script>
	<script type="text/javascript" src="<?php echo base_url('assets/grocery_crud/js/jquery_plugins/jquery.chosen.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/sweetalert2.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/app.js'); ?>" ></script>	
	<div class="body-crud">
		<div class="ui-widget-content ui-corner-all datatables">
			<h1><?php echo $heading; ?></h1>
			<?php echo $message; ?>
		</div>
	</div>
	</div>
</body>
</html>