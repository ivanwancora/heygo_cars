<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php echo $this->config->item('project_name'); ?></title>
    <style type="text/css">
        a {
            color: #7697b4;
            text-decoration: underline;
        }
        
        body {
            font-family: helvetica, arial;
            font-size: 14px;
            text-align: center
        }
        
        .bodyMail {
            width: 600px;
            margin: 20px auto;
            text-align: left;
        }
        
        .mailContent {
            text-align: left;
            margin-bottom: 30px;
            margin-top: 30px;
        }
        
        h4 {
            font-size: 16px;
        }

        table.info td {
            border-bottom:  1px solid #eaeaea;
            padding:  10px 0;
        }
        table.info tr:last-child td{
            border-bottom:  0px solid #eaeaea;
        }
        
        @media only screen and (max-width: 630px),
        screen and (max-width: 600px) {
            table[class="responsive"] {
                width: 100% !important;
            }
            td[class="responsive"] {
                width: 100% !important;
            }
        }
    </style>

</head>

<body bgcolor="#ffffff" style="padding:0; margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:100%; background-color:#ffffff;">
    <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" class="body" style="width: 600px; max-width: 600px;margin: 0 auto; background-color:#ffffff;">
        <tr>
            <td width="100%">
                <table class="responsive" border="0" width="600" cellspacing="0" cellpadding="0" align="left" bgcolor="#ffffff" style="border: 2px solid #7697b4;">
                    <tr>
                        <td bgcolor="#ffffff" valign="top"  align="center" style="line-height:1px; text-align: center; padding-top: 10px; padding-bottom: 5px;">
                            <img src="<?php echo base_url('assets/img/logo.jpg');?>" width="200" style="margin: 0 auto; width: 200px;display: block;" border="0" alt="<?php echo $this->config->item('project_name'); ?>" title="<?php echo $this->config->item('project_name'); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" valign="top" align="left" style="line-height:18px; font-size: 14px; font-family: 'Arial', sans-serif; color: #333333;">
                            <table class="responsive" width="500" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" style="width: 500px; margin: 0 auto;">
                                <tr>
                                    <td>
                                        <h3 align="center" style="text-align: center; line-height:18px; font-size: 18px; font-family: 'Arial', sans-serif; color: #87868A; font-weight:bold;"><?=lang('Contact')?></h3>
                                    </td>
                                </tr>    
                                <tr>
                                    <td valign="top" height="20">
                
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <p align="center" style="text-align: center; line-height:18px; font-size: 14px; font-family: 'Arial', sans-serif; color: #000000;"><?=lang('Contact info')?></p>
                                    </td>
                                </tr>    
                                <tr>
                                    <td valign="top" height="20">
                
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">       
                                        <h5 style="line-height:18px; font-size: 14px; font-family: 'Arial', sans-serif; color: #87868A; font-weight:bold; border-bottom: 1px solid #9A999E; padding: 10px 0px;"><?=lang('News_Datos_contact')?></h5>    
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="400"  class="info" align="center" style="width: 400px; margin: 0 auto;">
                                            <tr>
                                                <td style="text-align: left;"><b><?=lang('Name')?></b>: <?php if(isset($nombre)) echo $nombre; ?></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;"><b><?=lang('Telephone')?></b>: <?php if(isset($telefono)) echo $telefono; ?></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;"><b><?=lang('Email')?></b>: <?php if(isset($email)) echo $email; ?></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;"><b><?=lang('Subject')?></b>: <?php if(isset($motivo)) echo $motivo; ?></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;"><b><?=lang('Message')?></b>: <?php if(isset($mensaje)) echo $mensaje; ?></td>
                                            </tr>                                            
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" height="20">                
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="text-align:center; color: #87868A; font-size: 14px; font-family: 'Arial', sans-serif;">
                                        <?=lang('News_mas_info')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" height="20">                
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="text-align:center;">
                                        <a href="<?=lang('Message')?>" style="text-align:center; color: #87868A; font-size: 14px; font-family: 'Arial', sans-serif;">
                                            <?=lang('Message')?>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" height="50">                
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" height="30">               
            </td>
        </tr>
    </table>
</body>
</html>