<?php 
	$this->load->view($this->config->item('theme_path_frontend'). 'header');
	$this->load->view($this->config->item('theme_path_frontend'). 'nav', $this->data);
?>
<section class="auth basic-main">
	<div class="hero-content hero-grey hero-section">
        <h2 class="headline"><?php echo lang('Recover password');?></h2>
        <span class="divider"></span>
    </div>

	<div class="w-content features login center">

		<p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
		
			<?php echo form_open("auth/forgot_password", "class='sec6_5_form'");?>
					<p id="infoMessage"><?php echo $message;?></p>	
					<div class="grid-row">
						<div class="grid-xs-12">	
							<label><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label>
						</div>	
					</div>
					<div class="grid-row">
						<div class="grid-xs-12">
							<?php echo form_input($identity);?>						
						</div>	
					</div>
					<div class="grid-row">
						<div class="grid-xs-12">
							<?php echo form_submit('submit', lang('forgot_password_submit_btn'), "class='button'");?>
						</div>
					</div>
			<?php echo form_close();?>
	</div>
</section>
<?php
	$this->load->view($this->config->item('theme_path_frontend'). 'footer',$this->data);
?>