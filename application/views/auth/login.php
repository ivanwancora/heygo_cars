<section class="p-auth-page">
	<div class="w-content">	
		<?php echo form_open("auth/login" , "class='sec6_5_form'");?>
		  <div class="o-form--auth">  
			<p><?php echo lang('login_identity_label', 'identity');?></p>
			<?php echo form_input($identity, "class='form-control'");?>
			<p><?php echo lang('login_password_label', 'password');?></p>
			<?php echo form_input($password, "class='form-control'");?>
			<div id="infoMessage"><?php echo $message;?></div>
			<div class="remember">
				<p>
					<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
					<?php echo lang('login_remember_label', 'remember');?>
				</p>
			</div>
			<?php echo form_submit('submit', lang('login_submit_btn'),"class='a-button--auth'");?><br>


			<p class="center forgot"><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>
		  </div>
		<?php echo form_close();?>
	</div>
</section>