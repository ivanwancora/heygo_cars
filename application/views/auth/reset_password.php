<section class="auth basic-main">
	<div class="hero-content hero-grey hero-section">
		<h1><?php echo lang('reset_password_heading');?></h1>
		<span class="divider"></span>
    </div>

	

	<div class="w-content features login center">

		<?php echo form_open('auth/reset_password/' . $code, "class='sec6_5_form'");?>

			<p><div id="infoMessage"><?php echo $message;?></div></p>
			<p>
				<label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label> <br />
				<?php echo form_input($new_password, '', 'id="new_password" ');?>
			</p>

			<p>
				<?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?> <br />
				<?php echo form_input($new_password_confirm, '', 'id="new_password_confirm" ');?>
			</p>

			<?php echo form_input($user_id);?>
			<?php echo form_hidden($csrf); ?>

			<p><?php echo form_submit('submit', lang('reset_password_submit_btn'), "class='button'");?></p>

		<?php 
			echo form_close();
			
		?>

	</div>

</section>