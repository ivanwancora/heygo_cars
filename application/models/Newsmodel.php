<?php

class NewsModel extends CI_Model{


	function __construct(){
		parent::__construct();
		
	}

	public function getTotalNews($categoria=null) 
    {
    	$this->load->database();   
    	$this->db->where('n.status', 'active');
    	if(isset($categoria)) $this->db->where('( n.id IN (SELECT nc.idnew FROM news_categories nc JOIN categories c ON c.id = nc.idcategory where c.name_slug_'.$this->lang->lang().' = \''.$categoria.'\') )', null, false);	
        return $this->db->count_all_results('news n');
    }

	public function getNews($id=null, $orderby=null, $limit=null, $start=null, $categoria=null){
		$this->load->database();
		$this->db->select('n.*');	
		$this->db->select('DATE_FORMAT(n.publication_date, \'%d/%m/%Y\') AS publication_date_format');				
		$this->db->from('news n');	
		$this->db->where('n.title_'.$this->lang->lang().' !=', null);
		$this->db->where('n.title_'.$this->lang->lang().' !=', "");	
		$this->db->where('n.content_'.$this->lang->lang().' !=', null);
		$this->db->where('n.content_'.$this->lang->lang().' !=', "");	
		$this->db->where('n.status', 'active');	
		$currentdate = date('Y-m-d H:i:s');
		$this->db->where('(n.publication_date <= "'.$currentdate.'")', null, false);		
		if(isset($categoria)) $this->db->where('( n.id IN (SELECT nc.idnew FROM news_categories nc JOIN categories c ON c.id = nc.idcategory where c.name_slug_'.$this->lang->lang().' = \''.$categoria.'\') )', null, false);
		if(isset($id)) $this->db->where('n.id', $id);	
		if(isset($limit) and isset($start)) $this->db->limit($limit, $start);
		if(isset($orderby)) $this->db->order_by($orderby, "desc");
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	public function getBackNew($id=null){
		$this->load->database();
		$this->db->select('*');	
		$this->db->select('DATE_FORMAT(publication_date, \'%d %M %Y\') AS publication_date_format');
		$this->db->select('DATE_FORMAT(publication_date, \'%d\') AS fp_day');		
		$this->db->select('DATE_FORMAT(publication_date, \'%M\') AS fp_month');
		$this->db->select('DATE_FORMAT(publication_date, \'%Y\') AS fp_year');		
		$this->db->from('news');
		$this->db->where('title_'.$this->lang->lang().' !=', null);
		$this->db->where('title_'.$this->lang->lang().' !=', "");	
		$this->db->where('content_'.$this->lang->lang().' !=', null);
		$this->db->where('content_'.$this->lang->lang().' !=', "");
		$this->db->where('status', 'active');
		$this->db->where('id <', $id);
		$currentdate = date('Y-m-d H:i:s');
		$this->db->where('(publication_date <= "'.$currentdate.'")', null, false);		
		$this->db->limit(1);
		$this->db->order_by('id', "desc");					
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->row());
	}

	public function getForwardNew($id=null){
		$this->load->database();
		$this->db->select('*');	
		$this->db->select('DATE_FORMAT(publication_date, \'%d %M %Y\') AS publication_date_format');
		$this->db->select('DATE_FORMAT(publication_date, \'%d\') AS fp_day');		
		$this->db->select('DATE_FORMAT(publication_date, \'%M\') AS fp_month');
		$this->db->select('DATE_FORMAT(publication_date, \'%Y\') AS fp_year');		
		$this->db->from('news');
		$this->db->where('title_'.$this->lang->lang().' !=', null);
		$this->db->where('title_'.$this->lang->lang().' !=', "");	
		$this->db->where('content_'.$this->lang->lang().' !=', null);
		$this->db->where('content_'.$this->lang->lang().' !=', "");
		$this->db->where('status', 'active');
		$this->db->where('id >', $id);
		$currentdate = date('Y-m-d H:i:s');
		$this->db->where('(publication_date <= "'.$currentdate.'")', null, false);		
		$this->db->limit(1);	
		$this->db->order_by('id', "asc");				
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->row());
	}

	public function getFeaturedNew(){
		$this->load->database();
		$this->db->select('*');	
		$this->db->select('DATE_FORMAT(publication_date, \'%d-%m-%Y\') AS publication_date_format');	
		$this->db->select('DATE_FORMAT(publication_date, \'%d\') AS fp_day');		
		$this->db->select('DATE_FORMAT(publication_date, \'%M\') AS fp_month');
		$this->db->select('DATE_FORMAT(publication_date, \'%Y\') AS fp_year');			
		$this->db->from('news');
		$this->db->where('title_'.$this->lang->lang().' !=', null);
		$this->db->where('title_'.$this->lang->lang().' !=', "");	
		$this->db->where('content_'.$this->lang->lang().' !=', null);
		$this->db->where('content_'.$this->lang->lang().' !=', "");
		$this->db->where('status', 'active');
		$currentdate = date('Y-m-d H:i:s');
		$this->db->where('(publication_date <= "'.$currentdate.'")', null, false);		
		$this->db->limit(1);
		$this->db->order_by('publication_date', "desc");			
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	public function getNewsByUrlSlug($urlslug){
		$this->load->database();
		$this->db->select('*');	
		$this->db->select('DATE_FORMAT(publication_date, \'%d-%m-%Y\') AS publication_date_format');	
		$this->db->select('DATE_FORMAT(publication_date, \'%d\') AS fp_day');		
		$this->db->select('DATE_FORMAT(publication_date, \'%M\') AS fp_month');
		$this->db->select('DATE_FORMAT(publication_date, \'%Y\') AS fp_year');			
		$this->db->from('news');	
		$this->db->where('status', 'active');		
		$currentdate = date('Y-m-d H:i:s');
		$this->db->where('(publication_date <= "'.$currentdate.'")', null, false);				
		if(isset($urlslug)) {
			$this->db->where('url_slug_'.$this->lang->lang(), $urlslug);			
			$this->db->where('title_'.$this->lang->lang().' !=', null);
			$this->db->where('title_'.$this->lang->lang().' !=', "");	
			$this->db->where('content_'.$this->lang->lang().' !=', null);
			$this->db->where('content_'.$this->lang->lang().' !=', "");				
		}	
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	public function getCategories($id=null){
		$this->load->database();
		$this->db->select('*');					
		$this->db->from('categories');					
		if(isset($id)) $this->db->where('id', $id);
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	public function getCategoriaByNameSlug($name_slug=null){
		$this->load->database();
		$this->db->select('c.id, c.name_slug_es, c.name_slug_en, c.name_es, c.name_en, c.created_at, c.updated_at');					
		$this->db->from('categories c');			
		$this->db->where('c.name_slug_'.$this->lang->lang(), $name_slug);	
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->row());
	}

	public function getCategoriesByIDNew($idnew=null){
		$this->load->database();
		$this->db->select('c.id, c.name_slug_es, c.name_slug_en, c.name_es, c.name_en, c.created_at, c.updated_at');					
		$this->db->from('news_categories nc');
		$this->db->join('categories c', 'c.id = nc.idcategory');			
		$this->db->where('nc.idnew', $idnew);	
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}


	public function insertNew($array_data){
		if(is_array($array_data)){
			$this->load->database();		
			$this->db->insert('news', $array_data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function updateCategory($id, $array_data){
		if(isset($id) and is_array($array_data)){
			$this->load->database();			
			$this->db->where('id', $id);
			return $this->db->update('categories',$array_data);
		}
		return false;
	}
		
	public function updateNew($id, $array_data){
		if(isset($id) and is_array($array_data)){
			$this->load->database();			
			$this->db->where('id', $id);
			return $this->db->update('news',$array_data);
		}
		return false;
	}

	public function deleteNew($id){
		if(isset($id)){
			$this->load->database();
			$this->db->where('id', $id);
			return $this->db->delete('news');
		}
		return false;
	}

	
	
}


?>