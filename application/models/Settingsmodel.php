<?php

class SettingsModel extends CI_Model{


	function __construct(){
		parent::__construct();		
	}

	public function getSettings($nombre){
		if(isset($nombre)){
			$this->db->select('*');				
			$this->db->from('settings');				
			$this->db->where('nombre', $nombre);				
			$query = $this->db->get();
			if($query->num_rows() == 0){
				return null;
			}
			return($query->row());
		}
		return null;
	}
	
}


?>