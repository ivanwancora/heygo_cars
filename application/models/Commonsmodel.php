<?php

class CommonsModel extends CI_Model{

	//$this->output->enable_profiler(TRUE);	

	function __construct(){
		parent::__construct();		 
	}
		
	public function getByID($table=null, $id=null, $lang=null){
		if(!is_null($table)){
			$this->db->select('*');				
			$this->db->from($table);
			$this->db->where('id', $id); 
			$query = $this->db->get();
			if($query->num_rows() == 0){
				return null;
			}
			return($query->row());
		}
		return null;
	}

	public function getAll($table=null, $order=null, $idioma=null){
		if(!is_null($table)){
			$this->db->select('*');				
			$this->db->from($table);
			if(!is_null($idioma)){ $this->db->where('lang', $idioma); }
			if(!is_null($order)){ $this->db->order_by($order, 'ASC');  }
				
			$query = $this->db->get();
			if($query->num_rows() == 0){
				return null;
			}
			return($query->result());
		}
		return null;
	}

	public function insert($table, $array_data){
		if(is_array($array_data)){
			$this->db->insert($table, $array_data);
			return $this->db->insert_id();
		}
		return false;
	}	
	
	public function insert_batch($table, $array_data){
		if(is_array($array_data)){
			$this->db->insert_batch($table, $array_data);
			return $this->db->insert_id();
		}
		return false;
	}	

	public function truncate($table){
		if(isset($table)){
			$this->db->truncate($table);
			return true;
		}
		return false;
	}	

	public function update($id, $table, $array_data){
		if(isset($id) and is_array($array_data)){
			$this->db->where('id', $id);
			return $this->db->update($table,$array_data);
		}
		return false;
	}

	public function updateOficinas($id, $table, $array_data){
		if(isset($id) and is_array($array_data)){
			$this->db->where('idOficina', $id);
			return $this->db->update($table,$array_data);
		}
		return false;
	}

	public function getNews($id=null, $orderby=null, $limit=null, $start=null, $categoria=null){
		$this->db->select('n.*, c.*');	
		$this->db->select('DATE_FORMAT(n.publication_date, \'%d/%m/%Y\') AS publication_date_format');				
		$this->db->from('news n');	
		$this->db->join('news_categories nc', 'nc.idnew = n.id');
		$this->db->join('categories c', 'c.id = nc.idcategory');			
		$this->db->where('n.title_'.$this->lang->lang().' !=', null);
		$this->db->where('n.title_'.$this->lang->lang().' !=', "");	
		$this->db->where('n.content_'.$this->lang->lang().' !=', null);
		$this->db->where('n.content_'.$this->lang->lang().' !=', "");	
		$this->db->where('n.status', 'active');	
		$currentdate = date('Y-m-d H:i:s');
		$this->db->where('(n.publication_date <= "'.$currentdate.'")', null, false);		
		if(isset($id)) $this->db->where('n.id', $id);	
		if(isset($limit) and isset($start)) $this->db->limit($limit, $start);
		if(isset($orderby)) $this->db->order_by($orderby, "desc");
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	public function getCentros($idioma){
		$this->db->select('id, title_'.$idioma. ' as title, place_'.$idioma. ' as place, address_'.$idioma. ' as address, latitude, longitude, schedule_'.$idioma. ' as schedule, phone, email, image');				
		$this->db->from('centros');
		$this->db->where('status', 'active'); 
		$this->db->order_by('title_'.$idioma, 'ASC'); 		
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	public function getCentrosImagenes(){
		$this->db->select('idCentro, count(idCentro) as numIdCentro');				
		$this->db->from('centros_imagenes');
		$this->db->where('status', 'active'); 
		$this->db->group_by('idCentro'); 		
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	public function getCentrosDealer($dealer=null, $idioma=null){
		if(isset($dealer)){
			$this->db->select('id, title_'.$idioma. ' as title, place_'.$idioma. ' as place, address_'.$idioma. ' as address, latitude, longitude, schedule_'.$idioma. ' as schedule, phone, email, image');				
			$this->db->from('centros');
			$this->db->where('dealerid', $dealer); 
			$this->db->where('status', 'active'); 
			$this->db->order_by('title_'.$idioma, 'ASC'); 
			
			$query = $this->db->get();
			if($query->num_rows() == 0){
				return null;
			}
			return($query->row());
		}		
	}

	public function getDestacados($idioma){
		$this->db->select('id, title_'.$idioma. ' as title, content_'.$idioma. ' as content, image');				
		$this->db->from('featureds');
		$this->db->where('status', 'active'); 
		$this->db->order_by('title_'.$idioma, 'ASC'); 		
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	public function getMarcas($idioma){
		$this->db->select('id, title_'.$idioma. ' as title, image');				
		$this->db->from('marcas');
		$this->db->where('status', 'active'); 
		$this->db->order_by('title_'.$idioma, 'ASC'); 		
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	public function getSliders($idioma){
		$this->db->select('id, content_'.$idioma. ' as content, image, image_mobile, image_tablet, button, text_button_'.$idioma. ' as text_button, link_button_'.$idioma. ' as link_button');				
		$this->db->from('slider');
		$this->db->where('status', 'active'); 
		$this->db->order_by('created_at', 'ASC'); 		
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	public function getAllEquipo($idioma=null){
		$this->db->select('id, name_'.$idioma. ' as name, job_'.$idioma. ' as job, image, status');
		$this->db->from('equipo');
		$this->db->where('status', 'active'); 
		$this->db->order_by('orden', 'ASC'); 		
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	public function getTipoFlotas($idioma){
		$this->db->select('id, title_'.$idioma. ' as title, icono');				
		$this->db->from('flotas_tipo');
		$this->db->where('status', 'active'); 
		//$this->db->order_by('title_'.$idioma, 'ASC'); 		
		$this->db->order_by('orden', 'ASC'); 		
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	public function getCategoriesByIDNew($idnew=null){
		$this->db->select('c.id, c.name_slug_es, c.name_slug_en, c.name_es, c.name_en, c.created_at, c.updated_at');					
		$this->db->from('news_categories nc');
		$this->db->join('categories c', 'c.id = nc.idcategory');			
		$this->db->where('nc.idnew', $idnew);	
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	public function getAllFlotas($table=null, $order=null, $idioma=null){
		if(!is_null($table)){
			$this->db->select('*,url_slug_'.$idioma. ' as url_slug');			
			$this->db->from($table);
			if(!is_null($idioma)){ $this->db->where('lang', $idioma); }
			if(!is_null($order)){ $this->db->order_by($order, 'DESC');  }
			$this->db->where('available', '1');
			//$this->db->limit('12,0');

			$query = $this->db->get();
			if($query->num_rows() == 0){
				return null;
			}
			return($query->result());
		}
		return null;
	}

	public function getFlotasPagination($vehicleid=null, $orderby=null, $limit=null, $start=null){
		$this->db->select('*,url_slug_'. $this->lang->lang(). ' as url_slug');	
		$this->db->from('flotas');	
		$this->db->where('available', 1);	
		$this->db->where('lang', $this->lang->lang());	
		if(isset($vehicleid)) $this->db->where('vehicleid', $vehicleid);	
		if(isset($limit) and isset($start)) $this->db->limit($limit, $start);
		//if(!is_null($cartype)) $this->db->where('cartype', $cartype);	
		if(isset($orderby)) $this->db->order_by($orderby, "desc");
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	public function getTotalFlotas() 
    {
    	$this->db->where('available', 1);	 
        return $this->db->count_all_results('flotas');
    }


	public function getSearchFlotas($sql, $counter=null){

		if(is_null($counter)){
			$query = $this->db->query("SELECT *, url_slug_".$this->lang->lang(). " as url_slug FROM flotas $sql");
			return $query->result();
		}else{
			$queryCount = $this->db->query("SELECT count(*) as total FROM flotas $sql");	
			return $queryCount->row()->total;
		}
				
	}

	public function getAllFlotasByVehicleIds($vehicleids=null, $idioma=null){
		if(!is_null($vehicleids)){
			$this->db->select('*,url_slug_'.$idioma. ' as url_slug');			
			$this->db->from('flotas');
			if(!is_null($idioma)){ $this->db->where('lang', $idioma); }
			$this->db->where('available', '1');
			$this->db->where_in('vehicleid', $vehicleids);
			
			$query = $this->db->get();
			if($query->num_rows() == 0){
				return null;
			}
			return($query->result());
		}
	}

	public function getAllFlotasHistorico($table=null, $order=null, $idioma=null){
		if(!is_null($table)){
			$this->db->select('*');			
			$this->db->from($table);
			if(!is_null($idioma)){ $this->db->where('lang', $idioma); }
			if(!is_null($order)){ $this->db->order_by($order, 'DESC');  }
			
			$query = $this->db->get();
			if($query->num_rows() == 0){
				return null;
			}
			return($query->result());
		}
		return null;
	}

	public function getFlotaBySlug($url_slug=null, $idioma=null){
		$this->db->select('f.*,f.url_slug_'.$idioma. ' as url_slug, ft.icono as icono');				
		$this->db->from('flotas as f');
		$this->db->join('flotas_tipo ft', 'ft.title_'.$idioma.' = f.bodytype', 'LEFT');
		$this->db->where('url_slug_'.$idioma, $url_slug);	
		if(!is_null($idioma)){ $this->db->where('lang', $idioma); }			
		
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->row());
	}

	public function getFlotaBySlugAndBrand($url_slug=null, $idioma=null){
		$sql = "SELECT m.image, m.title_".$idioma." as title FROM marcas as m, flotas as f WHERE m.title_".$idioma." like concat('%',f.make,'%') and f.url_slug_".$idioma." = '".$url_slug."'";
		
		$query = $this->db->query($sql);
		if($query->num_rows() == 0){
			return null;
		}
		return($query->row()); 
	}

	public function getAllFotosFlotaByVehicleId($vehicleid=null, $idioma=null){
		$this->db->select('*');			
		$this->db->from('flotas_images');
		
		$this->db->where('vehicleid', $vehicleid); 
		$this->db->where('lang', $idioma); 
		
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	public function getFlotaByBodyType($bodytype=null, $idioma=null){
		$this->db->select('*, url_slug_'.$idioma. ' as url_slug');			
		$this->db->from('flotas');
		$this->db->where('bodytype', $bodytype); 
		$this->db->where('lang', $idioma); 
		$this->db->limit('3,0'); 
		
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	public function getFlotaByCarType($cartype=null, $idioma=null){
		$this->db->select('*, url_slug_'.$idioma. ' as url_slug');			
		$this->db->from('flotas');
		$this->db->where('lang', $idioma); 
		$this->db->like('cartype', $cartype, 'both'); 
		$this->db->limit('3,0'); 
		$this->db->order_by('id','random');
		
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());
	}

	
	public function cronjobs_byslus_auto($flotas_new, $flotas_images, $flotas_old){

		$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 
		$this->db->trans_begin();		
		
		//agafem flotas_dev i copiem a la taula històric
		if(isset($flotas_old)){ 
			$this->db->insert_batch('flotas_historico', $flotas_old);  
		
			//truncate a la taula antiga
			$this->db->truncate('flotas');
		}
				
		if(isset($flotas_new)){ 
			//copiem nous valors de flota a la taula
			$this->db->insert_batch('flotas', $flotas_new); 
		}

		if(isset($flotas_new)){ 
			//copiem imatges a la taula flota_images
			$this->db->insert_batch('flotas_images', $flotas_images); 
		}

		if($this->db->trans_status() === FALSE)
        {
			$this->set_flash_error();
			$this->db->trans_rollback();
            return 'FALSE';  
		}else{
			$this->db->trans_commit();
			return 'TRUE'; 
		}
		
	}
	
	public function getMarcasModel(){
		$this->db->select('*');
		$this->db->from('marcas_model');
		$this->db->order_by('make', 'ASC'); 		
		$this->db->group_by('model');
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());		
	}
	
	public function getMarcasModelFlota(){
		$this->db->select('make, model');
		$this->db->from('flotas');
		$this->db->where('available', '1'); 
		$this->db->order_by('make', 'ASC'); 		
		$this->db->group_by('model');
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());		
	}

	//public function cronjobs_flotas_auto($flotas_new, $flotas_old){
	public function cronjobs_flotas_auto($flotas_new){

		//$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 
		//$this->db->trans_begin();		
		
		//agafem flotas_dev i copiem a la taula històric
		/*if(isset($flotas_old)){ 
			$this->db->insert_batch('flotas_historico', $flotas_old);  
		
			//truncate a la taula antiga
			$this->db->truncate('flotas');
		}*/
				

		if(isset($flotas_new)){ 
			//copiem nous valors de flota a la taula
			$this->db->truncate('flotas');
			$this->db->insert_batch('flotas', $flotas_new); 
		}

		/*if($this->db->trans_status() === FALSE)
        {
			$this->set_flash_error();
			$this->db->trans_rollback();
            return 'FALSE';  
		}else{
			$this->db->trans_commit();
			return 'TRUE'; 
		}*/
		
	}

	public function updatelangfile($language='es', $tipus='cars'){      
		
		if($tipus=='cars'){
			$query=$this->db->get('traducciones');        
		}elseif($tipus=='rent'){
			$query=$this->db->get('rent_traducciones');        
		}

		//$query=$this->db->get('rent_traducciones');        

        if($query->num_rows() > 0){  
        	$my_lang = 'spanish';
	        if($language == 'es') $my_lang = 'spanish';
	        else if($language == 'en') $my_lang = 'english';
	        
        	$lang=array();
	        $currentdate = date('Y-m-d');
	        $langstr="<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
            /**
            *
            * Created:  ".$currentdate." by Wancora
            *
            * Description:  ".$my_lang." language file for general views
            *
			*/"."\n\n";
			
	        foreach ($query->result() as $row){
				$resStr = str_replace('"', '', $row->$language); 
	            
				if(isset($row->enum)) $langstr.= "\$lang['".$row->clave.'_'.$row->enum."'] = \"".$resStr."\";"."\n";
				else $langstr.= "\$lang['".$row->clave."'] = \"".$resStr."\";"."\n";
				
			}
			
			if($tipus=='cars'){
				write_file('./application/language/'.$my_lang.'/general_lang.php', $langstr);
			}elseif($tipus=='rent'){
				write_file('./application/language/'.$my_lang.'/general_lang_rent.php', $langstr);
			}

			//write_file('./application/language/'.$my_lang.'/general_lang.php', $langstr);
			//EXT: The PHP file extension
			//FCPATH: Path to the front controller (this file) (root of CI)
			//SELF: The name of THIS file (index.php)
			//BASEPATH: Path to the system folder
			//APPPATH: The path to the "application" folder

	    }
	    return true;
	}
	
	public function getReservaByID($reserva_id=null){
		if(!is_null($reserva_id)){
			$this->db->select('fc.*, fl.vehicleid as vehicleid, fl.finalprice as finalprice, fl.images as images, fl.make as make, fl.model as model, fl.finalprice as preciocoche');
			$this->db->from('fcontacts fc');
			$this->db->join('flotas fl', 'fl.vehicleid = fc.wwid', 'LEFT');	
			$this->db->where('reserva_id', $reserva_id); 
			$query = $this->db->get();
			if($query->num_rows() == 0){
				return null;
			}
			return($query->row());		
		}
		return null;
	}

	public function updateComandes($reserva_id, $array_data){
		if(isset($reserva_id) and is_array($array_data)){
			$this->db->where('reserva_id', $reserva_id);
			return $this->db->update('fcontacts', $array_data);
		}
		return false;
	}

	public function updateFlotas($reserva_id, $array_data){
		if(isset($reserva_id) and is_array($array_data)){
			$this->db->where('vehicleid', $reserva_id);
			return $this->db->update('flotas', $array_data);
		}
		return false;
	}	
	
}

?>