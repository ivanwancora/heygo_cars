<?php

class MetaseoModel extends CI_Model{

	function __construct(){
		parent::__construct();		
	}

	public function getMetaseo(){
		$this->db->select('*');				
		$this->db->from('metaseo');	
		
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->row());
	}
	
}

?>