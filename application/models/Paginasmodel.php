<?php

class PaginasModel extends CI_Model{


	function __construct(){
		parent::__construct();		
	}

	public function getPagina($idpagina=null){
		if(isset($idpagina)){
			$this->db->select('*');			
			$this->db->from('paginas');	
			$this->db->where('id', $idpagina);	
			$this->db->where('status', 'active');
			$this->db->order_by("id", "asc"); 
			$query = $this->db->get();
			if($query->num_rows() == 0){
				return null;
			}
			return($query->row());
		}
		
	}

	public function getPaginaUrlSlug($url_slug_param=null){
		if(isset($url_slug_param)){
			$this->db->select('id,nombre_'.$this->lang->lang().' as nombre, contenido_'.$this->lang->lang().' as contenido, url_slug_'.$this->lang->lang().' as url_slug');			
			$this->db->from('paginas');	
			$this->db->where('url_slug_'.$this->lang->lang(), $url_slug_param);	
			$this->db->where('status', 'active');
			
			$query = $this->db->get();
			if($query->num_rows() == 0){
				return null;
			}
			return($query->row());
		}
		
	}

	public function getPaginas($idioma){		
		$this->db->select('id,nombre_'.$this->lang->lang().' as nombre, contenido_'.$this->lang->lang().' as contenido, url_slug_'.$this->lang->lang().' as url_slug');			
		$this->db->from('paginas');			
		$this->db->where('status', 'active');
		$order = 'nombre_'.$this->lang->lang();
		$this->db->order_by($order, 'asc'); 
		$query = $this->db->get();
		if($query->num_rows() == 0){
			return null;
		}
		return($query->result());		
		
	}

	public function updatePagina($idpagina, $array_data){
		if(isset($idpagina) and is_array($array_data)){
			$this->db->where('id', $idpagina);
			return $this->db->update('paginas',$array_data);
		}
		return false;
	}
		
	
}


?>