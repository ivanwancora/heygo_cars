<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keygocars extends MY_Controller {
	
	//Controlador Principal 
	public function __construct()
	{
		parent::__construct();
		$this->data['seo'] = $this->metaseomodel->getMetaseo();					
	}

	//Esta funcion carga la pagina escogida 
	public function index($url_slug=null){
		if(isset($url_slug)){
			//Se recupera la pagina con el id indicado y se carga la vista
			$this->data['pagina'] = $this->paginasmodel->getPaginaUrlSlug($url_slug);
			$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
			$this->load->view($this->config->item('theme_path_frontend'). 'content/paginas',$this->data);
			$this->load->view($this->config->item('theme_path_frontend'). 'footer');			
		}
	}

	//Esta funcion carga la pagina de mis favoritos
	public function favoritos(){
		$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
		$this->load->view($this->config->item('theme_path_frontend'). 'content/favoritos',$this->data); 
		$this->load->view($this->config->item('theme_path_frontend'). 'footer');			
	}

	//subscribe to newsletter
	public function subscribe()
	{		
		if(!empty($_POST)){
			
			$email = trim($this->input->post("email"));

			$news = array(
				'email' => $email,
				'confirmed' => 'active',
				'status' => 'active'
			);
			
			$retorn = $this->commonsmodel->insert('newsletter', $news);
			
			if($retorn!=false){ redirect(base_url($this->lang->lang().'/keygocars/thank_you/newsletter')); }
		}
	}

	//subscribe to bbdd + keygocars
	public function contacto()
	{		
		if(!empty($_POST)){
			
			$name = trim($this->input->post("name"));
			$email = trim($this->input->post("email"));
			$phone = trim($this->input->post("phone"));
			$msg = trim($this->input->post("msg"));
			$ipaddress = $this->input->post("ipaddress");
			$navigator = trim($this->input->post("navigator"));

			$news = array(
				'name' => $name,
				'email' => $email,
				'phone' => $phone,
				'message' => $msg,
				'lang' => $this->lang->lang(),
				'ip' => $ipaddress,
				'navigator' => $navigator
			);
			
			$retorn = $this->commonsmodel->insert('fcontacts', $news);
			
			if($retorn!=false){ redirect(base_url($this->lang->lang().'/keygocars/thank_you/contacto')); }
		}
	}

	//Esta funcion carga la pagina escogida 
	public function thank_you($notice=null){
		if(isset($notice)){
			//Se recupera la pagina con el id indicado y se carga la vista
			$this->data['notice'] = $notice;
			$this->load->view($this->config->item('theme_path_frontend'). 'header', $this->data);
			$this->load->view($this->config->item('theme_path_frontend'). 'content/thank-you', $this->data);
			$this->load->view($this->config->item('theme_path_frontend'). 'footer');			
		}else{
			redirect(base_url($this->lang->lang())); 
		}		
	}

}
