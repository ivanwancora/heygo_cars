
function actualizarMisDatos(){


    $.ajax({
        type: 'POST',
        data: $('#formmisdatos').serialize(),
        url: base_url+'ajax/perfiles/actualizarDatos',
        success: function(data) {  
            if(data.result=="true"){ 
                swal(data.title, data.detail,'success').catch(swal.noop);      
                return true;    
            }
            else{
                swal(data.title,'<strong>¡Uupps! </strong>'+data.detail,'warning').catch(swal.noop);                 
                return false;           
            }               
        }
    });

    return false;
}


function cambiarpassword(){  

    $.ajax({
        type: 'POST',
        data: $('#formchangepassword').serialize(),
        url: base_url+'ajax/auth/change_password',
        success: function(data) {  
            if(data.result=="true"){   
                swal('Cambiar Contraseña', data.detail,'success').catch(swal.noop);                
                $("#currentpassword").val(''); 
                $("#newpassword").val('');
                $("#repnewpassword").val('');               
                return true;    
            }
            else{
                swal('<strong>¡Uupps! </strong>', data.detail,'warning').catch(swal.noop);                
                return false;           
            }               
        }
    });

    return false;
}


function recuperar(){

    $.ajax({
        type: 'POST',
        data: $('#form-recuperarpassword').serialize(),
        url: base_url+'ajax/auth/forgot_password',
        success: function(data) {  
            if(data.result=="true"){ 
                swal('Recuperar Contraseña',data.detail,'success').catch(swal.noop); 
                $("#form-recuperarpassword")[0].reset();                  
                return true;    
            }
            else{
                swal('<strong>¡Uupps! </strong>', data.detail,'warning').catch(swal.noop);        
                return false;           
            }               
        }
    });

    return false;

}

function recuperarnueva(){

    if($("#new").val() != $("#new_confirm").val()){     
        swal('<strong>Uupps! </strong>', 'Las contraseñas no coinciden', 'warning').catch(swal.noop);        
        return false;
    }


    $.ajax({
        type: 'POST',
        data: $('#form-recuperarnueva').serialize(),
        url: base_url+'ajax/auth/reset_password/'+$('#code').val(),
        success: function(data) {  
            if(data.result=="true"){  
                swal('Recuperar Contraseña',data.detail,'success').catch(swal.noop);                                    
                window.location.replace(base_url+"usuario/login");                            
                return true;    
            }
            else{
                swal('<strong>¡Uupps! </strong>', data.detail,'warning').catch(swal.noop);        
                return false;           
            }               
        }
    });

    return false;

}

function enviarformcontacto(){ 

    var data = new FormData();   
    
    data.append('name', $("#name").val());  
    data.append('phone', $("#phone").val());  
    data.append('email', $("#email").val());
    data.append('reason', $("#reason").val());    
    data.append('message', $("#message").val());     

    $("#btn-submit").prop('disabled', true);

    $("#loadingdiv").show();

    $.ajax({       
        url: base_url+'ajax/contactos/contacto',
        type:'POST', 
        contentType:false, 
        data:data, 
        processData:false, 
        cache:false 
    }).done(function(msg){  
        if(msg.result=="true"){                 
            //$('#infoMessage').html('<div class="event-message success">'+msg.detail+'</div>'); 
            swal(msg.title, msg.detail,'success').catch(swal.noop);
            $("#form-contacto")[0].reset(); 
            $("#btn-submit").prop('disabled', false);
            $("#loadingdiv").hide();
            gtag('event', 'Formulario', {
                'event_category': 'Contacto',
                'event_label': 'Formulario de contacto',
            });
                
            return true;    
        }
        else{
            //$('#infoMessage').html('<div class="event-message alert">'+msg.detail+'</div>');  
            swal('<strong>¡Uupps! </strong>', msg.detail,'warning').catch(swal.noop);   
            $("#btn-submit").prop('disabled', false);       
            return false;           
        }     
    }).fail(function(jqXHR, textStatus, errorThrown){
        //console.log('Error subiendo el archivo: '+textStatus+" "+errorThrown);      
        $("#btn-submit").prop('disabled', false);
        $("#loadingdiv").hide();
    });

    return false;

}


function suscribirmailchimp(){

    $("#btn-submit-mailchimp").prop('disabled', true);

    $.ajax({
        type: 'POST',
        data: $('#form-mailchimp').serialize(),
        url: base_url+'ajax/contactos/subscribe_mailchimp',
        success: function(data) {  
            if(data.result=="true"){ 
                swal(data.title, data.detail,'success').catch(swal.noop);    
                $("#form-mailchimp")[0].reset();        
                $("#btn-submit-mailchimp").prop('disabled', false);     
                return true;    
            }
            else{
                swal('<strong>Uupps! </strong>', data.detail ,'warning').catch(swal.noop);               
                $("#btn-submit-mailchimp").prop('disabled', false);     
                return false;           
            }               
        }
    });

    return false;

}

