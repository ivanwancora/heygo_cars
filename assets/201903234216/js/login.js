function login(){    
	
	$.ajax({
    	type: 'POST',
    	data: $('#formlogin').serialize(),
    	url: base_url+'ajax/auth/login',
    	success: function(data) {  
    		if(data.result=="true"){	
    			window.location.replace(base_url+"Back_dashboard/");
    			return true;	
    		}
    		else{
                swal('<strong>Uupps! </strong>', data.detail,'warning').catch(swal.noop);     			
                return false;    		
    		}	    		
    	}
    });

	return false;
}

function logout(){

    $.ajax({
        type: 'POST',         
        url: base_url+'ajax/auth/logout',
        success: function(data) {                 
            if(data.result=="true"){                                
                window.location.replace(base_url+"principal");          
                return true; 
            }                           
        }
    });
    return false;

}


