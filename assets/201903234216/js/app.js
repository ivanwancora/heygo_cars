$(document).ready(function () {
    responsiveMenu();
    customSelects();
    selectPairs();
    objectFixImg();
    stickIt();
});

var minTablet = '(min-width:768px)',
    maxTablet = '(max-width:767px)',
    minDesktop = '(min-width: 1240px)',
    maxDesktop = '(max-width: 1239px)';


function getCars() {
    var json = (localStorage.getItem('KGC_Vehicles') === null) ? { id: [] } : JSON.parse(localStorage.getItem('KGC_Vehicles'));
    if (json.id == undefined) { json.id = [] };
    return json;
}

function setCars(order) {
    localStorage.setItem('KGC_Vehicles', JSON.stringify(order));
}

function getSearch() {
    var json = (localStorage.getItem('KGC_Search') === null) ? { search: [] } : JSON.parse(localStorage.getItem('KGC_Search'));
    if (json.search == undefined) { json.search = [] };
    return json;
}

function setSearch(order) {
    localStorage.setItem('KGC_Search', JSON.stringify(order));
}

function responsiveMenu() {
    $('.m-menu--burger.primary').on('click', function () {
        $('.o-menu--mobile').toggleClass("menu-open");
        $('body').toggleClass("menu-open");
        scroll();
    });

    $(window).resize(function () {
        scroll();
        if (window.matchMedia(minDesktop).matches) {
            $('.o-menu--mobile').removeClass("menu-open");
            $('body').removeClass("menu-open");
        }
    });

    $('.has-children').find('.arrow').on('click', function () {
        $(this).find('.m-menu--dropdown').fadeToggle(600);
    });

    $('.a-button--vehiculos-form').on('click', function () {
        $('.o-form--mobile').fadeIn();
    });
    $('.a-button--search-form-growing').on('click', function () {
        var menu = $(this).closest('.o-menu--mobile');

        if ($(menu).length) {
            $(menu).removeClass("menu-open");
        }

        $('.o-form--mobile').fadeIn();
    });
    $('.o-form--mobile').find('.m-menu--burger').off('click').on('click', function () {
        $('.o-form--mobile').fadeOut();
    });

    var header = $("#o-header--sticky");

    $(window).scroll(function () {
        scroll();
    });

    scroll();
    function scroll() {
        var scroll = $(window).scrollTop();

        if (scroll >= 300) {
            header.addClass("scroll");
        } else {
            header.removeClass("scroll");
        }
    }
}

function stickIt() {
    var el = stickybits('.sticky-element', { stickyBitStickyOffset: 80 });
}

// Swiper
(function () {
    if ($('.o-swiper--hero').length) {
        swiperHeroHome();
    }

    if ($('.o-swiper--flotas').length) {
        swiperFlota();
    }
    if ($('.o-swiper--featured').length) {
        swiperHomeFeatured();
    }
    if ($('.o-swiper--nos-veh').length) {
        swiperNuestrosVehiculos();
    }

    if ($('.o-swiper--centros').length) {
        swiperCentros();
    }

    if ($('.o-swiper--marcas').length) {
        swiperMarcas();
    }

    if ($('.o-swiper--blog-home').length) {
        swiperBlogHome();
    }

    if ($('.o-swiper--vehicle-item').length) {
        swiperVehicleItem();
    }

    if ($('.o-swiper--home-sales').length) {
        swiperSalesHome();
    }


    if ($('.o-swiper--veh-single').length) {
        swiperVehSingle();
    }

    if ($('.o-swiper--keygonline').length) {
        swiperKeygonline();
    }

    function swiperFlota() {
        if ($('.o-swiper--flotas').length) {
            var i = 0;

            $('.o-swiper--flotas').each(function () {
                $(this).addClass('o-s--flota-' + i);
                $(this).find('.swiper-flota-form-button-next').addClass('s-flota-next-' + i);
                $(this).find('.swiper-flota-form-button-prev').addClass('s-flota-prev-' + i);

                var sliderSelector = '.o-s--flota-' + i,
                    options = {
                        init: false,
                        speed: 800,
                        slidesPerView: 8,
                        spaceBetween: 20,
                        grabCursor: true,
                        parallax: true,
                        simulateTouch: false,
                        observer: true,
                        observeParents: true,
                        // Navigation arrows
                        navigation: {
                            nextEl: '.s-flota-next-' + i,
                            prevEl: '.s-flota-prev-' + i,
                        },
                        breakpoints: {
                            1239: {
                                slidesPerView: 3,
                            },
                            767: {
                                slidesPerView: 2,
                            }
                        },
                    };

                var mySwiper = new Swiper(sliderSelector, options);

                // Initialize slider
                mySwiper.init();

                i++;
            });

        }
    };

    function swiperHeroHome() {
        var sliderSelector = '.o-swiper--hero',
            options = {
                init: false,
                loop: true,
                speed: 800,
                slidesPerView: 1,
                spaceBetween: 100,
                centeredSlides: true,
                grabCursor: true,
                simulateTouch: false,
                preloadImages: false,
                lazy: true,
                pagination: {
                    el: '.swiper-home-hero-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.swiper-home-hero-button-next',
                    prevEl: '.swiper-home-hero-button-prev',
                },
                autoplay: {
                    delay: 5000,
                }
            };

        responsiveSlider(sliderSelector);

        $(window).resize(function () {
            responsiveSlider(sliderSelector);
        });

        var mySwiper = new Swiper(sliderSelector, options);

        // Initialize slider
        mySwiper.init();
    };

    function swiperHomeFeatured() {
        var sliderSelector = '.o-swiper--featured',
            options = {
                init: false,
                loop: true,
                speed: 800,
                slidesPerView: 1,
                spaceBetween: 100,
                centeredSlides: true,
                grabCursor: true,
                parallax: true,
                simulateTouch: false,
                observer: true,
                observeParents: true,
                // Navigation arrows
                navigation: {
                    nextEl: '.swiper-featured-button-next',
                    prevEl: '.swiper-featured-button-prev',
                },
                autoplay: {
                    delay: 5000,
                }
            };

        var mySwiper = new Swiper(sliderSelector, options);

        // Initialize slider
        mySwiper.init();
    };

    function swiperNuestrosVehiculos() {
        var swiper = $('.o-swiper--nos-veh'),
            swiperActive,
            swiperOptions = {
                init: true,
                loop: true,
                speed: 800,
                slidesPerView: 1,
                centeredSlides: true,
                observer: true,
                observeParents: true,
                pagination: {
                    el: '.nos-veh-pagination',
                    clickable: true,
                },
                autoplay: {
                    delay: 4000,
                }
            };

        if (window.matchMedia(maxTablet).matches) {
            swiperActive = new Swiper(swiper, swiperOptions);
            swiperActive.init();
            $(swiper).addClass('on').removeClass('off');
        } else {
            $(swiper).addClass('off').removeClass('on');
        }

        $(window).resize(function () {
            if (window.matchMedia(maxTablet).matches) {
                if ($(swiper).hasClass('off')) {
                    swiperActive = new Swiper(swiper, swiperOptions);
                    swiperActive.init();
                    swiper.addClass('on').removeClass('off');
                    $('.nos-veh-pagination').show();
                }
            } else {
                if (!$(swiper).hasClass('off')) {
                    swiper.addClass('off').removeClass('on');
                    swiperActive.destroy(true, true);
                    $('.nos-veh-pagination').hide();
                }
            }
        });
    }

    function swiperCentros() {
        if ($('.o-swiper--centros').length) {
            var i = 0;

            $('.o-swiper--centros').each(function () {
                $(this).addClass('o-s--cen-' + i);
                $(this).find('.swiper--centros-pagination').addClass('s-cen-pag-' + i);

                var sliderSelector = '.o-s--cen-' + i,
                    options = {
                        init: false,
                        loop: true,
                        speed: 800,
                        slidesPerView: 1,
                        spaceBetween: 100,
                        centeredSlides: true,
                        grabCursor: true,
                        parallax: true,
                        simulateTouch: false,
                        observer: true,
                        observeParents: true,
                        effect: 'fade',
                        pagination: {
                            el: '.s-cen-pag-' + i,
                            clickable: true,
                        },
                        autoplay: {
                            delay: 5000,
                        }
                    };

                var mySwiper = new Swiper(sliderSelector, options);

                // Initialize slider
                mySwiper.init();

                i++;
            });

        }
    };

    function swiperMarcas() {
        var sliderSelector = '.o-swiper--marcas',
            options = {
                init: false,
                loop: false,
                speed: 800,
                slidesPerView: 5,
                slidesPerColumn: 2,
                slidesPerGroup: 5,
                spaceBetween: 30,
                centeredSlides: false,
                grabCursor: true,
                parallax: true,
                observer: true,
                observeParents: true,
                // Navigation arrows
                navigation: {
                    nextEl: '.swiper-marcas-button-next',
                    prevEl: '.swiper-marcas-button-prev'
                },
                breakpoints: {
                    1239: {
                        slidesPerView: 3,
                        slidesPerColumn: 3,
                        slidesPerGroup: 3,
                        spaceBetween: 18,
                    },
                    767: {
                        slidesPerView: 2,
                        slidesPerColumn: 2,
                        slidesPerGroup: 2,
                        spaceBetween: 11,
                    }
                },
                autoplay: {
                    delay: 5000,
                },
            };

        var mySwiper = new Swiper(sliderSelector, options);

        // Initialize slider
        mySwiper.init();
    };

    function swiperSalesHome() {
        var i = 0;

        $('.o-swiper--home-sales').each(function () {
            $(this).addClass('o-s--home-sales-' + i);
            $(this).find('.home-sales-pagination').addClass('home-sales-pag-' + i);
            var $that = $('.o-s--home-sales-' + i);
            var swiper = $('.o-s--home-sales-' + i),
                swiperActive,
                swiperOptions = {
                    init: false,
                    loop: false,
                    speed: 800,
                    slidesPerView: 3,
                    centeredSlides: false,
                    grabCursor: true,
                    parallax: true,
                    effect: 'fade',
                    observer: true,
                    observeParents: true,
                    // Navigation arrows
                    pagination: {
                        el: '.home-sales-pag-' + i,
                        clickable: true,
                    },
                    breakpoints: {
                        1239: {
                            slidesPerView: 1,
                            slidesPerGroup: 1,
                            spaceBetween: 0,
                        }
                    },
                    autoplay: {
                        delay: 5000,
                    },
                    on: {
                        slideChangeTransitionStart: function () {
                            $($that).find('.popup').hide();
                        },
                    }
                };

            if (window.matchMedia(maxDesktop).matches) {
                swiperActive = new Swiper(swiper, swiperOptions);
                swiperActive.init();
                $(swiper).addClass('on').removeClass('off');
            } else {
                $(swiper).addClass('off').removeClass('on');
            }

            $(window).resize(function () {
                if (window.matchMedia(maxDesktop).matches) {
                    if ($(swiper).hasClass('off')) {
                        swiperActive = new Swiper(swiper, swiperOptions);
                        swiperActive.init();
                        swiper.addClass('on').removeClass('off');
                    }
                } else {
                    if (!$(swiper).hasClass('off')) {
                        swiper.addClass('off').removeClass('on');
                        swiperActive.destroy(true, true);
                    }
                }
            });

            i++;

        });
    };

    function swiperBlogHome() {
        var swiper = $('.o-swiper--blog-home'),
            swiperActive,
            swiperOptions = {
                init: false,
                loop: false,
                speed: 800,
                slidesPerView: 3,
                spaceBetween: 60,
                centeredSlides: false,
                grabCursor: true,
                parallax: true,
                observer: true,
                observeParents: true,
                // Navigation arrows
                pagination: {
                    el: '.blog-home-pagination',
                    clickable: true,
                },
                breakpoints: {
                    1239: {
                        slidesPerView: 2,
                        slidesPerGroup: 1,
                        spaceBetween: 20,
                    },
                    767: {
                        slidesPerView: 1,
                        slidesPerGroup: 1,
                        spaceBetween: 0,
                    }
                },
                autoplay: {
                    delay: 5000,
                },
            };

        if (window.matchMedia(maxDesktop).matches) {
            swiperActive = new Swiper(swiper, swiperOptions);
            swiperActive.init();
            $(swiper).addClass('on').removeClass('off');
        } else {
            $(swiper).addClass('off').removeClass('on');
        }

        $(window).resize(function () {
            if (window.matchMedia(maxDesktop).matches) {
                if ($(swiper).hasClass('off')) {
                    swiperActive = new Swiper(swiper, swiperOptions);
                    swiperActive.init();
                    swiper.addClass('on').removeClass('off');
                }
            } else {
                if (!$(swiper).hasClass('off')) {
                    swiper.addClass('off').removeClass('on');
                    swiperActive.destroy(true, true);
                }
            }
        });
    };

    function swiperVehSingle() {
        var sliderSelector = '.o-swiper--veh-single',
            $btn = $(sliderSelector).find('.a-button--lightbox'),
            options = {
                init: false,
                loop: true,
                speed: 800,
                slidesPerView: 1,
                //spaceBetween: 100,
                centeredSlides: true,
                grabCursor: true,
                parallax: true,
                simulateTouch: false,
                observer: true,
                observeParents: true,
                preloadImages: false,
                lazy: {
                    loadPrevNext: true,
                },
                // Navigation arrows
                navigation: {
                    nextEl: '.swiper-veh-single-button-next',
                    prevEl: '.swiper-veh-single-button-prev',
                },
                autoplay: {
                    delay: 5000,
                },
                on: {
                    slideChangeTransitionEnd: function () {
                        initButton();
                    }
                }
            };

        var mySwiper = new Swiper(sliderSelector, options);

        // Initialize slider
        mySwiper.init();
        mySwiper.lazy.load();
        initButton();

        function initButton() {
            var el = $('.swiper-slide-active').data('id');
            $btn.off('click').on('click', function () {
                $('#' + el).trigger('click');
            });
        }
    };

    function swiperKeygonline() {
        if ($('.o-swiper--keygonline').length) {
            var i = 0;

            $('.o-swiper--keygonline').each(function () {
                $(this).addClass('o-s--keygonline-' + i);
                $(this).find('.keygonline-pagination').addClass('keygonline-pagination-' + i);
                var swiper = '.o-s--keygonline-' + i,
                    options = {
                        init: false,
                        speed: 800,
                        slidesPerView: 3,
                        spaceBetween: 20,
                        grabCursor: true,
                        parallax: true,
                        simulateTouch: false,
                        observer: true,
                        observeParents: true,
                        pagination: {
                            el: '.keygonline-pagination-' + i,
                            clickable: true,
                        },
                        breakpoints: {
                            1239: {
                                slidesPerView: 1,
                            },
                        },
                    };

                if (window.matchMedia(maxDesktop).matches) {
                    swiperActive = new Swiper(swiper, options);
                    swiperActive.init();
                    $(swiper).addClass('on').removeClass('off');
                } else {
                    $(swiper).addClass('off').removeClass('on');
                }

                $(window).resize(function () {
                    if (window.matchMedia(maxDesktop).matches) {
                        if ($(swiper).hasClass('off')) {
                            swiperActive = new Swiper(swiper, options);
                            swiperActive.init();
                            $(swiper).addClass('on').removeClass('off');
                        }
                    } else {
                        if (!$(swiper).hasClass('off')) {
                            $(swiper).addClass('off').removeClass('on');
                            swiperActive.destroy(true, true);
                        }
                    }
                });

                i++;

            });
        }
    };

})();

function number_format(number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

function swiperVehicleItem() {
    if ($('.o-swiper--vehicle-item').length) {
        var i = 0;

        $('.o-swiper--vehicle-item').each(function () {
            $(this).addClass('o-s--vehicle-item-' + i);
            $(this).find('.swiper-vehicle-item-button-next').addClass('s-vehicle-item-next-' + i);
            $(this).find('.swiper-vehicle-item-button-prev').addClass('s-vehicle-item-prev-' + i);

            var sliderSelector = '.o-s--vehicle-item-' + i,
                options = {
                    init: false,
                    speed: 800,
                    slidesPerView: 1,
                    grabCursor: true,
                    parallax: true,
                    simulateTouch: false,
                    observer: true,
                    observeParents: true,
                    preloadImages: false,
                    lazy: {
                        loadPrevNext: true,
                    },
                    // Navigation arrows
                    navigation: {
                        nextEl: '.s-vehicle-item-next-' + i,
                        prevEl: '.s-vehicle-item-prev-' + i,
                    },
                };

            var mySwiper = new Swiper(sliderSelector, options);

            // Initialize slider
            mySwiper.init();
            mySwiper.lazy.load();
            i++;
        });

    }
};

function swiperVehicleItemLoop(oldI) {
    if ($('.o-swiper--vehicle-item').length) {
        var i = oldI;

        $('.m-vehicle-item.inactive').each(function () {
            $(this).removeClass('inactive');
            $(this).find('.o-swiper--vehicle-item').addClass('o-s--vehicle-item-' + i);
            $(this).find('.swiper-vehicle-item-button-next').addClass('s-vehicle-item-next-' + i);
            $(this).find('.swiper-vehicle-item-button-prev').addClass('s-vehicle-item-prev-' + i);

            var sliderSelector = '.o-s--vehicle-item-' + i,
                options = {
                    init: false,
                    speed: 800,
                    slidesPerView: 1,
                    grabCursor: true,
                    parallax: true,
                    simulateTouch: false,
                    observer: true,
                    observeParents: true,
                    preloadImages: false,
                    lazy: {
                        loadPrevNext: true,
                    },
                    // Navigation arrows
                    navigation: {
                        nextEl: '.s-vehicle-item-next-' + i,
                        prevEl: '.s-vehicle-item-prev-' + i,
                    },
                };

            var mySwiper = new Swiper(sliderSelector, options);

            // Initialize slider
            mySwiper.init();
            mySwiper.lazy.load();

            i++;
        });

    }
};

function responsiveSlider(slider) {
    $(slider).find('.swiper-slide').each(function () {
        var url = urlSlide($(this));
        $(this).css('background-image', 'url(' + url + ')');
    });

    function urlSlide(slide) {
        if (window.matchMedia(maxTablet).matches) {
            return $(slide).data('mobile');
        } else if (window.matchMedia(maxDesktop).matches) {
            return $(slide).data('tablet');
        } else {
            return $(slide).data('desktop');
        }
    }
};

(function () {
    if ($('.m-form--watch-price').length) {
        $('.a-button--baja-precio').on('click', function () {
            $('.m-form--watch-price, .m-form--watch-price-bg, body').addClass('menu-open');
        });

        $('.watch-cross, .m-form--watch-price-bg').on('click', function () {
            $('.m-form--watch-price, .m-form--watch-price-bg, body').removeClass('menu-open');
        });
    }

    if ($('.a-button--reservar').length) {
        $('.a-button--reservar').on('click', function () {
            $('.m-form--reserve, .m-form--reserve-bg, body').addClass('menu-open');
        });

        $('.reserva-cross, .m-form--reserve-bg').on('click', function () {
            $('.m-form--reserve, .m-form--reserve-bg, body').removeClass('menu-open');
        });
    }
})();


// Tag Vehicles
function tagVehicle() {
    $('.m-tag-item').find('.close').off('click').on('click', function () {
        var tag = $(this).closest('.m-tag-item'),
            flota = $(tag).data('flota'),
            sales = $(tag).data('sales'),
            multiple = $(tag).data('multiple');
        search = $(tag).data('input')
        select = $(tag).data('select');
        if (flota) {
            $('.m-flota-pick-item.' + flota).removeClass('active');
            $('.m-flota-pick-item.' + flota).find("input:checkbox").prop('checked', false);
            $('.m-flota-selec-item.' + flota).removeClass('active');
            $('.m-flota-selec-item.' + flota).find("input:checkbox").prop('checked', false);
        } else if (multiple) {
            $('.m-group-values.' + multiple).find('.mgv-option').removeClass('active');
            $('.m-group-values.' + multiple).find('.input-val').val('');
        } else if (sales) {
            $('#oferta').prop('checked', false);
            $('#sales').prop('checked', false);
        } else if (search) {
            $('#search').val('');
            $('#generalSearch').val('');
        } else if (select) {
            $('#' + select).val('').trigger('change');
            $('select[name=' + select + ']').val('').trigger('change');
        }

        $('#formsearchitems').submit();

    });
};

//Enviar marca al cercador principal
function enviarmarca() {
    $('#home_marcas').on('click', '.m-marcas-item', function () {

        var arr = [],
            suborder = {}
        mark = $(this).data('type');
        suborder.type = 'input';
        suborder.name = 'search';
        suborder.value = mark.toUpperCase();
        arr.push(suborder);

        setSearch({ search: arr });
        location.href = base_url + 'vehiculos/';
    });
}

// Dateppicker
(function () {
    $('.datepicker-date').datepicker({
        language: 'en',
        classes: 'custom-datepicker calendar',
        dateFormat: "dd/mm/yyyy"
    });


    $('.datepicker-time').datepicker({
        language: 'en',
        timepicker: true,
        onlyTimepicker: true,
        classes: 'custom-datepicker time',
        timeFormat: "hh:ii"
    });
})();

(function () {
    $('.btn-to-id').on('click', function () {
        var id = $(this).data('id');
        $('html, body').animate({
            scrollTop: $(id).offset().top
        }, 1000);
    });
})();

(function () {
    $('.p-vehiculos-single-page #btn-back').on('click', function () {
        location.href = base_url + 'vehiculos/';
    });

    $('.btn-menu-vehiculos').on('click', function () {
        setSearch({ search: [] });
        location.href = base_url + 'vehiculos/';
    });
})();

// Menu Options initzialització
(function () {
    if ($('#m-menu--sales--home').length) {
        menuOptions({
            menu: '#m-menu--sales--home',
            item: '.o-swiper--home-sales',
            data: 'type',
            oficinas: [],
            initialize: true,
            callback: function (num) {
                var item = this.oficinas[num],
                    $mopt = $('.mmo-option.' + item),
                    $aopt = $(this.item + '.' + item);
                $(this.menu).find('.mmo-option').not($mopt).removeClass('active');
                $(this.menu).find('.mmo-option').not($mopt).removeClass('init');
                $mopt.addClass('active');
                $mopt.addClass('init');
                $(this.item).not($aopt).removeClass('active');
                $aopt.addClass('active');
            }
        });
    };

    if ($('#m-menu--sales--flota').length) {
        menuOptions({
            menu: '#m-menu--sales--flota',
            data: 'type',
            oficinas: [],
            initialize: false,
            callback: function (num) {
                var item = (num === 'no-init') ? getCartypeItem() : this.oficinas[num];

                var cls = item.toLowerCase();
                cls = (cls == "") ? 'todos' : cls;

                $mopt = $('.mmo-option.' + cls);
                $(this.menu).find('.mmo-option').not($mopt).removeClass('active');
                $(this.menu).find('.mmo-option').not($mopt).removeClass('init');
                $mopt.addClass('active');
                $mopt.addClass('init');
                if (num !== 'no-init') {
                    $('#cartype').val(item);
                    $('#formsearchitems').submit();
                }
            }
        });
    }
})();

function menuOptions(config) {
    $(config.menu).find('.mmo-option').each(function () {
        config.oficinas.push($(this).data(config.data));
    });

    var i = 0;
    var total = config.oficinas.length;

    if (config.initialize != false) {
        config.callback(i);
    } else {
        config.callback('no-init');
    }

    $(config.menu).find('.mmo-prev').on('click', function () {
        i--;
        if (i < 0) { i = total - 1 };
        config.callback(i);
    });

    $(config.menu).find('.mmo-next').on('click', function () {
        i++;
        if (i == total) { i = 0 };
        config.callback(i);
    });

    $(config.menu).find('.mmo-option').on('click', function (e) {
        e.preventDefault();
        var office = $(this).data(config.data);
        i = config.oficinas.indexOf(office);
        config.callback(i);
    });
}

function getCartypeItem() {
    var order = getSearch();
    var opt = [];

    order.search.forEach(function (item) {
        if (item.name == "cartype") { opt.push(item); }
    });

    return (opt.length > 0) ? opt[0].value : 'Todos';
}

//Popups Single Vehicle
(function () {
    vehiclePopUps();
})();

function vehiclePopUps() {
    if ($('.svi-fee .icon-info').length) {
        $('.svi-fee .icon-info').off('click').on('click', function () {
            var $that = $(this).find('.popup');
            $('.icon-info .popup').not($that).fadeOut();
            $($that).fadeToggle();
        });
    };

    if ($('.m-vehicle-item .icon-share').length) {
        $('.m-vehicle-item .icon-share').off('click').on('click', function () {
            var $that = $(this).closest('.share');
            $('.m-vehicle-item .share').not($that).find('.popup').fadeOut();
            $($that).find('.popup').fadeToggle();
            $('.m-vehicle-item .share').not($that).removeClass('active');
            $($that).toggleClass('active');
        });
    };
}
//Formulari vehicles
(function () {
    if ($('.m-form--flota').length) {
        $('.m-form--flota').find('.a-button--filters').on('click', function () {
            $(this).closest('.m-form--flota').toggleClass('active');
            $(this).toggleClass('active');
        });

        $('.m-form--flota').find('.a-button--remove-filters').on('click', function () {
            clearForm();
            blinkRemove(this);
            cSubmit(this);
            paramsVehicle();
        });
    }

    $('.m-flota-pick-item, .m-flota-selec-item').on('click', function () {
        var cls = $(this).data('flota');
        if (!$(this).hasClass('active')) {
            $('.m-flota-pick-item.' + cls).addClass('active');
            $('.m-flota-pick-item.' + cls).find("input:checkbox").prop('checked', true);
            $('.m-flota-selec-item.' + cls).addClass('active');
            $('.m-flota-selec-item.' + cls).find("input:checkbox").prop('checked', true);
        } else {
            $('.m-flota-pick-item.' + cls).removeClass('active');
            $('.m-flota-pick-item.' + cls).find("input:checkbox").prop('checked', false);
            $('.m-flota-selec-item.' + cls).removeClass('active');
            $('.m-flota-selec-item.' + cls).find("input:checkbox").prop('checked', false);
        };
        cSubmit(this);
    })

    $('.mff-flota-selector').find('.msi-select').on('click', function () {
        $(this).closest('.mff-flota-selector').find('.m-menu--dropdown').fadeIn(600);
    });

    $('.mff-flota-selector').find('.a-button--ok').on('click', function () {
        $(this).closest('.m-menu--dropdown').fadeOut(600);
    });

    $('#formsearchitems').find('.a-input--search .icon-search').on('click', function () {
        cSubmit(this);
    });

    $('#formsearchitems').find('#oferta').on('click', function () {
        cSubmit(this);
    });
})();

function clearForm() {
    $('#search').val('');
    $('#generalSearch').val('');
    $('.select--vehicle-form').val('');
    $('.m-form--flota').find('select').val('');
    $('.m-form--flota').find('input[name="sales"]').prop('checked', false);
    $('.m-form--flota').find('checkbox[name="flota-pick"]').prop('checked', false);
    $('.m-form--flota').find('.mgv-option').removeClass('active');
    $('.m-form--flota').find('.input-val').val('');
    $('.m-form--flota').find('#generalSearch').val('');
    $('.m-flota-pick-item').removeClass('active');
    $('.m-flota-pick-item').find("input:checkbox").prop('checked', false);
    $('.m-flota-selec-item').removeClass('active');
    $('.m-flota-selec-item').find("input:checkbox").prop('checked', false);
    $('.m-group-values').find('.mgv-option').removeClass('active');
    $('.m-group-values').find('.input-val').val('');
    $('#sales').prop('checked', false);
    $('#oferta').prop('checked', false);
}

function cSubmit(el) {
    var $form = $(el).closest('#formsearchitems');
    if ($form.length) {
        $('#formsearchitems').submit();
    }
}

function blinkRemove(el) {
    $(el).addClass('active');
    setTimeout(function () {
        $(el).removeClass('active');
    }, 750);
}

(function () {
    if ($('.m-group-values').length) {
        $('.m-group-values').find('.mgv-option').on('click', function () {
            var value = $(this).data('option');
            $(this).addClass('active');
            $(this).closest('.m-group-values').find('.mgv-option').not(this).removeClass('active');
            $(this).closest('.m-group-values').find('.input-val').val(value);

            cSubmit(this);
        });
    }
})();
//Centros tabs i drop
(function () {
    if ($('.m-menu--centros').length && $('.p-centros-section').length) {

        $('.m-menu--centros').find('.mm-centro-item-drop').on('click', function () {
            action($(this));
        });

        $('.m-menu--centros').find('.mm-centro-item').on('click', function () {
            action($(this));
        });

        function action($this) {
            var text = $($this).data('center'),
                cls = $($this).data('class');
            $($this).closest('.m-menu--centros').find('.center').text(text);
            $($this).closest('.m-menu--centros').find('.mm-centro-item').not('.mm-centro-item.' + cls).removeClass('active');
            $($this).closest('.m-menu--centros').find('.mm-centro-item.' + cls).addClass('active');
            $($this).closest('.m-menu--centros').find('.mm-centro-item-drop').not('.mm-centro-item-drop.' + cls).removeClass('active');
            $($this).closest('.m-menu--centros').find('.mm-centro-item-drop.' + cls).addClass('active');
            $($this).closest('.p-centros-section').find('.m-centros-item--left').not('.m-centros-item--left.' + cls).removeClass('active');
            $($this).closest('.p-centros-section').find('.m-centros-item--left.' + cls).addClass('active');
        }
    }
})();
//Tabs (Single Vehicle,...)
(function () {
    if ($('.m-menu--tabs').length) {
        $('.m-menu--tabs a').on('click', function () {
            $('.m-menu--tabs a').not(this).removeClass('active');
            $(this).addClass('active');
            $(this).closest('.o-tabs-module').find('.tabs-section').removeClass('active');
            var id = $(this).data('section');
            $(id).addClass('active');
        });
    };
})();

function isInViewport(elem) {
    var distance = elem.getBoundingClientRect();

    return (
        distance.top >= 0 &&
        distance.left >= 0 &&
        distance.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        distance.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
};

function animDash(findMe) {
    if (isInViewport(findMe)) {
        findMe.classList.add('active');
    }
}

(function () {
    var elems = document.querySelectorAll('.dashed_animation');

    window.addEventListener('scroll', function (event) {
        elems.forEach(function (el) {
            animDash(el);
        });
    }, false);
})();

//Select2
function customSelects() {
    if (!$('#formsearchitems').length) {
        $('.select--vehicle-form').select2({
            width: '100%',
            containerCssClass: 'select2--vehicle-form'
        });

        $('.select--vehicle-form').on('select2:select', function (e) {
            markSelects($(this), e);
        });

        $('.select--vehicle-form').each(function () {
            if ($(this).val() !== "") {
                var selection = $(this).closest('.select').find(".select2-selection");
                $(selection).addClass("blueSelected");
            }
        });

        function markSelects(el, e) {
            var selection = $(el).closest('.select').find(".select2-selection");
            var data = e.params.data;
            if (data.element.value !== "") {
                $(selection).addClass("blueSelected");
            } else {
                $(selection).removeClass("blueSelected");
            }
        }

    } else {
        var $selectOrderGrid = $('.select--order-grid').select2({
            width: '100%',
            containerCssClass: 'select2--order-grid'
        });

        $selectOrderGrid.on("select2:select", function (e) { $('#formsearchitems').submit(); });
    }
};

function selectPairs() {
    $('.selects-pair.from').on('change', function () {
        var to = $(this).data('pair'),
            val = parseInt($(this).val());
        setDisableToValues(to, val);
    });
    $('.selects-pair.to').on('change', function () {
        var from = $(this).data('pair'),
            val = parseInt($(this).val());

        setDisableFromValues(from, val);
    });
    $('.selects-dependent').on('change', function () {
        var el = $(this).data('pair'),
            val = $(this).val();

        setDisableUndependentValues(el, val);
    });
};


function setDisableToValues(to, val) {
    var $sel = $('select[name=' + to + ']'),
        $selID = $('#' + to),
        $opt = $($sel).find('option');
    $opt.each(function () {
        var actual = parseInt($(this).val());
        if (actual <= val) {
            $sel.find('option[value=' + actual + ']').prop('disabled', true);
            $selID.find('option[value=' + actual + ']').prop('disabled', true);
        } else {
            $sel.find('option[value=' + actual + ']').prop('disabled', false);
            $selID.find('option[value=' + actual + ']').prop('disabled', false);
        }
    });

    regenerateSelects($sel, $selID);
}

function setDisableFromValues(from, val) {
    var $sel = $('select[name=' + from + ']'),
        $selID = $('#' + from),
        $opt = $($sel).find('option');
    $opt.each(function () {
        var actual = parseInt($(this).val());
        if (actual >= val) {
            $sel.find('option[value=' + actual + ']').prop('disabled', true);
            $selID.find('option[value=' + actual + ']').prop('disabled', true);
        } else {
            $sel.find('option[value=' + actual + ']').prop('disabled', false);
            $selID.find('option[value=' + actual + ']').prop('disabled', false);
        }
    });

    regenerateSelects($sel, $selID);
}

function setDisableUndependentValues(id, val) {
    var $sel = $('select[name=' + id + ']'),
        $selID = $('#' + id),
        $opt = $($sel).find('option');
    $opt.each(function () {
        var actual = $(this).data('make'),
            actualVal = $(this).val();

        if (val == null || val == "" || actualVal == null || actualVal == "" || actual.toLowerCase() === val.toLowerCase()) {
            $sel.find('option[value="' + actualVal + '"]').prop('disabled', false);
            $selID.find('option[value="' + actualVal + '"]').prop('disabled', false);
        } else {
            $sel.find('option[value="' + actualVal + '"]').prop('disabled', true);
            $selID.find('option[value="' + actualVal + '"]').prop('disabled', true);
        }
    });

    regenerateSelects($sel, $selID);
}

function setFather(id) {
    var $sel = $('select[name=' + id + ']'),
        $selID = $('#' + id);

    regenerateSelects($sel, $selID);
};

function regenerateSelects($sel, $selID) {
    if ($sel.next().hasClass('select2')) {
        $sel.select2("destroy").select2({
            width: '100%',
            containerCssClass: 'select2--vehicle-form'
        });
    } else {
        $sel.select2({
            width: '100%',
            containerCssClass: 'select2--vehicle-form'
        });
    }

    if ($selID.next().hasClass('select2')) {
        $selID.select2("destroy").select2({
            width: '100%',
            containerCssClass: 'select2--vehicle-form'
        })
    } else {
        $selID.select2({
            width: '100%',
            containerCssClass: 'select2--vehicle-form'
        });
    }

    $sel.on('select2:select', function (e) { $('#formsearchitems').submit(); });

    $('.select--vehicle-form').on('select2:select', function (e) {
        markSelects($(this), e);
    });

    $('.select--vehicle-form').each(function () {
        if ($(this).val() !== "") {
            var selection = $(this).closest('.select').find(".select2-selection");
            $(selection).addClass("blueSelected");
        }
    });

    function markSelects(el, e) {
        var selection = $(el).closest('.select').find(".select2-selection");
        var data = e.params.data;
        if (data.element.value !== "") {
            $(selection).addClass("blueSelected");
        } else {
            $(selection).removeClass("blueSelected");
        }
    }
}

function objectFixImg() {
    objectFitImages();
}

function resetSelect2() {
    $('.selects-pair.from').each(function () {
        var to = $(this).data('pair'),
            val = parseInt($(this).val());
        setDisableToValues(to, val);
    });
    $('.selects-pair.to').each(function () {
        var from = $(this).data('pair'),
            val = parseInt($(this).val());
        setDisableFromValues(from, val);

    });
    $('.selects-dependent').each(function () {
        var el = $(this).data('pair'),
            val = $(this).val();

        setDisableUndependentValues(el, val);
    });

    setFather('cMarca');

    setFather('cGas');

    setFather('cChange');

}

//Favoritos
(function () {
    favorits();
})();

function checkVehicleId(id) {
    var $item = $('.m-vehicle-item[data-vehicleid=' + id + ']');
    if ($item.length) {
        $item.find('.a-icon-heart').addClass('active');
    }
};

function setVehicleId(id) {
    var vehicles = getCars(),
        exit = false,
        aux = vehicles.id.filter(function (vehicleid) {
            if (vehicleid !== id) {
                return vehicleid;
            } else {
                exit = true;
                return vehicleid;
            }
        })

    if (!exit) { aux.push(id); }

    vehicles.id = aux;
    setCars(vehicles);
    var def = getCars();

    $.ajax({
        url: base_url + 'ajax/commons/check_favoritos',
        type: 'POST',
        data: { my_cars: def.id },
        cache: false
    }).done(function (msg) {
        if (msg.result == "true") {
            return true;
        }
        else {
            return false;
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
    });
}

function unsetVehicleId(id) {
    var vehicles = getCars();
    aux = vehicles.id.filter(function (vehicleid) {
        if (vehicleid !== id) {
            return vehicleid;
        }
    })

    vehicles.id = aux;
    setCars(vehicles);
    var def = getCars();

    $.ajax({
        url: base_url + 'ajax/commons/check_favoritos',
        type: 'POST',
        data: { my_cars: def.id },
        cache: false
    }).done(function (msg) {
        if (msg.result == "true") {
            return true;
        }
        else {
            return false;
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
    });
}

function favorits() {
    function init() {
        var vehicles = getCars();
        vehicles.id.forEach(function (id) {
            checkVehicleId(id);
        });
    };

    init();

    $('.a-icon-heart').off('click').on('click', function () {
        var $that = $(this),
            $id = $that.closest('.m-vehicle-item').data('vehicleid');

        $that.toggleClass('active');

        if ($that.hasClass('active')) {
            setVehicleId($id);
        } else {
            unsetVehicleId($id);
        }
    });
}

(function(){
    $('.input-mirror').each(function(){
        $(this).on('keyup',function(){
            var $that_id = '#'+$(this).data("mirror");
            $($that_id).val($(this).val());
        })
    });
})();

//PETICIONS AJAX

//Registre Newsletter
function enviarformnewsletter($fr) {

    var data = new FormData();

    data.append('email', $("#email").val());
    data.append('fnews', $("#fnews").val());

    $("#btn-submit-newsletter").prop('disabled', true);
    $('.loader').show();

    $.ajax({
        url: base_url + 'ajax/contact/send_newsletter_crm',
        type: 'POST',
        contentType: false,
        data: data,
        processData: false,
        cache: false
    }).done(function (msg) {
        $('.loader').hide();
        if (msg.result == "true") {
            showNotification(msg.detail, 'success');
            $("#form-newsletter")[0].reset();
            $fr.destroy();
            $('#sms').html(msg.sms);
            return true;
        }
        else {
            showNotification(msg.detail, 'error');
            $("#btn-submit-newsletter").prop('disabled', false);
            return false;
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        $("#btn-submit-newsletter").prop('disabled', false);
        $('.loader').hide();
    });

    return false;

}

//Registre Contacte
function enviarformcontacto() {
    var data = new FormData();
    var full_url = base_url + 'ajax/contact/send_contacto_crm';

    var fnews = ($("#fnews").prop("checked") == true) ? 1 : 0;

    data.append('name', $("#name").val());
    data.append('email', $("#email").val());
    data.append('phone', $("#phone").val());
    data.append('msg', $("#msg").val());
    data.append('ipaddress', $("#ipaddress").val());
    data.append('navigator', $("#navigator").val());
    data.append('fnews', fnews);

    $("#btn-submit-contacto").prop('disabled', true);
    $('.loader').show();

    $.ajax({
        url: full_url,
        type: 'POST',
        contentType: false,
        data: data,
        processData: false,
        cache: false
    }).done(function (msg) {
        $('.loader').hide();
        if (msg.result == "true") {
            showNotification(msg.detail, 'success');
            $("#form-contacto")[0].reset();
            $("#btn-submit-contacto").prop('disabled', false);
            $('#sms').html(msg.sms);
            location.href = base_url + 'contacto/thankyou/';
            return true;
        }
        else {
            showNotification(msg.detail, 'error');
            $("#btn-submit-contacto").prop('disabled', false);
            return false;
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        $("#btn-submit-contacto").prop('disabled', false);
        $('.loader').hide();
    });

    return false;
}

//Registre reserva + tpv
function enviarreservavehiculo() {
    var data = new FormData();
    var full_url = base_url + 'ajax/contact/send_reserva_crm';
    var full_url_tpv = base_url + 'contacto/pagoTPV';
    var fnews = ($("#reserva_message-newsletter").prop("checked") == true) ? 1 : 0;
    var reserva_id = $("#reserva_id").val();
    var reserva_name = $("#reserva_name").val();
    var reserva_email = $("#reserva_email").val();
    var reserva_phone = $("#reserva_phone").val();
    var reserva_sms = $("#reserva_sms").val();
    var reserva_make = $("#reserva_make").val();
    var reserva_vehicleid = $("#reserva_vehicleid").val();
    var reserva_emailinteresado = $("#reserva_emailinteresado").val();
    var reserva_price = $("#reserva_price").val();
    var reserva_precio = $("#reserva_precio").val();
    var reserva_cp = $("#reserva_cp").val();
    var ipaddress = $("#ipaddress").val();
    var navigator = $("#navigator").val();
    var fnews = fnews;

    data.append('reserva_id', reserva_id);
    data.append('reserva_make', reserva_make);
    data.append('reserva_vehicleid', reserva_vehicleid);
    data.append('reserva_price', reserva_price);
    data.append('reserva_precio', reserva_precio);
    data.append('reserva_emailinteresado', reserva_emailinteresado);
    data.append('reserva_name', reserva_name);
    data.append('reserva_email', reserva_email);
    data.append('reserva_phone', reserva_phone);
    data.append('reserva_cp', reserva_cp);
    data.append('reserva_sms', reserva_sms);
    data.append('ipaddress', ipaddress);
    data.append('navigator', navigator);
    data.append('fnews', fnews);


    $("#btn-reserva_submit").prop('disabled', true);
    $('.loader').show();

    $.ajax({
        url: full_url,
        type: 'POST',
        contentType: false,
        data: data,
        processData: false,
        cache: false
    }).done(function (msg) {
        $('.loader').hide();
        if (msg.result == "true") {
            showNotification(msg.detail, 'success');
            $("#form-reserve")[0].reset();
            $("#btn-reserva_submit").prop('disabled', false);
            $('#sms').html(msg.sms);
            //creem formulari nou i enviem a Redsys
            form = document.createElement("form");
            form.method = "post";
            form.action = full_url_tpv;
            input_reserva_id = document.createElement("input");
            input_reserva_id.name = 'reserva_id';
            input_reserva_id.value = reserva_id;
            input_reserva_name = document.createElement("input");
            input_reserva_name.name = 'reserva_name';
            input_reserva_name.value = reserva_name;
            input_reserva_email = document.createElement("input");
            input_reserva_email.name = 'reserva_email';
            input_reserva_email.value = reserva_email;
            input_reserva_phone = document.createElement("input");
            input_reserva_phone.name = 'reserva_phone';
            input_reserva_phone.value = reserva_phone;
            input_reserva_sms = document.createElement("input");
            input_reserva_sms.name = 'reserva_sms';
            input_reserva_sms.value = reserva_sms;
            input_reserva_make = document.createElement("input");
            input_reserva_make.name = 'reserva_make';
            input_reserva_make.value = reserva_make;
            input_reserva_vehicleid = document.createElement("input");
            input_reserva_vehicleid.name = 'reserva_vehicleid';
            input_reserva_vehicleid.value = reserva_vehicleid;
            input_reserva_emailinteresado = document.createElement("input");
            input_reserva_emailinteresado.name = 'reserva_emailinteresado';
            input_reserva_emailinteresado.value = reserva_emailinteresado;
            input_reserva_price = document.createElement("input");
            input_reserva_price.name = 'reserva_price';
            input_reserva_price.value = reserva_price;
            input_reserva_cp = document.createElement("input");
            input_reserva_precio = document.createElement("input");
            input_reserva_precio.name = 'reserva_precio';
            input_reserva_precio.value = reserva_precio;
            input_reserva_cp = document.createElement("input");
            input_reserva_cp.name = 'reserva_cp';
            input_reserva_cp.value = reserva_cp;
            input_reserva_ipaddress = document.createElement("input");
            input_reserva_ipaddress.name = 'ipaddress';
            input_reserva_ipaddress.value = ipaddress;
            input_reserva_navigator = document.createElement("input");
            input_reserva_navigator.name = 'navigator';
            input_reserva_navigator.value = navigator;
            input_reserva_fnews = document.createElement("input");
            input_reserva_fnews.name = 'fnews';
            input_reserva_fnews.value = fnews;
            form.appendChild(input_reserva_id);
            form.appendChild(input_reserva_name);
            form.appendChild(input_reserva_email);
            form.appendChild(input_reserva_phone);
            form.appendChild(input_reserva_sms);
            form.appendChild(input_reserva_make);
            form.appendChild(input_reserva_vehicleid);
            form.appendChild(input_reserva_emailinteresado);
            form.appendChild(input_reserva_price);
            form.appendChild(input_reserva_cp);
            form.appendChild(input_reserva_ipaddress);
            form.appendChild(input_reserva_navigator);
            form.appendChild(input_reserva_fnews);
            document.body.appendChild(form);
            form.submit();
            return true;
        }
        else {
            showNotification(msg.detail, 'error');
            $("#btn-reserva_submit").prop('disabled', false);
            return false;
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        $("#btn-reserva_submit").prop('disabled', false);
        $('.loader').hide();
    });

    return false;
}

//formulari Keygonline
function enviarformkeygonline() {
    var data = new FormData();
    var full_url = base_url + 'ajax/contact/send_keygonline_crm';
    var fnews = ($("#fnews").prop("checked") == true) ? 1 : 0;

    data.append('name', $("#name").val());
    data.append('email', $("#email").val());
    data.append('phone', $("#phone").val());
    data.append('vehiculos', $("#vehiculos").val());
    data.append('dia', $("#dia").val());
    data.append('hora', $("#hora").val());
    data.append('msg', $("#msg").val());
    data.append('ipaddress', $("#ipaddress").val());
    data.append('navigator', $("#navigator").val());
    data.append('fnews', fnews);

    $("#btn-submit-keygonline").prop('disabled', true);
    $('.loader').show();

    $.ajax({
        url: full_url,
        type: 'POST',
        contentType: false,
        data: data,
        processData: false,
        cache: false
    }).done(function (msg) {
        $('.loader').hide();
        if (msg.result == "true") {
            showNotification(msg.detail, 'success');
            $("#form-video-cita")[0].reset();
            $("#btn-submit-keygonline").prop('disabled', false);
            $('#sms').html(msg.sms);
            location.href = base_url + 'contacto/thankyoukeygonline/';
            return true;
        }
        else {
            showNotification(msg.detail, 'error');
            $("#btn-submit-keygonline").prop('disabled', false);
            return false;
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        $("#btn-submit-keygonline").prop('disabled', false);
        $('.loader').hide();
    });

    return false;
}

//Formulario vehículo
function enviarformvehiculo() {

    var data = new FormData();
    var full_url = base_url + 'ajax/contact/send_vehiculo_crm';

    var fnews = ($("#message-newsletter").prop("checked") == true) ? 1 : 0;

    data.append('name', $("#vehiculo_name").val());
    data.append('email', $("#vehiculo_email").val());
    data.append('phone', $("#vehiculo_phone").val());
    data.append('cp', $("#vehiculo_cp").val());
    data.append('msg', $("#vehiculo_sms").val());
    data.append('vehicleid', $("#vehicleid").val());
    data.append('precio', $("#precio").val());
    data.append('make', $("#make").val());
    data.append('emailinteresado', $("#emailinteresado").val());
    data.append('fnews', fnews);

    $("#btn-submit-vehiculo").prop('disabled', true);
    $('.loader').show();

    $.ajax({
        url: full_url,
        type: 'POST',
        contentType: false,
        data: data,
        processData: false,
        cache: false
    }).done(function (msg) {
        $('.loader').hide();
        if (msg.result == "true") {
            showNotification(msg.detail, 'success');
            $("#form-vsingle")[0].reset();
            $("#btn-submit-vehiculo").prop('disabled', false);
            $('#sms').html(msg.sms);
            location.href = base_url + 'contacto/thankyouvsingle/';
            return true;
        }
        else {
            showNotification(msg.detail, 'error');
            $("#btn-submit-vehiculo").prop('disabled', false);
            return false;
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        $("#btn-submit-vehiculo").prop('disabled', false);
        $('.loader').hide();
    });

    return false;

}

function vehicleSlides(ocasion, id) {
    if(ocasion !== null){
        var content = '';
        for (var i = 0; i < ocasion.length; i++) {
            switch (ocasion[i].tipoficha) {
                case '1':
                    content += vehicleT1(ocasion[i]);
                    break;
                case '2':
                    content += vehicleT2(ocasion[i]);
                    break;
                case '3':
                    content += vehicleT3(ocasion[i]);
                    break;
                case '4':
                    content += vehicleT4(ocasion[i]);
                    break;
                case '5':
                    content += vehicleT5(ocasion[i]);
                    break;
            }
        }
    
        $(id).append(content);
        $(id).find('.m-vehicle-item').each(function () {
            $(this).addClass("swiper-slide");
        });
    }
}

// Params Vehicles
function paramsVehicle() {
    var order = getSearch(),
        json = order.search;

    if (json.length > 0) {
        clearForm();
        json.forEach(function (el) {
            var name = el.name,
                value = el.value;

            switch (el.type) {
                case 'select':
                    $('select[name=' + name + ']').val(value);
                    $('#' + name).val(value);
                    break;
                case 'input':
                    $('input[name=' + name + ']').val(value);
                    $('#' + name).val(value);
                    break;
                case 'multiple':
                    $('.m-group-values.' + name).find('.mgv-option[data-option="' + value + '"]').addClass('active');
                    $('.m-group-values.' + name).find('.input-val').val(value);
                    break;
                case 'flota':
                    $('.m-flota-pick-item.' + name).addClass('active');
                    $('.m-flota-pick-item.' + name).find("input:checkbox").prop('checked', true);
                    $('.m-flota-selec-item.' + name).addClass('active');
                    $('.m-flota-selec-item.' + name).find("input:checkbox").prop('checked', true);
                    break;
                case 'orderby':
                    $('select[name=' + name + ']').val(value);
                    break;
                case 'cartype':
                    $('#cartype').val(value);
                    break;
                case 'sales':
                    $('#sales').prop('checked', true);
                    $('#oferta').prop('checked', true);
                    break;
                default:
                    break;
            }
        });
    }

    resetSelect2();
};

function storeFrom(tipus_form) {

    //var kSelects = ['cMarca','cModel','cKmFrom','cKmTo','cPriceFrom','cPriceTo','cYearFrom','cYearTo','cPowerFrom','cPowerTo','cGas','cChange','cColor','cLocation'],
    var kSelects = ['cMarca', 'cModel', 'cKmFrom', 'cKmTo', 'cPriceFrom', 'cPriceTo', 'cYearFrom', 'cYearTo', 'cPowerFrom', 'cPowerTo', 'cGas', 'cChange'],
        kMultiple = ['cSeats', 'cDoors'],
        kInput = ['search'],
        arr = [];

    if (tipus_form == 'mobile' && (typeof tipus_form !== 'undefined')) { //mobile

        kSelects.forEach(function (key) {
            var val = $("#" + key).val();

            if (val !== "") {
                var suborder = {};
                suborder.type = 'select';
                suborder.name = key;
                suborder.value = val;

                arr.push(suborder);
            }
        });

        kMultiple.forEach(function (key) {
            if ($("#" + key).val() !== "") {
                var suborder = {};
                suborder.type = 'multiple';
                suborder.name = key;
                suborder.value = $("#" + key).val();
                arr.push(suborder);
            }
        });

        $(".m-form--flota--mobile .m-flota-selec-item input:checkbox:checked").each(function () {
            var suborder = {};
            suborder.type = 'flota';
            suborder.name = $(this).val();
            suborder.value = $(this).val();
            arr.push(suborder);
        });

        $("#sales:checked").each(function () {
            var suborder = {};
            suborder.type = 'sales';
            suborder.name = 'sales';
            suborder.value = $(this).val();
            arr.push(suborder);
        });

        kInput.forEach(function (key) {
            var suborder = {};
            suborder.type = 'input';
            suborder.name = key;
            suborder.value = $("#" + key).val();
            arr.push(suborder);
        });

        if ($('#formsearchitems').length != 0) {
            $('#cartype').each(function () {
                var suborder = {};
                suborder.type = 'cartype';
                suborder.name = 'cartype';
                suborder.value = $(this).val();
                arr.push(suborder);
            });

            $("#orderBy").each(function () {
                var suborder = {};
                suborder.type = 'orderby';
                suborder.name = 'orderBy';
                suborder.value = $(this).val();
                arr.push(suborder);
            });
        }
    } else if (tipus_form == 'home' && (typeof tipus_form !== 'undefined')) {
        kSelects.forEach(function (key) {
            var val = $("select[name=" + key + "]").val();

            if (val !== "") {
                var suborder = {};
                suborder.type = 'select';
                suborder.name = key;
                suborder.value = val;
                arr.push(suborder);
            }
        });

        kMultiple.forEach(function (key) {
            if ($("#id" + key).val() !== "") {
                var suborder = {};
                suborder.type = 'multiple';
                suborder.name = key;
                suborder.value = $("#id" + key).val();
                arr.push(suborder);
            }
        });

        $(".m-form--flota--home .m-flota-pick-item input:checkbox:checked").each(function () {
            var suborder = {};
            suborder.type = 'flota';
            suborder.name = $(this).closest('.m-flota-pick-item').data('flota');
            suborder.value = $(this).val();
            arr.push(suborder);
        });

        $(".m-form--flota--reduced .m-flota-pick-item input:checkbox:checked").each(function () {
            var suborder = {};
            suborder.type = 'flota';
            suborder.name = $(this).closest('.m-flota-pick-item').data('flota');
            suborder.value = $(this).val();
            arr.push(suborder);
        });

        $("#oferta:checked").each(function () {
            var suborder = {};
            suborder.type = 'sales';
            suborder.name = 'sales';
            suborder.value = $(this).val();
            arr.push(suborder);
        });

        if ($('#formsearchitems').length != 0) {
            kInput.forEach(function (key) {
                var suborder = {};
                suborder.type = 'input';
                suborder.name = key;
                suborder.value = $("input[name=" + key + "]").val();
                arr.push(suborder);
            });

            $('#cartype').each(function () {
                var suborder = {};
                suborder.type = 'cartype';
                suborder.name = 'cartype';
                suborder.value = $(this).val();
                arr.push(suborder);
            });

            $("#orderBy").each(function () {
                var suborder = {};
                suborder.type = 'orderby';
                suborder.name = 'orderBy';
                suborder.value = $(this).val();
                arr.push(suborder);
            });
        }
    };



    setSearch({ search: arr });
}

function searchMobile(e, tipus_form) {
    e.preventDefault();
    storeFrom(tipus_form);
    paramsVehicle();
    $('#formsearchitems').submit();
    $('.o-form--mobile').fadeOut();
}

function searchAndGo(e, tipus_form) {
    e.preventDefault();
    storeFrom(tipus_form);
    location.href = base_url + 'vehiculos/';
}