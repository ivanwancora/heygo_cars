
function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}

function numCheck(e, field) { 
	key = e.keyCode ? e.keyCode : e.which; 
	// backspace 
	if (key == 8) 
		return true; 
	// 0-9 
	if (key > 47 && key < 58) { 
		if (field.value == "") 
			return true; 
		regexp = /.[0-9]{6}$/; 
		return !(regexp.test(field.value)); 
	} 
	// . 
	if (key == 46) {
		if (field.value == "") 
			return false; 
		regexp = /^[0-9]+$/;
		return regexp.test(field.value);
	} 
	// other key 
	return false; 
}

function isDate(string){ 
	//string estará en formato dd/mm/yyyy (dí­as < 32 y meses < 13)
	var ExpReg = /^([0][1-9]|[12][0-9]|3[01])(\/|-)([0][1-9]|[1][0-2])\2(\d{4})$/;
	return (ExpReg.test(string));
}

function netejarForm(miForm) {
	// recorremos todos los campos que tiene el formulario
	$(':input', miForm).each(function() {
		var type = this.type;
		var tag = this.tagName.toLowerCase();
		//limpiamos los valores de los campos…
		if (type == 'text' || type == 'password' || type == 'email' || tag == 'textarea')
			this.value = "";
		// excepto de los checkboxes y radios, le quitamos el checked
		// pero su valor no debe ser cambiado
		else if (type == 'checkbox' || type == 'radio')
			this.checked = false;
		// los selects le ponesmos el indice a 0
		else if (tag == 'select')
			this.selectedIndex = 0;
	});
}

function validateEmail(_email) {
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if( !emailReg.test( _email ) ) {
		return false;
	} else {
		return true;
	}
}

function netejarfitxer(idfitxer){
	var input = $('#'+idfitxer);  
	input.replaceWith(input.val('').clone(true));  
}

//Aquest mètode retorna el valor2 per substituir en cas que el valor1 sigui null
function nvl(value1,value2){
	if (value1 == null)
		return value2;
	return value1;
}

//Aquest mètode rep un numero i posa els punts per cada miler ex: 4.000.000,00
function numformat(x){
	if(x == null || isNaN(x)) return x;
	var parts = x.toString().split(".");
	if(parts.length == 1 && isNaN(parts[0])) return x;
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    return parts.join(",");
}

//Mètode que retorna una cadena passada per paràmetre amb la primera lletra amb majúscules
function upperFirstLetter(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}

//Funció que donada una data en format String DDMMYYYYHHMMSS la converteix en UTC y després 
//la retorna en format String hora local del tipus DD/MM/YYYY HH:MM:SS
function convertStringDateToLocaleFromUTC(stringDate){
	var dia = stringDate.substring(0, 2);
    var mes = stringDate.substring(2, 4);
    var any = stringDate.substring(4, 8);
    var hora = stringDate.substring(8, 10);
    var minut = stringDate.substring(10, 12);
    var segon = stringDate.substring(12, 14);
    //Convertim la data en format UTC ja que es com la mostra el servidor EDS                                
    var dateUTC = new Date(Date.UTC(any, Number(mes)-1, dia, hora, minut, segon));   
    return dateUTC.toLocaleString();

}

function convertStringToDate(stringDate){
	var dia = stringDate.substring(0, 2);
    var mes = stringDate.substring(2, 4);
    var any = stringDate.substring(4, 8);
    var hora = stringDate.substring(8, 10);
    var minut = stringDate.substring(10, 12);
    var segon = stringDate.substring(12, 14);                                   
    return new Date(any, Number(mes)-1, dia, hora, minut, segon).toLocaleString();       

}


//Retorna un número entre el inferior i el superior.
function numAleatorio(inferior, superior){
   var numPosibilidades = superior - inferior;
   var aleat = Math.random() * numPosibilidades;
   aleat = Math.floor(aleat);
   return parseInt(inferior) + aleat;
} 

//Funció que retorna un color en Hexadecimal de forma aleatoria.
function getRandomHexaColor(){
   var hexadecimal = new Array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F");
   var randomColor = "#";
   for (var i=0; i<6; i++){
      posarray = numAleatorio(0, hexadecimal.length);
      randomColor += hexadecimal[posarray];
   }
   return randomColor;
} 


function isEmptyJSON(obj) {
  for(var i in obj) { return false; }
  return true;
}



function cargarModal(_title, _content){	
	$('#headermodal').html(_title);
    $('#bodymodal').html(_content);
    $('#ventanamodal').modal('show'); 

}

function mostrarVistaPrevia(inputfile){
 
    var Archivos, Lector;     
 
    Archivos = $('#'+inputfile)[0].files;
    if(Archivos.length>0){
         
        Lector = new FileReader();
        Lector.onloadend = function(e){
            var origen, tipo;
 
            //Envía la imagen a la pantalla
            origen = e.target; //objeto FileReader
             
            //Prepara la información sobre la imagen
            tipo = obtenerTipoMIME(origen.result.substring(0, 30));
 
            $('#'+inputfile).text(Archivos[0].name + ' (Tipo: ' + tipo + ')');
            $('#'+inputfile).text('Tamaño: ' + e.total + ' bytes');
            //Si el tipo de archivo es válido lo muestra, 
            //sino muestra un mensaje 
            if(tipo!=='image/jpeg' && tipo!=='image/png' && tipo!=='image/jpg'){
                cargarModal('<strong>Uupps! </strong>El formato de imagen no es válido: debe seleccionar una imagen JPG o PNG.');                
            }
            else{
                $('.vistaprevia').attr('src', origen.result);
            }
 
        };
        Lector.onerror = function(e){
            console.log(e)
        }
        Lector.readAsDataURL(Archivos[0]); 
 
    }
    else{
        var objeto = $('#'+inputfile).val();
        objeto.replaceWith(objeto.val('').clone());
        $('.vistaprevia').attr('src', base_url+origen.result);
        $('#'+inputfile).text('[Seleccione una imagen]');
        $('#'+inputfile).text('');
    };
 
 
};

function borrarVistaPrevia(inputfile){
    netejarfitxer(inputfile);
    $('.vistaPrevia').attr('src', base_url+'assets/img/default-profile.png');
    $('#'+inputfile).text('[Seleccione una imagen]');
    $('#'+inputfile).text('');
}

//Lee el tipo MIME de la cabecera de la imagen
function obtenerTipoMIME(cabecera){
    return cabecera.replace(/data:([^;]+).*/, '\$1');
}

// Create Element.remove() function if not exist
if (!('remove' in Element.prototype)) {
    Element.prototype.remove = function() {
        if (this.parentNode) {
            this.parentNode.removeChild(this);
        }
    };
}