-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-06-2019 a las 17:43:09
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bbdd_heygocars`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traducciones`
--

CREATE TABLE `traducciones` (
  `id` bigint(20) NOT NULL,
  `clave` text NOT NULL COMMENT 'Clave que puede ser la palabra o frase a traducir o el campo de la tabla que hace referencia',
  `es` text,
  `en` text,
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `traducciones`
--

INSERT INTO `traducciones` (`id`, `clave`, `es`, `en`, `creado`, `modificado`) VALUES
(275, 'header_mail', 'hola@heygocars.com', 'hola@heygocars.com', '2019-03-25 13:27:55', '2019-04-08 09:58:26'),
(277, 'header_telf_num', '34685188453', '34685188453', '2019-03-25 13:28:50', '2019-06-03 09:36:50'),
(278, 'header_telf', '(+34) 685 188 453', '(+34) 685 188 453', '2019-03-25 13:32:38', '2019-06-03 09:36:35'),
(279, 'header_encuentra', 'encuentra <span>tu coche ideal</span>', 'encuentra <span>tu coche ideal</span>', '2019-04-08 09:59:16', '2019-04-08 09:59:16'),
(280, 'menu_inicio', 'Inicio', 'Home', '2019-04-08 10:02:36', '2019-04-08 10:02:36'),
(281, 'menu_vehiculos', 'Vehículos', 'Vehicles', '2019-04-08 10:03:21', '2019-04-08 10:03:21'),
(282, 'menu_centros', 'Centros', 'Centers', '2019-04-08 10:03:45', '2019-04-08 10:03:45'),
(283, 'menu_empresa', 'Empresa', 'Company', '2019-04-08 10:04:25', '2019-04-08 10:04:25'),
(284, 'menu_noticias', 'Noticias', 'News', '2019-04-08 10:04:34', '2019-04-08 10:04:34'),
(285, 'menu_alquiler', 'Alquiler', 'Rent', '2019-04-08 10:04:48', '2019-04-08 10:04:48'),
(286, 'menu_contatco', 'Contacto', 'Contact', '2019-04-08 10:04:57', '2019-04-08 10:04:57'),
(287, 'footer_direccion', 'Avda. Real Monasterio Sta María del Poblet nº 82 N-III; Vía de Servicio de Valencia al Aeropuerto) 46930 Quart de Poblet (Valencia)', 'Avda. Real Monasterio Sta María del Poblet nº 82 N-III; Vía de Servicio de Valencia al Aeropuerto) 46930 Quart de Poblet (Valencia)', '2019-04-08 10:05:52', '2019-06-03 09:56:03'),
(288, 'socket_srqu', 'brand & web design by', 'brand & web design by', '2019-04-08 10:11:32', '2019-04-08 10:11:32'),
(289, 'socket_privacidad', 'Política de privacidad', 'Privacy policy', '2019-04-08 10:11:59', '2019-04-08 10:11:59'),
(290, 'socket_cookies', 'Uso de cookies', 'Use of cookies', '2019-04-08 10:12:13', '2019-04-08 10:12:13'),
(291, 'socket_legal', 'Aviso legal', 'Legal warning', '2019-04-08 10:12:32', '2019-04-08 10:12:32'),
(292, 'quienes_titulo', 'Quiénes somos', 'About us', '2019-04-08 10:17:53', '2019-04-08 10:17:53'),
(293, 'quienes_subheader', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit tempor incididunt ut labore et dolore magna aliqua.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit tempor incididunt ut labore et dolore magna aliqua.', '2019-04-08 10:18:28', '2019-04-08 10:18:28'),
(294, 'quienes_subcontent', 'Lorem ipsum dolor sit amet, consectetur adipiscing', 'Lorem ipsum dolor sit amet, consectetur adipiscing', '2019-04-08 10:18:57', '2019-04-08 10:18:57'),
(295, 'quienes_content', 'HeyGo CARS pertenece al Grupo Ugarte Automoción, un grupo empresarial con más de cincuenta años de experiencia en el sector.<br><br>\r\n                    En nuestras instalaciones encontrarás un amplio stock de vehículos de ocasión, seminuevos y de segunda mano en constante renovación.<br><br>\r\n                    Ven a visitarnos a nuestras instalaciones de Quart de Poblet, en la Vía de Servicio de Valencia al Aeropuerto.', 'HeyGo CARS pertenece al Grupo Ugarte Automoción, un grupo empresarial con más de cincuenta años de experiencia en el sector.<br><br>\r\n                    En nuestras instalaciones encontrarás un amplio stock de vehículos de ocasión, seminuevos y de segunda mano en constante renovación.<br><br>\r\n                    Ven a visitarnos a nuestras instalaciones de Quart de Poblet, en la Vía de Servicio de Valencia al Aeropuerto.', '2019-04-08 10:19:30', '2019-04-08 10:19:30'),
(296, 'general_masinfo', 'más info', 'more info', '2019-04-08 10:19:55', '2019-04-08 10:19:55'),
(297, 'news_titulo', 'Noticias', 'News', '2019-04-08 10:25:27', '2019-04-08 10:25:27'),
(298, 'news_subheader', 'Mantente siempre informado. ¡Te contamos las últimas noticias del sector automovilístico en el blog de HeyGo Cars ', 'Mantente siempre informado. ¡Te contamos las últimas noticias del sector automovilístico en el blog de HeyGo Cars ', '2019-04-08 10:25:59', '2019-04-08 10:25:59'),
(299, 'general_view_map', 'ver en mapa', 'view in map', '2019-04-08 12:36:57', '2019-06-03 08:54:22'),
(300, 'centro_att', 'Atención a cliente:', 'Customer service:', '2019-04-08 12:42:39', '2019-04-08 12:42:39'),
(301, 'general_ver_stock', 'ver stock', 'view stock', '2019-04-08 12:44:05', '2019-04-08 12:44:05'),
(302, 'centros_title', 'Centros HeyGo!', 'Centros HeyGo!', '2019-04-08 14:23:05', '2019-04-08 14:23:05'),
(303, 'centros_subheader', 'Estás a un paso de estrenar coche.<br>\r\n            ¡Descubre tu oficina HeyGo Cars más cercana!', 'Estás a un paso de estrenar coche.<br>\r\n            ¡Descubre tu oficina HeyGo Cars más cercana!', '2019-04-08 14:23:54', '2019-04-08 14:23:54'),
(304, 'marcas_title', 'Marcas más buscadas ', 'Our brands', '2019-04-08 15:02:41', '2019-06-03 08:56:55'),
(305, 'marcas_subheader', 'Trabajamos con las mejores marcas y tenemos en nuestro stock modelos de coches de última generación', 'Trabajamos con las mejores marcas y tenemos en nuestro stock modelos de coches de última generación', '2019-04-08 15:02:52', '2019-04-08 15:02:52'),
(306, 'nuestros_vehiculos_titulo', 'Nuestros vehículos', 'Our Vehicle', '2019-04-08 15:31:07', '2019-04-08 15:31:07'),
(307, 'nuestros_vehiculos_subheader', 'Coches nuevos, de segunda mano o de km0.<br>\r\n            Todas las gamas, marcas y modelos. Tenemos el vehículo que buscas.', 'Coches nuevos, de segunda mano o de km0.<br>\r\n            Todas las gamas, marcas y modelos. Tenemos el vehículo que buscas.', '2019-04-08 15:31:18', '2019-04-08 15:31:18'),
(308, 'nuestros_vehiculos_dest1_titulo', 'Prueba tu coche antes de comprarlo', 'Prueba tu coche antes de comprarlo', '2019-04-08 15:32:23', '2019-04-08 15:32:23'),
(309, 'nuestros_vehiculos_dest2_titulo', 'Tu coche con hasta 2 años de garantía', 'Tu coche con hasta 2 años de garantía', '2019-04-08 15:32:37', '2019-04-08 15:32:37'),
(310, 'nuestros_vehiculos_dest3_titulo', 'Valoración y compra de tu vehículo', 'Valoración y compra de tu vehículo', '2019-04-08 15:33:10', '2019-04-08 15:33:10'),
(311, 'nuestros_vehiculos_dest4_titulo', 'Financiación flexible 100% a tu medida', 'Financiación flexible 100% a tu medida', '2019-04-08 15:33:30', '2019-04-08 15:33:30'),
(312, 'nuestros_vehiculos_dest1_desc', 'En HeyGo Cars te lo ponemos fácil. Prueba tu coche antes de comprarlo. El test drive es la mejor manera de encontrar el vehículo que mejor se adapta a tus necesidades.', 'En HeyGo Cars te lo ponemos fácil. Prueba tu coche antes de comprarlo. El test drive es la mejor manera de encontrar el vehículo que mejor se adapta a tus necesidades.', '2019-04-08 15:34:23', '2019-04-08 15:34:23'),
(313, 'nuestros_vehiculos_dest2_desc', 'Todos nuestros vehículos están totalmente revisados y certificados por nuestro equipo técnico. Además, disponen, como mínimo, de un año de garantía, llegando en muchos casos hasta los 2 años.', 'Todos nuestros vehículos están totalmente revisados y certificados por nuestro equipo técnico. Además, disponen, como mínimo, de un año de garantía, llegando en muchos casos hasta los 2 años.', '2019-04-08 15:34:45', '2019-04-08 15:34:45'),
(314, 'nuestros_vehiculos_dest3_desc', 'Si por algo destaca nuestro equipo, es por la precisión de su trabajo. A nuestros expertos no se les escapa nada, y se encargan de certificar el estado de nuestros vehículos con la mayor precisión. No te quedes con ninguna duda ¡pregúntanos todo lo que necesites saber!', 'Si por algo destaca nuestro equipo, es por la precisión de su trabajo. A nuestros expertos no se les escapa nada, y se encargan de certificar el estado de nuestros vehículos con la mayor precisión. No te quedes con ninguna duda ¡pregúntanos todo lo que necesites saber!', '2019-04-08 15:35:07', '2019-04-08 15:35:07'),
(315, 'nuestros_vehiculos_dest4_desc', '¿Prefieres pago al contado o financiado? Elige la forma que mejor se adapte a ti. En HeyGo Cars trabajamos con las mejores entidades financieras, para que disfrutes de las mejores condiciones en el pago de tu nuevo coche.', '¿Prefieres pago al contado o financiado? Elige la forma que mejor se adapte a ti. En HeyGo Cars trabajamos con las mejores entidades financieras, para que disfrutes de las mejores condiciones en el pago de tu nuevo coche.', '2019-04-08 15:35:21', '2019-04-08 15:35:21'),
(316, 'general_ver_stock_vehiculos', 'ver stock vehículos', 'ver stock vehículos', '2019-04-08 15:41:37', '2019-04-08 15:41:37'),
(317, 'ofertas_titulo', 'Ofertas Especiales', 'Special offers', '2019-04-08 15:42:59', '2019-04-08 15:42:59'),
(318, 'ofertas_subheader', 'En HeyGo Cars dispones de la mejor oferta de vehículos nuevos, seminuevos y de km0, al mejor precio.', 'En HeyGo Cars dispones de la mejor oferta de vehículos nuevos, seminuevos y de km0, al mejor precio.', '2019-04-08 15:43:19', '2019-04-08 15:43:19'),
(319, 'ofertas_ocasion', 'Ocasión', 'Ocasión', '2019-04-08 15:44:03', '2019-04-08 15:44:03'),
(320, 'ofertas_km', 'Km0', 'Km0', '2019-04-08 15:44:16', '2019-04-08 15:44:16'),
(321, 'ofertas_seminuevos', 'seminuevos', 'seminuevos', '2019-04-08 15:44:25', '2019-04-08 15:44:25'),
(322, 'filtro_title', '¿Qué tipo de coche buscas?', '¿Qué tipo de coche buscas?', '2019-04-08 15:45:27', '2019-04-08 15:45:27'),
(323, 'filtro_reset', 'eliminar filtros', 'eliminar filtros', '2019-04-08 15:46:42', '2019-04-08 15:46:42'),
(324, 'filtro_ofertas', 'solo ofertas', 'solo ofertas', '2019-04-08 15:46:57', '2019-04-08 15:46:57'),
(325, 'filtro_vermas', 'ver más filtros', 'ver más filtros', '2019-04-08 15:47:09', '2019-04-08 15:47:09'),
(326, 'filtro_vermenos', 'ver menos filtros', 'ver menos filtros', '2019-04-08 15:47:20', '2019-04-08 15:47:20'),
(327, 'filtro_search', 'buscar', 'search', '2019-04-08 15:47:29', '2019-04-08 15:47:29'),
(328, 'filtro_tipo_vehiculo_title', 'Tipo de vehículo:', 'Tipo de vehículo:', '2019-04-09 07:41:46', '2019-04-09 07:41:46'),
(329, 'filtro_marcavehiculo', 'MARCA vehículo', 'MARCA vehículo', '2019-04-09 07:46:33', '2019-04-09 07:46:33'),
(330, 'filtro_modelovehiculo', 'MODELO vehículo', 'MODELO vehículo', '2019-04-09 07:46:43', '2019-04-09 07:46:43'),
(331, 'filtro_kmprecio', 'Kilometros/Precio', 'Kilometros/Precio', '2019-04-09 07:46:54', '2019-04-09 07:46:54'),
(332, 'filtro_km', 'Kilómetros', 'Kilómetros', '2019-04-09 07:47:08', '2019-04-09 07:47:08'),
(333, 'filtro_fromkm', 'KMS desde', 'KMS desde', '2019-04-09 07:47:19', '2019-04-09 07:47:19'),
(334, 'filtro_tokm', 'KMS hasta', 'KMS hasta', '2019-04-09 07:47:32', '2019-04-09 07:47:32'),
(335, 'filtro_precio', 'Precio', 'Price', '2019-04-09 07:47:44', '2019-04-09 07:47:44'),
(336, 'filtro_fromprecio', 'PRECIO desde', 'PRECIO desde', '2019-04-09 07:47:54', '2019-04-09 07:47:54'),
(337, 'filtro_toprecio', 'PRECIO hasta', 'PRECIO hasta', '2019-04-09 07:48:13', '2019-04-09 07:48:13'),
(338, 'filtro_yearmat', 'Año matriculación', 'Año matriculación', '2019-04-09 07:48:35', '2019-04-09 07:48:35'),
(339, 'filtro_fromyearmat', 'AÑO desde', 'AÑO desde', '2019-04-09 07:49:00', '2019-04-09 07:49:00'),
(340, 'filtro_toyearmat', 'AÑO hasta', 'AÑO hasta', '2019-04-09 07:49:11', '2019-04-09 07:49:11'),
(341, 'filtro_potencia', 'Potencia', 'Potencia', '2019-04-09 07:49:19', '2019-04-09 07:49:19'),
(342, 'filtro_frompotencia', 'POTENCIA desde', 'POTENCIA desde', '2019-04-09 07:49:30', '2019-04-09 07:49:30'),
(343, 'filtro_topotencia', 'POTENCIA hasta', 'POTENCIA hasta', '2019-04-09 07:49:39', '2019-04-09 07:49:39'),
(344, 'filtro_otros', 'Otros', 'Otros', '2019-04-09 07:49:51', '2019-04-09 07:49:51'),
(345, 'filtro_combustible', 'COMBUSTIBLE', 'COMBUSTIBLE', '2019-04-09 07:50:02', '2019-04-09 07:50:02'),
(346, 'filtro_cambio', 'CAMBIO', 'CAMBIO', '2019-04-09 07:50:10', '2019-04-09 07:50:10'),
(347, 'filtro_color', 'COLOR', 'COLOR', '2019-04-09 07:50:22', '2019-04-09 07:50:22'),
(348, 'filtro_equipamiento', 'EQUIPAMIENTO', 'EQUIPAMIENTO', '2019-04-09 07:50:34', '2019-04-09 07:50:34'),
(349, 'filtro_localizacion', 'Localización', 'Localización', '2019-04-09 07:50:43', '2019-04-09 07:50:43'),
(350, 'filtro_provincia', 'PROVINCIA', 'PROVINCIA', '2019-04-09 07:50:52', '2019-04-09 07:50:52'),
(351, 'filtro_nplazas', 'Nº Plazas', 'Nº Plazas', '2019-04-09 07:51:05', '2019-04-09 07:51:05'),
(352, 'filtro_npuertas', 'Nª Puertas', 'Nª Puertas', '2019-04-09 07:51:20', '2019-04-09 07:51:20'),
(353, 'filter_selecciona', 'Selecciona un o más tipos', 'Selecciona un o más tipos', '2019-04-09 11:42:44', '2019-04-09 11:42:44'),
(354, 'vehiculos_title', 'Stock Vehículos', 'Stock Vehículos', '2019-04-11 12:49:52', '2019-04-11 12:49:52'),
(355, 'vehiculos_subtitle', 'Berlinas, familiares, industriales, todoterrenos… En HeyGo Cars contamos con más de 500 vehículos en nuestro stock. ¡Tenemos el coche que estás buscando!', 'Berlinas, familiares, industriales, todoterrenos… En HeyGo Cars contamos con más de 500 vehículos en nuestro stock. ¡Tenemos el coche que estás buscando!', '2019-04-11 12:50:03', '2019-04-11 12:50:03'),
(356, 'vehiculos_search_filter', 'filtrar <span>búsqueda</span>', 'filtrar <span>búsqueda</span>', '2019-04-11 12:50:36', '2019-04-11 12:50:36'),
(357, 'vehiculos_wish', 'favoritos', 'favoritos', '2019-04-11 12:51:20', '2019-04-11 12:51:20'),
(358, 'vehiculos_ocasion', 'Ocasión', 'Ocasión', '2019-04-11 12:54:23', '2019-04-11 12:54:23'),
(359, 'vehiculos_km0', 'km0', 'km0', '2019-04-11 12:54:42', '2019-04-11 12:54:42'),
(360, 'vehiculos_seminuevos', 'seminuevos', 'seminuevos', '2019-04-11 12:55:15', '2019-04-11 12:55:15'),
(361, 'vehiculos_found', 'vehículos encontrados', 'vehículos encontrados', '2019-04-11 12:57:06', '2019-04-11 12:57:06'),
(362, 'vehiculos_orderby', 'ordenar por:', 'order by:', '2019-04-11 12:57:56', '2019-04-11 12:57:56'),
(363, 'vehiculos_orderby_maxprice', 'mayor precio', 'mayor precio', '2019-04-11 12:58:20', '2019-04-11 12:58:20'),
(364, 'vehiculos_orderby_minprice', 'menor precio', 'menor precio', '2019-04-11 12:58:47', '2019-04-11 12:58:47'),
(365, 'vehiculos_orderby_old', 'antigüedad', 'antigüedad', '2019-04-11 12:59:03', '2019-04-11 12:59:03'),
(366, 'vehiculos_orderby_new', 'más recientes', 'más recientes', '2019-04-11 12:59:12', '2019-04-11 12:59:12'),
(367, 'pagination_prev', 'Anterior', 'Preview', '2019-04-11 13:04:01', '2019-04-11 13:04:01'),
(368, 'pagination_next', 'Siguiente', 'Next', '2019-04-11 13:04:11', '2019-04-11 13:04:11'),
(369, 'vehiculos_like', 'me interesa', 'me interesa', '2019-04-11 14:44:53', '2019-04-11 14:44:53'),
(370, 'vehiculos_offer', 'oferta', 'offer', '2019-04-11 15:28:01', '2019-04-11 15:28:01'),
(371, 'vehiculos_from', 'desde', 'from', '2019-04-11 15:37:34', '2019-04-11 15:37:34'),
(372, 'vehiculos_month', 'mes', 'month', '2019-04-11 15:37:44', '2019-04-11 15:37:44'),
(373, 'vehiculo_accept_politica', 'Aceptar <a target=\"_blank\" href=\"http://127.0.0.1/heygo_cars/es/heygocars/politica-de-privacidad\">política de privacidad</a>', 'Aceptar <a target=\"_blank\" href=\"http://127.0.0.1/heygo_cars/en/heygocars/privacy-policy\">privacy policy</a>', '2019-04-12 11:00:17', '2019-05-09 14:47:41'),
(374, 'vehiculo_accept_newsletter', '<a href=\"\">Suscribirme a newsletter HeyGo CARS</a>', '<a href=\"\">Suscribirme a newsletter HeyGo CARS</a>', '2019-04-12 11:00:45', '2019-04-12 11:00:45'),
(375, 'vehiculo_contactar', 'contactar', 'contact', '2019-04-12 11:01:47', '2019-04-12 11:01:47'),
(376, 'vehiculo_llamar', 'te llamamos', 'te llamamos', '2019-04-12 11:01:58', '2019-04-12 11:01:58'),
(377, 'vehiculo_bajar_precio', 'avísame si baja de precio', 'avísame si baja de precio', '2019-04-12 11:02:11', '2019-04-12 11:02:11'),
(378, 'vehiculo_cname', 'Nombre / Apellidos', 'Name / Surname', '2019-04-12 11:13:39', '2019-04-12 11:13:55'),
(379, 'vehiculo_email', 'Email', 'Email', '2019-04-12 11:15:01', '2019-04-12 11:15:01'),
(380, 'vehiculo_phone', 'Teléfono', 'Phone', '2019-04-12 11:15:13', '2019-04-12 11:15:13'),
(381, 'vehiculo_cp', 'Código postal', 'Código postal', '2019-04-12 11:15:24', '2019-04-12 11:15:24'),
(382, 'vehiculo_otros', 'Otros vehículos', 'Otros vehículos', '2019-04-16 15:27:25', '2019-04-16 15:27:25'),
(383, 'vehiculo_otros_usuarios', 'Otros usuarios que consultaron este mismo vehículo también miraron estas otras opciones', 'Otros usuarios que consultaron este mismo vehículo también miraron estas otras opciones', '2019-04-16 15:28:07', '2019-04-16 15:28:07'),
(384, 'vehiculo_share', 'compartir', 'share', '2019-04-16 15:28:45', '2019-04-16 15:28:45'),
(385, 'vehiculo_avisame', 'avísame si baja de precio', 'avísame si baja de precio', '2019-04-16 15:29:44', '2019-04-16 15:29:44'),
(386, 'vehiculo_oferta', 'oferta', 'offer', '2019-04-16 15:31:18', '2019-04-16 15:31:18'),
(387, 'vehiculo_year', 'Año', 'Year', '2019-04-17 11:58:11', '2019-04-17 11:58:11'),
(388, 'vehiculo_kms', 'Kilómetros', 'Kilómetros', '2019-04-17 11:58:20', '2019-04-17 11:58:20'),
(389, 'vehiculo_capacity', 'Capacidad', 'Capacity', '2019-04-17 11:58:40', '2019-04-17 11:58:40'),
(390, 'vehiculo_fuel', 'Combustible', 'Fuel', '2019-04-17 11:59:01', '2019-04-17 11:59:01'),
(391, 'vehiculo_cambio', 'Cambio', 'Cambio', '2019-04-17 11:59:43', '2019-04-17 11:59:43'),
(392, 'vehiculo_cilindrada', 'Cilindrada', 'Cilindrada', '2019-04-17 12:00:03', '2019-04-17 12:00:03'),
(393, 'vehiculo_npuertas', 'Nº Puertas', 'Nº Puertas', '2019-04-17 12:00:14', '2019-04-17 12:00:14'),
(394, 'vehiculo_garantia', 'Garantía', 'Garantía', '2019-04-17 12:00:27', '2019-04-17 12:00:27'),
(395, 'vehiculo_nmeses', 'meses', 'months', '2019-04-17 12:01:28', '2019-04-17 12:01:28'),
(396, 'vehiculo_puertas', 'puertas', 'puertas', '2019-04-17 12:01:37', '2019-04-17 12:01:37'),
(397, 'vehiculo_personas', 'personas', 'people', '2019-04-17 12:02:05', '2019-04-17 12:02:05'),
(398, 'vehiculo_offer_heygo', 'Oferta HeyGo Cars', 'Oferta HeyGo Cars', '2019-04-17 12:03:18', '2019-04-17 12:03:18'),
(399, 'vehiculo_customfinance', 'Financiación a medida', 'Financiación a medida', '2019-04-17 12:03:29', '2019-04-17 12:03:29'),
(400, 'vehiculo_equipamiento_serie', 'Equipamiento de Serie', 'Equipamiento de Serie', '2019-04-17 12:04:50', '2019-04-17 12:04:50'),
(401, 'vehiculo_equipamiento_opcional', 'Equipamiento Opcional', 'Equipamiento Opcional', '2019-04-17 12:05:00', '2019-04-17 12:05:00'),
(402, 'vehiculo_coche_rev', 'coche revisado<br>y certificado', 'coche revisado<br>y certificado', '2019-04-17 12:05:44', '2019-04-17 12:05:44'),
(403, 'vehiculo_coche_garantia100', 'garantía 100%', 'garantía 100%', '2019-04-17 12:06:06', '2019-04-17 12:06:06'),
(404, 'vehiculo_available', 'Vehículo disponible en', 'Vehículo disponible en', '2019-04-17 12:44:44', '2019-04-17 12:44:44'),
(405, 'vehiculo_offer_heygo_desc', 'Descuento de 1.000€ ya aplicado al PVP. Gastos de matriculación + constitución incluidos en PVP. Cambio de nombre no incluido.', 'Descuento de 1.000€ ya aplicado al PVP. Gastos de matriculación + constitución incluidos en PVP. Cambio de nombre no incluido.', '2019-04-17 13:01:15', '2019-04-17 13:01:15'),
(406, 'vehiculo_customfinance_desc', 'Cuota desde 270€/mes sujeta a contratación y aprobación del crédito por las entidades financieras HeyGoCARS. Ejemplo de cuota con el 20% de entrada. Plazo 72 meses. Gastos de plan de pago protegido incluidos en cuota. TIN 9,99%. TAE 11,6%.', 'Cuota desde 270€/mes sujeta a contratación y aprobación del crédito por las entidades financieras HeyGoCARS. Ejemplo de cuota con el 20% de entrada. Plazo 72 meses. Gastos de plan de pago protegido incluidos en cuota. TIN 9,99%. TAE 11,6%.', '2019-04-17 13:01:27', '2019-04-17 13:01:27'),
(407, 'vehiculo_like', 'me interesa', 'me interesa', '2019-04-17 13:04:38', '2019-04-17 13:04:38'),
(408, 'empresa_title', 'Quiénes somos', 'Quiénes somos', '2019-04-17 16:04:51', '2019-04-17 16:04:51'),
(409, 'empresa_subtitle', 'HeyGo Cars es tu centro multi marca de coches casi nuevos. Proponemos una nueva forma de entender la compra-venta de vehículos.', 'HeyGo Cars es tu centro multi marca de coches casi nuevos. Proponemos una nueva forma de entender la compra-venta de vehículos.', '2019-04-17 16:05:17', '2019-04-17 16:05:17'),
(410, 'empresa_header', 'Pasión por el automóvil', 'Pasión por el automóvil', '2019-04-17 16:05:34', '2019-04-17 16:05:34'),
(411, 'empresa_sectionprimary', 'Nos apasiona el mundo de la automoción. Nos obsesiona la transparencia. Son precisamente estos dos aspectos: nuestra pasión y nuestra obsesión, lo que nuestros clientes más valoran de nosotros. ', 'Nos apasiona el mundo de la automoción. Nos obsesiona la transparencia. Son precisamente estos dos aspectos: nuestra pasión y nuestra obsesión, lo que nuestros clientes más valoran de nosotros. ', '2019-04-17 16:06:37', '2019-04-17 16:06:37'),
(412, 'empresa_section_one', 'HeyGo Cars es tu centro multi marca de coches casi nuevos. Bienvenido a una nueva forma de entender la compra-venta de vehículos. Tanto online, como en persona, desde tu casa o en nuestros centros. Te acompañamos durante todo el proceso de compra o venta de tu vehículo, para que sea lo más rápido y sencillo posible, y consigas siempre tu objetivo. ', 'HeyGo Cars es tu centro multi marca de coches casi nuevos. Bienvenido a una nueva forma de entender la compra-venta de vehículos. Tanto online, como en persona, desde tu casa o en nuestros centros. Te acompañamos durante todo el proceso de compra o venta de tu vehículo, para que sea lo más rápido y sencillo posible, y consigas siempre tu objetivo. ', '2019-04-17 16:06:57', '2019-04-17 16:06:57'),
(413, 'empresa_section_two', 'En nuestros centros encontrarás siempre el mejor precio para vender tu vehículo usado o comprar tu coche casi nuevo o “kilómetro cero”, pudiendo elegir entre multitud de marcas diferentes. Actualmente disponemos de un stock de más de 500 vehículos. ', 'En nuestros centros encontrarás siempre el mejor precio para vender tu vehículo usado o comprar tu coche casi nuevo o “kilómetro cero”, pudiendo elegir entre multitud de marcas diferentes. Actualmente disponemos de un stock de más de 500 vehículos. ', '2019-04-17 16:07:15', '2019-04-17 16:07:15'),
(414, 'empresa_item1_head', 'Pasión', 'Pasión', '2019-04-17 16:08:19', '2019-04-17 16:08:19'),
(415, 'empresa_item2_head', 'Confianza', 'Confianza', '2019-04-17 16:08:34', '2019-04-17 16:08:34'),
(416, 'empresa_item3_head', 'Transparencia', 'Transparencia', '2019-04-17 16:08:52', '2019-04-17 16:08:52'),
(417, 'empresa_item4_head', 'Tecnología', 'Tecnología', '2019-04-17 16:09:05', '2019-04-17 16:09:05'),
(418, 'empresa_item5_head', 'Una empresa de Grupo Ugarte', 'Una empresa de Grupo Ugarte', '2019-04-17 16:09:29', '2019-04-17 16:09:29'),
(419, 'empresa_ourteam_head', 'Nuestro Equipo', 'Nuestro Equipo', '2019-04-17 16:10:02', '2019-04-17 16:10:02'),
(420, 'empresa_ourteam_desc', 'En HeyGo Cars somos un equipo de profesionales que trabajamos para brindarte el mejor servicio. Nos avalan nuestros años de experiencia y nuestro saber hacer.', 'En HeyGo Cars somos un equipo de profesionales que trabajamos para brindarte el mejor servicio. Nos avalan nuestros años de experiencia y nuestro saber hacer.', '2019-04-17 16:10:32', '2019-04-17 16:10:32'),
(421, 'empresa_item1_desc', 'La pasión por nuestro trabajo es nuestra verdadera razón de ser. Nos entusiasma el sector de la automoción, así como ayudar a nuestros clientes a encontrar lo que buscan.', 'La pasión por nuestro trabajo es nuestra verdadera razón de ser. Nos entusiasma el sector de la automoción, así como ayudar a nuestros clientes a encontrar lo que buscan.', '2019-04-17 16:11:36', '2019-04-17 16:11:36'),
(422, 'empresa_item2_desc', 'La confianza es la clave de nuestro éxito. Llevamos muchos años comprometidos con nuestros clientes, brindándoles un proceso de compra y venta fácil, seguro y veraz.', 'La confianza es la clave de nuestro éxito. Llevamos muchos años comprometidos con nuestros clientes, brindándoles un proceso de compra y venta fácil, seguro y veraz.', '2019-04-17 16:11:47', '2019-04-17 16:11:47'),
(423, 'empresa_item3_desc', 'Somos 100% transparentes en todas las fases del proceso de compra y venta: estado del vehículo, precios, tasaciones, garantía…', 'Somos 100% transparentes en todas las fases del proceso de compra y venta: estado del vehículo, precios, tasaciones, garantía…', '2019-04-17 16:11:59', '2019-04-17 16:11:59'),
(424, 'empresa_item4_desc', 'La innovación constante es nuestro mejor aliado. Empleamos siempre la última tecnología para certificar el estado de nuestros vehículos con la mayor precisión posible.', 'La innovación constante es nuestro mejor aliado. Empleamos siempre la última tecnología para certificar el estado de nuestros vehículos con la mayor precisión posible.', '2019-04-17 16:12:09', '2019-04-17 16:12:09'),
(425, 'empresa_item5_desc', 'HeyGo Cars pertenece a Grupo Ugarte, una compañía con amplia experiencia en el sector del automóvil.', 'HeyGo Cars pertenece a Grupo Ugarte, una compañía con amplia experiencia en el sector del automóvil.', '2019-04-17 16:12:24', '2019-04-17 16:12:24'),
(426, 'centros_subtitle', 'Visita cualquiera de nuestros centros de compra/venta de vehículos y déjate asesorar por profesionales con amplia experiencia en el sector. ¡Descubre tu oficina HeyGo Cars más cercana!', 'Visita cualquiera de nuestros centros de compra/venta de vehículos y déjate asesorar por profesionales con amplia experiencia en el sector. ¡Descubre tu oficina HeyGo Cars más cercana!', '2019-04-17 16:21:13', '2019-04-17 16:21:13'),
(427, 'centros_slidetitle', 'Grupo de automoción con una gran experiencia en la compra/venta de coches de ocasión. Miles de clientes satisfechos avalan nuestra trayectoria', 'Grupo de automoción con una gran experiencia en la compra/venta de coches de ocasión. Miles de clientes satisfechos avalan nuestra trayectoria', '2019-04-17 16:22:16', '2019-04-17 16:22:16'),
(428, 'centros_item1', '+7 años<br> experiencia', '+7 años<br> experiencia', '2019-04-17 16:23:03', '2019-04-17 16:23:03'),
(429, 'centros_item3', '+500 coches<br> en stock', '+500 coches<br> en stock', '2019-04-17 16:23:18', '2019-04-17 16:23:18'),
(430, 'centros_item2', '+3.500 clientes<br> satisfechos', '+3.500 clientes<br> satisfechos', '2019-04-17 16:23:24', '2019-04-17 16:23:24'),
(431, 'centros_why', '¿Porqué HeyGo Cars?', '¿Porqué HeyGo Cars?', '2019-04-17 16:24:01', '2019-04-17 16:24:01'),
(432, 'centros_answer1', 'Prueba tu coche<br>antes de comprarlo', 'Prueba tu coche<br>antes de comprarlo', '2019-04-17 16:24:49', '2019-04-17 16:24:49'),
(433, 'centros_answer2', 'Tu coche con hasta<br>2 años de garantía', 'Tu coche con hasta<br>2 años de garantía', '2019-04-17 16:25:14', '2019-04-17 16:25:14'),
(434, 'centros_answer3', 'Valoración y compra<br>de tu vehículo', 'Valoración y compra<br>de tu vehículo', '2019-04-17 16:25:21', '2019-04-17 16:25:21'),
(435, 'centros_answer4', 'Financiación flexible<br>100% a tu medida', 'Financiación flexible<br>100% a tu medida', '2019-04-17 16:25:32', '2019-04-17 16:25:32'),
(436, 'centros_answer4', 'Financiación flexible<br>100% a tu medida', 'Financiación flexible<br>100% a tu medida', '2019-04-17 16:25:32', '2019-04-17 16:25:32'),
(437, 'centros_ver_stock', 'ver stock vehículos', 'ver stock vehículos', '2019-04-17 16:26:15', '2019-04-17 16:26:15'),
(438, 'home_scroll', 'scroll', 'scroll', '2019-04-18 10:03:56', '2019-04-18 10:03:56'),
(439, 'home_down', 'down', 'down', '2019-04-18 10:04:15', '2019-04-18 10:04:15'),
(440, 'newsletter_title', 'HeyGo Cars', 'HeyGo Cars', '2019-04-18 12:14:17', '2019-04-18 12:14:17'),
(441, 'newsletter_subtitle', 'Suscríbete y sé el primero en recibir nuestras mejores ofertas y promociones', 'Suscríbete y sé el primero en recibir nuestras mejores ofertas y promociones', '2019-04-18 12:14:28', '2019-04-18 12:14:28'),
(442, 'newsletter_check', 'He leído y acepto los <a target=\"_blank\" href=\"http://127.0.0.1/heygo_cars/es/heygocars/aviso-legal\">términos de uso</a> y la <a href=\"http://127.0.0.1/heygo_cars/es/heygocars/politica-de-privacidad\">política de privacidad</a>', 'I have read and accept the <a target=\"_blank\" href=\"http://127.0.0.1/heygo_cars/en/heygocars/legal-warning\">legal warningo</a> and the <a href=\"http://127.0.0.1/heygo_cars/en/heygocars/privacy-policy\">privacy policy</a>', '2019-04-18 12:15:18', '2019-05-09 14:47:57'),
(443, 'newsletter_send', 'enviar', 'send', '2019-04-18 12:15:41', '2019-04-18 12:15:41'),
(444, 'contact_title', 'Contacto', 'Contact', '2019-04-18 12:40:15', '2019-04-18 12:40:15'),
(445, 'contact_subtitle', 'Si tienes cualquier duda, ponte en contacto con nosotros, estaremos encantados de ayudarte.', 'Si tienes cualquier duda, ponte en contacto con nosotros, estaremos encantados de ayudarte.', '2019-04-18 12:40:25', '2019-04-18 12:40:25'),
(446, 'contact_info_general', 'Información General', 'Información General', '2019-04-18 12:40:57', '2019-04-18 12:40:57'),
(447, 'contact_att_cliente', 'Atención al cliente', 'Atención al cliente', '2019-04-18 12:41:16', '2019-04-18 12:41:16'),
(448, 'contact_horario_title', 'Horario Comercial', 'Horario Comercial', '2019-04-18 12:42:17', '2019-04-18 12:42:17'),
(449, 'contact_exp_ventas', 'Exposición y Ventas', 'Exposición y Ventas', '2019-04-18 12:42:37', '2019-04-18 12:42:37'),
(450, 'contact_horario_desc', 'L a V: 09:00 a 14:00 h<br>\n16:00 a 20:00 h<br>\nSábados: 10:00 a 13.30 h\n', 'L a V: 09:00 a 14:00 h<br>\n16:00 a 20:00 h<br>\nSábados: 10:00 a 13.30 h', '2019-04-18 12:43:26', '2019-06-03 09:39:28'),
(451, 'contact_form_title', 'Formulario <br>de contacto', 'Formulario <br>de contacto', '2019-04-18 12:44:12', '2019-04-18 12:44:12'),
(452, 'contact_form_subtitle', 'Si necesitas ayuda durante el proceso de alquiler, no dudes en contactar con nosotros. Puedes llamarnos por teléfono, escribirnos un email o contactar a través del formulario:', 'Si necesitas ayuda durante el proceso de alquiler, no dudes en contactar con nosotros. Puedes llamarnos por teléfono, escribirnos un email o contactar a través del formulario:', '2019-04-18 12:44:23', '2019-04-18 12:44:23'),
(453, 'contact_form_check', 'Aceptar <a target=\"_blank\" href=\"http://127.0.0.1/heygo_cars/es/heygocars/politica-de-privacidad\">política de privacidad</a>', 'Aceptar <a target=\"_blank\" href=\"http://127.0.0.1/heygo_cars/en/heygocars/privacy-policy\">privacy-policy</a>', '2019-04-18 12:46:00', '2019-05-09 14:47:48'),
(454, 'contact_form_newsletter', 'Aceptar <a href=\"\">suscripción a newsletter HeyGo Cars</a>', 'Aceptar <a href=\"\">suscripción a newsletter HeyGo Cars</a>', '2019-04-18 12:46:13', '2019-04-18 12:46:13'),
(455, 'contact_form_send', 'enviar', 'send', '2019-04-18 12:46:24', '2019-04-18 12:46:24'),
(456, 'contact_form_name', 'Nombre/Apellidos', 'Name/Surname', '2019-04-18 12:46:44', '2019-04-18 12:46:44'),
(457, 'contact_form_email', 'Email', 'Email', '2019-04-18 12:46:53', '2019-04-18 12:46:53'),
(458, 'contact_form_phone', 'Teléfono', 'Phone', '2019-04-18 12:47:05', '2019-04-18 12:47:05'),
(459, 'contact_form_msg', 'Mensaje', 'Message', '2019-04-18 12:47:17', '2019-04-18 12:47:17'),
(460, 'nuestros_vehiculos_titulo_comp', 'Ventajas HeyGo!', 'Ventajas HeyGo!', '2019-04-18 13:02:50', '2019-04-18 13:02:50'),
(461, 'nuestros_vehiculos_titulo_subcomp', 'Coches nuevos, de segunda mano o de kilómetro cero. Todas las gamas, marcas y modelos. Tenemos el vehículo que buscas.', 'Coches nuevos, de segunda mano o de kilómetro cero. Todas las gamas, marcas y modelos. Tenemos el vehículo que buscas.', '2019-04-18 13:03:24', '2019-04-18 13:03:24'),
(462, 'required_email', 'Email requerido', 'Required email', '2019-05-07 10:18:05', '2019-05-07 10:18:05'),
(463, 'valid_email', 'Email válido', 'Valid email', '2019-05-07 10:18:21', '2019-05-07 10:18:21'),
(464, 'newsletter_registered', 'Registrado correctamente a nuestra newsletter! :)', 'Registrado correctamente a nuestra newsletter! :)', '2019-05-08 15:37:30', '2019-05-08 15:37:58'),
(465, 'favoritos_title', 'Mis coches favoritos', 'My favorite cars', '2019-05-09 10:01:13', '2019-05-09 10:01:13'),
(466, 'favoritos_subtitle', 'Estos son tus coches favoritos', 'These are your favorite cars', '2019-05-09 10:01:39', '2019-05-09 10:01:39'),
(467, 'filtro_search_text', 'Busca por marca/modelo', 'Search by brand/model', '2019-05-10 08:56:21', '2019-05-10 08:56:21'),
(468, 'contact_sended', 'Formulario de contacto enviado satisfactoriamente!', 'Contact form sended successfully!', '2019-05-10 10:35:46', '2019-05-10 10:35:46'),
(469, 'vehiculo_sended', 'Formulario de contacto enviado satisfactoriamente!', 'Contact form sended successfully!', '2019-05-13 12:36:00', '2019-05-13 12:36:00'),
(470, 'required_name', 'Nombre requerido', 'Required name', '2019-05-15 11:19:37', '2019-05-15 11:19:37'),
(471, 'required_phone', 'Teléfono requerido', 'Required phone', '2019-05-15 11:19:53', '2019-05-15 11:19:53'),
(472, 'required_msg', 'Mensaje requerido', 'Required message ', '2019-05-15 11:20:14', '2019-05-15 11:20:14'),
(473, 'valid_name', 'Nombre válido', 'Valid name', '2019-05-15 11:20:31', '2019-05-15 11:20:31'),
(474, 'valid_phone', 'Teléfono válido', 'Valid phone', '2019-05-15 11:20:46', '2019-05-15 11:20:46'),
(475, 'valid_msg', 'Mensaje válido', 'Valid message', '2019-05-15 11:21:03', '2019-05-15 11:21:03'),
(476, 'required_cp', 'Código postal requerido', 'Required CP', '2019-05-15 11:34:41', '2019-05-15 11:34:41'),
(477, 'valid_cp', 'Código postal válido', 'Valid CP', '2019-05-15 11:34:56', '2019-05-15 11:34:56'),
(478, 'vehiculos_orderby_default', 'Selecciona opción', 'Select option', '2019-05-20 11:45:23', '2019-05-20 11:45:23'),
(479, 'no_items_found', 'No hay resultados', 'No results', '2019-05-20 12:00:19', '2019-05-20 12:00:19'),
(480, 'watch-header', 'Te avisamos cuando este<br>vehículo baje de precio', 'Te avisamos cuando este<br>vehículo baje de precio', '2019-06-03 11:13:02', '2019-06-03 11:13:02'),
(481, 'watch-body', 'Déjanos tu email y te avisaremos si cambian las condiciones de venta de este vehículo:', 'Déjanos tu email y te avisaremos si cambian las condiciones de venta de este vehículo:', '2019-06-03 11:13:35', '2019-06-03 11:13:35'),
(482, 'watch-placeholder', 'Escribe tu email', 'Escribe tu email', '2019-06-03 11:40:52', '2019-06-03 11:41:03'),
(483, 'vehiculos_to', 'Hasta', 'To', '2019-05-22 13:56:33', '2019-05-22 13:56:33'),
(484, 'vehiculo_asientos', 'Asientos', 'Seats', '2019-05-22 15:53:46', '2019-05-22 15:53:46'),
(485, 'search_otro_vehiculo', 'buscar otro vehículo', 'buscar otro vehículo', '2019-05-27 16:26:33', '2019-05-27 16:26:33');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `traducciones`
--
ALTER TABLE `traducciones`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `traducciones`
--
ALTER TABLE `traducciones`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=483;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
